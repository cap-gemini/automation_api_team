package com.psd2.wrapper;

import java.util.ResourceBundle;

import com.psd2.core.SeleniumUtility;
import com.psd2.core.TestBase;
import com.psd2.logger.TestLogger;
import com.psd2.logger.TestVerification;
import com.psd2.utils.PropertyUtils;

public class ConsentOperation{

	public SeleniumUtility driverOps = new SeleniumUtility();
	public TestVerification testVP = new TestVerification();
	ResourceBundle bundle = TestBase.loadResourceBundle("com.psd2.testdata.ObjectRepository");
	public static String accountName;

	@SuppressWarnings("static-access")
	public String authoriseAISPConsent(String url) throws Throwable{
		try {
			TestLogger.logStep("[Step 4-1] : Navigate to consent page and verify page title");	
			driverOps.navigateToURL(url);
			/*driverOps.explicitWaitWithClickableEle(bundle.getString("Obj_ClosePopup"),20);
			driverOps.click(bundle.getString("Obj_ClosePopup"));*/
			testVP.assertTrue(TestBase.driver.getTitle().equalsIgnoreCase(bundle.getString("homescreen_title").trim()),
					"Navigate to home screen successfully");
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 4-2] : Enter User and Password");
			if(url.contains("CMA2_AISP_V2")){
				driverOps.setText(bundle.getString("Obj_User"), PropertyUtils.getProperty("usr_name_other"));
				driverOps.setText(bundle.getString("Obj_Pass"), PropertyUtils.getProperty("pwd_other"));
			}
			else{
				driverOps.setText(bundle.getString("Obj_User"), PropertyUtils.getProperty("usr_name"));
				driverOps.setText(bundle.getString("Obj_Pass"), PropertyUtils.getProperty("pwd"));
			}
			driverOps.click(bundle.getString("Obj_Login"));
		
			TestLogger.logStep("[Step 4-3] : Select Accounts");	
			//driverOps.explicitWaitWithClickableEle(bundle.getString("Obj_SelectAccount"),30);
			driverOps.clickByJS(bundle.getString("Obj_SelectAccount"));
			Thread.sleep(1000);
			driverOps.pressKey("PAGE_DOWN");
			Thread.sleep(1000);
			driverOps.takeScreenShot();
	
			TestLogger.logStep("[Step 4-4] : Confirm and autorize Consent");
			driverOps.pressKey("PAGE_DOWN");
			driverOps.clickByJS(bundle.getString("Obj_Confirm_Payment_check"));
			driverOps.click(bundle.getString("Obj_ConfirmConsent"));
			driverOps.waitForAuthCode();
			String authCode = driverOps.getURL().substring(driverOps.getURL().indexOf("code=")+5, driverOps.getURL().indexOf("&id_token"));
			TestLogger.logVariable("Auth Code : " + authCode);
			TestLogger.logBlankLine();
			
			return authCode;
		}catch(Exception ex){
			driverOps.takeScreenShot();
			ex.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("static-access")
	public void rejectAISPConsent(String url) throws Throwable{
		try {
			TestLogger.logStep("[Step 4-1] : Navigate to consent page and verify page title");
			driverOps.navigateToURL(url);
			/*driverOps.explicitWaitWithClickableEle(bundle.getString("Obj_ClosePopup"),20);
			driverOps.click(bundle.getString("Obj_ClosePopup"));*/
			testVP.assertTrue(TestBase.driver.getTitle().equalsIgnoreCase(bundle.getString("homescreen_title").trim()),
					"Navigate to home screen successfully");
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 4-2] : Enter User and Password");
			driverOps.setText(bundle.getString("Obj_User"), PropertyUtils.getProperty("usr_name"));
			driverOps.setText(bundle.getString("Obj_Pass"), PropertyUtils.getProperty("pwd"));
			driverOps.click(bundle.getString("Obj_Login"));
		
			TestLogger.logStep("[Step 4-3] : Select Accounts");
			driverOps.clickByJS(bundle.getString("Obj_SelectAccount"));
			driverOps.takeScreenShot();
	
			TestLogger.logStep("[Step 4-4] : Cancel Consent");
			driverOps.click(bundle.getString("Obj_CancelConsent"));		
			driverOps.click(bundle.getString("Obj_ConfirmCancelConsent"));		
			driverOps.waitForPageLoad();
			
			TestLogger.logBlankLine();
		}catch(Exception ex){
			driverOps.takeScreenShot();
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("static-access")
	public String authorisePISPConsent(String url, boolean addDebtorAccountFlag) throws Throwable{
		try {
              String debtorAccountNumber = null;
              TestLogger.logStep("[Step 4-1] : Navigate to consent page and verify page title");  
              driverOps.navigateToURL(url.split("##")[0]);
              /*driverOps.explicitWaitWithClickableEle(bundle.getString("Obj_ClosePopup"),20);
              driverOps.click(bundle.getString("Obj_ClosePopup"));*/
              testVP.assertTrue(TestBase.driver.getTitle().equalsIgnoreCase(bundle.getString("homescreen_title").trim()),
                          "Navigate to home screen successfully");
              TestLogger.logBlankLine();
              
              TestLogger.logStep("[Step 4-2] : Enter User and Password");
              if(url.contains("CMA_PISP_V1")){
                    driverOps.setText(bundle.getString("Obj_User"), PropertyUtils.getProperty("usr_name_other"));
                    driverOps.setText(bundle.getString("Obj_Pass"), PropertyUtils.getProperty("pwd_other"));
              }else{
                    driverOps.setText(bundle.getString("Obj_User"), PropertyUtils.getProperty("usr_name"));
                    driverOps.setText(bundle.getString("Obj_Pass"), PropertyUtils.getProperty("pwd"));
              }
              if (addDebtorAccountFlag){
                    driverOps.click(bundle.getString("Obj_Login"));
                    TestLogger.logStep("[Step 4-3] : Select Accounts");
                    driverOps.waitForPageLoad();
                    driverOps.pressKey("PAGE_DOWN");
                    driverOps.selectValueFromDropdown(bundle.getString("Obj_SelectAccountToPay"), 1);
                    driverOps.takeScreenShot();
                    driverOps.waitForPageLoad();
                    TestLogger.logStep("[Step 4-4] : Confirm and autorize Consent");
                    driverOps.clickByJS(bundle.getString("Obj_Confirm_Payment_check"));
                    driverOps.click(bundle.getString("Obj_ConfirmPayment"));
              }else{
            	  //	if(PropertyUtils.getProperty("cma_ver").equalsIgnoreCase("v3.0")){
            	  		driverOps.click(bundle.getString("Obj_Login"));
	        	  		 TestLogger.logStep("[Step 4-3] : Select Accounts");
	                     driverOps.waitForPageLoad();
	                    /* driverOps.pressKey("PAGE_DOWN");
	                     driverOps.selectValueFromDropdown(bundle.getString("Obj_SelectAccountToPay"), 1);*/
	                     driverOps.takeScreenShot();
                        driverOps.clickByJS(bundle.getString("Obj_Confirm_Payment_check"));
                        driverOps.click(bundle.getString("Obj_ConfirmPayment"));
            	  	/*}else {
            	  		debtorAccountNumber = url.split("##")[1];
                        String consent_debtorAccountNumber=driverOps.getText(bundle.getString("Obj_getDebtorDetails"));
                        testVP.assertStringEquals(consent_debtorAccountNumber, "Debitor Account No:"+debtorAccountNumber,
                                    "Debtor Account No. is correct");
                        driverOps.click(bundle.getString("Obj_Login"));
            	  	}*/
              }
                    
              driverOps.waitForAuthCode();
              
              String authCode = driverOps.getURL().substring(driverOps.getURL().indexOf("code=")+5, driverOps.getURL().indexOf("&id_token"));
              TestLogger.logVariable("Auth Code : " + authCode);
              
              TestLogger.logBlankLine();
              
              return authCode;
        }catch(Exception ex){
              driverOps.takeScreenShot();
              ex.printStackTrace();
              return null;
        }
  }
	
	@SuppressWarnings("static-access")
	public void rejectPISPConsent(String url) throws Throwable{
		try {
			TestLogger.logStep("[Step 4-1] : Navigate to consent page and verify page title");	
			driverOps.navigateToURL(url);
			/*driverOps.explicitWaitWithClickableEle(bundle.getString("Obj_ClosePopup"),20);
			driverOps.click(bundle.getString("Obj_ClosePopup"));*/
			testVP.assertTrue(TestBase.driver.getTitle().equalsIgnoreCase(bundle.getString("homescreen_title").trim()),
					"Navigate to home screen successfully");
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 4-2] : Enter User and Password");
			driverOps.setText(bundle.getString("Obj_User"), PropertyUtils.getProperty("usr_name"));
			driverOps.setText(bundle.getString("Obj_Pass"), PropertyUtils.getProperty("pwd"));
			driverOps.click(bundle.getString("Obj_Login"));
			driverOps.waitForPageLoad();
			
			TestLogger.logStep("[Step 4-4] : Reject Consent");
			driverOps.pressKey("PAGE_DOWN");
			driverOps.click(bundle.getString("Obj_CancelPISPConsent"));		
			driverOps.click(bundle.getString("Obj_ConfirmPISPCancelConsent"));
			driverOps.waitForPageLoad();
					
			TestLogger.logBlankLine();
		}catch(Exception ex){
			driverOps.takeScreenShot();
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("static-access")
	public String authoriseCISPConsent(String url) throws Throwable{
		try {
			TestLogger.logStep("[Step 4-1] : Navigate to consent page and verify page title");	
			driverOps.navigateToURL(url);
			/*driverOps.explicitWaitWithClickableEle(bundle.getString("Obj_ClosePopup"),20);
			driverOps.click(bundle.getString("Obj_ClosePopup"));*/
			testVP.assertTrue(TestBase.driver.getTitle().equalsIgnoreCase(bundle.getString("homescreen_title").trim()),
					"Navigate to home screen successfully");
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 4-2] : Enter User and Password");
			driverOps.setText(bundle.getString("Obj_User"), PropertyUtils.getProperty("usr_name"));
			driverOps.setText(bundle.getString("Obj_Pass"), PropertyUtils.getProperty("pwd"));
			driverOps.click(bundle.getString("Obj_Login"));		
	
			TestLogger.logStep("[Step 4-3] : Confirm and autorize Consent");
			driverOps.clickByJS(bundle.getString("Obj_AcceptConsent"));
			driverOps.click(bundle.getString("Obj_ConfirmCISPConsent"));
			driverOps.takeScreenShot();	
			driverOps.waitForAuthCode();
			String authCode = driverOps.getURL().substring(driverOps.getURL().indexOf("code=")+5, driverOps.getURL().indexOf("&id_token"));
			TestLogger.logVariable("Auth Code : " + authCode);
			
			TestLogger.logBlankLine();
		
			return authCode;
		}catch(Exception ex){
			driverOps.takeScreenShot();
			ex.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("static-access")
	public void rejectCISPConsent(String url) throws Throwable{
		try {
			TestLogger.logStep("[Step 4-1] : Navigate to consent page and verify page title");	
			driverOps.navigateToURL(url);
			/*driverOps.explicitWaitWithClickableEle(bundle.getString("Obj_ClosePopup"),20);
			driverOps.click(bundle.getString("Obj_ClosePopup"));*/
			testVP.assertTrue(TestBase.driver.getTitle().equalsIgnoreCase(bundle.getString("homescreen_title").trim()),
					"Navigate to home screen successfully");
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 4-2] : Enter User and Password");
			driverOps.setText(bundle.getString("Obj_User"), PropertyUtils.getProperty("usr_name"));
			driverOps.setText(bundle.getString("Obj_Pass"), PropertyUtils.getProperty("pwd"));
			driverOps.click(bundle.getString("Obj_Login"));
			driverOps.waitForPageLoad();
		
			TestLogger.logStep("[Step 4-3] : Cancel Consent");
			driverOps.explicitWaitWithClickableEle(bundle.getString("Obj_CancelCISPConsent"),20);
			driverOps.clickByJS(bundle.getString("Obj_CancelCISPConsent"));	
			driverOps.explicitWaitWithClickableEle(bundle.getString("Obj_ConfirmCancelCISPConsent"),20);
			driverOps.click(bundle.getString("Obj_ConfirmCancelCISPConsent"));
			driverOps.takeScreenShot();
			driverOps.waitForPageLoad();
			
			TestLogger.logBlankLine();
		}catch(Exception ex){
			driverOps.takeScreenShot();
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("static-access")
	public String authoriseAISPConsentWithSpecificAccounts(String url, String accounts) throws Throwable{
		try {
			TestLogger.logStep("[Step 4-1] : Navigate to consent page and verify page title");	
			driverOps.navigateToURL(url);
			/*driverOps.explicitWaitWithClickableEle(bundle.getString("Obj_ClosePopup"),20);
			driverOps.click(bundle.getString("Obj_ClosePopup"));*/
			testVP.assertTrue(TestBase.driver.getTitle().equalsIgnoreCase(bundle.getString("homescreen_title").trim()),
					"Navigate to home screen successfully");
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 4-2] : Enter User and Password");
			driverOps.setText(bundle.getString("Obj_User"), PropertyUtils.getProperty("usr_name"));
			driverOps.setText(bundle.getString("Obj_Pass"), PropertyUtils.getProperty("pwd"));
			driverOps.click(bundle.getString("Obj_Login"));
			
			TestLogger.logStep("[Step 4-3] : Select Accounts");
			driverOps.waitForPageLoad();
			driverOps.clickByJS(bundle.getString(accounts));
			accountName=driverOps.getText(bundle.getString("Obj_AccountName"));
			
			driverOps.takeScreenShot();
			TestLogger.logStep("[Step 4-4] : Confirm and autorize Consent");
			driverOps.pressKey("PAGE_DOWN");
			driverOps.clickByJS(bundle.getString("Obj_Confirm_Payment_check"));
			driverOps.click(bundle.getString("Obj_ConfirmConsent"));
			driverOps.waitForAuthCode();
			
			String authCode = driverOps.getURL().substring(driverOps.getURL().indexOf("code=")+5, driverOps.getURL().indexOf("&id_token"));
			TestLogger.logVariable("Auth Code : " + authCode);
			
			TestLogger.logBlankLine();
			
			return authCode;
		}catch(Exception ex){
			driverOps.takeScreenShot();
			ex.printStackTrace();
			return null;
		}
	}
	
	@SuppressWarnings("static-access")
	public String renewAISPConsent(String url, boolean cancelConsent, boolean denyAuthorisation) throws Throwable{
		try {
			TestLogger.logStep("[Step 4-1] : Navigate to consent page and verify page title");	
			driverOps.navigateToURL(url);
			/*driverOps.explicitWaitWithClickableEle(bundle.getString("Obj_ClosePopup"),20);
			driverOps.click(bundle.getString("Obj_ClosePopup"));*/
			testVP.assertTrue(TestBase.driver.getTitle().equalsIgnoreCase(bundle.getString("homescreen_title").trim()),
					"Navigate to home screen successfully");
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 4-2] : Enter User and Password");
			driverOps.setText(bundle.getString("Obj_User"), PropertyUtils.getProperty("usr_name"));
			driverOps.setText(bundle.getString("Obj_Pass"), PropertyUtils.getProperty("pwd"));
			if(cancelConsent){
				driverOps.click(bundle.getString("Obj_BackToThirdParty"));
				Thread.sleep(2000);
				/*if(driverOps.isDisplayed(bundle.getString("Obj_UnauthorizeErrorMessage"))){
					testVP.assertEquals(driverOps.getText(bundle.getString("Obj_InvalidPayerAccount_ErrorMessage")),("  Error : You do not have the authority to access the requested page."),
							"Error message is correct when reject the authorised AISP consent");
				}*/
				return null;
			}else if(denyAuthorisation){
				driverOps.click(bundle.getString("Obj_Login"));	
				driverOps.waitForPageLoad();
				driverOps.click(bundle.getString("Obj_DenyAuthorisation"));
				driverOps.click(bundle.getString("Obj_ConfirmDenyAuthorisation"));
				driverOps.waitForPageLoad();
				return null;
			}
			else{
				//testVP.assertStringEquals(driverOps.getText(bundle.getString("Obj_RenewConsentAccountName")), accountName, "Account Detail is correct in Renewal flow");
				driverOps.click(bundle.getString("Obj_Login"));	
				driverOps.waitForPageLoad();
				driverOps.click(bundle.getString("Obj_RenewAuthorisation"));	
			}
			driverOps.waitForAuthCode();
			String authCode = driverOps.getURL().substring(driverOps.getURL().indexOf("code=")+5, driverOps.getURL().indexOf("&id_token"));
			TestLogger.logVariable("Auth Code : " + authCode);
			TestLogger.logBlankLine();
			
			return authCode;
		}catch(Exception ex){
			driverOps.takeScreenShot();
			ex.printStackTrace();
			return null;
		}	
	}
	
	@SuppressWarnings("static-access")
	public String invalidPayerAccont(String url) throws Throwable{
		try {
			TestLogger.logStep("[Step 4-1] : Navigate to consent page and verify page title");	
			driverOps.navigateToURL(url);
			/*driverOps.explicitWaitWithClickableEle(bundle.getString("Obj_ClosePopup"),20);
			driverOps.click(bundle.getString("Obj_ClosePopup"));*/
			testVP.assertTrue(TestBase.driver.getTitle().equalsIgnoreCase(bundle.getString("homescreen_title").trim()),
					"Navigate to home screen successfully");
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 4-2] : Enter User and Password");
			driverOps.setText(bundle.getString("Obj_User"), PropertyUtils.getProperty("usr_name"));
			driverOps.setText(bundle.getString("Obj_Pass"), PropertyUtils.getProperty("pwd"));
			driverOps.click(bundle.getString("Obj_Login"));
			driverOps.waitForPageLoad();
			return driverOps.getText(bundle.getString("Obj_InvalidPayerAccount_ErrorMessage"));
		}catch(Exception ex){
			driverOps.takeScreenShot();
			ex.printStackTrace();
			return null;
		}
	}
}