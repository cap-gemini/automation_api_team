package com.psd2.test.aisp.cma2api;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of CMA compliance for GET Account Request URI 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class GetAccountRequest extends TestBase{	
	
	@Test
	public void m_GetAccountRequest() throws Throwable{	
		
		TestLogger.logStep("[Step 1 : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.assertStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();		
		TestLogger.logVariable("AccessToken : " + access_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Account SetUp....");
		
		accountSetup.setBaseURL(apiConst.backcomp_as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.submit();
		
		testVP.assertStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct for Account SetUp");
		consentId = accountSetup.getAccountRequestId();
		TestLogger.logVariable("Account Request Id : " + consentId);	
		
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 3] : Verify the CMA compliance for GET Account Request URI ");
		restRequest.setURL(apiConst.backcomp_as_endpoint+"/"+consentId);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Content-Type", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(),"200", 
				"Response Code is correct for get account request "+restRequest.getResponseStatusCode());
		testVP.verifyStringEquals(restRequest.getURL(),apiConst.backcomp_as_endpoint+"/"+consentId, 
				"URI for GET account request is as per open banking standard");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();	
	}	
}

