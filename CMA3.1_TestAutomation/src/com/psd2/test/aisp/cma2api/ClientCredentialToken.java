package com.psd2.test.aisp.cma2api;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of Client-Credential Access Token URI 
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class ClientCredentialToken extends TestBase
{
	@Test
	public void m_ClientCredentialToken() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials uri");
		testVP.verifyStringEquals(createClientCred.getURL(),apiConst.cc_endpoint, 
				"URI for client credential token is as per open banking standard");
		TestLogger.logBlankLine();
	}
}
