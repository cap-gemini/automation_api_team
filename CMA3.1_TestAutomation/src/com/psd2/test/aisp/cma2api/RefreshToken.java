package com.psd2.test.aisp.cma2api;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of Refresh Token URI 
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class RefreshToken extends TestBase{	
	
	@Test
	public void m_RefreshToken() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.assertStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();		
		TestLogger.logVariable("AccessToken : " + access_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Account SetUp....");
		
		accountSetup.setBaseURL(apiConst.backcomp_as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.submit();
		
		testVP.assertStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct for Account SetUp");
		consentId = accountSetup.getAccountRequestId();
		TestLogger.logVariable("Account Request Id : " + consentId);	
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : JWT Token Creation....");
		
		reqObject.setValueField(consentId);
		outId = reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Go to URL and authenticate consent");	
		redirecturl = apiConst.aispconsentcma2_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		authCode = consentOps.authoriseAISPConsent(redirecturl);		
		closeDriverInstance();
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 5] : Get access and refresh token");	
		accesstoken.setBaseURL(apiConst.at_endpoint);
		accesstoken.setAuthCode(authCode);
		accesstoken.submit();
		
		testVP.assertStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
				"Response Code is correct for get access token request");	
		access_token = accesstoken.getAccessToken();
		refresh_token = accesstoken.getRefreshToken();
		TestLogger.logVariable("Access Token : " + access_token);
		TestLogger.logVariable("Refresh Token : " + refresh_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 6] : Verification of refresh token URI");

		createAccessToken.setBaseURL(apiConst.at_endpoint);
		createAccessToken.setRefreshToken(refresh_token);
		createAccessToken.submit();
		
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"200", 
				"Verified refresh token URI");	
		
		testVP.verifyStringEquals(createAccessToken.getURL(),apiConst.at_endpoint, 
				"URI for Access Token generation using refresh token is as per open banking standard");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 6] : Get Accound Id");	
		getAccount.setBaseURL(apiConst.backcomp_account_endpoint);
		getAccount.setHeadersString("Authorization:Bearer "+createAccessToken.getAccessToken());
		getAccount.submit();
		
		testVP.assertStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"200", 
				"Response Code is correct for get accounts request");	
	
		testVP.testResultFinalize();		
	}
}

