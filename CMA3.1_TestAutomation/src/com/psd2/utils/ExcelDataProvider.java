package com.psd2.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelDataProvider {
	
	private static XSSFSheet ExcelWSheet;	 
	private static XSSFWorkbook ExcelWBook;	 

	
	@SuppressWarnings("rawtypes")
	public static HashMap getTestNames(HashMap<String, String> mapping, String filePath, String sheetName) throws Exception {  
		 
		HashMap<String, String> newMap = new HashMap<String, String>();
		HashMap<String, String> testPackageMapping = mapping;
		
		   try {		 
			   FileInputStream ExcelFile = new FileInputStream(filePath);		 
			   
			   ExcelWBook = new XSSFWorkbook(ExcelFile);		
			   ExcelWSheet = ExcelWBook.getSheet(sheetName);		 
			 
			  	XSSFRow nextData;
				for(int i=1; i<ExcelWSheet.getLastRowNum()+1; i++){				
					nextData = ExcelWSheet.getRow(i);
					if(nextData.getCell(0).toString().equalsIgnoreCase("y")){
						newMap.put(nextData.getCell(1).toString(), testPackageMapping.get(nextData.getCell(1).toString()));
					}
				}			         
		   }
		   catch (FileNotFoundException e){		 
			   System.out.println("Could not read the Excel sheet");
			   e.printStackTrace();		 
		   }		 
		   catch (IOException e){		 
			   System.out.println("Could not read the Excel sheet");		 
			   e.printStackTrace();		 
		   }	
		return(newMap);		 
	}
	
	@SuppressWarnings("rawtypes")
	public static HashMap getClassDetails(String filePath, String sheetName, String className) throws Exception {  
		 
		HashMap<String, String> newMap = new HashMap<String, String>();
		
		   try {		 
			   FileInputStream ExcelFile = new FileInputStream(filePath);		 
			   
			   ExcelWBook = new XSSFWorkbook(ExcelFile);		
			   ExcelWSheet = ExcelWBook.getSheet(sheetName);
			 
			  	XSSFRow nextData;
				for(int i=1; i<ExcelWSheet.getLastRowNum()+1; i++){				
					nextData = ExcelWSheet.getRow(i);
					if(nextData.getCell(0).toString().trim().equalsIgnoreCase(className.trim())){
						newMap.put(nextData.getCell(0).toString(), nextData.getCell(1).toString()+"##"+nextData.getCell(2).toString());
						return(newMap);		 
					}
				}			         
		   }
		   catch (FileNotFoundException e){		 
			   System.out.println("Could not read the Excel sheet");
			   e.printStackTrace();		 
		   }		 
		   catch (NullPointerException e){		 
			   System.out.println("Could not read the Excel sheet");		 
			   e.printStackTrace();		 
		   }	
		return(null);		 
	}
	
	public static ArrayList<String> getExcelData(String groupName){
		String filePath=System.getProperty("user.dir") + "/RunConfiguration.xlsx";
		String sheetName="Config";
		ArrayList<String> excelData=new ArrayList<String>();
		try{
			FileInputStream ExcelFile = new FileInputStream(new File(filePath));		 
			ExcelWBook = new XSSFWorkbook(ExcelFile);		
			ExcelWSheet = ExcelWBook.getSheet(sheetName);
			for(int i=1;i<=ExcelWSheet.getLastRowNum();i++)
			{
				if(ExcelWSheet.getRow(i).getCell(2).getStringCellValue().equals(groupName)){
					excelData.add(ExcelWSheet.getRow(i).getCell(1).getStringCellValue());
				}
			}
			return excelData;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
	
	public static void setExcelData(ArrayList<String> data){
		String filePath=System.getProperty("user.dir") + "/RunConfiguration.xlsx";
		String sheetName="Config";
		try{
			
			FileInputStream ExcelFile = new FileInputStream(new File(filePath));		 
			ExcelWBook = new XSSFWorkbook(ExcelFile);		
			ExcelWSheet = ExcelWBook.getSheet(sheetName);
			FileOutputStream fos=new FileOutputStream(new File(filePath));
			for(int i=1;i<=ExcelWSheet.getLastRowNum();i++)
			{
				boolean flag = false;
				for(int j=0;j<data.size();j++){
					if(ExcelWSheet.getRow(i).getCell(1).getStringCellValue().equals(data.get(j))){
						ExcelWSheet.getRow(i).getCell(0).setCellValue("Y");
						flag = true;
						break;
					}
				}
					if(!flag){
						ExcelWSheet.getRow(i).getCell(0).setCellValue("N");
				}
			}
			ExcelWBook.write(fos);
			fos.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public static void setConfData(ArrayList<String> data){
		String filePath=System.getProperty("user.dir") + "/RunConfiguration.xlsx";
		String sheetName="ConfData";
		System.out.println(data.size());
		try{
			
			FileInputStream ExcelFile = new FileInputStream(new File(filePath));		 
			ExcelWBook = new XSSFWorkbook(ExcelFile);		
			ExcelWBook.createSheet(sheetName);
			ExcelWSheet = ExcelWBook.getSheet(sheetName);
			XSSFRow row=ExcelWSheet.createRow(0);
			FileOutputStream fos=new FileOutputStream(new File(filePath));
			for(int i=0;i<data.size();i++)
			{
				row.createCell(i).setCellValue(data.get(i));
			}
			ExcelWBook.write(fos);
			fos.close();
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public static ArrayList<String> getConfData(){
		String filePath=System.getProperty("user.dir") + "/RunConfiguration.xlsx";
		String sheetName="ConfData";
		ArrayList<String> excelData=new ArrayList<String>();
		try{
			FileInputStream ExcelFile = new FileInputStream(new File(filePath));		 
			ExcelWBook = new XSSFWorkbook(ExcelFile);		
			ExcelWSheet = ExcelWBook.getSheet(sheetName);
			if(ExcelWSheet!=null){
				for(int i=0;i<ExcelWSheet.getRow(0).getLastCellNum();i++)
				{
					excelData.add(ExcelWSheet.getRow(0).getCell(i).getStringCellValue());
				}
				int index=ExcelWBook.getSheetIndex(sheetName);
				ExcelWBook.removeSheetAt(index);
				FileOutputStream fos=new FileOutputStream(new File(filePath));
				ExcelWBook.write(fos);
				fos.close();
			}
			return excelData;
		}catch(Exception ex){
			ex.printStackTrace();
			return null;
		}
	}
}