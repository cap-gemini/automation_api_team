
package com.psd2.signature;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.Map;

import javax.json.JsonObject;

import com.nimbusds.jose.Header;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jwt.SignedJWT;

public abstract class AbstractJWSVerifier {

	public boolean verifyJWS(String jwks, SignedJWT signedJWT)
			throws InvalidKeySpecException, NoSuchAlgorithmException, JOSEException, ParseException {
		JsonObject jsonObj = JWKSUtility.getJWKSJsonObject(jwks);
		Map<String, Key> keyMap = JWKSUtility.buildKeyMap(jsonObj);
		String kid = signedJWT.getHeader().getKeyID();
		Key key=keyMap.get(kid);
		JWSVerifier jwsVerifier= createJWSVerifier(key,signedJWT.getHeader());
		return signedJWT.verify(jwsVerifier);
	}
	
	/**
	 * Method gives JWSverifier like RSASSAVerifier, ECDSAVerifier etc as per implementation.
	 * @param key
	 * @return RSASSAVerifier, ECDSAVerifier etc
	 */
	public abstract JWSVerifier createJWSVerifier(Key key,Header header);
	
	public boolean verifyJWS(Key publicKey,SignedJWT signedJWT) throws JOSEException {
		JWSVerifier jwsVerifier= createJWSVerifier(publicKey,signedJWT.getHeader());
		return signedJWT.verify(jwsVerifier);
	}

}
