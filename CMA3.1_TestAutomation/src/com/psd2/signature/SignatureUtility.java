package com.psd2.signature;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.NumericDate;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.bc.BouncyCastleProviderSingleton;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jwt.SignedJWT;
import com.psd2.utils.API_Constant;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

public class SignatureUtility {
	
	public static String signature="";
	static KeyStore keystore = null;
	public static String default_password = "password";
	static InputStream in = null;
	static{
		Security.addProvider(new BouncyCastleProvider());
	}
	public static String generateSignature(String requestBody) throws Exception {
		try{
			in = new FileInputStream(new File(API_Constant.signKeystore));
			keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			keystore.load(in, default_password.toCharArray());
			if(!keystore.isKeyEntry(PropertyUtils.getProperty("signin_alias"))){
				importCertificateToKeystore();
			}
			Set<String> critHeaders=new HashSet<>();
			critHeaders.add("b64");
			critHeaders.add("http://openbanking.org.uk/iat");
			critHeaders.add("http://openbanking.org.uk/iss");
			//critHeaders.add("http://openbanking.org.uk/tan");
			NumericDate date=NumericDate.now();
			date.addSeconds(30);
			JWSHeader header=new JWSHeader.Builder(JWSAlgorithm.PS256)
										.criticalParams(critHeaders)
										.customParam("b64", false)
										
										.customParam("http://openbanking.org.uk/iss", getSubject())
										.customParam("http://openbanking.org.uk/iat", date.getValue())
										//.customParam("http://openbanking.org.uk/tan", "openbanking.org.uk")
										.type(JOSEObjectType.JOSE)
										.keyID(PropertyUtils.getProperty("sign_key"))
										.contentType("application/json")
										.build();
			Base64URL base64url=new Base64URL(requestBody);
			Payload payload=new Payload(base64url);
			JWSObject jwsObj=new JWSObject(header, payload);
			
			PrivateKey key=(PrivateKey)keystore.getKey(PropertyUtils.getProperty("signin_alias"), default_password.toCharArray());
			JWSSigner signer=new RSASSASigner(key);
			signer.getJCAContext().setProvider(BouncyCastleProviderSingleton.getInstance());
			jwsObj.sign(signer);
			String tok=jwsObj.serialize();
			signature=tok.split("\\.")[0]+".."+jwsObj.getSignature();
		}catch(KeyStoreException | UnrecoverableKeyException | NoSuchAlgorithmException | JOSEException ex) {ex.printStackTrace();}
		return signature;
	}
	
	public static String generateFileSignature(String filePath) throws Exception{
		try {
		Set<String> critHeaders=new HashSet<>();
		critHeaders.add("b64");
		critHeaders.add("http://openbanking.org.uk/iat");
		critHeaders.add("http://openbanking.org.uk/iss");
		//critHeaders.add("http://openbanking.org.uk/tan");
		NumericDate date=NumericDate.now();
		date.addSeconds(30);
		JWSHeader header=new JWSHeader.Builder(JWSAlgorithm.PS256)
									.criticalParams(critHeaders)
									.customParam("b64", false)
									.customParam("http://openbanking.org.uk/iss", getSubject())
									.customParam("http://openbanking.org.uk/iat", date.getValue())
									//.customParam("http://openbanking.org.uk/tan", "openbanking.org.uk")
									.type(JOSEObjectType.JOSE)
									.keyID(PropertyUtils.getProperty("sign_key"))
									.contentType("application/json")
									.build();
		
		File file=new File(filePath);
		byte[] bytesArray = new byte[(int) file.length()]; 
		FileInputStream fis = new FileInputStream(file);
		fis.read(bytesArray);
		
		Base64URL base=new Base64URL(Arrays.toString(bytesArray));
		JWSObject jwsObj=new JWSObject(header, new Payload(base));
		
		in = new FileInputStream(new File(API_Constant.signKeystore));
		keystore = KeyStore.getInstance(KeyStore.getDefaultType());
		keystore.load(in, default_password.toCharArray());
		if(!keystore.isKeyEntry(PropertyUtils.getProperty("signin_alias"))){
			importCertificateToKeystore();
		}
		PrivateKey key=(PrivateKey)keystore.getKey(PropertyUtils.getProperty("signin_alias"), default_password.toCharArray());
		JWSSigner signer=new RSASSASigner(key);
		signer.getJCAContext().setProvider(BouncyCastleProviderSingleton.getInstance());
		jwsObj.sign(signer);
		String tok=jwsObj.serialize();
		signature=tok.split("\\.")[0]+".."+jwsObj.getSignature();
		fis.close();
		}catch(IOException | CertificateException | KeyStoreException | UnrecoverableKeyException | NoSuchAlgorithmException | JOSEException ex) {ex.printStackTrace();}
		return signature;
	}
	
	public static boolean verifySignature(String responseBody, String responseSignature)
	{
		boolean verifiedSignature=false;
		try{
			KeyStore keystore = null;
			InputStream in = new FileInputStream(new File(API_Constant.netKeystore));
			keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			keystore.load(in, default_password.toCharArray());
			String[] headerParts = responseSignature.split("\\.");
	
			SignedJWT signedJwt = populateSignedJWT(headerParts, responseBody);
	
			X509Certificate certificate = (X509Certificate) keystore.getCertificate(PropertyUtils.getProperty("network_alias"));
	
			AbstractJWSVerifier verifier = new RSAVerifierGenerator();
			PublicKey publicKey = certificate.getPublicKey();
			verifiedSignature = verifier.verifyJWS(publicKey, signedJwt);
		}catch(JOSEException | NoSuchAlgorithmException | IOException | CertificateException | KeyStoreException ex){ex.printStackTrace();}
		return verifiedSignature;
	}
		
	private static SignedJWT populateSignedJWT(String[] headerParts, String body) {

		Base64URL base64url1 = new Base64URL(headerParts[0]);
		Base64URL base64url2 = new Base64URL(body);
		Base64URL base64url3 = new Base64URL(headerParts[2]);
		try {
			return new SignedJWT(base64url1, base64url2, base64url3);
		} catch (java.text.ParseException e) {
			System.out.println("parse exception");
			return null;
		}
	}
	
	public static String getSubject() throws Exception {
		X509Certificate certificate=null;
		try{
			in = new FileInputStream(new File(API_Constant.signKeystore));
			keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			keystore.load(in, default_password.toCharArray());
			
			certificate = (X509Certificate) keystore.getCertificate(PropertyUtils.getProperty("signin_alias"));
		}catch(KeyStoreException | IOException ex)
		{ex.printStackTrace();}
		return "         "+certificate.getSubjectDN().toString().replaceAll(",", ",   ")+"        ";
	}
	
	@SuppressWarnings("unused")
	 public static void importCertificateToKeystore() throws Exception{
		
		String keyFileLoc = null;;
		String pemFileLoc = null;
		File file=null;
		
		String fileLocation = PropertyUtils.propertyMap.get("InFilesPath")+"/Signing/";
		String certificatePath = fileLocation+"Certificate.p12";
		keyFileLoc = Misc.getFileUsingExt(fileLocation, ".key");
		pemFileLoc = Misc.getFileUsingExt(fileLocation, ".pem");
		file = new File(certificatePath);
		
		if(keyFileLoc != null && pemFileLoc != null){				
			String command="openssl pkcs12 -export -out "+certificatePath+" -inkey "+keyFileLoc+" -in "+pemFileLoc+" -password pass:"+default_password+ " -name \""+PropertyUtils.getProperty("signin_alias")+"\"";
			Runtime r=Runtime.getRuntime();
			Process p=r.exec(command);
			System.out.println("Signing certificate has been created successfully");
			Thread.sleep(2000);
			String importcommand = " -importkeystore "
					+ "-deststorepass "+default_password
					+ " -destkeystore "+API_Constant.signKeystore
					+ " -srckeystore "+certificatePath
					+ " -srcstoretype PKCS12 "
					+ " -keypass "+default_password
					+ " -srcstorepass "+default_password;
			
			execute(importcommand);
			Thread.sleep(2000);
			Files.deleteIfExists(Paths.get(certificatePath));
		}else{
			System.out.println("Either key or pem file are not correct or not found");
		}
	}
	
	// Execute the commands
    @SuppressWarnings("restriction")
	public static void execute(String command){
        try{
        	String[] options = command.trim().split("\\s+");
            sun.security.tools.keytool.Main.main(options);
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
    public static String generateJWT(String requestBody){
    	String jwt="";
    	try{
    	in = new FileInputStream(new File(API_Constant.signKeystore));
		keystore = KeyStore.getInstance(KeyStore.getDefaultType());
		keystore.load(in, "password".toCharArray());
		if(!keystore.isKeyEntry(PropertyUtils.getProperty("signin_alias"))){
			importCertificateToKeystore();
		}

		JsonWebSignature signer =new JsonWebSignature();
		signer.setKeyIdHeaderValue(PropertyUtils.getProperty("sign_key"));
		if(PropertyUtils.getProperty("jwt_algorithm").equalsIgnoreCase("PS256")){
			signer.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_PSS_USING_SHA256);
		}else if(PropertyUtils.getProperty("jwt_algorithm").equalsIgnoreCase("RS256")){
			signer.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
		}
		signer.setHeader("typ", "JWT");
		signer.setPayload(requestBody);
		PrivateKey key=(PrivateKey)keystore.getKey(PropertyUtils.getProperty("signin_alias"), "password".toCharArray());
		signer.setKey(key);
		signer.sign();
		jwt=signer.getCompactSerialization();
    	}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
    	return jwt;
    }
}