package com.psd2.tests.pisp.GET_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation block
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GFPC_036 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFPC_036() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Data/Initiation block");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET File Payment Consent URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.FileType")).isEmpty(), 
				"Mandatory field FileType is present under Data/Initiation block");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.FileHash")).isEmpty(), 
				"Mandatory field FileHash is present under Data/Initiation block");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.FileReference")).isEmpty(), 
				"Optional field FileReference is present under Data/Initiation block and is not empty");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.NumberOfTransactions")).isEmpty(), 
				"Optional field NumberOfTransactions is present under Data/Initiation block and is not empty");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument")).isEmpty(), 
				"Optional field LocalInstrument is present under Data/Initiation block");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.ControlSum")).isEmpty(), 
				"Optional field ControlSum is present under Data/Initiation block");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.RequestedExecutionDateTime")).isEmpty(), 
				"Optional field RequestedExecutionDateTime is present under Data/Initiation block");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Data.Initiation.DebtorAccount")!=null, 
				"Optional field DebtorAccount is present under Data/Initiation block and is not empty");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Data.Initiation.RemittanceInformation")!=null, 
				"Optional field RemittanceInformation is present under Data/Initiation block and is not empty");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
