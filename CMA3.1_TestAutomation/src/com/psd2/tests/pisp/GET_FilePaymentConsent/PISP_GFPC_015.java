package com.psd2.tests.pisp.GET_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of x-fapi-interaction-id value when NOT sent in the request with the response header that created successfully with HTTP Code 200 OK
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GFPC_015 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFPC_015() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
        
	    TestLogger.logStep("[Step 2] : Verification of x-fapi-interaction-id value when NOT sent in the request");
	    restRequest.setURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
	    restRequest.setHeadersString("x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+",Authorization:Bearer "+consentDetails.get("cc_access_token")+", Accept:application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
	    testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
	    		"Response Code is correct for GET File Payment Consents");
	    testVP.verifyTrue(!(restRequest.getResponseHeader("x-fapi-interaction-id")).isEmpty(), 
	    		"x-fapi-interaction-id is present in the response");
	    
	    TestLogger.logBlankLine();
	    
	    TestLogger.logStep("[Step 3] : Verification of the request with Invalid value of x-fapi-interaction-id header");
		
	    filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		filePayment.setHeadersString("x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+",Authorization:Bearer "+consentDetails.get("cc_access_token"));
		filePayment.addHeaderEntry("x-fapi-interaction-id", "1234");
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"400", 
				"Response Code is correct for GET File Payment Consents URI when invalid value is used for x-fapi-interaction-id header");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Errors[0].Message")).isEmpty(), 
				"Message for error code is '"+filePayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of the request with null value of x-fapi-interaction-id header");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		filePayment.setHeadersString("x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+",Authorization:Bearer "+consentDetails.get("cc_access_token"));
		filePayment.addHeaderEntry("x-fapi-interaction-id", "");
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET File Payment Consents URI when null value is used for x-fapi-interaction-id header");
		
		TestLogger.logBlankLine();
        
		testVP.testResultFinalize();		
	}
}
