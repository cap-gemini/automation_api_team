package com.psd2.tests.pisp.GET_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY Amount block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GFPC_033 extends TestBase {	
	
	@Test
	public void m_PISP_GFPC_033() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST File Payment Consent");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.setControlSum("222.00");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for POST File Payment Consent URI");
		
		consentId=filePayment.getConsentId();
		TestLogger.logVariable("Consent Id : "+consentId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the values into MANDATORY Amount block where Request has sent successfully and returned a HTTP Code 201 Created");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentId);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET File Payment Consent URI");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Data.Charges[0].Amount")!=null, 
				"Amount block under Data/Charges block is present and is not nulll");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Charges[0].Amount.Amount")).isEmpty(), 
				"Amount field is present under Data/Charges/Amount block");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Charges[0].Amount.Currency")).isEmpty(), 
				"Currency field is present under Data/Charges/Amount block");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}