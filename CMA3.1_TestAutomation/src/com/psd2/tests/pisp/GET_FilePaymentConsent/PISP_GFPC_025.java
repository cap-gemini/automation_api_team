package com.psd2.tests.pisp.GET_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY ConsentId field where Request has sent successfully and returned a HTTP Code 200 OK
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GFPC_025 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFPC_025() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY ConsentId field where multiple request has sent successfully within 24 hr with same TPP and Idempotency key");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET File Payment Consent URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.ConsentId")).isEmpty(), 
				"ConsentId field is present under Data block");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Data.ConsentId").length()<=128, 
				"ConsentId length is correct i.e. less than or equal to 128 chracters");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
