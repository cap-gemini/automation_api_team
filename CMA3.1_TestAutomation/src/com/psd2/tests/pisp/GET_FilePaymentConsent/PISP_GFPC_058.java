package com.psd2.tests.pisp.GET_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of GET response for the consent API when the ControlSum starts with "700" in the POST resquest payload
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GFPC_058 extends TestBase {	
	
	@Test
	public void m_PISP_GFPC_058() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Post File Payment Consent");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.setControlSum("700.00");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for File Payment Consent URI");
		
		consentId=filePayment.getConsentId();
		TestLogger.logVariable("Consent Id : "+consentId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of GET response for the consent API when the ControlSum starts with \"700\" in the POST resquest payload");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentId);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"500", 
				"Response Code is correct for GET File Payment Consent URI");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
