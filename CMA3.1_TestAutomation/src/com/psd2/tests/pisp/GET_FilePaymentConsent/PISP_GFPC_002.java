package com.psd2.tests.pisp.GET_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of GET File Payment Consents URL that is NOT as per Open Banking standards as specified format
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GFPC_002 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFPC_002() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
        
	    TestLogger.logStep("[Step 1-2] : Verification of GET File Payment Consents URL that is NOT as per Open Banking standards as specified format");
	    filePayment.setBaseURL(apiConst.invalid_paymentconsent_endpoint+"/"+consentDetails.get("consentId"));
	    filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
	    filePayment.setMethod("GET");
	    filePayment.submit();
	    testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"404", 
	    		"Response Code is correct for GET File Payment Consents when URL is NOT as per Open Banking standards");
	    TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

