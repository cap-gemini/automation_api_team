package com.psd2.tests.pisp.GET_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Authorisation block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GFPC_052 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFPC_052() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Data/Authorisation block where Request has sent successfully and returned a HTTP Code 201 Created");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET  File Payment Consent URI");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Data.Authorisation")!=null,
				"Optional field Authorisation is present and is not null");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Authorisation.AuthorisationType")).isEmpty(),
				"Mandatory field AuthorisationType is present under Authorisation");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Authorisation.CompletionDateTime")).isEmpty(),
				"Optional field CompletionDateTime is present under Authorisation");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
