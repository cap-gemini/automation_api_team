package com.psd2.tests.pisp.GET_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with expired token value into Authorization header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GFPC_011 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFPC_011() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request without token value OR key-value pair into Authorization header");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		filePayment.setHeadersString("Authorization:Bearer "+ apiConst.expiredPispCCAccessToken);
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"401", 
				"Response Code is correct for File Payment Consents URI with expired token value in the Authorization (Access Token) header");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

