package com.psd2.tests.pisp.GET_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of NON file-payment-consents URL that is as per Open Banking standards
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GFPC_003 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFPC_003() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
        
	    TestLogger.logStep("[Step 1-2] : Verification of NON file-payment-consents URL that is as per Open Banking standards");
	    filePayment.setBaseURL(apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"));
	    filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
	    filePayment.setMethod("GET");
	    filePayment.submit();
	    testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"400", 
	    		"Response Code is correct for GET File Payment Consents when NON file-payment-consents URL sent in request");
	    
	    testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Resource.NotFound", 
				"Error code for the response is correct i.e. '"+filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Errors[0].Message").equals("payment setup id is not found in platform"), 
				"Message for error code is '"+filePayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();

		
		testVP.testResultFinalize();		
	}
}

