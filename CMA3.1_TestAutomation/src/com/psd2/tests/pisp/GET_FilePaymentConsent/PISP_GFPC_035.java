package com.psd2.tests.pisp.GET_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY Currency field where Request has sent successfully 
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GFPC_035 extends TestBase {	
	
	@Test
	public void m_PISP_GFPC_035() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST File Payment Consent");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.setControlSum("222.00");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for File Payment Consent URI");
		
		consentId=filePayment.getConsentId();
		TestLogger.logVariable("Consent Id : "+consentId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the values into MANDATORY Currency field where Request has sent successfully");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentId);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET File Payment Consent URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Charges[0].Amount.Currency")).isEmpty(), 
				"Currency field is present under Data/Charges/Amount block");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Data.Charges[0].Amount.Currency").length()==3, 
				"Currency field length is correct");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}