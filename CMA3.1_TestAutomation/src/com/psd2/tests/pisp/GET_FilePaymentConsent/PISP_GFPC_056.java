package com.psd2.tests.pisp.GET_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY Links/Self where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GFPC_056 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFPC_056() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Self field under Links block where Request has sent successfully and returned a HTTP Code 201 Created");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET  File Payment Consent URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Links.Self")).isEmpty(), 
				"Self field is present under Links");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Links.Self"), apiConst.fPaymentConsent_endpoint+"/"+filePayment.getConsentId(), 
				"Self link is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}