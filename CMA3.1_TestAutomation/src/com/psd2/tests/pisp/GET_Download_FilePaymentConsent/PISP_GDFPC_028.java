package com.psd2.tests.pisp.GET_Download_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request with BLANK or Invalid value of OPTIONAL x-fapi-customer-last-logged-time header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDFPC_028 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDFPC_028() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Upload file");	
		uploadFile.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		uploadFile.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		uploadFile.setFileType(filePayment.getFileType());
		uploadFile.setIdempotencyKey(consentDetails.get("idempotency_key"));
		uploadFile.submit();
		
		testVP.verifyStringEquals(String.valueOf(uploadFile.getResponseStatusCode()),"200", 
				"Response Code is correct for POST Upload File URI");	

        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the request with Invalid value of OPTIONAL x-fapi-customer-last-logged-time header");
		
		restRequest.setURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("x-fapi-customer-last-logged-time", "1234");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for GET Download File Payment Consents URI when invalid value is used for x-fapi-customer-last-logged-time header");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Errors[0].Message")).isEmpty(), 
				"Message for error code is '"+restRequest.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of the request with BLANK value of OPTIONAL x-fapi-customer-last-logged-time header");
		
		restRequest.setURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("x-fapi-customer-last-logged-time", "");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for GET Download File Payment Consents URI when invalid value is used for x-fapi-customer-last-logged-time header");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Errors[0].Message")).isEmpty(), 
				"Message for error code is '"+restRequest.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}