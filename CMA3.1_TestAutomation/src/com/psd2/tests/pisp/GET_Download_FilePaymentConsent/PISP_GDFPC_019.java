package com.psd2.tests.pisp.GET_Download_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without token value OR key-value pair into Authorization header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDFPC_019 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDFPC_019() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Upload file");	
		uploadFile.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		uploadFile.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		uploadFile.setFileType(filePayment.getFileType());
		uploadFile.setIdempotencyKey(consentDetails.get("idempotency_key"));
		uploadFile.submit();
		
		testVP.verifyStringEquals(String.valueOf(uploadFile.getResponseStatusCode()),"200", 
				"Response Code is correct for POST Upload File URI");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the request with null value of Authorization header");
		
		restRequest.setURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		restRequest.setHeadersString("Authorization:Bearer "+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", 
				"Response Code is correct for GET Download File Payment Consents URI with no token value in the Authorization (Access Token) header");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of the request without Authorization header");
		
		restRequest.setURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		restRequest.setHeadersString("x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", 
				"Response Code is correct for GET Download File Payment Consents URI without Authorization (Access Token) header");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}