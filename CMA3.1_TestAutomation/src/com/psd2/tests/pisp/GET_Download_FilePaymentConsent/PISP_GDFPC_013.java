package com.psd2.tests.pisp.GET_Download_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of GET file-payment-consents/{consentId}/file URL sending with consentId having status Consumed
 * @author : Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDFPC_013 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDFPC_013() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, true);
		TestLogger.logBlankLine();	

        TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Verification of GET file-payment-consents/{consentId}/file URL sending with consentId having status Verification of GET file-payment-consents/{consentId}/file URL sending with consentId having status Consumed");	
		restRequest.setURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for GET Download File Payment Consent URI when Status is Consumed");	
        TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
}
