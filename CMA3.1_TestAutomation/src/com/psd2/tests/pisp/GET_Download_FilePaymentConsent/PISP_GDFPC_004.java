package com.psd2.tests.pisp.GET_Download_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request through other than GET method
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDFPC_004 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDFPC_004() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Upload file");	
		uploadFile.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		uploadFile.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		uploadFile.setFileType(filePayment.getFileType());
		uploadFile.setIdempotencyKey(consentDetails.get("idempotency_key"));
		uploadFile.submit();
		
		testVP.verifyStringEquals(String.valueOf(uploadFile.getResponseStatusCode()),"200", 
				"Response Code is correct for POST Upload File URI");
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 3] : Verification of request through other than GET method");	
        restRequest.setURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
        restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
        restRequest.setMethod("PUT");
        restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"405", 
				"Response Code is correct for GET Download File Payment Consent URI");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
