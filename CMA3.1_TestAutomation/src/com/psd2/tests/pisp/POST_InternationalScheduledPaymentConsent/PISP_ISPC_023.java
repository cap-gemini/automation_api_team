package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Comparison of x-fapi-interaction-id value sent in the request with the response header that created successfully with HTTP Code 201 Created 
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_023 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_023() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Comparison of x-fapi-interaction-id value sent in the request with the response header that created successfully with HTTP Code 201 Created");
			
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.addHeaderEntry("x-fapi-interaction-id", PropertyUtils.getProperty("inter_id"));
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International Schedule Payment Consent SetUp URI");
			testVP.verifyStringEquals(iScheduledPayment.getResponseHeader("x-fapi-interaction-id"),PropertyUtils.getProperty("inter_id"), 
					"x-fapi-interaction-id value in the response header is correct and matching with one sent in the request.");
			
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();
			
			
	}
	}
