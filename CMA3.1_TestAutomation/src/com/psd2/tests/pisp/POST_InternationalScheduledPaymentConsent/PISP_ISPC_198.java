package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL RemittanceInformation/Reference field
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_198 extends TestBase {	
	
	@Test
	public void m_PISP_ISPC_198() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL RemittanceInformation/Reference field");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for Post International Scheduled Payment Consents URI");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.RemittanceInformation.Reference")).isEmpty(), 
				"Reference field under RemittanceInformation is present and is not null");
		
		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.RemittanceInformation.Reference").length()<=35, 
				"Reference field length under RemittanceInformation is less than 35 characters");	

		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();
		
			}
}
