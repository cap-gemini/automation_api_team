package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL ContractIdentification block where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_279 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_279() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment Consent URI");		
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ContractIdentification"))!=null,
					"ContractIdentification field under ExchangeRateInformation block is present in POST International Payment Consent response body"+(iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ContractIdentification")));
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ContractIdentification")), iScheduledPayment._contractIdentification, 
					"Mandatory field ContractIdentification is present in Initiation block and has same value sent in request");	
			testVP.verifyTrue(String.valueOf((iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ContractIdentification"))).length()>=1&&String.valueOf((iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ContractIdentification"))).length()<=256 
					,"ContractIdentification value is present and is of range 1 to 256");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
