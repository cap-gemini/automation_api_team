package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request with same payload with same x-idempotency-key after 24hrs
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Database"})
public class PISP_ISPC_014 extends TestBase {	
	
	@Test
	public void m_PISP_ISPC_014() throws Throwable{	
		String idemPotencyKey=PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3);
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request with x-idempotency-key value as "+idemPotencyKey+" in the header");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey);
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Consents when x-idempotency-key value in header is "+idemPotencyKey+"");
		
		consentId = iScheduledPayment.getConsentId();
		TestLogger.logVariable("ConsentId : " + consentId);
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Change the createdAt date to previous date from current date");
		
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_paymentSetupPlatform"));
		mongo.updateDocumentObject("paymentConsentId:"+consentId,"createdAt:"+Misc.previousDate().replace(":", "#"));
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of request with same x-idempotency-key i.e. "+idemPotencyKey+" and same payload after 24hrs");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey);
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Consents when x-idempotency-key value is same i.e. "+idemPotencyKey+" and payload is same after 24hrs");
		
		testVP.verifyTrue(!(consentId.equals(iScheduledPayment.getConsentId())), 
				"ConsentId created after 24hrs is different");
		
	    TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}