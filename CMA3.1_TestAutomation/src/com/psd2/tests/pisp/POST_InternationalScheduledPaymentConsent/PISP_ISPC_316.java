package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/RemittanceInformation block where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_316 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_316() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment Consent URI");		
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation"))!=null, 
					"Mandatory field Unstructured is present under RemittanceInformation block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation.Unstructured"))!=null, 
					"Mandatory field Unstructured is present under RemittanceInformation block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation.Reference"))!=null, 
					"Mandatory field Reference is present under RemittanceInformation block");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
