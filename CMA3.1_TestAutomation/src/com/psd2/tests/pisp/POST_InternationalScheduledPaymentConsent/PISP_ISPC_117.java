package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the value of MANDATORY CreditorAccount/Identification field
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_117 extends TestBase {	
	
	@Test
	public void m_PISP_ISPC_117() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY CreditorAccount/Identification field");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.submit();
		

		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Consent URI");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.Identification")).isEmpty(), 
				"Mandatory field Identification under DebtorAccount is present and is not null");
		
		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.Identification").length()<=256, 
				"Length of Identification is not more than 256 characters");
		
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 2] : Verification of MANDATORY CreditorAccount/Identification haven't sent");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		String requestBody=iScheduledPayment.genRequestBody().replace("\"Identification\": \"JO94CBJO0010000000000131000302\",","");
		iScheduledPayment.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Scheduled Payment Consent URI when CreditorAccount Identification is not sent");
		
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Missing", 
				"Error code for the response is correct i.e. '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Missing required field [Identification]"), 
				"Message for error code is '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		testVP.testResultFinalize();		
	}
}

