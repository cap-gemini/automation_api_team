package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY Initiation block where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_264 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_264() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			String requestBody=iScheduledPayment.genRequestBody().replace("\"Purpose\": \"Test\",","\"Purpose\":\"Test\", \"ChargeBearer\":\"Shared\",");
			iScheduledPayment.submit(requestBody);
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment Consent URI");		
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructionIdentification"))!=null, 
					"Mandatory field InstructionIdentification is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructionIdentification"));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.EndToEndIdentification"))!=null, 
					"Mandatory field EndToEndIdentification is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.EndToEndIdentification"));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.LocalInstrument"))!=null,
					"Optional field LocalInstrument is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.LocalInstrument"));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructionPriority"))!=null,
					"Optional field InstructionPriority is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructionPriority"));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Purpose"))!=null,
					"Optional field Purpose is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.Purpose"));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer"))!=null,
					"Optional field ChargeBearer is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer"));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.RequestedExecutionDateTime"))!=null,
					"Optional field RequestedExecutionDateTime is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.RequestedExecutionDateTime"));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructedAmount"))!=null,
					"Mandatory Block InstructedAmount is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructedAmount"));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount"))!=null,
					"Mandatory Block CreditorAccount is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount"));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CurrencyOfTransfer"))!=null,
					"Mandatory field CurrencyOfTransfer is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CurrencyOfTransfer"));
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
