package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of  International-Schedule-Payment-Consents URL that is NOT as per Open Banking
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_002 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_002() throws Throwable{	
		
		    TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : International Scheduled Payment Consent URL is not as per Open Banking Standards");
			
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"12222");
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"404", "Response Code is correct for NON International-Scheduled-Payment-Consents API URL");
			
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();
}
}
