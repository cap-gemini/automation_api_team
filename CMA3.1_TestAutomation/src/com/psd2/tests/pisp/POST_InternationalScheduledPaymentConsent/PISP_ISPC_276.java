package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY UnitCurrency field where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_276 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_276() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment Consent URI");		
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency"))!=null,
					"UnitCurrency field under ExchangeRateInformation block is present in POST International Payment Consent response body "
					+(iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency")));		
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency")), iScheduledPayment._unitCurrency, 
					"Mandatory field UnitCurrency is present in Initiation block and has same value sent in request");	
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency")).length()==3  
					,"UnitCurrency value is present and is of 3 characters");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
