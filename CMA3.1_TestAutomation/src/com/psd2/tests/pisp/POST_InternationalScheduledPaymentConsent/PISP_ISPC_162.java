package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Scheme Name and Identification or Name and Postal Address   
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_162 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_162() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 3] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			String requestBody = iScheduledPayment.genRequestBody().replace("\"SchemeName\": \"BICFI\",","");
			requestBody = requestBody.replace("\"Identification\": \"BIC\",","");
			iScheduledPayment.submit(requestBody);
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for Post International Scheduled Payment Consents URI");	
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.Name")), iScheduledPayment._crAgentName, 
					"SchemeName field value is correct");		
			
			testVP.verifyTrue(!(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress"))).isEmpty(), 
					"PostalAddress block is not empty");	
			
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.SchemeName")==null, 
					"Mandatory field SchemeName is not present under Data/Initiation/CreditorAgent/SchemeName field is not present");
			
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.Identification")==null, 
					"Mandatory field Identification is not present under Data/Initiation/CreditorAgent/Identification and is not present");
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
			iScheduledPayment.setCrAgentSchemeName("UK.OBIE.BICFI");
			iScheduledPayment.setCrAgentIdentification("BIC");
			iScheduledPayment.removeCreditorAgentPostalAddress();
			requestBody=iScheduledPayment.genRequestBody().replace(",\"Name\": \"CreditAgentTest\"","");
			iScheduledPayment.submit(requestBody);
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for Post International Scheduled Payment Consents URI");	
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.SchemeName")), iScheduledPayment._crAgentSchemeName, 
					"SchemeName field value is correct");		
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.Identification")), iScheduledPayment._crAgentIdentification, 
					"Identification field value is correct");	
			
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.Name")==null, 
					"Mandatory field Name is not present under Data/Initiation/CreditorAgent/Name field is not present");
			
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress")==null, 
					"Mandatory field PostalAddress is not present under Data/Initiation/CreditorAgent/PostalAddress block and is not present");
			
			TestLogger.logBlankLine();
			
			
			testVP.testResultFinalize();
	}
}
