package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request with BLANK, NULL or Invalid value of MANDATORY x-jws-signature header
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_025 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_025() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with Invalid value of x-jws-signature header");
		restRequest.setURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		String requestBody=iScheduledPayment.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+cc_token+", Content-Type:application/json,  x-fapi-interaction-id:"+PropertyUtils.getProperty("inter_id")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json, x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		restRequest.addHeaderEntry("x-jws-signature", "1234");
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Consent URI when invalid value is used for x-jws-signature header");
		
		TestLogger.logStep("[Step 3] : Verification of the request with null value of x-jws-signature header");
		
		restRequest.setURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		String requestBody1=iScheduledPayment.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+cc_token+", Content-Type:application/json,  x-fapi-interaction-id:"+PropertyUtils.getProperty("inter_id")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json, x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		restRequest.addHeaderEntry("x-jws-signature", "");
		restRequest.setRequestBody(requestBody1);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Consent URI when invalid value is used for x-jws-signature header");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
			
	}
}
