package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the request with Null or Invalid value of x-fapi-financial-id header. 
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_021 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_021() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("accounts");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] :Verification of the request with Null value of x-fapi-financial-id header.");
			
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.addHeaderEntry("x-fapi-financial-id", "");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"403", "Response Code is correct for Null or Invalid value of x-fapi-financial-id header for International Schedule Payment Consent SetUp");
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-3] :Verification of the request with Invalid value of x-fapi-financial-id header.");
			
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.addHeaderEntry("x-fapi-financial-id", "1234");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"403", "Response Code is correct for Null or Invalid value of x-fapi-financial-id header for International Schedule Payment Consent SetUp");
			
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();
}
}
