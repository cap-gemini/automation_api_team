package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Risk/DeliveryAddress block haven't sent
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_220 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_220() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			String requestBody=iScheduledPayment.genRequestBody().replace(",\"DeliveryAddress\": {"
					+ "\"AddressLine\":[\"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ\",\"12345678\"],"					
					+ "\"StreetName\": \"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ12345678\","
					+ "\"BuildingNumber\": \"ABCDEF1234567890\","
					+ "\"PostCode\": \"ABCDEF 123456789\","
					+ "\"TownName\": \"ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789\","
					+ "\"CountrySubDivision\": \"test\","
					+ "\"Country\": \"GB\"}", "");
			iScheduledPayment.submit(requestBody);

			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment Consent URI when OPTIONAL Risk/DeliveryAddress block is not sent");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
