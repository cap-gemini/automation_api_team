package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL ExchangeRate field where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_260 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_260() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment Consent URI");	
			testVP.verifyEquals(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExchangeRate")), iScheduledPayment._exchangeRate, 
					"ExchangeRate in response has same value as sent in request");
			testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExchangeRate").split("\\.")[0].length()<=13
					&& iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExchangeRate").split("\\.")[1].length()<=5, 
					"ExchangeRate field is present under Data in response body and is of decimal format");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
