package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values of MANDATORY Initiation/EndToEndIdentification field having length variation
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_040 extends TestBase {	
	
	@Test
	public void m_PISP_ISPC_040() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 2] : Verification of the values of MANDATORY Initiation/EndToEndIdentification field");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Consent URI when EndToEndIdentification is sent with characters between 1-35 characters");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.EndToEndIdentification")).isEmpty(), 
				"Mandatory field EndToEndIdentification is present and is not null");
		
		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.EndToEndIdentification").length()<=35, 
				"EndToEndIdentification field length is less than or equal to 35 characters");
		
        TestLogger.logStep("[Step 2] : Verification of the values of MANDATORY Initiation/EndToEndIdentification field having length variation");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.setEndToEndIdentification("FRESCO.21302.GFX.20FRESCO.21302.GFRFGTY");
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Scheduled Payment Consent URI when EndToEndIdentification is passed with more than 35 characters");

		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 35 for field [EndToEndIdentification], but got [FRESCO.21302.GFX.20FRESCO.21302.GFRFGTY]"), 
				"Message for error code is '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
