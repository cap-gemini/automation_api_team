package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of response for the consent API when the RateType is provided as "Agreed" & then it must not respond with the ExpirationDateTime in the ExchangeRateInformation Array
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_351 extends TestBase {	
	
	@Test
	public void m_PISP_ISPC_351() throws Throwable{	
		String idemPotencyKey=PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3);
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of response for the consent API when the RateType is provided as \"Agreed\" in the resquest payload");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint	);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Consent URI");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.ExchangeRateInformation")).isEmpty(), 
				"ExchangeRateInformation field under Initiation array is present and is not null");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency")).isEmpty(), 
				"UnitCurrency field under ExchangeRateInformation array is present and is not null");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.ExchangeRateInformation.RateType")).isEmpty(), 
				"RateType field under ExchangeRateInformation array is present and is not null");
		
		testVP.verifyTrue((iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.ExchangeRateInformation.RateType")).equals("Agreed"), 
				"RateType field under ExchangeRateInformation array is as Actual");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation")).isEmpty(), 
				"ExchangeRateInformation block under Data array is present and is not null");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.UnitCurrency")).isEmpty(), 
				"UnitCurrency field under ExchangeRateInformation array is present and is not null");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExchangeRate")).isEmpty(), 
				"ExchangeRate field under ExchangeRateInformation array is present and is not null");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.RateType")).isEmpty(), 
				"RateType field under ExchangeRateInformation array is present and is not null");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ContractIdentification")).isEmpty(), 
				"ContractIdentification field under ExchangeRateInformation array is present and is not null");
		
		testVP.verifyTrue((iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExpirationDateTime"))==null, 
				"ExpirationDateTime field under ExchangeRateInformation array is null");
		
        TestLogger.logStep("[Step 3] : Verification of response for the consent API when the RateType is provided as \"Agreed\" without giving ExchangeRate");
		
		//iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint	);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey+"112");
		iScheduledPayment.setRateType("Agreed");
		String requestBody=iScheduledPayment.genRequestBody().replace("\"ExchangeRate\": "+iScheduledPayment._exchangeRate+",","");
		iScheduledPayment.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Scheduled Payment Consent URI");
		
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Expected", 
				"Error code for the response is correct i.e. '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Exchange Rate and Contract Identification details are expected"), 
				"Message for error code is '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 4] : Verification of response for the consent API when the RateType is provided as \"Agreed\" without giving ContractIdentification");
		
		//iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint	);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey+"112444");
		iScheduledPayment.setRateType("Agreed");
		String requestBody1 = iScheduledPayment.genRequestBody().replace(",\"ContractIdentification\": \""+iScheduledPayment._contractIdentification+"\"","");
		iScheduledPayment.submit(requestBody1);
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Scheduled Payment Consent URI");
		
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Expected", 
				"Error code for the response is correct i.e. '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Exchange Rate and Contract Identification details are expected"), 
				"Message for error code is '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 5] : Verification of response for the consent API when the RateType is provided as \"Agreed\" without giving ContractIdentification");
		
		//iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey+"1124567");
		iScheduledPayment.setRateType("Agreed");
		String requestBody2=iScheduledPayment.genRequestBody().replace("\"ExchangeRate\": "+iScheduledPayment._exchangeRate+",","");
		requestBody2 = requestBody.replace(",\"ContractIdentification\": \""+iScheduledPayment._contractIdentification+"\"","");
		iScheduledPayment.submit(requestBody2);
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Scheduled Payment Consent URI");
		
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Expected", 
				"Error code for the response is correct i.e. '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Exchange Rate and Contract Identification details are expected"), 
				"Message for error code is '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
