package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Risk/DeliveryAddress block. 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_219 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_219() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment Consent URI");			
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.TownName")).isEmpty(), 
					"Mandatory field TownName is present under DeliveryAddress block");		
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.Country")).isEmpty(), 
					"Mandatory field Country is present under DeliveryAddress block");		
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.AddressLine")).isEmpty(), 
					"Optional field AddressLine is present under DeliveryAddress block");		
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.BuildingNumber")).isEmpty(), 
					"Optional field BuildingNumber is present under DeliveryAddress block");		
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.StreetName")).isEmpty(), 
					"Optional field StreetName is present under DeliveryAddress block");			
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.PostCode")).isEmpty(), 
					"Optional field PostCode is present under DeliveryAddress block");		
			/*testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.CountrySubDivision")).isEmpty(), 
					"Optional field CountrySubDivision is present under DeliveryAddress block");*/
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
