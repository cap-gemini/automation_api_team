package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into OPTIONAL ExpirationDateTime block where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_263 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_263() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.setRateType("Actual");
			String requestBody=iScheduledPayment.genRequestBody().replace("\"ExchangeRate\": "+iScheduledPayment._exchangeRate+",","");
			requestBody = requestBody.replace(",\"ContractIdentification\": \"123identification\"","");
			iScheduledPayment.submit(requestBody);
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International Scheduled Payment Consent URI");
				
		    testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExpirationDateTime")!=null, 
						"ExpirationDateTime field is present under Data in response body");		
		    
		    testVP.verifyTrue(Misc.verifyDateTimeFormat(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExpirationDateTime").split("T")[0], "yyyy-MM-dd") && 
						(Misc.verifyDateTimeFormat(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExpirationDateTime").split("T")[1], "HH:mm:ss+00:00")), 
						"ExpirationDateTime is as per expected format");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
