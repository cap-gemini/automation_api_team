package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of response for the consent API when Currency value is provided as "INR"
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_362 extends TestBase {	
	
	@Test
	public void m_PISP_ISPC_362() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of response for the consent API when Currency value is provided as \"INR\"");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.setCurrency("INR");
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for Post International Scheduled Payment Consents URI");
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Status")), "AwaitingAuthorisation", 
				"Status field value is rejected when currency is sent as USD in request");
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Charges[0].ChargeBearer")), "BorneByDebtor", 
				"ChargeBearer field value is BorneByCreditor when currency is sent as INR in request");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
