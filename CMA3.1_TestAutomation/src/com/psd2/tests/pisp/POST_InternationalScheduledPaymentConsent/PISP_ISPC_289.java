package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the value of OPTIONAL CreditorAccount/SecondaryIdentification where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_289 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_289() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment Consent URI");		
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount"))!=null,
					"MANDATORY CreditorAccount block present in POST International Payment Consent response body ");
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SecondaryIdentification"))!=null,
					"OPTIONAL SecondaryIdentification field present in POST International Payment Consent response body ");
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SecondaryIdentification")), iScheduledPayment._crAccountSrIdentification, "SecondaryIdentification value is same as sent in request");		
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
