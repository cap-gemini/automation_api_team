package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the value of OPTIONAL CreditorAgent/PostalAddress/AddressType field 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_175 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_175() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
		    		"Response Code is correct for Post International Scheduled Payment Consents");
			
			testVP.verifyTrue(!(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType")).isEmpty()), 
					"AddressType is present in response body and is not null");
			testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("Business")
					|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("Correspondence")
					|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("DeliveryTo")
					|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("MailTo")
					|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("POBox")
					|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("Postal")
					|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("Residential")
					|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("Statement"), 
					"Optional field AddressType is present under CreditorAgent.PostalAddress and have correct value");
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
