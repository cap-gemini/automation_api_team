package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/ChargeBearer field
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_059 extends TestBase {	
	
	@Test
	public void m_PISP_ISPC_059() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values of OPTIONAL Initiation/ChargeBearer field");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		String requestBody=iScheduledPayment.genRequestBody().replace("\"Purpose\": \"Test\",","\"Purpose\":\"Test\", \"ChargeBearer\":\"Shared\",");
		iScheduledPayment.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Consent URI");
		
		testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer").equals("BorneByCreditor")
				|| iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer").equals("BorneByDebtor")
				|| iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer").equals("FollowingServiceLevel")
				|| iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer").equals("Shared"),
				"ChargeBearer field value is as per OB defined values");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
