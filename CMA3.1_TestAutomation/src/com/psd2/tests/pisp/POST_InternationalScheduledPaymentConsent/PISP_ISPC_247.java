package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the values into MANDATORY Status field when consent has been cancelled from consent page
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_247 extends TestBase {	
	
	API_E2E_Utility apiUtility=new  API_E2E_Utility();
	
	@Test
	public void m_PISP_ISPC_247() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create PISP consent with status rejected");
		consentDetails=apiUtility.generatePayments(true,"International-Scheduled-Payments",false,false);

		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Status field when consent has been cancelled from consent page");
		
		restRequest.setURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for Get Post International Scheduled Payment Consents URI");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Data.Status")), "Rejected",
				"Status of the consent field is correct i.e. Rejected");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
