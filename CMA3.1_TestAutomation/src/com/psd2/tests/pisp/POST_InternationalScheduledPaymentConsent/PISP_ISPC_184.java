package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the value of OPTIONAL CreditorAgent/PostalAddress/BuildingNumber field having length variation 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_184 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_184() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.setCrAgentBuildingNumber("ABCDEFGHIJ1234565231735");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
					"Response Code is correct for International Scheduled Payment Consent URI when CreditorPostalAddress/BuildingNumber is passed with more than 16 characters");		
			testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
					"Error code for the response is correct i.e. '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");		
			testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 16 for field [BuildingNumber], but got [ABCDEFGHIJ1234565231735]"), 
					"Message for error code is '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
