package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of response for the consent API when the RateType is provided as "Actual" & then it must respond with the Actual ExchangeRate
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_339 extends TestBase {	
	
	@Test
	public void m_PISP_ISPC_339() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of response for the consent API when the RateType is provided as \"Actual\" in the resquest payload");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint	);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.setRateType("Actual");
		String requestBody=iScheduledPayment.genRequestBody().replace("\"ExchangeRate\": "+iScheduledPayment._exchangeRate+",","");
		requestBody = requestBody.replace(",\"ContractIdentification\": \""+iScheduledPayment._contractIdentification+"\"","");
		iScheduledPayment.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Consent URI");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.ExchangeRateInformation")).isEmpty(), 
				"ExchangeRateInformation field under Initiation array is present and is not null");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency")).isEmpty(), 
				"UnitCurrency field under ExchangeRateInformation array is present and is not null");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.ExchangeRateInformation.RateType")).isEmpty(), 
				"RateType field under ExchangeRateInformation array is present and is not null");
		
		testVP.verifyTrue((iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.ExchangeRateInformation.RateType")).equals("Actual"), 
				"RateType field under ExchangeRateInformation array is as Actual");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation")).isEmpty(), 
				"ExchangeRateInformation block under Data array is present and is not null");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.UnitCurrency")).isEmpty(), 
				"UnitCurrency field under ExchangeRateInformation array is present and is not null");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExchangeRate")).isEmpty(), 
				"ExchangeRate field under ExchangeRateInformation array is present and is not null");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.RateType")).isEmpty(), 
				"RateType field under ExchangeRateInformation array is present and is not null");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ContractIdentification")).isEmpty(), 
				"ContractIdentification field under ExchangeRateInformation array is present and is not null");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
