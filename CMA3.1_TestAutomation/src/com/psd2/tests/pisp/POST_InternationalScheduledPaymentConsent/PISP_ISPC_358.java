package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values of InstructedAmount array and CurrencyOfTransfer in the response of consent API 
 * Amount : 100 GBP
 * Unit Currency : USD
 * CurrencyOfTransfer : INR
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_358 extends TestBase {	
	
	@Test
	public void m_PISP_ISPC_358() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values of InstructedAmount array and CurrencyOfTransfer in the response of consent API ");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.setCurrencyOfTransfer("INR");
		iScheduledPayment.setCurrency("GBP");
		iScheduledPayment.setUnitCurrency("USD");
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for Post International Scheduled Payment Consents");
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.CurrencyOfTransfer")), iScheduledPayment._currencyOfTransfer, 
				"CurrencyOfTransfer field has the same value as sent in request");
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructedAmount.Currency")), iScheduledPayment._currency, 
				"InstructedAmount Currency field has the same value as sent in request");
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency")), iScheduledPayment._unitCurrency, 
				"UnitCurrency field has the same value as sent in request");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();
		
			}
}
