package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = UK.OBIE.BBAN
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_093 extends TestBase {	
	
	@Test
	public void m_PISP_ISPC_093() throws Throwable{	
		
        TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = UK.OBIE.BBAN");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.setDrAccountSchemeName("UK.OBIE.BBAN");
		iScheduledPayment.setDrAccountIdentification("A21243######");
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400",
				"Response Code is correct for International Scheduled Payment URI when DebtorAccount Identification value is not valid");

		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Unsupported.AccountIdentifier",
				"Error code for the response is correct i.e. '"+ iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+ "'");

		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message").equals("invalid account identifier provided corresponding to scheme"),"Message for error code is '"
						+ iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message")+ "'");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
