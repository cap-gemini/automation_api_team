package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of request through other than POST method 
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_004 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_004() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
				
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Verification of request through other than POST method for Domestic Scheduled Payment Consent SetUp");
			
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.setMethod("GET");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"405", "Response Code is correct for request through other than POST method for International Scheduled Payment Consent SetUp");
			
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
}
}
