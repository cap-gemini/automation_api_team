package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of x-fapi-interaction-id value when NOT sent in the request with the response header that created successfully with HTTP Code 201 Created 
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_024 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_024() throws Throwable{	
		
		    TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
				
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Verification of x-fapi-interaction-id value when NOT sent in the request with the response header that created successfully with HTTP Code 201 Created");
			
			restRequest.setURL(apiConst.iScheduledPaymentConsent_endpoint);
		    iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		    String requestBody=iScheduledPayment.genRequestBody();
		    restRequest.setHeadersString("Authorization:Bearer "+cc_token+", Content-Type:application/json, x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json, x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
			restRequest.setRequestBody(requestBody);
			restRequest.setMethod("POST");
			restRequest.submit();
			
		    testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"201", 
		    		"Response Code is correct for Post International Scheduled Payment Consent");
		    testVP.verifyTrue(!(restRequest.getResponseHeader("x-fapi-interaction-id")).isEmpty(), 
		    		"x-fapi-interaction-id is present in the response");
		    
            TestLogger.logStep("[Step 1-3] :Verification of the request with Null value of x-fapi-interaction-id header.");
			
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.addHeaderEntry("x-fapi-interaction-id", "");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct when  x-fapi-interaction-id header is removed for International Schedule Payment Consent SetUp");
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-4] :Verification of the request with Invalid value of x-fapi-interaction-id header.");
			
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.addHeaderEntry("x-fapi-interaction-id", "1234");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", "Response Code is correct when  x-fapi-interaction-id header is removed for International Schedule Payment Consent SetUp");
			
			testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
					"Error code for the response is correct i.e. '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message"), "invalid headers found in request",
					"Message for error code is correct i.e.  '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
			
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();
			
			
	}
	}
