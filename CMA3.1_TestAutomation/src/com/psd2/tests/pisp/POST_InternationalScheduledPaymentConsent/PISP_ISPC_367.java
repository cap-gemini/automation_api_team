package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of response for the consent API when SecondaryIdentification value starts with "333"
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_367 extends TestBase {	
	
	@Test
	public void m_PISP_ISPC_367() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of response for the consent API when SecondaryIdentification value starts with \"333\"");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.setCrAccountSrIdentification("333");
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for Post International Scheduled Payment Consents URI");
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Errors[0].ErrorCode")), "UK.OBIE.Unsupported.AccountSecondaryIdentifier", 
				"Error code is correct when CreditorAccount/SecondaryIdentification field value is 333");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
