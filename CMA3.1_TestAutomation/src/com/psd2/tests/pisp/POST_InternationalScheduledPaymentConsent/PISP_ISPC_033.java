package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation block
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_033 extends TestBase {	
	
	@Test
	public void m_PISP_ISPC_033() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Data/Initiation block");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Consent URI");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.InstructionIdentification")).isEmpty(), 
				"Mandatory field InstructionIdentification is present under Data/Initiation block");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.EndToEndIdentification")).isEmpty(), 
				"Mandatory field EndToEndIdentification is present under Data/Initiation block");
		
		testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructedAmount")!=null, 
				"Mandatory field InstructedAmount is present under Data/Initiation block and is not empty");
		
		testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount")!=null, 
				"Mandatory field CreditorAccount is present under Data/Initiation block and is not empty");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument")).isEmpty(), 
				"Optional field LocalInstrument is present under Data/Initiation block");
		
		testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.DebtorAccount")!=null, 
				"Optional field DebtorAccount is present under Data/Initiation block and is not empty");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
