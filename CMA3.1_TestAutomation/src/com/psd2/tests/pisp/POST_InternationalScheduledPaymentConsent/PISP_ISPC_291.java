package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/Creditor/PostalAddress block where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_291 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_291() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment Consent URI");		
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressType"))!=null, 
					"Mandatory field AddressType is present in CreditorPostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.Department"))!=null, 
					"Mandatory field Department is present in CreditorPostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.SubDepartment"))!=null, 
					"Optional field SubDepartment is present in CreditorPostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.StreetName"))!=null, 
					"Optional field StreetName is present in CreditorPostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.BuildingNumber"))!=null, 
					"Optional field BuildingNumber is present in CreditorPostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.PostCode"))!=null, 
					"Optional field PostCode is present in CreditorPostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.TownName"))!=null, 
					"Optional field TownName is present in CreditorPostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.CountrySubDivision"))!=null, 
					"Optional field CountrySubDivision is present in CreditorPostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.Country"))!=null, 
					"Optional field Country is present in CreditorPostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressLine"))!=null, 
					"Optional field AddressLine is present in CreditorPostalAddress block");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
