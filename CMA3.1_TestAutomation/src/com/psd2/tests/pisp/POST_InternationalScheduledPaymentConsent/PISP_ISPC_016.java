package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without token value OR key-value pair into Authorization (Access Token) header 
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_016 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_016() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
				
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Verification of the request without token value OR key-value pair into Authorization (Access Token) header");
			
			restRequest.setURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			String requestBody=iScheduledPayment.genRequestBody();
			restRequest.setHeadersString("Content-Type:application/json, x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-fapi-interaction-id:"+PropertyUtils.getProperty("inter_id")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json, x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
			restRequest.setRequestBody(requestBody);
			restRequest.setMethod("POST");
			restRequest.submit();
			
			testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", "Response Code is correct for Malformed token value into Authorization (Access Token) header for Domestic Schedule Payment Consent SetUp");
			
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();
}
}
