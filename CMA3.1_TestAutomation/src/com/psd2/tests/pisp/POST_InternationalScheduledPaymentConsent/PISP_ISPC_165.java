package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the values of OPTIONAL CreditorAgent/SchemeName field having OB defined values 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_165 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_164() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.setCrAgentSchemeName("UK.OBIE.BICFI");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for POST InternationalScheduledPaymentConsent URI when scheme name is UK.OBIE.BICFI and starting with UK.OBIE");
			testVP.verifyEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.SchemeName")), iScheduledPayment._crAgentSchemeName, 
					"SchemeName in response is same as sent in the request");
			
			TestLogger.logBlankLine();	
			
			TestLogger.logStep("[Step 3] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.setHeadersString("Content-Type:application/json, x-fapi-interaction-id:"+PropertyUtils.getProperty("inter_id")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json, x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
			iScheduledPayment.setCrAgentSchemeName("BICFI");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for POST InternationalScheduledPaymentConsent URI when scheme name is BICFI");
			testVP.verifyEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.SchemeName")), iScheduledPayment._crAgentSchemeName, 
					"SchemeName in response is same as sent in the request");
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
	}
}
