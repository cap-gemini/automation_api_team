package com.psd2.tests.pisp.POST_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/CreditorAgent/PostalAddress block 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISPC_174 extends TestBase{	
	
	@Test
	public void m_PISP_ISPC_174() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
		    		"Response Code is correct for Post International Scheduled Payment Consents");
			testVP.verifyTrue(!(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress")).isEmpty()), 
					"CreditorAgent.PostalAddress block is present in response body and is not null");	
			testVP.verifyTrue(!(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType")).isEmpty()), 
					"AddressType is present in response body and is not null");
			testVP.verifyTrue(!(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.Department")).isEmpty()), 
					"Department is present in response body and is not null");
			testVP.verifyTrue(!(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.SubDepartment")).isEmpty()), 
					"SubDepartment is present in response body and is not null");
			testVP.verifyTrue(!(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.StreetName")).isEmpty()), 
					"StreetName is present in response body and is not null");
			testVP.verifyTrue(!(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.BuildingNumber")).isEmpty()), 
					"BuildingNumber is present in response body and is not null");
			testVP.verifyTrue(!(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.PostCode")).isEmpty()), 
					"PostCode is present in response body and is not null");
			testVP.verifyTrue(!(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.TownName")).isEmpty()), 
					"TownName is present in response body and is not null");
			testVP.verifyTrue(!(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.CountrySubDivision")).isEmpty()), 
					"CountrySubDivision is present in response body and is not null");
			testVP.verifyTrue(!(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.Country")).isEmpty()), 
					"Country is present in response body and is not null");
			testVP.verifyTrue(!(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressLine")).isEmpty()), 
					"AddressLine is present in response body and is not null");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
