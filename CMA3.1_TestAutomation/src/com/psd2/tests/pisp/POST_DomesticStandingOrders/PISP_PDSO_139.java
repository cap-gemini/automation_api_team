package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY DeliveryAddress/TownName field haven't sent
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_139 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_PDSO_139() throws Throwable{	
		
        TestLogger.logStep("[Step 1] : Generate access token");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY DeliveryAddress/TownName field haven't sent");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
        dStandingOrder.setConsentId(consentDetails.get("consentId"));
        String requestBody=dStandingOrder.genRequestBody().replace("\"TownName\": \"ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789\",", "");
		dStandingOrder.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
		"Response Code is correct for Domestic Standing Orders Submission URL when MANDATORY DeliveryAddress/TownName field haven't sent");		
				
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Missing", 
				"Error Code are matched");
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Missing required field [TownName]", 
				"Error Message are matched");		
				
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
	}

