package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of NULL and Invalid Frequency field under Initiation/frequency
 * @author Mohit Patidar
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_PDSO_039 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_PDSO_039() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate access token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : Verification of NULL Frequency field under Initiation/frequency");

		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setFrequency("");
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();

		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400",
				"Response Code is correct for Domestic Standing Orders URL for Frequency fields which is set as NULL");
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Unsupported.Frequency", 
				"Error Code is correct"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"));
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").isEmpty()),
				"Error Message is "+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"));

		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of invalid Frequency field under Initiation/frequency");

		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setFrequency("invalid");
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();

		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400",
				"Response Code is correct for Domestic Standing Orders URL for Frequency fields which is set as Invalid");
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Unsupported.Frequency", 
				"Error Code is correct"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"));
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").isEmpty()),
				"Error Message is "+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"));

		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}
