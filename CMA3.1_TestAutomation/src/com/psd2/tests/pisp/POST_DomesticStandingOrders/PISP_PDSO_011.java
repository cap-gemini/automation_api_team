package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request where same  request payload has been sent into BODY with same x-idempotency-key by same TPP within 24 hrs
 * @author Mohit Patidar
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_PDSO_011 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_PDSO_011() throws Throwable {

		String idemPotencyKey=PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3);
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST Domestic Standing Orders Submission");

		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",x-idempotency-key:"+idemPotencyKey);
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();

		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Orders when x-idempotency-key value in header is "+idemPotencyKey+"");
		
		String paymentId=dStandingOrder.getPaymentId();
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.Status"), "InitiationCompleted",
				"Status field value is correct");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of request with same x-idempotency-key i.e. "+idemPotencyKey+" and same payload");
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",x-idempotency-key:"+idemPotencyKey);
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Orders when x-idempotency-key value is same i.e. "+idemPotencyKey+" and payload is same");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.Status"), "InitiationCompleted",
				"Status field value is correct");
		
		testVP.assertStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.DomesticStandingOrderId"), paymentId, 
				"PaymentId is same as the request sent previously");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
