package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Initiation/FirstPaymentDateTime field is old dated
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_047 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_PDSO_047() throws Throwable{	
		dStandingOrder.addNumberOfPayments();
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		apiUtility.pispAccessToken=true;
		consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Initiation/FirstPaymentDateTime field is old dated");
		dStandingOrder.setFirstPaymentDateTime("2018-02-25T06:06:06+00:00");
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();
	
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400",
				"Response Code is correct for Domestic Standing Orders URI when MANDATORY Initiation/FirstPaymentDateTime field value is old dated");
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Resource.ConsentMismatch", 
				"Error Code is correct i.e "+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"));
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"),"Payload comparison failed with the consent resource",
				"Error Message is correct i.e "+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"));

		TestLogger.logBlankLine();	
		testVP.testResultFinalize();		
	}
}