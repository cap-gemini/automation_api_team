package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of domestic-Standing Order URL having older
 * @author Mohit Patidar
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression","Invalid" })
public class PISP_PDSO_006 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_PDSO_006() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST Domestic Standing Order Submission");
		//dStandingOrder.setBaseURL(apiConst.backcomp_dso_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();

		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"404",
				"Response Code is correct for POST Domestic Standing Order Submission for version 1.1 in PISP");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
