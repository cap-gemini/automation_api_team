package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the value of OPTIONAL DebtorAccount/SecondaryIdentification field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_103 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PDSO_103() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of NULL value of MANDATORY DebtorAccount/SecondaryIdentification field");
		
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.setDrAccountSrIdentification("");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for POST Domestic Standing Orders Submission URI when DebtorAccount SecondaryIdentification is NULL");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected min length 1 for field [SecondaryIdentification], but got []"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the value of MANDATORY DebtorAccount/SecondaryIdentification field having length variations");
		
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.setDrAccountSrIdentification("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567834782469723054");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for POST Domestic Standing Orders Submission URI when DebtorAccount SecondaryIdentification is more than 34 characters");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 34 for field [SecondaryIdentification], but got [ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567834782469723054]"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of the value of OPTIONAL DebtorAccount/SecondaryIdentification field");
		
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.setDrAccountSrIdentification("54988");
		dStandingOrder.submit();
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SecondaryIdentification")).isEmpty(), 
				"Name field under DebtorAccount is present and is not null");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SecondaryIdentification").length()<=34, 
				"Name field under DebtorAccount is less than 34 characters");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
