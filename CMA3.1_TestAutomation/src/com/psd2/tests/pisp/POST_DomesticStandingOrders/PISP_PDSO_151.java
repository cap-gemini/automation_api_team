package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY DomesticStandingOrderId field where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_151 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_PDSO_151() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Standing Orders Submission");	
		
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct Domestic Standing Orders URI");
		
		testVP.verifyFalse(dStandingOrder.getResponseNodeStringByPath("Data.ConsentId").equals(dStandingOrder.getResponseNodeStringByPath("Data.DomesticStandingOrderId")), 
				"Mandatory DomesticStandingOrderId different from consentId");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.DomesticStandingOrderId").length()<=128, 
				"DomesticStandingOrderId length is less than or equal to 128 characters");
	
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
