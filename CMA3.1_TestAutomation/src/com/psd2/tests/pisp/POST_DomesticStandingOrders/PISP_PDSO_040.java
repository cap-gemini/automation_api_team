package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/Reference field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_040 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_PDSO_040() throws Throwable{	

			TestLogger.logStep("[Step 1] : Create access token");
	        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : Verification of the values of OPTIONAL Initiation/Reference field");	
			
			dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
			dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dStandingOrder.setConsentId(consentDetails.get("consentId"));
			dStandingOrder.submit();
		
		    testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Orders Submission URL");
		    
		    testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Reference").isEmpty()), 
		    		"Reference field under Data/Initiation block is present and is not null");
		    
		    testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Reference").length()<=35, 
		    		"Reference field length is correct i.e. less than or equal to 35 characters");
	
		    TestLogger.logBlankLine();
		
		    testVP.testResultFinalize();		
	}
}
