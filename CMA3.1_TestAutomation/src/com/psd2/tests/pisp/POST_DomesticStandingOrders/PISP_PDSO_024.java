package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of x-fapi-interaction-id value when NOT sent in the request with the response header 
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_024 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_PDSO_024() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of x-fapi-interaction-id value when NOT sent in the request with the response header");
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		restRequest.setURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		String requestBody= dStandingOrder.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", Content-Type:application/json, x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Orderss");
		testVP.verifyTrue(restRequest.getResponseHeader("x-fapi-interaction-id")!=null, 
				"x-fapi-interaction-id value in the response header is provided by ASPSP if not sent in the request"+(restRequest.getResponseHeader("x-fapi-interaction-id")));
	
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
