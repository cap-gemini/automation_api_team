package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through POST method with mandatory and optional fields
 * @author Mohit Patidar
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_PDSO_004 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_PDSO_004() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : Checking the request (UTF-8 character encoded) status through POST method with mandatory and optional fields.");
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();

		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()), "201",
				"Response Code is correct for POST Domestic Standing Orders Submission");
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data") != null,
				"Mandatory field Data is present in response and is not empty");
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Links") != null,
				"Mandatory field Links is present in response and is not empty");
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Links.Self")).isEmpty(),
				"Mandatory field Self is present in response and is not empty");
		testVP.verifyStringEquals(dStandingOrder.getResponseHeader("Content-Type"),
				"application/json;charset=UTF-8",
				"Response is UTF-8 character encoded");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
