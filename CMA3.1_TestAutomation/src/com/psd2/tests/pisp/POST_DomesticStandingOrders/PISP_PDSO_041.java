package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/Reference field having length variation
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_041 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_PDSO_041() throws Throwable{	
	
		TestLogger.logStep("[Step 1] : Generate access token");
	    consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
			
		TestLogger.logStep("[Step 2] : Verification of the values of OPTIONAL Initiation/Reference field having length variation");
			
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.setReference("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEKLMNOPQYZ12345AasdasdAs");
		dStandingOrder.submit();
			
        testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Standing Orders URI when Reference field length is more than 35 characters");
			
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
		    		"Error Code is correct i.e. "+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"));
			
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Expected max length 35 for field [Reference], but got [ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEKLMNOPQYZ12345AasdasdAs]", 
					"Error Message is correct i.e. "+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"));
			
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of OPTIONAL Initiation/Reference field with null value");
		
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.setReference("");
		dStandingOrder.submit();
			
        testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Standing Orders URI when Reference field value is null");
			
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
		    		"Error Code is correct i.e. "+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"));
			
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Expected min length 1 for field [Reference], but got []", 
					"Error Message is correct i.e. "+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"));
			
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
