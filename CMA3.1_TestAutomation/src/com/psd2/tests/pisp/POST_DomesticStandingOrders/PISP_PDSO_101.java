package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of Identification field under DebtorAccount/Identification having length variation
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_101 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PDSO_101() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of Identification field under DebtorAccount/Identification having length variation");
		
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.setDrAccountIdentification("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567891241244JO94CBJO001000000000013100030213571263581263912749udhkJO94CBJO001000000000013100030213571263581263912749udhkJO94CBJO001000000000013100030213571263581263912749udhkJO94CBJO001000000000013100030213571263581263912749udhk");
		dStandingOrder.submit();
		

		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for POST Domestic Standing Orders Submission URI when DebtorAccount Identification is more than 35 characters");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 256 for field [Identification], but got [ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567891241244JO94CBJO001000000000013100030213571263581263912749udhkJO94CBJO001000000000013100030213571263581263912749udhkJO94CBJO001000000000013100030213571263581263912749udhkJO94CBJO001000000000013100030213571263581263912749udhk]"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

