package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Amount field where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_163 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_PDSO_163() throws Throwable{	
	
				
			TestLogger.logStep("[Step 1] : Generate access token");
	        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : POST Domestic Standing Orders Submission");	
			
			dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
			dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dStandingOrder.setConsentId(consentDetails.get("consentId"));
			dStandingOrder.submit();
		
		   testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic Standing Orders URI");
		   testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Charges[0].Amount.Amount").split("\\.")[0].length()<=13
				   && dStandingOrder.getResponseNodeStringByPath("Data.Initiation.FirstPaymentAmount.Amount").split("\\.")[1].length()<=5, 
				   "Amount field value is correct i.e. upto 13 digits and 5 precisions");
			
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
