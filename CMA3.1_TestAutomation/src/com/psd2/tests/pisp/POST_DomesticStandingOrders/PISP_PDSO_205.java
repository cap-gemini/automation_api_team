package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/FirstPaymentAmount/Amount field where Amount starts with 900
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_205 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_PDSO_205() throws Throwable{	
		dStandingOrder.setFirstPaymentAmount("900.00");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		apiUtility.pispAccessToken=true;
		consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Standing Orders Submission");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();
			
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
			"Response Code is correct for Domestic Standing Orders Submission URI");
		    
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Rules.AfterCutOffDateTime", 
	    		"Error Code is correct i.e. "+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"));
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}