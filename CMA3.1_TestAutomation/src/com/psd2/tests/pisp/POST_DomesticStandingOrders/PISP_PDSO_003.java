package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of domestic-standing-order-consents URL that is should as per Open Banking standards with different PISP/Other Url
 * 
 * @author Mohit Patidar
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_PDSO_003 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_PDSO_003() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : Verification of domestic-standing-order-consents URL that is should as per Open Banking standards with different PISP/Other Url");
		dStandingOrder.setBaseURL(apiConst.dsp_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();

		testVP.verifyStringEquals(dStandingOrder.getURL(),apiConst.dsp_endpoint,
				"URL for POST Domestic Standing Orders Submission is as per open banking standard");

		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()), "400",
				"Response Code is correct for POST Domestic Standing Orders Submission");

		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Field.Missing", 
				"Error Code is correct i.e "+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"));
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"),
				"Error validating JSON. Error: - Missing required field [InstructionIdentification]","Error Message is correct i.e "+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"));

		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
