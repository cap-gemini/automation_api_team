package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of OPTIONAL DeliveryAddress/PostCode field having length variation
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_137 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_PDSO_137() throws Throwable{	
	
			
		TestLogger.logStep("[Step 1] : Generate access token");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Standing Orders Submission");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.setRiskAddressPostCode("ABCDEF 1234567891458");
		dStandingOrder.submit();
			
				
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Post Domestic Standing Orders Submission");
		
	    testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Expected max length 16 for field [PostCode], but got [ABCDEF 1234567891458]",
				"Error Message are matched");
			    
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

