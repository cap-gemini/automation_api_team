package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : 	Verification of the values into OPTIONAL Data/MultiAuthorisation block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_191 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PDSO_191() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
		consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Data/MultiAuthorisation block where Request has sent successfully and returned a HTTP Code 201 Created");
		
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();
		
	    testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Post Domestic Payment URI");
		
	    testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.MultiAuthorisation")!=null,
	    		"MultiAuthorisation block is present and is not null");
	    
	    testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.MultiAuthorisation.Status")).isEmpty(),
	    		"Status field under MultiAuthorisation block is present and is not null");
	    
	    testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.MultiAuthorisation.NumberRequired")).isEmpty(),
	    		"Status field under MultiAuthorisation block is present and is not null");
	    
	    testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.MultiAuthorisation.NumberReceived")).isEmpty(),
	    		"Status field under MultiAuthorisation block is present and is not null");
	    
	    testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.MultiAuthorisation.LastUpdateDateTime")).isEmpty(),
	    		"Status field under MultiAuthorisation block is present and is not null");
	    
	    testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.MultiAuthorisation.ExpirationDateTime")).isEmpty(),
	    		"Status field under MultiAuthorisation block is present and is not null");
	    
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

