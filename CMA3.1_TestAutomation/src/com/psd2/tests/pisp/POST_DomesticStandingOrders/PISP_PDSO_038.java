package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY Initiation/frequency field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_038 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_PDSO_038() throws Throwable{	
	
			TestLogger.logStep("[Step 1] : Create access token");
	        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY Initiation/frequency field");
			
			dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
			dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dStandingOrder.setConsentId(consentDetails.get("consentId"));
			dStandingOrder.submit();
		
		    testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Orders submission");
		    
		    testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency").contains("EvryDay")
		    		|| dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency").contains("EvryWorkgDay	")
		    		|| dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency").contains("IntrvlDay")
		    		|| dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency").contains("IntrvlWkDay")
		    		|| dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency").contains("WkInMnthDay")
		    		|| dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency").contains("IntrvlMnthDay")
		    		|| dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency").contains("QtrDay	"),
					"Frequency field value is correct i.e. "+dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency"));
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
