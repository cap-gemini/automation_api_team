package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY DebtorAccount/Identification field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_098 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PDSO_098() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY DebtorAccount/Identification field");
		
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();
		

		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for POST Domestic Standing Orders Submission URI");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification")).isEmpty(), 
				"Mandatory field Identification under DebtorAccount is present and is not null");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification").length()<=35, 
				"Length of Identification is not more than 35 characters");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

