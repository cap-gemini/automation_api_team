package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of MANDATORY DebtorAccount/SchemeName field having OB defined values
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_095 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PDSO_095() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values of MANDATORY DebtorAccount/SchemeName field having OB defined values");
		
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();

		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for POST Domestic Standing Orders Submission URI");
		
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.SortCodeAccountNumber")
				|| dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.IBAN")
				|| dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.PAN")
				|| dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.Paym")
				|| dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.BBAN")
				|| dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme1")
				|| dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme2"), 
				"MANDATORY DebtorAccount/SchemeName field has OB defined values");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

