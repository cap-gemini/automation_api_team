package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without OR invalid value of Content-Type header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_030 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	String requestBody;
	@Test
	public void m_PISP_PDSO_030() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Standing Orders");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Standing Orders Submission with Invalid Content-Type value");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		restRequest.setURL(apiConst.dsoSubmission_endpoint);
		requestBody= dStandingOrder.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3)+", x-jws-signature:"+SignatureUtility.generateSignature(requestBody));
		restRequest.addHeaderEntry("Content-Type", "application/xml");
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"415", 
				"Response Code is correct with invalid Content-Type value in request header");
		
		TestLogger.logStep("[Step 3] : POST Domestic Standing Orders Submission without Content-Type value");	
		restRequest.setURL(apiConst.dsoSubmission_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3)+", x-jws-signature:"+SignatureUtility.generateSignature(requestBody));
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"415", 
				"Response Code is correct without Content-Type value in request header");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
