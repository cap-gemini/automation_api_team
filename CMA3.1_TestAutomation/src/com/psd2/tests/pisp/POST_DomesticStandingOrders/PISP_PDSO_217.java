package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of date and time for below mentioned dates fields without giving seconds
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_217 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_PDSO_217() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
        String key = dStandingOrder.getHeaderEntry("x-idempotency-key");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of date and time for FinalPaymentDateTime field without giving seconds");
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-idempotency-key:"+key);
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.setFinalPaymentDateTime("2021-03-20T06:06");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Orders Submission URI when FinalPaymentDateTime field value is without seconds");
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Provided value 2021-03-20T06:06 is not compliant with the format datetime provided in rfc3339 for FinalPaymentDateTime", "Error Message are matched");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of date and time for FinalPaymentDateTime field without giving seconds");
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-idempotency-key:"+key);
		dStandingOrder.setFinalPaymentDateTime("2021-03-20T06:06Z");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Orders Submission URI when FinalPaymentDateTime field value is without seconds");
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Provided value 2021-03-20T06:06Z is not compliant with the format datetime provided in rfc3339 for FinalPaymentDateTime", "Error Message are matched");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of date and time for FinalPaymentDateTime field without giving seconds");
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-idempotency-key:"+key);
		dStandingOrder.setFinalPaymentDateTime("2021-03-20T06:06+00:00");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Orders Submission URI when FinalPaymentDateTime field value is without seconds");
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Provided value 2021-03-20T06:06+00:00 is not compliant with the format datetime provided in rfc3339 for FinalPaymentDateTime", "Error Message are matched");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
