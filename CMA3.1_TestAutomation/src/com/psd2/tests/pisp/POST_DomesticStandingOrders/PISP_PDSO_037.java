package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of status when MANDATORY Initiation/frequency field NOT sent
 * @author Mohit Patidar
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_PDSO_037 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_PDSO_037() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate access token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST Domestic Standing Orders Submission");

		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		String requestBody = dStandingOrder.genRequestBody().replace("\"Frequency\": \"EvryDay\",","");
		dStandingOrder.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400",
				"Response Code is correct for Domestic Standing Orders URI when MANDATORY Initiation/frequency field block haven't sent");
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Field.Missing", 
				"Error Code is correct i.e "+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"));
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"),"Error validating JSON. Error: - Missing required field [Frequency\"",
				"Error Message is correct i.e "+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"));

		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}
