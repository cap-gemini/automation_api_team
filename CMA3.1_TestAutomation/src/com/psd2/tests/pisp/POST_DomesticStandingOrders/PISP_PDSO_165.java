package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation block
 * @author Mohit Patidar
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_PDSO_165 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_PDSO_165() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate accesss token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST Domestic Standing Orders Submission");

		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		//dStandingOrder.addNumberOfPayments();
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()), "201",
				"Response Code is correct for Domestic Standing Orders URI");
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 3] : Verification of the values into MANDATORY Initiation block where Request has sent successfully and returned a HTTP Code 201 Created");

		
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Initiation") != null,
				"Mandatory block Initiation is present and is not null");

		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency")).isEmpty(),
				"Frequency field is present under Initiation");

		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Reference")).isEmpty(),
				"Reference field is present under Initiation block");

		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.FirstPaymentDateTime")).isEmpty(),
				"FirstPaymentDateTime field is present under Initiation block");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.RecurringPaymentDateTime")).isEmpty(),
				"RecurringPaymentDateTime field is present under Initiation block");
		
		/*testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.FinalPaymentDateTime")).isEmpty(),
				"FinalPaymentDateTime field is present under Initiation block");*/

		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Initiation.FirstPaymentAmount") != null,
				"FirstPaymentAmount field is present under Initiation block");
		
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Initiation.RecurringPaymentAmount") != null,
				"RecurringPaymentAmount field is present under Initiation block");
		
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Initiation.FinalPaymentAmount") != null,
				"FinalPaymentAmount field is present under Initiation block");

		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount") != null,
				"Mandatory field CreditorAccount is present under Initiation block");

		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Initiation.DebtorAccount") != null,
				"Optional field DebtorAccount is present under Initiation block");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
