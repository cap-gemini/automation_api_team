package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of Domestic Standing Orders URL that should be as per Open Banking standards 
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class PISP_PDSO_001 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PDSO_001() throws Throwable{
		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : POST Domestic Standing Orders");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.submit();
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Post Domestic Standing Orders Submission");
		
		testVP.verifyTrue(SignatureUtility.verifySignature(dStandingOrder.getResponseString(), dStandingOrder.getResponseHeader("x-jws-signature")), 
				"Response that created successfully with HTTP Code 201 MUST be digitally signed");
		
		String paymentId=dStandingOrder.getPaymentId();
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.Status"), "InitiationCompleted",
				"Status field value is correct");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : POST Domestic Standing Orders");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.submit();
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Post Domestic Standing Orders Submission");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.Status"), "InitiationCompleted",
				"Status field value is correct");
		
		testVP.assertStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.DomesticStandingOrderId"), paymentId, 
				"PaymentId is same as the request sent previously");
		
		testVP.testResultFinalize();		
	}
}