package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of BLANK Amount field under RecurringPaymentAmount/Amount
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_073 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_PDSO_073() throws Throwable{	
	
				
		TestLogger.logStep("[Step 1] : Generate access token");
	    consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
			
		TestLogger.logStep("[Step 2] : Verification of BLANK Amount field under RecurringPaymentAmount/Amount");	
			
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.setRecurringPaymentAmount("");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Standing Orders URI");
		  
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
		    		"Error Code is correct i.e. "+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"));
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Invalid value ''. Expected ^\\d{1,13}\\.\\d{1,5}$ for Amount", 
					"Error Message is correct i.e. "+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"));
			
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
