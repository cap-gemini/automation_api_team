package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of OPTIONAL DeliveryAddress/CountrySubDivision field having length variation
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_143 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_PDSO_143() throws Throwable{	
		
        TestLogger.logStep("[Step 1] : Generate access token");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of OPTIONAL DeliveryAddress/CountrySubDivision field having length variation");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setRiskCountrySubDivision("ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789485765");
        dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
	    "Response Code is correct for Domestic Standing Orders URL for CountrySubDivision fields having length more than 35 characters");		
						
		/*testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error Code are matched");
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Invalid array element ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789485765.", 
				"Error Message are matched");	*/	
						
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();	
	}
	}

