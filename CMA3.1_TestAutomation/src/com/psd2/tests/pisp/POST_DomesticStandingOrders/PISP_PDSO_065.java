package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/RecurringPaymentAmount block
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_065 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_PDSO_065() throws Throwable{	
	
			
			TestLogger.logStep("[Step 1] : Generate access token");
			
	        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : POST Domestic Standing Orders Submission");
			
			dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
			dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dStandingOrder.setConsentId(consentDetails.get("consentId"));
			dStandingOrder.submit();
		
		    testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic Standing Orders URI");
			
		    testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Initiation.RecurringPaymentAmount")!=null, 
		    		"RecurringPaymentAmount field under Initiation block is present and is not null");
		    
		    testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.RecurringPaymentAmount.Amount").isEmpty()), 
		    		"Amount field under RecurringPaymentAmount block is present and is not null");
		    
		    testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.RecurringPaymentAmount.Currency").isEmpty()), 
		    		"Currency field under RecurringPaymentAmount block is present and is not null");
		   
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
