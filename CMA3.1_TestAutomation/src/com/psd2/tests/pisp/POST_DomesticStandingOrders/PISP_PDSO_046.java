package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of MANDATORY Initiation/FirstPaymentDateTime field not followed ISO 8601 date-time format
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_046 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_PDSO_046() throws Throwable{	
	
			
			TestLogger.logStep("[Step 1] : Generate access token");
	        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
			TestLogger.logBlankLine();
				
			TestLogger.logStep("[Step 2] : Verification of the values of MANDATORY Initiation/FirstPaymentDateTime field not followed ISO 8601 date-time format");
			
			dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
			dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dStandingOrder.setConsentId(consentDetails.get("consentId"));
			dStandingOrder.setFirstPaymentDateTime("20-02-2021");
			dStandingOrder.submit();
		
			testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400",
					"Response Code is correct for Domestic Standing Orders URI when MANDATORY Initiation/FirstPaymentDateTime field not followed ISO 8601 date-time format");
			testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Field.Invalid", 
					"Error Code is correct i.e "+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"));
			testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"),"Error validating JSON. Error: - Provided value 20-02-2021 is not compliant with the format datetime provided in rfc3339 for FirstPaymentDateTime",
					"Error Message is correct i.e "+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"));

			TestLogger.logBlankLine();
			
		    testVP.testResultFinalize();		
	}
}
