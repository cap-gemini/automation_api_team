package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the length of MANDATORY DebtorAccount/SchemeName field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_093 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PDSO_093() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
        TestLogger.logBlankLine();
				
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY DebtorAccount/SchemeName field");
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();

		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for POST Domestic Standing Orders Submission URI");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName").length()<=40, 
				"Mandatory field SchemeName under DebtorAccount has length not more than 40 characters");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

