package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/NumberOfPayments field having length variation
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_043 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_PDSO_043() throws Throwable{	
	
		TestLogger.logStep("[Step 1] : Generate access token");
	    consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
			
		TestLogger.logStep("[Step 2] : Verification of the values of OPTIONAL Initiation/NumberOfPayments field having length variation");
		dStandingOrder.addNumberOfPayments();
		dStandingOrder.setNumberOfPayments("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEKLMNOPQYZ12345AasdasdAs");
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();
			
        testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Standing Orders URI when NumberOfPayments field length is more than 35 characters");
			
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
		    		"Error Code is correct i.e. "+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"));
			
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Expected max length 35 for field [NumberOfPayments], but got [ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEKLMNOPQYZ12345AasdasdAs]", 
					"Error Message is correct i.e. "+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"));
			
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of OPTIONAL Initiation/NumberOfPayments field with null value");
		dStandingOrder.addNumberOfPayments();
		dStandingOrder.setNumberOfPayments("");
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();
			
        testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Standing Orders URI when NumberOfPayments field value is null");
			
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
		    		"Error Code is correct i.e. "+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"));
			
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Expected min length 1 for field [NumberOfPayments], but got []", 
					"Error Message is correct i.e. "+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message"));
			
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
