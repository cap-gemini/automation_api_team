package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Risk/DeliveryAddress block haven't sent
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PDSO_129 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PDSO_129() throws Throwable{	
		String requestBody="";
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Standing Orders Consent");
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		requestBody=dStandingOrder.genRequestBody().replace(",\"DeliveryAddress\": {"
				+ "\"AddressLine\":[\"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ\",\"12345678\"],"					
				+ "\"StreetName\": \"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ12345678\","
				+ "\"BuildingNumber\": \"ABCDEF1234567890\","
				+ "\"PostCode\": \"ABCDEF 123456789\","
				+ "\"TownName\": \"ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789\","
				+ "\"CountrySubDivision\":[ \"test\"],"
				+ "\"Country\": \"GB\"}", "");
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.submit(requestBody);
			
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Post Domestic Standing Orders Consent");
		consentId = dStandingOrder.getConsentId();	
		TestLogger.logVariable("Consent Id : " + consentId);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : JWT Token Creation........");
		
		//reqObject.setBaseURL(apiConst.ro_endpoint);
		reqObject.setValueField(consentId);
		reqObject.setScopeField("payments");
		outId = reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Go to URL and authenticate consent");	
		redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		
		authCode = consentOps.authorisePISPConsent(redirecturl+"##"+dStandingOrder._drAccountIdentification,dStandingOrder.addDebtorAccount);
		closeDriverInstance();
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 5] : Get access token");	
		accesstoken.setBaseURL(apiConst.at_endpoint);
		accesstoken.setAuthCode(authCode);
		accesstoken.submit();
		
		testVP.verifyStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
				"Response Code is correct for get access token request");	
		access_token = accesstoken.getAccessToken();
		TestLogger.logVariable("Access Token : " + access_token);		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Risk/DeliveryAddress block haven't sent");
		
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+access_token);
		dStandingOrder.setConsentId(consentId);
		requestBody=dStandingOrder.genRequestBody().replace(",\"DeliveryAddress\": {"
				+ "\"AddressLine\":[\"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ\",\"12345678\"],"					
				+ "\"StreetName\": \"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ12345678\","
				+ "\"BuildingNumber\": \"ABCDEF1234567890\","
				+ "\"PostCode\": \"ABCDEF 123456789\","
				+ "\"TownName\": \"ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789\","
				+ "\"CountrySubDivision\":[ \"test\"],"
				+ "\"Country\": \"GB\"}", "");
		dStandingOrder.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Orders Submission URI");
		
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Risk.DeliveryAddress")==null, 
				"DeliveryAddress is not present");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}