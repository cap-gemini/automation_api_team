package com.psd2.tests.pisp.POST_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = UK.OBIE.BBAN
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Blocked"})
public class PISP_PDSO_227 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_PDSO_227() throws Throwable{	
		dStandingOrder.setDrAccountSchemeName("UK.OBIE.BBAN");
		dStandingOrder.setDrAccountIdentification("ABCD12345678901234");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Standing Orders Submission");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dStandingOrder.setConsentId(consentDetails.get("consentId"));
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for POST Domestic Standing Orders Submission URI");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName"), "UK.OBIE.BBAN", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification"), "ABCD12345678901234", 
				"Identification field value is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}