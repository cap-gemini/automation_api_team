package com.psd2.tests.pisp.GET_DomesticScheduledPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with BLANK or Invalid value of OPTIONAL x-fapi-customer-ip-address header 
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSP"})
public class PISP_GDSPS_020 extends TestBase{	
	
	 API_E2E_Utility apiUtility=new API_E2E_Utility();
		
		@Test
		public void m_PISP_GDSPS_020() throws Throwable{	
			
			TestLogger.logStep("[Step 1-1] : Creating client credetials....");
	        createClientCred.setBaseURL(apiConst.cc_endpoint);
	        createClientCred.setScope("payments");
	        createClientCred.submit();
	        
	        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	        cc_token = createClientCred.getAccessToken();
	        TestLogger.logVariable("AccessToken : " + cc_token);
	        TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Verification of x-fapi-interaction-id value when NOT sent in the request with the response header that created successfully with HTTP Code 200 OK");	
			
			dspConsent.setBaseURL(apiConst.dsp_endpoint+"/"+API_Constant.getDsp_PaymentId());
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.addHeaderEntry("x-fapi-customer-ip-address", "UUXGUISGC^%");
			dspConsent.setMethod("GET");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled payments URI when invalid x-fapi-customer-id-address is passed");
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
					"Response Error Code is correct");
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].Message")),"Invalid value found in x-fapi-customer-ip-address header", 
					"Response Error Message is correct");
			
            TestLogger.logStep("[Step 3] : All the above steps would remains same where request is having Blank value of OPTIONAL x-fapi-customer-last-logged-time header");
			
			dspConsent.setBaseURL(apiConst.dsp_endpoint+"/"+API_Constant.getDsp_PaymentId());
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.addHeaderEntry("x-fapi-customer-ip-address", "");
			dspConsent.setMethod("GET");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled Payment URI when x-fapi-customer-ip-address passed blank value");
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
					"Response Error Code is correct");
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].Message")),"Invalid value found in x-fapi-customer-ip-address header", 
					"Response Error Message is correct");
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();		
	}
}
