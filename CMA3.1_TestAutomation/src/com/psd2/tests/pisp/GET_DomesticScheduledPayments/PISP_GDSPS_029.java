package com.psd2.tests.pisp.GET_DomesticScheduledPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation block
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSP"})
public class PISP_GDSPS_029 extends TestBase{	
	
	 API_E2E_Utility apiUtility=new API_E2E_Utility();
		
		@Test
		public void m_PISP_GDSPS_029() throws Throwable{	
			
			TestLogger.logStep("[Step 1-1] : Creating client credetials....");
	        createClientCred.setBaseURL(apiConst.cc_endpoint);
	        createClientCred.setScope("payments");
	        createClientCred.submit();
	        
	        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	        cc_token = createClientCred.getAccessToken();
	        TestLogger.logVariable("AccessToken : " + cc_token);
	        TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Verification of the values into MANDATORY Data");	
			
			dspConsent.setBaseURL(apiConst.dsp_endpoint+"/"+API_Constant.getDsp_PaymentId());
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.setMethod("GET");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
					"Response Code is correct for Domestic Scheduled Payments URI");
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.InstructionIdentification"))!=null, 
					"Mandatory field InstructionIdentification is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.InstructionIdentification"));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.EndToEndIdentification"))!=null, 
					"Mandatory field EndToEndIdentification is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.EndToEndIdentification"));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.InstructedAmount"))!=null,
					"Mandatory field InstructedAmount is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.InstructedAmount"));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorAccount"))!=null, 
					"Mandatory field CreditorAccount is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.CreditorAccount"));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress"))!=null,
					"Optional field CreditorPostalAddress is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress"));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation"))!=null,
					"Optional field CreditorPostalAddress is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation"));
			
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();		
	}
}
