package com.psd2.tests.pisp.GET_DomesticScheduledPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of request when DomesticPaymentId does not exist i.e. DomesticPaymentId format is correct but it is not available into database.
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSP"})
public class PISP_GDSPS_007 extends TestBase{	
	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	
	@Test
	public void m_PISP_GDSPS_007() throws Throwable{	
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-2] : Get Domestic scheduled-payments Submission ");	
		
		dspConsent.setBaseURL(apiConst.dsp_endpoint+"/"+API_Constant.getDsp_PaymentId()+"123");
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
		dspConsent.setMethod("GET");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Get Domestic scheduled Payment Submission URL is NOT as per Open Banking standards");	
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Resource.NotFound", 
				"Response Error Code is correct");
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].Message")),"submission id is missing",
				"Error message is correct");
		
        			
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();
	}
}
