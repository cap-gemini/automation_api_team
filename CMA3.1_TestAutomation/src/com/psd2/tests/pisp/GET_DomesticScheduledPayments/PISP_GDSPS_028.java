package com.psd2.tests.pisp.GET_DomesticScheduledPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSP"})
public class PISP_GDSPS_028 extends TestBase{	
	
	 API_E2E_Utility apiUtility=new API_E2E_Utility();
		
		@Test
		public void m_PISP_GDSPS_028() throws Throwable{	
			
			TestLogger.logStep("[Step 1-1] : Creating client credetials....");
	        createClientCred.setBaseURL(apiConst.cc_endpoint);
	        createClientCred.setScope("payments");
	        createClientCred.submit();
	        
	        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	        cc_token = createClientCred.getAccessToken();
	        TestLogger.logVariable("AccessToken : " + cc_token);
	        TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Verification of the values into MANDATORY Data");	
			
			dspConsent.setBaseURL(apiConst.dsp_endpoint+"/"+API_Constant.getDsp_PaymentId());
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.setMethod("GET");
			dspConsent.submit();
			
			testVP.verifyStringEquals(dspConsent.getURL(),apiConst.dsp_endpoint+"/"+API_Constant.getDsp_PaymentId(), 
					"URI for GET domestic scheduled payment request is as per open banking standard");
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.DomesticScheduledPaymentId"))!=null,
					"DomesticScheduledPaymentId field under Data is present in Get Domestic Scheduled Payments response body"+String.valueOf(dspConsent.getResponseValueByPath("Data.DomesticScheduledPaymentId")));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.ConsentId"))!=null,
					"ConsentId field under Data is present in Get Domestic Scheduled Payments response body"+String.valueOf(dspConsent.getResponseValueByPath("Data.ConsentId")));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.CreationDateTime"))!=null,
					"CreationDateTime field under Data is present in Get Domestic Scheduled Payments response body"+(dspConsent.getResponseValueByPath("Data.CreationDateTime")));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.StatusUpdateDateTime"))!=null,
					"StatusUpdateDateTime field under Data is present in Get Domestic Scheduled Payments response body"+String.valueOf(dspConsent.getResponseValueByPath("Data.StatusUpdateDateTime")));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Status"))!=null,
					"Status field under Data is present in Get Domestic Payment Consent response body"+String.valueOf(dspConsent.getResponseValueByPath("Data.Status")));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.ExpectedExecutionDateTime"))!=null,
					"ExpectedExecutionDateTime field under Data is present in Get Domestic Scheduled Payments response body"+String.valueOf(dspConsent.getResponseValueByPath("Data.ExpectedExecutionDateTime")));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.ExpectedSettlementDateTime"))!=null,
					"ExpectedSettlementDateTime field under Data is present in Get Domestic Scheduled Payments response body"+String.valueOf(dspConsent.getResponseValueByPath("Data.ExpectedSettlementDateTime")));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Charges"))!=null,
					"Charges field under Data is present in Get Domestic Scheduled Payments response body"+String.valueOf(dspConsent.getResponseValueByPath("Data.Charges")));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation"))!=null,
					"Initiation field under Data is present in Get Domestic Scheduled Payments response body"+String.valueOf(dspConsent.getResponseValueByPath("Data.Initiation")));
			
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();		
	}
}
