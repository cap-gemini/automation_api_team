package com.psd2.tests.pisp.GET_DomesticScheduledPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with BLANK or Invalid value of OPTIONAL x-fapi-auth-date header
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSP"})
public class PISP_GDSPS_021 extends TestBase{	
	
	 API_E2E_Utility apiUtility=new API_E2E_Utility();
		
		@Test
		public void m_PISP_GDSPS_021() throws Throwable{	
			
			TestLogger.logStep("[Step 1-1] : Creating client credetials....");
	        createClientCred.setBaseURL(apiConst.cc_endpoint);
	        createClientCred.setScope("payments");
	        createClientCred.submit();
	        
	        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	        cc_token = createClientCred.getAccessToken();
	        TestLogger.logVariable("AccessToken : " + cc_token);
	        TestLogger.logBlankLine();
			
            TestLogger.logStep("[Step 1-2] : Verification of x-fapi-auth-date value");	
			
			dspConsent.setBaseURL(apiConst.dsp_endpoint+"/"+API_Constant.getDsp_PaymentId());
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.addHeaderEntry("x-fapi-customer-last-logged-time", "FSTYWFUD^%^");
			dspConsent.setMethod("GET");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled payments URI when invalid x-fapi-auth-date is passed");
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
					"Response Error Code is correct");
			
			  testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].Message")),"Invalid value '[FSTYWFUD^%^]' for header x-fapi-customer-last-logged-time. Invalid value 'FSTYWFUD^%^'. Expected ^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), \\d{2} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \\d{4} \\d{2}:\\d{2}:\\d{2} (GMT|UTC)$", 
					    "Error Message are matched");

			
            TestLogger.logStep("[Step 3] : All the above steps would remains same where request is having Blank value of OPTIONAL x-fapi-customer-last-logged-time header");
			
			dspConsent.setBaseURL(apiConst.dsp_endpoint+"/"+consentDetails.get("paymentId"));
			dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
			dspConsent.addHeaderEntry("x-fapi-customer-last-logged-time", "");
			dspConsent.setMethod("GET");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled Payment URI when x-fapi-auth-date passed blank value");
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
					"Response Error Code is correct");
			
			testVP.verifyTrue(!String.valueOf(dspConsent.getResponseValueByPath("Errors[0].Message")).isEmpty(), 
					"Response Error Message is correct");
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();		
	}
}