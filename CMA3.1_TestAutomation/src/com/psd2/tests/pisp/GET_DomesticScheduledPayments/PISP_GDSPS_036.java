package com.psd2.tests.pisp.GET_DomesticScheduledPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into OPTIONAL ExpectedSettlementDateTime field
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSP"})
public class PISP_GDSPS_036 extends TestBase{	
	
	 API_E2E_Utility apiUtility=new API_E2E_Utility();
		
		@Test
		public void m_PISP_GDSPS_036() throws Throwable{	
			
			TestLogger.logStep("[Step 1-1] : Creating client credetials....");
	        createClientCred.setBaseURL(apiConst.cc_endpoint);
	        createClientCred.setScope("payments");
	        createClientCred.submit();
	        
	        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	        cc_token = createClientCred.getAccessToken();
	        TestLogger.logVariable("AccessToken : " + cc_token);
	        TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Verification of the values into MANDATORY Data");	
			
			dspConsent.setBaseURL(apiConst.dsp_endpoint+"/"+API_Constant.getDsp_PaymentId());
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.setMethod("GET");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
					"Response Code is correct for Get Domestic Scheduled Payment Submission");	
			
			testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.ExpectedSettlementDateTime")!=null, 
					 "Data field is present under ExpectedSettlementDateTime field");
			 
			testVP.verifyTrue(Misc.verifyDateTimeFormat(dspConsent.getResponseNodeStringByPath("Data.ExpectedSettlementDateTime").split("T")[0], "yyyy-MM-dd") && 
					(Misc.verifyDateTimeFormat(dspConsent.getResponseNodeStringByPath("Data.ExpectedSettlementDateTime").split("T")[1], "HH:mm:ss+00:00")), 
					"ExpectedSettlementDateTime is as per expected format");
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();		
	}
}

		
		