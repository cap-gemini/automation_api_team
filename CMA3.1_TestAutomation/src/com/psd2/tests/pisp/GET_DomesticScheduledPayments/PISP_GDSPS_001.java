package com.psd2.tests.pisp.GET_DomesticScheduledPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of domestic-scheduled payment-consents URL that should be as per Open Banking standards as specified
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity","DSP"})
public class PISP_GDSPS_001 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPS_001() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, true);
        API_Constant.setDsp_PaymentId(consentDetails.get("paymentId"));
        TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Get Domestic scheduled-payments Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint+"/"+consentDetails.get("paymentId"));
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dspConsent.setMethod("GET");
		dspConsent.submit();
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Get Domestic scheduled Payment Submission");	
		
		testVP.verifyTrue(SignatureUtility.verifySignature(dspConsent.getResponseString(), dspConsent.getResponseHeader("x-jws-signature")), 
				"Response that created successfully with HTTP Code 201 MUST be digitally signed");
		
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();		
	}
}
