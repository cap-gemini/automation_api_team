package com.psd2.tests.pisp.GET_DomesticScheduledPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through POST method with mandatory and optional fields.
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSP"})
public class PISP_GDSPS_004 extends TestBase{	
	
       API_E2E_Utility apiUtility=new API_E2E_Utility();
	
	@Test
	public void m_PISP_GDSPS_003() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-2] : Get Domestic scheduled-payments Submission ");	
		
		dspConsent.setBaseURL(apiConst.dsp_endpoint+"/"+API_Constant.getDsp_PaymentId());
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
		dspConsent.setMethod("GET");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic scheduled payments URI");
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data"))!=null,
				"Mandatory field Data is present in response and is not empty");
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Links"))!=null, 
				"Mandatory field Links is present in response and is not empty");
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Links.Self"))!=null, 
				"Mandatory field Self is present in response and is not empty");
		
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();		
	}
}
