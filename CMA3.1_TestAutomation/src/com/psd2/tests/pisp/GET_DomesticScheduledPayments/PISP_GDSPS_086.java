package com.psd2.tests.pisp.GET_DomesticScheduledPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of response for the Domestic Scheduled payments API when the Amount starts with "700" in the POST resquest payload 
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSP"})
public class PISP_GDSPS_086 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPS_086() throws Throwable{
		dspConsent.setAmount("700.00");
		TestLogger.logStep("[Step 1] : Generate Payment Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, true);
		TestLogger.logBlankLine();
		 
		TestLogger.logStep("[Step 2] : Get Domestic Schedule Payment ");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint+"/"+consentDetails.get("paymentId"));
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dspConsent.setMethod("GET");
		dspConsent.submit();
			
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"500", 
			"Response Code is correct for POST Domestic schedule Payment Submission");
		
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();
	}
}