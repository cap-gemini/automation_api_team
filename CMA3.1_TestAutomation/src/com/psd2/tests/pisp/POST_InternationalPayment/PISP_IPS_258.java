package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/CreditorAgent/PostalAddress block
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_258 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_258() throws Throwable{	
	
			
        TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for POST International Payment Submission");
		
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress")!=null, 
			"Optional field CreditorAgent PostalAddress is present under Initiation block");
	    
	    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType")!=null, 
				"Optional field AddressType is present under CreditorAgent PostalAddress block");
	    
	    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Department")!=null, 
				"Optional field Department is present underCreditorAgent PostalAddress block");
	    
	    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.SubDepartment")!=null, 
				"Optional field SubDepartment is present under CreditorAgent PostalAddress block");
	
	    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.StreetName")!=null, 
				"Optional field StreetName is present under CreditorAgent PostalAddress block");
	    
	    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.BuildingNumber")!=null, 
				"Optional field BuildingNumber is present under CreditorAgent PostalAddress block");
	    
	    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.PostCode")!=null, 
				"Optional field PostCode is present under CreditorAgent PostalAddress block");
	    
	    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.TownName")!=null, 
				"Optional field TownName is present under CreditorAgent PostalAddress block");
	
	    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.CountrySubDivision")!=null, 
				"Optional field CountrySubDivision is present underCreditorAgent PostalAddress block");
	    
	    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Country")!=null, 
				"Optional field Country is present under CreditorAgent PostalAddress block");
	    
	    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressLine")!=null, 
				"Optional field AddressLine is present underCreditorAgent PostalAddress block");
		
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();		
	}
}
