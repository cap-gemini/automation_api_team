package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the values into OPTIONAL ExchangeRateInformation block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_211 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IPS_211 () throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		
		internationalPayment.submit();
		
	    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
		"Response Code is correct for GET International Payment Submission");
	    
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation"))!=null,
				"ExchangeRateInformation Block under is present in Post International Payment Submission response body");
		
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.UnitCurrency"))!=null,
				"UnitCurrency Field under ExchangeRateInformation is present in Post International Payment Submission response body");
		
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExchangeRate"))!=null,
				"ExchangeRate Field under ExchangeRateInformation is present in Post International Payment Submission response body");
		
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType"))!=null,
				"RateType Field is present in Post International Payment Submission response body");
		
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ContractIdentification"))!=null,
				"ContractIdentification Field is present in Post International Payment Submission response body");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
		}
}	

