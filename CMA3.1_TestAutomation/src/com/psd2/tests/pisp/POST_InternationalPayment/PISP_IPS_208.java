package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Amount block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_208 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IPS_208() throws Throwable{	
	
			
		TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Internatinal Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment URI");
		
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Charges[0].Amount")!=null,
				"Amount block under Charges block is present and is not null");
		
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Charges[0].Amount.Amount")).isEmpty(),
				"Amount under Charges block consists of Amount field");
		
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Charges[0].Amount.Currency")).isEmpty(),
				"Amount under Charges block consists of Currency field");
			    
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}

