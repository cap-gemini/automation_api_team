package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation
 * block
 * 
 * @author Snehal Chaudhari
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_IPS_033 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_IPS_033() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST International Payment Submission");

		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();

		TestLogger.logStep("[Step 3] : Verification of the values into MANDATORY Initiation block where Request has sent successfully and returned a HTTP Code 201 Created");

		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.InstructionIdentification"))!=null, 
		"Mandatory field InstructionIdentification is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.InstructionIdentification"));
		
		  testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.EndToEndIdentification"))!=null, 
		"Mandatory field EndToEndIdentification is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.EndToEndIdentification"));
		  
			testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.LocalInstrument"))!=null,
		"Mandatory field LocalInstrument is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.LocalInstrument"));
			
			testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.InstructionPriority"))!=null, 
		"Mandatory field InstructionPriority is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.InstructionPriority"));
			
			testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Purpose"))!=null,
		"Optional field Purpose is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.Purpose"));
			
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.ChargeBearer"))!=null,
		"Optional field ChargeBearer is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.ChargeBearer"));
		
       testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CurrencyOfTransfer"))!=null,
		"Optional field CurrencyOfTransfer is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.CurrencyOfTransfer"));	
		

		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}
