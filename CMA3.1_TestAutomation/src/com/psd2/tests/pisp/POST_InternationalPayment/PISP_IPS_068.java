package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the MANDATORY InstructedAmount/Currency field haven't sent
 * @authorSnehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_068 extends TestBase{	
	
		API_E2E_Utility apiUtility = new API_E2E_Utility();
		
		@Test
		public void m_PISP_IPS_068() throws Throwable{	
			
		TestLogger.logStep("[Step 1] : Create International Payment Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		String _reqBody = internationalPayment.genRequestBody().replace(",\"Currency\": \"EUR\"","");
		internationalPayment.submit(_reqBody);
		
	    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International  Payment URI when mandatory Data/Initiation/InstructedAmount/Currency field haven't sent block has not been sent");
		
	    testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Missing", "Error Code are matched");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Missing required field [Currency]", "Error Message are matched");
					
		TestLogger.logBlankLine();
	    testVP.testResultFinalize();		
	}
}

