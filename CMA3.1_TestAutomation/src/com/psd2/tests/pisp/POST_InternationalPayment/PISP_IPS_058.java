package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of Amount field having both values in between 13 non-decimal and 5 decimals digits under InstructedAmount/Amount
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_058 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_058() throws Throwable{	
	
				
			TestLogger.logStep("[Step 1] : Create International Payment Consent");
	        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : POST International Payment Submission");	
			
			internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
			internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			internationalPayment.setConsentId(consentDetails.get("consentId"));
			internationalPayment.setAmount("300.12");
			internationalPayment.submit();
		
		    TestLogger.logStep("[Step 3] : Verification of the status when MANDATORY InstructedAmount/Amount field having both values in between 13 non-decimal and 5 decimals");
		    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International Payment URI when MANDATORY InstructedAmount/Amount field having both values in between 13 non-decimal and 5 decimals");
		    
		    testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.InstructedAmount.Amount").split("\\.")[0].length()<=13
					   && internationalPayment.getResponseNodeStringByPath("Data.Initiation.InstructedAmount.Amount").split("\\.")[1].length()<=5, 
					   "Amount field value is correct i.e. upto 13 digits and 5 precisions");
			
		  TestLogger.logBlankLine();
	      testVP.testResultFinalize();		
	}
}
