package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/LocalInstrument field having OB defined values
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_049 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_049() throws Throwable{	
	
			
		 TestLogger.logStep("[Step 1] : Create Internatioanl Payment Consent");
			
	        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : POST Internatioanl Payment Submission");
			
			internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
			internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			internationalPayment.setConsentId(consentDetails.get("consentId"));
			internationalPayment.submit();
		
		    TestLogger.logStep("[Step 3] : Verification of the values of MANDATORY Initiation/LocalInstrument field having length variation");
			
		    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International Payment URI");
		    
		    testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Link") 
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.FPS") 
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BACS")
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.CHAPS") 
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Paym") 
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BalanceTransfer")
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.MoneyTransfer")
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.SWIFT")
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Euro1")
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.SEPACreditTransfer")
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.SEPAInstantCreditTransfer")
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Target2"),
					"LocalInstrument field value is correct i.e. "+internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument"));
			
		    TestLogger.logBlankLine();
		    testVP.testResultFinalize();		
	}
}