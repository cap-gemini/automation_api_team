package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/InstructedAmount block must have Amount & Currency fields
 * @authorSnehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_056 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_056() throws Throwable{	
	
			
			TestLogger.logStep("[Step 1] : Create International Scheduled Payment");
			consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Data/Initiation/InstructedAmount block must have Amount & Currency fields");
			internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
			internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			internationalPayment.setConsentId(consentDetails.get("consentId"));
			internationalPayment.submit();
		
		    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International Payment Submission");
		    
		    testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.InstructedAmount"))!=null, 
					"InstructedAmount Block is present and is not null");
		    
			testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.InstructedAmount.Amount"))!=null, 
					"Amount field under InstructedAmount array is present");
			
			testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.InstructedAmount.Currency"))!=null, 
					"Currency field under InstructedAmount array is present");
			
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
