package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of NULL Identification field under CreditorAgent/Identification
 * haven't sent
 * 
 * @author Kiran Dewangan
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_IPS_135 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_IPS_135() throws Throwable {

		TestLogger.logStep("[Step 1] : Create Internatinal  Payment Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST International Payment Submission");
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.setCrAgentIdentification("");
		internationalPayment.submit();

		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400",
				"Response Code is correct for International Payment URI when CreditorAgent	 Identification is NULL");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Field.Invalid",
				"Error code for the response is correct i.e. '"+ internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+ "'");
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected min length 1 for field [Identification], but got []"),
				"Message for error code is '"+ internationalPayment.getResponseNodeStringByPath("Errors[0].Message")+ "'");

		String key = internationalPayment.getHeaderEntry("x-idempotency-key");
		TestLogger.logBlankLine();
		
         TestLogger.logStep("[Step 3] : Verification of Invalid Identification field under CreditorAgent/Identification");
         internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
         internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-idempotency-key:"+key);
         internationalPayment.setCrAgentIdentification("IBAN");
         internationalPayment.setConsentId(consentDetails.get("consentId"));
         internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for Inaternational Payment URI when internationalPayment/Identification is invalid");
		
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Resource.ConsentMismatch", 
				"Error code for the response is correct i.e. '"+internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Payload comparison failed with the consent resource"), 
				"Message for error code is '"+internationalPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 4] : Verification of the MANDATORY CreditorAgent/Identification field haven't sent");
        internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
        internationalPayment.setConsentId(consentDetails.get("consentId"));
        internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-idempotency-key:"+key);
		String requestBody=internationalPayment.genRequestBody().replace("\"Identification\": \""+internationalPayment._crAgentIdentification+"\",","");
		internationalPayment.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International  Payment URI when CreditorAgent/Identification is not sent");
		
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Resource.ConsentMismatch", 
				"Error code for the response is correct i.e. '"+internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Payload comparison failed with the consent resource"), 
				"Message for error code is '"+internationalPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
}
