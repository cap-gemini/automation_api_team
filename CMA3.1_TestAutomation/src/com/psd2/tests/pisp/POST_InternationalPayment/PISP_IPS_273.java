package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY MultiAuthorisation/Status where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_273 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IPS_273() throws Throwable{	
	
			
		TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment URI");
		
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.MultiAuthorisation.Status").equals("Authorised") 
				|| internationalPayment.getResponseValueByPath("Data.MultiAuthorisation.Status").equals("AwaitingFurtherAuthorisation") 
				|| internationalPayment.getResponseValueByPath("Data.MultiAuthorisation.Status").equals("Rejected"),
				"Status under MultiAuthorisation block is correct i.e. "+internationalPayment.getResponseValueByPath("Data.MultiAuthorisation.Status"));
		
		TestLogger.logBlankLine();
        testVP.testResultFinalize();		
	}
}

