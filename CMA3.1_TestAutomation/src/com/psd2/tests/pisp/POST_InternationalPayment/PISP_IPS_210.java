package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Currency field where Request has sent successfully and returned a HTTP Code 201 Created
 * @authorSnehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_210 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IPS_210 () throws Throwable{	
	
			
		TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment Consent URI");
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.Charges[0].Amount.Currency")).length()==3, 
				"Amount under Charges block consists of Currency field which is equal to 3 characters");
			    
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}

