package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of OPTIONAL CreditorPostalAddress/AddressLine field
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_127 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_IPS_127() throws Throwable{	
		
        TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
	    internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
		"Response Code is correct for International Payment URL for AddressLine fields having length in between 1-70 characters");		

		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressLine")!=null, 
		"Optional field AddressLine is present under CreditorPostalAddress block");
		
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressLine[0]").toString().length()<=70)&&(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressLine").toString().length()>=1), 
		"Optional field AddressLine value is between 1 and 70");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();	
		}
}
