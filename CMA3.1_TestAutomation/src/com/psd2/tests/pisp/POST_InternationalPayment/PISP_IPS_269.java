package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/RemittanceInformation block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_269 extends TestBase {	
API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_268() throws Throwable{	
	
			
        TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for POST International Payment Submission");
		
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation")!=null, 
				"RemittanceInformation block is present under Initiation block");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.RemittanceInformation.Unstructured")).isEmpty(),
				"Unstructured field is present under RemittanceInformation block");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.RemittanceInformation.Reference")).isEmpty(),
				"Reference field is present under RemittanceInformation block");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
