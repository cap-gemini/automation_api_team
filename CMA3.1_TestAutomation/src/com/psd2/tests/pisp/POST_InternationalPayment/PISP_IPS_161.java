package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/RemittanceInformation block
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_161 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_IPS_161() throws Throwable{	
		
        TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
		"Response Code is correct for International Payment URL for AddressLine fields having length in between 1-70 characters");		
		
		 testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation.Unstructured")!=null, 
		 " Unstructured field is present under RemittanceInformation block");
		 
		 testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation.Reference")!=null, 
		 " Reference field is present under RemittanceInformation block");
		 
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation.Unstructured").toString().length()<=140)&&(internationalPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation.Unstructured").toString().length()>=1), 
		"Optional field Unstructured value is between 1 and 140");
					
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation.Reference").toString().length()<=35)&&(internationalPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation.Reference").toString().length()>=1), 
		"Optional field Reference value is between 1 and 35");
				
		  TestLogger.logBlankLine();
		 testVP.testResultFinalize();  
    }
}
