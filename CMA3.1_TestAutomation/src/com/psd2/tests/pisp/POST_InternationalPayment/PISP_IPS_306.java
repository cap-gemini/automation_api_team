package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/InstructedAmount/Amount field starts with 600 for Currency as EUR/GBP
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_306 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IPS_306 () throws Throwable{	
		internationalPayment.setAmount("600.12");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		apiUtility.pispAccessToken=true;
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201",
				"Response Code is correct for International Payment URI when InstructedAmount/Amount as 600");
		
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Data.Status"), "Pending",
				"Status is correct");

		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the values into Status field of consent API where Request has sent successfully and returned a HTTP Code 201 Created");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for International Payment Orders URL");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Status").equals("Consumed"),
				"Status field value is correct i.e. "+internationalPayment.getResponseNodeStringByPath("Data.Status"));
			
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
