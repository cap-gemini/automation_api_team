package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with invalid value of scope for the flow into Authorization (Access Token) header
 * @author Mohit Patidar
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_IPS_019 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_IPS_019() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate Access Token");
		consentDetails=apiUtility.generateAISPConsent(null,false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with invalid value of scope for the flow into Authorization (Access Token) header");
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();

		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"403", 
				"Response Code is correct for International Standing Orders Submission URI when value of scope is other than payments");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
