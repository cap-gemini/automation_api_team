package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the value of OPTIONAL ExchangeRateInformation Block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_228 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IPS_228() throws Throwable{	
	
			
		TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
		
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
    "Response Code is correct for Post international Payment Submission");
	
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation"))!=null, 
	"Mandatory Block ExchangeRateInformation is present"+internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation"));
	
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency"))!=null, 
	"Mandatory field UnitCurrency is present in ExchangeRateInformation block "+ internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency"));
	
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ExchangeRate"))!=null, 
	"Mandatory field ExchangeRate is present in ExchangeRateInformation block "+internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ExchangeRate"));
	
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.RateType"))!=null, 
	"Optional field RateType is present in ExchangeRateInformation block "+internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.RateType"));
	
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ContractIdentification"))!=null, 
	"Optional field ContractIdentification is present in ExchangeRateInformation block "+internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ContractIdentification"));
	
	TestLogger.logBlankLine();
	testVP.testResultFinalize();

}
}	

