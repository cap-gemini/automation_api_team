package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_194 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_IPS_194() throws Throwable{	
		
        TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
        internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment URI");
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.InternationalPaymentId"))!=null, 
				"Mandatory field InternationalPaymentId is present and not empty");
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.ConsentId"))!=null, 
				"Mandatory field ConsentId is present and not empty");
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.CreationDateTime"))!=null, 
				"Mandatory field CreationDateTime is present and not empty");
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.Status"))!=null, 
				"Mandatory field Status is present and not empty");
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.StatusUpdateDateTime"))!=null,  
				"Mandatory field StatusUpdateDateTime is present and not empty");
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.ExpectedExecutionDateTime"))!=null, 
				"Mandatory field ExpectedExecutionDateTime is present and not empty");
		
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.ExpectedSettlementDateTime"))!=null,  
				"Mandatory field ExpectedSettlementDateTime is present and not empty");
				
        TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
		
	}
	}

