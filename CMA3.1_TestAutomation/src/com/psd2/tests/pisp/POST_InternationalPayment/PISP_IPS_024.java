package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request where MANDATORY x-jws-signature header haven't sent
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_024 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_024() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create International Payment Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		restRequest.setURL(apiConst.iPaymentSubmission_endpoint);
		String requestBody= internationalPayment.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", Content-Type:application/json, Accept:application/json, x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for International Payments");
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Missing", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].Message")),"Required header x-jws-signature not specified","Error message is correct");
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
