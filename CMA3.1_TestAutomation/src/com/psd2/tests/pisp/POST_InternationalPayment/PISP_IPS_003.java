package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through POST Method with mandatory and optional fields.. 
 * @author :Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_003 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IPS_003() throws Throwable{	
	TestLogger.logStep("[Step 1] : Generate Consent Id");
	consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, false, false);
	TestLogger.logBlankLine();	
		
    TestLogger.logStep("[Step 2] : POST International Payments");	
	internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
	internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
	internationalPayment.setConsentId(consentDetails.get("consentId"));
	internationalPayment.submit();
		
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()), "201",
		"Response Code is correct for POST International Payment Submission");
	
	testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data") != null,
			"Mandatory field Data is present in response and is not empty");
	
	testVP.verifyTrue(internationalPayment.getResponseValueByPath("Links") != null,
		"Mandatory field Links is present in response and is not empty");
	
	testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Links.Self")).isEmpty(),
		"Mandatory field Self is present in response and is not empty");
	
	testVP.verifyStringEquals(internationalPayment.getResponseHeader("Content-Type"),
		"application/json;charset=utf-8","Response is UTF-8 character encoded");	
	
	TestLogger.logBlankLine();	
	testVP.testResultFinalize();
	}
}
