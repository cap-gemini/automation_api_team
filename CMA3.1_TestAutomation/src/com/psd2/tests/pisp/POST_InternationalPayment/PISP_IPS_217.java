package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the values into Mandatory Data/Initiation block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_217 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IPS_217() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
		
	    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
		"Response Code is correct for Post International Payment Submission");
	    
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.InstructionIdentification"))!=null,
				"InstructionIdentification Block under is present in Post International Payment Submission response body");
		
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.EndToEndIdentification"))!=null,
				"EndToEndIdentification Field under ExchangeRateInformation is present in Post International Payment Submission response body");
		
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.LocalInstrument."))!=null,
				"LocalInstrument. Field under ExchangeRateInformation is present in Post International Payment Submission response body");
		
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.InstructionPriority"))!=null,
				"InstructionPriority Field is present in Post International Payment Submission response body");
		
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Purpose"))!=null,
				"Purpose Field is present in Post International Payment Submission response body");
		
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.ChargeBearer"))!=null,
				"ChargeBearer Field is present in Post International Payment Submission response body");
		
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CurrencyOfTransfer"))!=null,
				"CurrencyOfTransfer Field is present in Post International Payment Submission response body");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();

}
}	

