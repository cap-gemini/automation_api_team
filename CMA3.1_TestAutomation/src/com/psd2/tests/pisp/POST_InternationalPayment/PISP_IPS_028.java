package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without OR invalid value of Content-Type header
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_028 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_028() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 2] : International Payment Submission without or invalid Content-Type header");
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		restRequest.setURL(apiConst.iPaymentSubmission_endpoint);
		String requestBody= internationalPayment.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+cc_token+", Accept:application/json, x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3)+", x-jws-signature:"+SignatureUtility.generateSignature(requestBody));
		restRequest.addHeaderEntry("Content-Type", "application/xml");
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"415", 
				"Response Code is correct without Content-Type header");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
