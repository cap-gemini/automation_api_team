package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of MANDATORY Initiation/EndToEndIdentification field in between 1-35 characters
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_040 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_040() throws Throwable{	
	
			
		    TestLogger.logStep("[Step 1] : Create International Payment Consent");
			
	        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : POST International Payment Submission");	
			
			internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
			internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		    internationalPayment.setConsentId(consentDetails.get("consentId"));
			internationalPayment.submit();
		
		    TestLogger.logStep("[Step 3] : Verification of the values of MANDATORY Initiation/EndToEndIdentification field");
				
		    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment URL for EndToEndIdentification fields which is equal to 35 characters");
		
		    testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.EndToEndIdentification")).length()<=35,
					"EndToEndIdentification field length up to 35 characters");
		    
		    TestLogger.logBlankLine();
		
		    testVP.testResultFinalize();		
	}
}
