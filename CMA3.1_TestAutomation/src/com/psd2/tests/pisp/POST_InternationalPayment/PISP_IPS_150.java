package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of NULL value of Optional CreditorAgent/PostalAddress/BuildingNumber having length variation
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_150 extends TestBase {	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_150() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create International Payment Consent");

		consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();

        TestLogger.logStep("[Step 2] : Verification of the value of OPTIONAL CreditorAccount/Name field");
		
        internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
        internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
        internationalPayment.setCrAgentBuildingNumber("ABCDEFGHIJ123456123");
        internationalPayment.setConsentId(consentDetails.get("consentId"));
        internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Payment URI when BuildingNumber Field having length in between 1 to 16 characters");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 16 for field [BuildingNumber], but got [ABCDEFGHIJ123456123]"), 
				"Message for error code is '"+internationalPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
	
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
