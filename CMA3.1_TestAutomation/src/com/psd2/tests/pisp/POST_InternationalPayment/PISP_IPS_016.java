package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without token value OR
 * key-value pair into Authorization (Authorization Code) header
 * 
 * @author Snehal Chaudhari
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_IPS_016 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_IPS_016() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST International Payment Submission without token value");
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer ");
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();

		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"401",
				"Response Code is correct for International Payment Submission when request without token value");

		TestLogger.logStep("[Step 3] : POST International Payment Submission without Authorization");
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		restRequest.setURL(apiConst.iPaymentSubmission_endpoint);
		String requestBody= internationalPayment.genRequestBody();
		restRequest.setHeadersString("Accept:application/json, x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3)+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", 
				"Response Code is correct for without x-idempotency-key key-value pair into header for International Payment Submission");
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Missing", "Error Code are matched");
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].Message"), "Required header x-idempotency-key not specified", "Error Message are matched"); 
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
