package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL CreditorAgent/PostalAddress/AddressType Field..
 * @author Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_141 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_141 () throws Throwable{	
	
			
        TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for POST International Payment Submission");
		
	   testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("Business")||
	    internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("Correspondence")||
	    internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("DeliveryTo")||
	    internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("MailTo")||
	    internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("POBox")||
	    internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("Postal")||
	    internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("Residential")||
	    internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").equals("Statement"), 
        "AddressType field value is correct i.e. "+internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType"));
	    
	    TestLogger.logBlankLine();	
		testVP.testResultFinalize();		
	}
}
