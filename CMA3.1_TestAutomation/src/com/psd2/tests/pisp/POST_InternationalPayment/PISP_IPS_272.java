package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/MultiAuthorisation block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_272 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IPS_272() throws Throwable{	
	
			
		TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment Consent URI");
		
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.MultiAuthorisation")!=null,
				"Optional field Authorisation is present and is not null");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.MultiAuthorisation.Status")!=null,
				"Mandatory field Status is present under MultiAuthorisation");
		
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.MultiAuthorisation.NumberRequired")).isEmpty(),
				"Optional field NumberRequired is present under Authorisation");
		
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.MultiAuthorisation.NumberReceived")).isEmpty(),
				"Optional field NumberReceived is present under Authorisation");
		
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.MultiAuthorisation.LastUpdateDateTime")).isEmpty(),
				"Optional field LastUpdatedDateTime is present under Authorisation");
		
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.MultiAuthorisation.ExpirationDateTime")).isEmpty(),
				"Optional field ExpirationDateTime is present under Authorisation");
		
		TestLogger.logBlankLine();
	    testVP.testResultFinalize();		
	}
}

