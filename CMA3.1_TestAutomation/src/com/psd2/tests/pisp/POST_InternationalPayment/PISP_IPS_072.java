package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of MANDATORY DebtorAccount/SchemeName field having OB defined values
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_072 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_072() throws Throwable{	
	
TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");
			
			internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
			internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			internationalPayment.setConsentId(consentDetails.get("consentId"));
			internationalPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International Payment URI");
			
			testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.SortCodeAccountNumber")
					|| internationalPayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.IBAN")
					|| internationalPayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.PAN")
					|| internationalPayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.Paym")
					|| internationalPayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.BBAN")
					|| internationalPayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme1")
					|| internationalPayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme2"), 
					"MANDATORY DebtorAccount/SchemeName field has OB defined values");
			
		TestLogger.logBlankLine();
	    testVP.testResultFinalize();		
	}
}
