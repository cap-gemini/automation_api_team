package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the value of Mandatory DebtorAccount/SchemeName Block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_234 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IPS_234() throws Throwable{ 
	 
	TestLogger.logStep("[Step 1] : Generate Payment Id");
	consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
	TestLogger.logBlankLine();
	  
	TestLogger.logStep("[Step 2] : Internationl Payment Submission....");

	internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
	internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
	internationalPayment.setConsentId(consentDetails.get("consentId"));
	internationalPayment.submit();
	  
	  testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
	    "Response Code is correct for International Payment Submission URI");  
	
	testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName"), internationalPayment._drAccountSchemeName, 
			"SchemeName Field Value In Resonse is same as that sent in request");
	  
	TestLogger.logBlankLine();
	testVP.testResultFinalize();
    }
}	

