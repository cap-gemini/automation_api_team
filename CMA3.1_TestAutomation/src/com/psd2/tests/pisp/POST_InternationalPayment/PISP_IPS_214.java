package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the value of MANDATORY RateType Field where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_214 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_214() throws Throwable{	
	
TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");
			
			internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
			internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			internationalPayment.setConsentId(consentDetails.get("consentId"));
			internationalPayment.submit();
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
    "Response Code is correct for Post international Payment Consents");
	
   testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType"))!= null,
   "RateType field under ExchangeRateInformation block is present in POST International Payment Consent response body"+(internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType")));
   testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType")).equals("Actual")||
   (internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType")).equals("Agreed")||
	(internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType")).equals("Indicative"), 
	 "Mandatory field RateType is present in ExchangeRateInformation block and is not null");
   TestLogger.logBlankLine();
   testVP.testResultFinalize();
    	}
}	

