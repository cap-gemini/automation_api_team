package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/DebtorAccount block
 * @authorSnehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_069 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_069() throws Throwable{	
			
		TestLogger.logStep("[Step 1] : Create International Payment Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Data/Initiation/DebtorAccount block");	
			internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
			internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			internationalPayment.setConsentId(consentDetails.get("consentId"));
			internationalPayment.submit();
			
		testVP.verifyStringEquals(internationalPayment.getResponseStatusCode(), "201", 
				"Response Code is correct for International Payment Submission");	
			
		testVP.verifyTrue((internationalPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount"))!=null, 
					"DebtorAccount block is present");
		
		testVP.verifyTrue((internationalPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName"))!=null, 
                 "SchemeAnme Field is present Under DebtorAccount Block and is not null");
		
		testVP.verifyTrue((internationalPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification"))!=null, 
                "Identification Field is present Under DebtorAccount Block and is not null");
		
		testVP.verifyTrue((internationalPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Name"))!=null, 
                "Name Field is present Under DebtorAccount Block and is not null");
		
		testVP.verifyTrue((internationalPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SecondaryIdentification"))!=null, 
                "SecondaryIdentification Field is present Under DebtorAccount Block and is not null");
				
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
