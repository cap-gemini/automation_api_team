package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of NULL value of Optional Data/Initiation/Creditor/name having length variation
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_107 extends TestBase {	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_107() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create International Payment Consent");

		consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();

        TestLogger.logStep("[Step 2] : Verification of the value of OPTIONAL CreditorAccount/Name field");
		
        internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
        internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
        internationalPayment.setCrName("CreditorTestabcdefghijklmnopqrstuvwxyxabcdefghijklmnopqrstuvwxyxabcdefghijklmnopqrstuvwxyxabcdefghijklmnopqrstuvwxyxabcdefghijklmnopqrstujvjhbjh");
        internationalPayment.setConsentId(consentDetails.get("consentId"));
        internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Payment URI when Name Field having length greater than 140 characters");
		
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Expected max length 140 for field [Name], but got [CreditorTestabcdefghijklmnopqrstuvwxyxabcdefghijklmnopqrstuvwxyxabcdefghijklmnopqrstuvwxyxabcdefghijklmnopqrstuvwxyxabcdefghijklmnopqrstujvjhbjh]", "Error Message are matched");
		
		TestLogger.logBlankLine();
	   testVP.testResultFinalize();
		
	  }
}
