package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of Amount field having value up to 13 non-decimal and more than 5 decimals digits under InstructedAmount/Amount
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_061 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_061() throws Throwable{	
	
			
         TestLogger.logStep("[Step 1] : Create International  Payment Consent");
		
	        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : POST International Payment Submission");	
			
			internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
			internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			internationalPayment.setConsentId(consentDetails.get("consentId"));
			internationalPayment.setAmount("3123456789012.12345111");
			internationalPayment.submit();
		
		    TestLogger.logStep("[Step 3] : Verification of the status when MANDATORY InstructedAmount/Amount field having value up to 13 non-decimal and more than 5 decimals digits");
				
		    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
					"Response Code is correct for International Payment URI when MANDATORY InstructedAmount/Amount field having value up to 13 non-decimal and more than 5 decimals digits");
		    testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
			testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Invalid value '3123456789012.12345111'. Expected ^\\d{1,13}\\.\\d{1,5}$ for Amount", "Error Message are matched");
			
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
