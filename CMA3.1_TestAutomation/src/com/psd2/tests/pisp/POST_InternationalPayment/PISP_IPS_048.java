package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/LocalInstrument field having length greater than 35 characters
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_048 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_048() throws Throwable{	
	
			
TestLogger.logStep("[Step 1] : Create Domestic Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");
			
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setLocalInstrument("AABIFIAIAIAIAIAINBDSSSAAQQQQQQQQDSAAAAA");
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
		
		TestLogger.logStep("[Step 3] : Verification of the values of MANDATORY Initiation/LocalInstrument field having length variation");
				
	    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
			"Response Code is correct for International Payment URL for LocalInstrument fields which is greater than 35 char");
	
	    testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Expected max length 35 for field [LocalInstrument], but got [AABIFIAIAIAIAIAINBDSSSAAQQQQQQQQDSAAAAA]", "Error Message are matched");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
