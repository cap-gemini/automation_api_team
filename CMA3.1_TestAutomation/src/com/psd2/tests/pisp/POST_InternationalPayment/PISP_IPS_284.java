package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of response for the consent API when the RateType is provided as "Actual" in the resquest payload
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_284 extends TestBase {	
	
	@Test
	public void m_PISP_IPS_284() throws Throwable{	
		
        TestLogger.logStep("[Step 1-1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200","Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
	    TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-2] : International Payment Consent SetUp....");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setRateType("Actual");
		 
		String requestBody= internationalPayment.genRequestBody().replace(",\"ExchangeRate\": "+internationalPayment._exchangeRate+",\"ContractIdentification\": \""+internationalPayment._contractIdentification+"\"", "");
		internationalPayment.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", "Response Code is correct for International Payment Consent");
		consentId = internationalPayment.getConsentId();
		TestLogger.logVariable("Consent Id : " + consentId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-3] : JWT Token Creation........");
		reqObject.setValueField(consentId);
		reqObject.setScopeField("payments");
		outId = reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-4] : Go to URL and authenticate consent");	
		redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		authCode = consentOps.authorisePISPConsent(redirecturl+"##"+internationalPayment._drAccountIdentification,internationalPayment.addDebtorAccount);		
		closeDriverInstance();
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 1-5] : Get access token");	
		accesstoken.setBaseURL(apiConst.at_endpoint);
		accesstoken.setAuthCode(authCode);
		accesstoken.submit();
		
		testVP.verifyStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
				"Response Code is correct for get access token request");	
		access_token = accesstoken.getAccessToken();
		TestLogger.logVariable("Access Token : " + access_token);		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-6] : International Payment ");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+access_token);
		internationalPayment.setConsentId(consentId);
		requestBody= internationalPayment.genRequestBody().replace(",\"ExchangeRate\": "+internationalPayment._exchangeRate+",\"ContractIdentification\": \""+internationalPayment._contractIdentification+"\"", "");
		internationalPayment.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for internationalPayment Consent URI with Actual Rate Type");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExpirationDateTime")!=null, 
				"Response Code is correct for internationalPayment Consent with Actual Rate Type"+internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExpirationDateTime"));
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();			
	}
}