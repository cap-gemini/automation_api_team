package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request without x-idempotency-key value
 * OR key-value pair in the header
 * 
 * @author Snehal Chaudhari
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_IPS_006 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();
	String requestBody;
	@Test
	public void m_PISP_IPS_006() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : International Payment Submission ");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		restRequest.setURL(apiConst.iPaymentSubmission_endpoint);
		requestBody= internationalPayment.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", Accept:application/json, x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for without x-idempotency-key key-value pair into header for International Payment Submission");
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Missing", "Error Code are matched");
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].Message"), "Required header x-idempotency-key not specified", "Error Message are matched"); 
		
		TestLogger.logStep("[Step 3] : Interntional Payment Submission ");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		restRequest.setURL(apiConst.iPaymentSubmission_endpoint);
		requestBody= internationalPayment.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", Accept:application/json, x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("x-idempotency-key","");
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for without x-idempotency-key key-value pair into header for International Payment Submission");
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].Message"), "Invalid value '' for header x-idempotency-key. Invalid value ''. Expected ^(?!\\s)(.*)(\\S)$", "Error Message are matched"); 
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();
	}
}