package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of CreditorAccount/SecondaryIdentification field
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_105 extends TestBase {	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_105() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create International Payment Consent");

		consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();

        TestLogger.logStep("[Step 2] : Verification of the value of OPTIONAL CreditorAccount/SecondaryIdentification field");
		
        internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
        internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
        internationalPayment.setConsentId(consentDetails.get("consentId"));
        internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment URI");
		
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SecondaryIdentification")).isEmpty(), 
				"Name field under CreditorAccount is present and is not null");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SecondaryIdentification").length()<=34, 
				"Name field under CreditorAccount is less than 34 characters");
		String key = internationalPayment.getHeaderEntry("x-idempotency-key");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of NULL value of MANDATORY CreditorAccount/SecondaryIdentification field");
		
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+key);
		internationalPayment.setCrAccountSrIdentification("");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Payment Consent URI when CreditorAccount SecondaryIdentification is NULL");
		
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected min length 1 for field [SecondaryIdentification], but got []"), 
				"Message for error code is '"+internationalPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of the value of MANDATORY CreditorAccount/SecondaryIdentification field having length variations");
		
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+key);
		internationalPayment.setCrAccountSrIdentification("ABCDEFGHIJKLMNOPQRSTUVWXYZ123456788768");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Payment Consent URI when CreditorAccount SecondaryIdentification is more than 34 characters");
		
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 34 for field [SecondaryIdentification], but got [ABCDEFGHIJKLMNOPQRSTUVWXYZ123456788768]"), 
				"Message for error code is '"+internationalPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
	    testVP.testResultFinalize();	
		
	}
}
