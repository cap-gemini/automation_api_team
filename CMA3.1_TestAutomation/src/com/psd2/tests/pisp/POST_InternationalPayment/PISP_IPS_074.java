package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of SchemeName field under DebtorAccount/SchemeName having length variation
 * 
 * @author Snehal Chaudhari
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_IPS_074 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_IPS_074() throws Throwable {

		TestLogger.logStep("[Step 1] : Create International Payment Consent");

		consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST International Payment Submission");

		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.setDrAccountSchemeName("UK.OBIE.IBAN12465234253816249274023482hdsghxdzjhvxzxk");
		internationalPayment.submit();

		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400",
				"Response Code is correct for International Payment URI when DebtorAccount SchemeName is more than 40 characters");

		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Field.Invalid",
				"Error code for the response is correct i.e. '"+ internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+ "'");

		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 40 for field [SchemeName], but got [UK.OBIE.IBAN12465234253816249274023482hdsghxdzjhvxzxk]"),"Message for error code is '"
						+ internationalPayment.getResponseNodeStringByPath("Errors[0].Message")+ "'");

		TestLogger.logBlankLine();
        testVP.testResultFinalize();
	}
}
