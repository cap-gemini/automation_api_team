package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/InstructedAmount/Amount field starts with 900 for Currency as EUR/GBP
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_307 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IPS_307() throws Throwable{	
		internationalPayment.setAmount("900.12");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		apiUtility.pispAccessToken=true;
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Payment Data/Initiation/InstructedAmount/Amount field starts with 900 for Currency as EUR/GBP");
		
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Rules.AfterCutOffDateTime", 
				"Error code for the response is correct i.e. '"+internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Errors[0].Message").equals("SandBox AfterCutOffDate Mocked Error"), 
				"Message for error code is '"+internationalPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		TestLogger.logBlankLine();		
		
		testVP.testResultFinalize();		
	}
}