package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the status when MANDATORY Data/Initiation/InstructedAmount block haven't sent
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_057 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_IPS_057() throws Throwable{	
	
			
			TestLogger.logStep("[Step 1] : Create International  Payment");
			consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : POST International Payment Submission");
			internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
			internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			internationalPayment.setConsentId(consentDetails.get("consentId"));
			String _reqBody = internationalPayment.genRequestBody().replace(",\"InstructedAmount\": { "+ "\"Amount\": \"300.12\","+ "\"Currency\": \"EUR\""+ "}","");
			internationalPayment.submit(_reqBody);
			
		    TestLogger.logStep("[Step 3] : Verification of the status when MANDATORY Data/Initiation/InstructedAmount block haven't sent");
		    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
					"Response Code is correct for International Payment URI when mandatory Data/Initiation/InstructedAmount block has not been sent");
			
		    testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Missing", "Error Code are matched");
			testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Missing required field [InstructedAmount]", "Error Message are matched");
			
		TestLogger.logBlankLine();
        testVP.testResultFinalize();		
	}
}
