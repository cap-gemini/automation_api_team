package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : All the above steps would remains same where different request payload has been sent into BODY with different x-idempotency-key by same TPP within 24 hrs.
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_012 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IPS_012() throws Throwable{	
		String idemPotencyKey=PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3);
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");

		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",x-idempotency-key:"+idemPotencyKey);
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();

		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment when x-idempotency-key value in header is "+idemPotencyKey);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of request with different x-idempotency-key i.e. "+idemPotencyKey+" and different payload");
		
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",x-idempotency-key:"+idemPotencyKey+"1213");
		internationalPayment.setCrAccountName("xyz");
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Payment when x-idempotency-key value is different and payload is different");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Resource.NotFound", 
				"Error code for the response is correct i.e. '"+internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Errors[0].Message").equals("invalid payment id format"), 
				"Message for error code is '"+internationalPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
			TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
