package com.psd2.tests.pisp.POST_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of OPTIONAL CreditorPostalAddress/AddressLine field having length variation
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPS_128 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_IPS_128() throws Throwable{	
		
        TestLogger.logStep("[Step 1] : Create International Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		internationalPayment.setCrAddressLine("jhgdfs,jzgfjhzsgfjsegfujehgfk js,hsjd,jfdsgfjsgfdkjghjkhajhdkjwshuhauh");
		internationalPayment.setConsentId(consentDetails.get("consentId"));
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
		"Response Code is correct for Domestic Payment URL for AddressLine fields having length in between 1-70 characters");		
				
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Resource.ConsentMismatch", "Error Code are matched");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"), "Payload comparison failed with the consent resource", "Error Message are matched");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();	
     }
}
