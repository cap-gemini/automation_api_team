package com.psd2.tests.pisp.GET_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into OPTIONAL MultiAuthorisation/ExpirationDateTime where Request has sent successfully 
 * @author Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSO"})
public class PISP_GDSO_070 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSO_070() throws Throwable{
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Get Domestic Standing Orders");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId());
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"200", 
				"Response Code is correct for Get Domestic Standing Order Submission");	
		testVP.verifyEquals(dStandingOrder.getURL(), apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId(), 
				"URL is correct and matching with Get Domestic Standing Order Submission URL");
		testVP.verifyTrue(String.valueOf(dStandingOrder.getResponseValueByPath("Data.MultiAuthorisation"))!=null,
				"OPTIONAL MultiAuthorisation block present in Get Domestic Standing Order Submission response body ");
		testVP.verifyTrue(String.valueOf(dStandingOrder.getResponseValueByPath("Data.MultiAuthorisation.ExpirationDateTime"))!=null,
				"MANDATORY ExpirationDateTime field present in Get Domestic Standing Order Submission response body ");
		testVP.verifyTrue(Misc.verifyDateTimeFormat(dStandingOrder.getResponseNodeStringByPath("Data.MultiAuthorisation.ExpirationDateTime").split("T")[0], "yyyy-MM-dd") && 
				(Misc.verifyDateTimeFormat(dStandingOrder.getResponseNodeStringByPath("Data.MultiAuthorisation.ExpirationDateTime").split("T")[1], "HH:mm:ss+00:00")), 
				"ExpirationDateTime is as per expected format");
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();		
	}
}
