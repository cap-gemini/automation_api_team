package com.psd2.tests.pisp.GET_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into MANDATORY StatusUpdateDateTime field where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSO"})
public class PISP_GDSO_032 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSO_032() throws Throwable{

		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Get Domestic Standing Orders");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId());
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"200", 
				"Response Code is correct for Get Domestic Standing Orders");	
		testVP.verifyEquals(dStandingOrder.getURL(), apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId(), 
				"URL is correct and matching with Get Domestic Standing Order Submission URL");
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.StatusUpdateDateTime")!=null, 
				 "StatusUpdateDateTime field is present under Data");	 
		testVP.verifyTrue(Misc.verifyDateTimeFormat(dStandingOrder.getResponseNodeStringByPath("Data.StatusUpdateDateTime").split("T")[0], "yyyy-MM-dd") && 
				(Misc.verifyDateTimeFormat(dStandingOrder.getResponseNodeStringByPath("Data.StatusUpdateDateTime").split("T")[1], "HH:mm:ss+00:00")), 
				"StatusUpdateDateTime is as per expected format");				
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
