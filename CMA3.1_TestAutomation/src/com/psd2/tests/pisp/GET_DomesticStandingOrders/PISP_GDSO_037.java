package com.psd2.tests.pisp.GET_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Amount field where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSO"})
public class PISP_GDSO_037 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSO_037() throws Throwable{
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Get Domestic Standing Orders");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId());
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"200", 
				"Response Code is correct for Get Domestic Standing Order Submission");	
		testVP.verifyEquals(dStandingOrder.getURL(), apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId(), 
				"URL is correct and matching with Get Domestic Standing Order Submission URL");
		testVP.verifyTrue(String.valueOf(dStandingOrder.getResponseValueByPath("Data.Charges[0].Amount.Amount"))!=null,
				"Amount field under Data is present in Get Domestic Standing Order Submission response body");
		testVP.verifyTrue((String.valueOf(dStandingOrder.getResponseValueByPath("Data.Charges[0].Amount.Amount")).split("\\.")[0].length()) <= 13 &&(String.valueOf(dStandingOrder.getResponseValueByPath("Data.Charges[0].Amount.Amount")).split("\\.")[1].length() <=5 )  
				,"Amount value is withing the range");
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();		
	}
}
