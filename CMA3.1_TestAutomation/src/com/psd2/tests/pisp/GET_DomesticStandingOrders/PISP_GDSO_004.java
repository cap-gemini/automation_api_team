package com.psd2.tests.pisp.GET_DomesticStandingOrders;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through GET method with mandatory and optional fields
 * 
 * @author : Soumya Banerjee
 *
 */

@Listeners({ TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSO"})
public class PISP_GDSO_004 extends TestBase {
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDSO_004() throws Throwable {

		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Get Domestic Standing Orders");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId());
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()), "200",
				"Response Code is correct for Domestic Scheduled Payment Consent URI");
		testVP.verifyEquals(dStandingOrder.getURL(), apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId(), 
				"URL is correct and matching with Get Domestic Standing Order Submission URL");
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data")) != null,
				"Mandatory field Data is present in response and is not empty");
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Charges")) != null,
				"Optional field Charges is present in response and is not empty");
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation")) != null,
				"Mandatory field Initiation is present in response and is not empty");
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.MultiAuthorisation")) != null,
				"Optional field MultiAuthorisation is present in response and is not empty");
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Links")) != null,
				"Mandatory field Links is present in response and is not empty");
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Links.Self")) != null,
				"Mandatory field Self is present in response and is not empty");
		testVP.verifyStringEquals(dStandingOrder.getResponseHeader("Content-Type"), "application/json;charset=UTF-8", 
				"Response is UTF-8 character encoded");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}

