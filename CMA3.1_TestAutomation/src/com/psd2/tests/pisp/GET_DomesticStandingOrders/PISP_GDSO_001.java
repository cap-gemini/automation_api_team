package com.psd2.tests.pisp.GET_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of domestic-standing-order URL that should be as per Open Banking standards 
 * @author Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity","DSO"})
public class PISP_GDSO_001 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSO_001() throws Throwable{
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, false, true);
        API_Constant.setDso_PaymentId(consentDetails.get("paymentId"));
        TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Get Domestic Standing Orders");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint+"/"+consentDetails.get("paymentId"));
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"200", 
				"Response Code is correct for Get Domestic Standing Order Submission URL that is as per Open Banking standards");	
		testVP.verifyEquals(dStandingOrder.getURL(), apiConst.dsoSubmission_endpoint+"/"+consentDetails.get("paymentId"), 
				"URL is correct and matching with Get Domestic Standing Order Submission URL");
		
		testVP.verifyTrue(SignatureUtility.verifySignature(dStandingOrder.getResponseString(), dStandingOrder.getResponseHeader("x-jws-signature")), 
				"Response that created successfully with HTTP Code 201 MUST be digitally signed");
		
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();		
	}
}