package com.psd2.tests.pisp.GET_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request having invalid value into OPTIONAL Accept header 
 * @author Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSO"})
public class PISP_GDSO_022 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSO_022() throws Throwable{

		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Get Domestic Standing Orders");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId());
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.addHeaderEntry("Accept", "asdgwe43948");
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"406", 
				"Response Code is correct for Domestic Standing Orders Submission URI when optional Accept header is passed with an INVALID value");	
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
