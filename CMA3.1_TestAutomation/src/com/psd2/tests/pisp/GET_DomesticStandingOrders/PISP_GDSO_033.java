package com.psd2.tests.pisp.GET_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Charges block where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSO"})
public class PISP_GDSO_033 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSO_033() throws Throwable{

		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Get Domestic Standing Orders");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId());
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"200", 
				"Response Code is correct for Get Domestic Standing Orders");
		testVP.verifyEquals(dStandingOrder.getURL(), apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId(), 
				"URL is correct and matching with Get Domestic Standing Order Submission URL");
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Charges")!=null, 
				 "Charges field is present under Data");
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Charges.ChargeBearer")!=null, 
				 "ChargeBearer field is present under Charges");
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Charges.Type")!=null, 
				 "Type field is present under Charges");
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Charges.Amount")!=null, 
				 "Amount field is present under Charges");	
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Charges.Amount.Amount")!=null, 
				 "Amount field is present under Amount");
		testVP.verifyTrue(dStandingOrder.getResponseValueByPath("Data.Charges.Amount.Currency")!=null, 
				 "Currency field is present under Amount");	
		TestLogger.logBlankLine();
		testVP.testResultFinalize();	
	}
}
