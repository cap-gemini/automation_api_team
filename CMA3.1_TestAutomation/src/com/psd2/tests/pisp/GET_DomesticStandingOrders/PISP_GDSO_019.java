package com.psd2.tests.pisp.GET_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with Invalid value of OPTIONAL x-fapi-customer-ip-address header 
 * @author Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSO"})
public class PISP_GDSO_019 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSO_019() throws Throwable{
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Get Domestic Standing Orders");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId());
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.addHeaderEntry("x-fapi-customer-ip-address", "UUXGUISGC^%");
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
			"Response Code is correct for Domestic Standing Orders Submission URI when invalid x-fapi-customer-ip-address is passed");
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
			"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseValueByPath("Errors[0].Message")),"Invalid value found in x-fapi-customer-ip-address header","Error message is correct");		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();	
	}
}
