package com.psd2.tests.pisp.GET_DomesticStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY DomesticStandingOrderId field where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DSO"})
public class PISP_GDSO_028 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSO_028() throws Throwable{

		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		        
		TestLogger.logStep("[Step 2] : Get Domestic Standing Orders");	
		dStandingOrder.setBaseURL(apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId());
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"200", 
				"Response Code is correct for domestic Standing Orders submission URI");		
		testVP.verifyStringEquals(dStandingOrder.getURL(),apiConst.dsoSubmission_endpoint+"/"+API_Constant.getDso_PaymentId(), 
				"URI for GET domestic Standing Orders submission  is as per open banking standard");			
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.DomesticStandingOrderId"))!=null,
				"DomesticStandingOrderId field under Data is present in Get domestic Standing Orders submission response body"+String.valueOf(dStandingOrder.getResponseValueByPath("Data.DomesticStandingOrderId")));			
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.DomesticStandingOrderId").toString().length()<=128)&&(dStandingOrder.getResponseValueByPath("Data.DomesticStandingOrderId").toString().length()>=1), 
				"DomesticStandingOrderId field value is between 1 and 128");					
		TestLogger.logBlankLine();
		testVP.testResultFinalize();	
	}
}
