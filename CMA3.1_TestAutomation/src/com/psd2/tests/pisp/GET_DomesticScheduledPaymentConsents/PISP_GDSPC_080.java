package com.psd2.tests.pisp.GET_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Risk/DeliveryAddress block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
 public class PISP_GDSPC_080 extends TestBase{	
	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPC_080() throws Throwable{	
		
	TestLogger.logStep("[Step 1] : Generate Consent");
    consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, true, false);
	TestLogger.logBlankLine();
			
	TestLogger.logStep("[Step 2] : GET Domestic schedule payment consent....");
	dspConsent.setBaseURL(apiConst.dspConsent_endpoint+"/"+consentDetails.get("consentId"));
	dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
	dspConsent.setMethod("GET");
	dspConsent.submit();
	testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
    "Response Code is correct for Get Domestic Scheduled Payment Consent");	

	testVP.verifyTrue((dspConsent.getResponseValueByPath("Risk.DeliveryAddress"))!=null,
		"DeliveryAddress block is present");
     testVP.verifyTrue((dspConsent.getResponseValueByPath("Risk.DeliveryAddress.TownName"))!=null, 
		"Mandatory field TownName is present in DeliveryAddress block");
	testVP.verifyTrue((dspConsent.getResponseValueByPath("Risk.DeliveryAddress.Country"))!=null, 
		"Mandatory field Country is present in DeliveryAddress block");
	testVP.verifyTrue((dspConsent.getResponseValueByPath("Risk.DeliveryAddress.AddressLine"))!=null,
		"Optional field AddressLine is present in Risk block");
	testVP.verifyTrue((dspConsent.getResponseValueByPath("Risk.DeliveryAddress.StreetName"))!=null, 
	  "Optional field Country is present in DeliveryAddress block");
	testVP.verifyTrue((dspConsent.getResponseValueByPath("Risk.DeliveryAddress.BuildingNumber"))!=null, 
	 "Optional field BuildingNumber is present in DeliveryAddress block");
	testVP.verifyTrue((dspConsent.getResponseValueByPath("Risk.DeliveryAddress.PostCode"))!=null, 
		"Optional field PostCode is present in DeliveryAddress block");
	/*testVP.verifyTrue((dspConsent.getResponseValueByPath("Risk.DeliveryAddress.CountrySubDivision"))!=null, 
	   "Optional field CountrySubDivision is present in DeliveryAddress block");*/
	TestLogger.logBlankLine();
	testVP.testResultFinalize();
			
	}
	
}
