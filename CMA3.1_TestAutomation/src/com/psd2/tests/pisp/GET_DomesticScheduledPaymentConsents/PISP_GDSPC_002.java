package com.psd2.tests.pisp.GET_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of domestic-scheduled payment-consents URL that is NOT as per Open Banking standards
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSPC_002 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPC_002() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.domesticScheduledPayments, true, false);
		TestLogger.logBlankLine();								
		TestLogger.logStep("[Step 2] : GET Domestic Scheduled payment consent....");
		dspConsent.setBaseURL(apiConst.invalid_dspc_endpoint); 
		dspConsent.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
		dspConsent.setMethod("GET");
		dspConsent.submit();		
		    testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"404", 
		    		"Response Code 404 is correct when Get Domestic Scheduled Payment Consent URL is NOT as per Open Banking standards");
		    TestLogger.logBlankLine();
			testVP.testResultFinalize();		
}
}




