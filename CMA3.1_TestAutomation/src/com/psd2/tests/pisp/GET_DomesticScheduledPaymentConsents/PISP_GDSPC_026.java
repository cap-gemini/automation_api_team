package com.psd2.tests.pisp.GET_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY ConsentId field where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSPC_026 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPC_026() throws Throwable{						
		 TestLogger.logStep("[Step 1] : Generate Consent Id");
			consentDetails = apiUtility.generatePayments(false,apiConst.domesticScheduledPayments, true, false);
			TestLogger.logBlankLine();							
			consentId = dspConsent.getConsentId();
			TestLogger.logVariable("Consent Id : " + consentId);			
			TestLogger.logStep("[Step 2] : GET Domestic Scheduled payment consent....");		
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint + "/"+ consentDetails.get("consentId"));
			dspConsent.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
			dspConsent.setMethod("GET");
			dspConsent.submit();			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");		
			testVP.verifyStringEquals(dspConsent.getURL(),apiConst.dspConsent_endpoint+"/"+consentDetails.get("consentId"), 
					"URI for GET domestic Scheduled payment consent request is as per open banking standard");			
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.ConsentId"))!=null,
					"ConsentId field under Data is present in Get Domestic Scheduled Payment Consent response body"+String.valueOf(dspConsent.getResponseValueByPath("Data.ConsentId")));			
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.ConsentId").toString().length()<=128)&&(dspConsent.getResponseValueByPath("Data.ConsentId").toString().length()>=1), 
					"ConsentId field value is between 1 and 128");					
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
}
}
