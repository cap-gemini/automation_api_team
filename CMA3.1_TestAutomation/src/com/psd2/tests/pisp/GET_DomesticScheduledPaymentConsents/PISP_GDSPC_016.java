package com.psd2.tests.pisp.GET_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Comparison of x-fapi-interaction-id value sent in the request with the response header that created successfully with HTTP Code 200 OK
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSPC_016 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPC_016() throws Throwable{			
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.domesticScheduledPayments, true, false);
		TestLogger.logBlankLine();
		TestLogger.logStep("[Step 2] : GET Domestic Scheduled Payment Consent SetUp....");
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint + "/"+ consentDetails.get("consentId"));
		dspConsent.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
		dspConsent.addHeaderEntry("x-fapi-interaction-id", PropertyUtils.getProperty("inter_id"));
		dspConsent.setMethod("GET");
		dspConsent.submit();	
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
					"Response Code is correct for Domestic Scheduled Payment SetUp URI");
			testVP.verifyStringEquals(dspConsent.getResponseHeader("x-fapi-interaction-id"),PropertyUtils.getProperty("inter_id"), 
					"x-fapi-interaction-id value in the response header is correct and matching with one sent in the request");
			
			TestLogger.logStep("[Step 3] : GET Domestic Scheduled Payment Consent SetUp....");
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint + "/"+ consentDetails.get("consentId"));
			dspConsent.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
			dspConsent.addHeaderEntry("x-fapi-interaction-id", "");
			dspConsent.setMethod("GET");
			dspConsent.submit();		
				testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
						"Response Code is correct for Domestic Scheduled Payment Consent URI when x-fapi-interaction-id value is not sent in request");		
				TestLogger.logBlankLine();
				testVP.testResultFinalize();
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
}
}
