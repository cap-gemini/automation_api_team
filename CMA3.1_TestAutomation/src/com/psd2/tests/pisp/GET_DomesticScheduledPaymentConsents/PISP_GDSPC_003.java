package com.psd2.tests.pisp.GET_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of Domestic Scheduled Payment Consent URL that is should as per Open Banking standards with different PISP/Other Url
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSPC_003 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPC_003() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.domesticScheduledPayments, true, false);
		TestLogger.logBlankLine();
								
		TestLogger.logStep("[Step 2] : GET Domestic Scheduled payment consent....");
		
		dspConsent.setBaseURL(apiConst.otherpispapi_dso_endpoint+ "/"+ consentDetails.get("consentId"));
		dspConsent.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
		dspConsent.setMethod("GET");
		dspConsent.submit();
		
			
		 
		    
		    testVP.verifyStringEquals(dspConsent.getURL(),apiConst.otherpispapi_dso_endpoint+ "/"+ consentDetails.get("consentId"),
                    "URL for GET Domestic Scheduled Payment Consent is as per open banking standard");
		    testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()), "400",
                    "Response Code is correct for GET Domestic Scheduled Payment Consent");
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"),
                    "UK.OBIE.Resource.NotFound", "Error Code are matched");
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].Message"),
                    "submission id is missing","Error Message are matched");
		    		    
		    TestLogger.logBlankLine();		
			testVP.testResultFinalize();
				
}
}




