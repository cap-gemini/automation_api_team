package com.psd2.tests.pisp.GET_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/RemittanceInformation block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
 public class PISP_GDSPC_070 extends TestBase{	
	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPC_070() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, true, false);
		TestLogger.logBlankLine();
			
		TestLogger.logStep("[Step 2] : GET Domestic schedule payment consent....");
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint+"/"+consentDetails.get("consentId"));
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dspConsent.setMethod("GET");
		dspConsent.submit();
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
	    "Response Code is correct for Get Domestic Scheduled Payment Consent");	
		
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation.Unstructured"))!=null, 
			"Mandatory field Unstructured is present in RemittanceInformation block");
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation.Reference"))!=null, 
				"Mandatory field Reference is present in RemittanceInformation block");
		testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Initiation.RemittanceInformation.Unstructured").length()<=140, 
				"Unstructured field length is less than or equal to 140 characters");
		testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Initiation.RemittanceInformation.Reference").length()<=35, 
				"Reference field length is less than or equal to 35 characters");
		TestLogger.logBlankLine();
        testVP.testResultFinalize();
}
	
}
