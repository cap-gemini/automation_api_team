package com.psd2.tests.pisp.GET_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Amount field where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSPC_038 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPC_038() throws Throwable{		
		 TestLogger.logStep("[Step 1] : Generate Consent Id");
			consentDetails = apiUtility.generatePayments(false,apiConst.domesticScheduledPayments, true, false);
			TestLogger.logBlankLine();							
			TestLogger.logStep("[Step 2] : GET Domestic Scheduled payment consent....");
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint + "/"+ consentDetails.get("consentId"));
			dspConsent.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
			dspConsent.setMethod("GET");
			dspConsent.submit();
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");	
			testVP.verifyTrue((String.valueOf(dspConsent.getResponseValueByPath("Data.Charges[0].Amount.Amount")).split("\\.")[0].length()) <= 13 &&(String.valueOf(dspConsent.getResponseValueByPath("Data.Charges[0].Amount.Amount")).split("\\.")[1].length() <=5 )  
					,"Amount value is withing the range");
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
}
}
