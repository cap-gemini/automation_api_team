package com.psd2.tests.pisp.GET_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into StatusUpdateDateTime field where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSPC_029 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPC_029() throws Throwable{				
		 TestLogger.logStep("[Step 1] : Generate Consent Id");
			consentDetails = apiUtility.generatePayments(false,apiConst.domesticScheduledPayments, true, false);
			TestLogger.logBlankLine();						
			consentId = dspConsent.getConsentId();
			TestLogger.logVariable("Consent Id : " + consentId);		
			TestLogger.logStep("[Step 2] : GET Domestic Scheduled payment consent....");	
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint + "/"+ consentDetails.get("consentId"));
			dspConsent.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
			dspConsent.setMethod("GET");
			dspConsent.submit();		
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
					"Response Code is correct for Get Domestic Scheduled Payment Consent");			
			testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.StatusUpdateDateTime")!=null, 
					 "Data field is present under StatusUpdateDateTime field");	 
			testVP.verifyTrue(Misc.verifyDateTimeFormat(dspConsent.getResponseNodeStringByPath("Data.StatusUpdateDateTime").split("T")[0], "yyyy-MM-dd") && 
					(Misc.verifyDateTimeFormat(dspConsent.getResponseNodeStringByPath("Data.StatusUpdateDateTime").split("T")[1], "HH:mm:ss+00:00")), 
					"StatusUpdateDateTime is as per expected format");				
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
}
}



