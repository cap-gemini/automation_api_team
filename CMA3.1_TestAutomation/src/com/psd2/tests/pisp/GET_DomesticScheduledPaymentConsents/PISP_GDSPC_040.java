package com.psd2.tests.pisp.GET_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Initiation block where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSPC_040 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPC_040() throws Throwable{				
		 TestLogger.logStep("[Step 1] : Generate Consent Id");
			consentDetails = apiUtility.generatePayments(false,apiConst.domesticScheduledPayments, true, false);
			TestLogger.logBlankLine();					
			TestLogger.logStep("[Step 2] : GET Domestic Scheduled payment consent....");	
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint + "/"+ consentDetails.get("consentId"));
			dspConsent.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
			dspConsent.setMethod("GET");
			dspConsent.submit();	
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.InstructionIdentification"))!=null, 
					"Mandatory field InstructionIdentification is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.InstructionIdentification"));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.EndToEndIdentification"))!=null, 
					"Mandatory field EndToEndIdentification is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.EndToEndIdentification"));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.InstructedAmount"))!=null,
					"Mandatory field InstructedAmount is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.InstructedAmount"));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorAccount"))!=null, 
					"Mandatory field EndToEndIdentification is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.CreditorAccount"));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.LocalInstrument"))!=null,
					"Optional field LocalInstrument is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.LocalInstrument"));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount"))!=null,
					"Optional field LocalInstrument is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount"));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress"))!=null,
					"Optional field CreditorPostalAddress is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress"));
			testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation"))!=null,
					"Optional field CreditorPostalAddress is present in Initiation block"+dspConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation"));	
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
}
}
