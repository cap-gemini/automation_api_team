package com.psd2.tests.pisp.GET_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/CreditorPostalAddress block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
 public class PISP_GDSPC_059 extends TestBase{	
	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPC_059() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, true, false);
		TestLogger.logBlankLine();
			
		TestLogger.logStep("[Step 2] : GET Domestic schedule payment consent....");
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint+"/"+consentDetails.get("consentId"));
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dspConsent.setMethod("GET");
		dspConsent.submit();
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
	    "Response Code is correct for Get Domestic Scheduled Payment Consent");	
			
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.AddressType"))!=null, 
		"Mandatory field AddressType is present in CreditorPostalAddress block");
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.Department"))!=null, 
		"Mandatory field Department is present in CreditorPostalAddress block");
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.SubDepartment"))!=null, 
		"Optional field SubDepartment is present in CreditorPostalAddress block");
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.StreetName"))!=null, 
		"Optional field StreetName is present in CreditorPostalAddress block");
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.BuildingNumber"))!=null, 
		"Optional field BuildingNumber is present in CreditorPostalAddress block");
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.PostCode"))!=null, 
		"Optional field PostCode is present in CreditorPostalAddress block");
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.TownName"))!=null, 
		"Optional field TownName is present in CreditorPostalAddress block");
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.CountrySubDivision"))!=null, 
		"Optional field CountrySubDivision is present in CreditorPostalAddress block");
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.Country"))!=null, 
		"Optional field Country is present in CreditorPostalAddress block");
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.AddressLine"))!=null, 
		"Optional field AddressLine is present in CreditorPostalAddress block");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
			
			
	}
	
}
