package com.psd2.tests.pisp.GET_DomesticScheduledPaymentConsents;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of CMA compliance for GET Domestic Scheduled Payment Consents URI 
 * @author : Soumya Banerjee
 *
 */
@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class PISP_GDSPC_001 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPC_001() throws Throwable{				
		 	TestLogger.logStep("[Step 1] : Generate Consent Id");
			consentDetails = apiUtility.generatePayments(false,apiConst.domesticScheduledPayments, true, false);
			TestLogger.logBlankLine();			
			TestLogger.logStep("[Step 2] : GET Domestic Scheduled payment consent....");
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint + "/"+ consentDetails.get("consentId"));
			dspConsent.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
			dspConsent.setMethod("GET");
			dspConsent.submit();		
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			testVP.verifyStringEquals(dspConsent.getURL(),apiConst.dspConsent_endpoint+"/"+consentDetails.get("consentId"), 
					"URI for GET domestic Scheduled payment consent request is as per open banking standard");
			
			testVP.verifyTrue(SignatureUtility.verifySignature(dspConsent.getResponseString(), dspConsent.getResponseHeader("x-jws-signature")), 
					"Response that created successfully with HTTP Code 201 MUST be digitally signed");
			
			TestLogger.logBlankLine();
}
}
