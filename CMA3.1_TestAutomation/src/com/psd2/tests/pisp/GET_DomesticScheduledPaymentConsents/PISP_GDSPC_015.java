package com.psd2.tests.pisp.GET_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : INVALID TEST CASE Verification of the request with invalid value of scope for the flow into Authorization (Access Token) header
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSPC_015 extends TestBase{		
	@Test
	public void m_PISP_GDSPC_015() throws Throwable{		
		     TestLogger.logStep("[Step 1] : Set the scope as Accounts");
		     createClientCred.setBaseURL(apiConst.cc_endpoint);
		     createClientCred.setScope("accounts");
			 createClientCred.submit();			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
						"Response Code is correct for client credetials");		
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);	
			TestLogger.logBlankLine();
			TestLogger.logStep("[Step 3] : GET Domestic Schedule Payment Consent SetUp....");
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint+ "/"+consentId);
			dspConsent.setHeadersString("Authorization:Bearer "+ cc_token);
			dspConsent.setMethod("GET");
			dspConsent.submit();
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"403", "Response Code is correct for Domestic Schedule Payment Consent");				
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
}
}

