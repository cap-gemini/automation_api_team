package com.psd2.tests.pisp.GET_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with invalid value of OPTIONAL x-fapi-customer-ip-address header 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSPC_017 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDSPC_017() throws Throwable{			
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.domesticScheduledPayments, true, false);
		TestLogger.logBlankLine();
		TestLogger.logStep("[Step 1-2] : GET Domestic Scheduled Payment Consent SetUp....");
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint + "/"+ consentDetails.get("consentId"));
		dspConsent.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
		dspConsent.addHeaderEntry("x-fapi-customer-ip-address", "UUXGUISGC^%");
		dspConsent.setMethod("GET");
		dspConsent.submit();		
				testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI when invalid x-fapi-customer-ip-address is passed");
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
					"Response Error Code is correct");
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].Message")),"Invalid value found in x-fapi-customer-ip-address header","Error message is correct");		
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
}
}
