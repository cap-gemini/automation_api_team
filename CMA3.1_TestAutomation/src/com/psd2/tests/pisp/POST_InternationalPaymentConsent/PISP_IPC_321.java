package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;

/**
 * Class Description : Verification of the Invalid account details into OPTIONAL Data/Initiation/DebtorAccount block where Request has sent successfully and returned a HTTP Code 201 Created. 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_321 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_321() throws Throwable{	
		
		  TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200","Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			API_Constant.setPisp_CC_AccessToken(cc_token);	
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Domestic Scheduled Payment Consent SetUp....");
			
			internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
			internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
			internationalPayment.setDrAccountSchemeName("IBAN");
			internationalPayment.setDrAccountIdentification("IE86BOFI90575812345678");
			internationalPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", "Response Code is correct for Domestic Scheduled Payment Consent");
			consentId = internationalPayment.getConsentId();
			TestLogger.logVariable("Consent Id : " + consentId);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-3] : JWT Token Creation........");
			
			//reqObject.setBaseURL(apiConst.ro_endpoint);
			reqObject.setValueField(consentId);
			reqObject.setScopeField("payments");
			outId = reqObject.submit();
			
			TestLogger.logVariable("JWT Token : " + outId);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-4] : Go to URL and authenticate consent");	
			redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
			
			startDriverInstance();
			String errorMsg= consentOps.invalidPayerAccont(redirecturl);

			closeDriverInstance();
			TestLogger.logBlankLine();
			
			testVP.verifyStringEquals(errorMsg, "Invalid payer account provided. Request cannot be processed.", 
					"Error message is correct for invalid payer account provided");
			closeDriverInstance();
			
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
