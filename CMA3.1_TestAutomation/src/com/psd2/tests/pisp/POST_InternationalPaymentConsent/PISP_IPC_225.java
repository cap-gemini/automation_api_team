package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_225 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_225() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 201 Created");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for internationalPayment Consent URI");
		
		testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.ConsentId")).isEmpty(),  
				"Mandatory field ConsentId is present and not empty");
		
		testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.CreationDateTime")).isEmpty(), 
				"Mandatory field CreationDateTime is present and not empty");
		
		testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.Status")).isEmpty(), 
				"Mandatory field Status is present and not empty");
		
		testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.StatusUpdateDateTime")).isEmpty(), 
				"Mandatory field StatusUpdateDateTime is present and not empty");
		
		testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.CutOffDateTime")).isEmpty(), 
				"Mandatory field CutOffDateTime is present and not empty");
		
		testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.ExpectedExecutionDateTime")).isEmpty(), 
				"Mandatory field ExpectedExecutionDateTime is present and not empty");
		
		testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.ExpectedSettlementDateTime")).isEmpty(), 
				"Mandatory field ExpectedExecutionDateTime is present and not empty");
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation"))!=null, 
				"Mandatory field Initiation is present and not empty");
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.Charges"))!=null, 
				"Mandatory field Charges is present and not empty");
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation"))!=null, 
				"Mandatory field ExchangeRateInformation is present and not empty");
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.Authorisation"))!=null, 
				"Mandatory field Authorisation is present and not empty");
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.SCASupportData"))!=null, 
				"Mandatory field SCASupportData is present and not empty");
		
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Risk"))!=null, 
				"Mandatory field Risk is present and not empty");
		
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
