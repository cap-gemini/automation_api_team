package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY Risk block
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_196 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_196() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Risk block");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment Consent URI");
		testVP.verifyTrue((internationalPayment.getResponseNodeStringByPath("Risk"))!=null, 
				"Risk block is present");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Risk.PaymentContextCode")).isEmpty(), 
				"PaymentContextCode field is present under Risk");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Risk.MerchantCategoryCode")).isEmpty(), 
				"MerchantCategoryCode field is present under Risk");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Risk.MerchantCustomerIdentification")).isEmpty(),  
				"MerchantCustomerIdentification field is present under Risk");
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Risk.DeliveryAddress")!=null,  
				"DeliveryAddress field is present under Risk");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
