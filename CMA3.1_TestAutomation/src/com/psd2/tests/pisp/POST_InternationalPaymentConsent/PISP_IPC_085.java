package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = any.bank.Scheme1
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_085 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_085() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = any.bank.Scheme1");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setDrAccountSchemeName("any.bank.scheme1");
		internationalPayment.setDrAccountIdentification("JO94CBJO0010000000000131");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for internationalPayment Consent URI");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName"), "any.bank.scheme1", 
				"SchemeName field value is correct");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification"), "JO94CBJO0010000000000131", 
				"Identification field value is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
