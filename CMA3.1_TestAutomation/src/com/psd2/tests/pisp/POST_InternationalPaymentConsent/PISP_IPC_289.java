package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/CreditorAgent/PostalAddress block  
 * @author : Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_289 extends TestBase{	
	@Test
	public void m_PISP_IPC_289() throws Throwable{	
			TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Payment Consent SetUp....");
			internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
			internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
			internationalPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
		    		"Response Code is correct for Post international Payment Consents");
			testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress"))!=null,"Postal Address Block is present");
			testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType")).isEmpty(),"AddressType field is present");
			testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Department")).isEmpty(),"Department field is present");
			testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.SubDepartment")).isEmpty(),"SubDepartment field is present");
			testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.StreetName")).isEmpty(),"StreetName field is present");
			testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.BuildingNumber")).isEmpty(),"BuildingNumber field is present");
			testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.PostCode")).isEmpty(),"PostCode field is present");
			testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.TownName")).isEmpty(),"TownName field is present");
			testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.CountrySubDivision")).isEmpty(),"CountrySubDivision field is present");
			testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Country")).isEmpty(),"Country field is present");
			testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressLine")).isEmpty(),"AddressLine field is present");			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
