package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : 	Verification of the values into MANDATORY Data/Initiation block
 * @author : Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_036 extends TestBase{	
	String requestBody;
	@Test
	public void m_PISP_IPC_036() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
			
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Payment Consent");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()), "201",
				"Response Code is correct for International Payment Consent URI");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data") != null,
				"Mandatory block Data is present and is not null");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.ConsentId")).isEmpty(),
				"Mandatory field ConsentId is present under Data");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.CreationDateTime")).isEmpty(),
				"Mandatory field CreationDateTime is present under Data");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Status")).isEmpty(),
				"Mandatory field Status is present under Data");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.StatusUpdateDateTime")).isEmpty(),
				"Mandatory field StatusUpdateDateTime is present under Data");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation") != null,
				"Mandatory block Initiation is present and is not null");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.InstructionIdentification")).isEmpty(),
				"Mandatory field InstructionIdentification is present under Initiation");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument")).isEmpty(),
				"Optional field LocalInstrument is present under Initiation block");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.EndToEndIdentification")).isEmpty(),
				"Mandatory field EndToEndIdentification is present under Initiation block");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.InstructionPriority")).isEmpty(),
				"Mandatory field InstructionPriority is present under Initiation block");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Purpose")).isEmpty(),
				"Optional field Purpose is present under Initiation block");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.CurrencyOfTransfer")).isEmpty(),
				"Mandatory field CurrencyOfTransfer is present under Initiation block");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.InstructedAmount") != null,
				"Mandatory field InstructedAmount is present under Initiation block");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation") != null,
				"Optional field ExchangeRateInformation is present under Initiation block");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAccount") != null,
				"Mandatory field CreditorAccount is present under Initiation block");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.DebtorAccount") != null,
				"Optional field DebtorAccount is present under Initiation block");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor") != null,
				"Optional field Creditor is present under Initiation block");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent") != null,
				"Optional field CreditorAgent is present under Initiation block");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation") != null,
				"Optional field RemittanceInformation is present under Initiation block");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Risk") != null,
				"Mandatory field Risk present under Initiation block");

		TestLogger.logBlankLine();		
		testVP.testResultFinalize();
	}
}
