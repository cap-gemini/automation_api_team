package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of NULL or Invalid value of MANDATORY CreditorAccount/Name field
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_123 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_123() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of NULL value of MANDATORY CreditorAccount/Name field");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setCrAccountName("");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for internationalPayment Consent URI when CreditorAccount Name is NULL");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected min length 1 for field [Name], but got []"), 
				"Message for error code is '"+internationalPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
		
		TestLogger.logStep("[Step 3] : Verification of the value of MANDATORY CreditorAccount/Name field having length variations");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		internationalPayment.setCrAccountName("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567834782469723054");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for internationalPayment Consent URI when CreditorAccount Name is more than 70 characters");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 70 for field [Name], but got [ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567834782469723054]"), 
				"Message for error code is '"+internationalPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
