package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : 	Verification of the request without MANDATORY x-jws-signature header
 * @author : Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_025 extends TestBase{	
	@Test
	public void m_PISP_IPC_025() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
			
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : International Payment Consent without MANDATORY x-jws-signature header");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		restRequest.setURL(apiConst.iPaymentConsent_endpoint);
		String requestBody= internationalPayment.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+cc_token+", Content-Type:application/json, Accept:application/json, x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for International Payment Consent");
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Missing", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].Message")),"Required header x-jws-signature not specified","Error message is correct");
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();	
	}
}
