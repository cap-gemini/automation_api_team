package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values of InstructedAmount array and CurrencyOfTransfer in the response of consent API when currency values are different 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_333 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_333() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of internationalPayment consent when Rate type provide as Agreed");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setCurrencyOfTransfer("GBP");
		internationalPayment.setCurrency("EUR");
		internationalPayment.setUnitCurrency("USD");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for internationalPayment Consent URI when currency are different");
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.CurrencyOfTransfer")),"GBP" ,
				"Response Code is correct for internationalPayment Consent CurrencyOfTransfer");
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.InstructedAmount.Currency")),"EUR" ,
				"Response Code is correct for internationalPayment Consent Currency");
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency")),"USD" ,
				"Response Code is correct for internationalPayment Consent UnitCurrency");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
