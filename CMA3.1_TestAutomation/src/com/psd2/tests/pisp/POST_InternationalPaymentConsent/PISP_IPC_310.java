package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Risk/DeliveryAddress block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_310 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_310() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Risk/DeliveryAddress block where Request has sent successfully and returned a HTTP Code 201 Created");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for internationalPayment Consent URI");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Risk.DeliveryAddress")!=null, 
				"Optional DeliveryAddress under risk is present and is not null");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.Country")).isEmpty(), 
				"Mandatory field i.e. Country is present and is not null");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.TownName")).isEmpty(), 
				"Mandatory field i.e. TownName is present and is not null");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.AddressLine")).isEmpty(), 
				"Optional field i.e. AddressLine is present and is not null");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.StreetName")).isEmpty(), 
				"Optional field i.e. StreetName is present and is not null");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.BuildingNumber")).isEmpty(), 
				"Optional field i.e. BuildingNumber is present and is not null");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.PostCode")).isEmpty(), 
				"Optional field i.e. PostCode is present and is not null");
		/*testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Risk.DeliveryAddress.CountrySubDivision[0]")).isEmpty(), 
				"Optional field i.e. CountrySubDivision is present and is not null");*/
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
