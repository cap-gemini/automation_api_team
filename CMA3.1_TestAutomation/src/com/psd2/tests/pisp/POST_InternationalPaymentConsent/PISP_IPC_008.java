package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request with different payload with same x-idempotency-key after 24hrs
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Database"})
public class PISP_IPC_008 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_008() throws Throwable{	
		String idemPotencyKey=PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3);
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request with x-idempotency-key value as "+idemPotencyKey+" in the header");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey);
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment Consents when x-idempotency-key value in header is "+idemPotencyKey+"");
		
		consentId = internationalPayment.getConsentId();
		TestLogger.logVariable("ConsentId : " + consentId);
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Change the createdAt date to previous date from current date");
		
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_paymentSetupPlatform"));
		mongo.updateDocumentObject("paymentConsentId:"+consentId,"createdAt:"+Misc.previousDate().replace(":", "#"));
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of request with same x-idempotency-key i.e. "+idemPotencyKey+" and different payload after 24hrs");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey);
		internationalPayment.setCrAccountName("Anything");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment Consents when x-idempotency-key value is same i.e. "+idemPotencyKey+" and payload is different after 24hrs");
		
		testVP.verifyTrue(!(consentId.equals(internationalPayment.getConsentId())), 
				"ConsentId created after 24hrs is different");
		
	    TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}