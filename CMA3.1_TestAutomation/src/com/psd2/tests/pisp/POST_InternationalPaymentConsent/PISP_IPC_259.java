package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description :Verification of the value of OPTIONAL ExchangeRateInformation Block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_259 extends TestBase{	
	
	@Test
	public void m_PISP_IPC_259() throws Throwable{	
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
	"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();
	
    TestLogger.logStep("[Step 2] : International Payment Consent SetUp....");
	internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
	internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
	internationalPayment.submit();
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
    "Response Code is correct for Post international Payment Consents");
	
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation"))!=null, 
	"Mandatory Block ExchangeRateInformation is present"+internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation"));
	
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency"))!=null, 
	"Mandatory field UnitCurrency is present in ExchangeRateInformation block "+ internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.UnitCurrency"));
	
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ExchangeRate"))!=null, 
	"Mandatory field ExchangeRate is present in ExchangeRateInformation block "+internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ExchangeRate"));
	
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.RateType"))!=null, 
	"Optional field RateType is present in ExchangeRateInformation block "+internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.RateType"));
	
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ContractIdentification"))!=null, 
	"Optional field ContractIdentification is present in ExchangeRateInformation block "+internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ContractIdentification"));
	TestLogger.logBlankLine();
	testVP.testResultFinalize();

}
}	

