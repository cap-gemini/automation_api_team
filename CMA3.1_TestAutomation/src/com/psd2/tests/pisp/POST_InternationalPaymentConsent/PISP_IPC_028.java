package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : 	Verification of the request with BLANK or Invalid value of OPTIONAL x-fapi-customer-ip-address header
 * @author : Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_028 extends TestBase{	
	@Test
	public void m_PISP_IPC_028() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
			
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Payment Consent with blank value in x-fapi-customer-ip-address header");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.addHeaderEntry("x-fapi-customer-ip-address", "");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Payment Consent");
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Errors[0].Message")),"Invalid value found in x-fapi-customer-ip-address header","Error message is correct");

		TestLogger.logStep("[Step 3] : International Payment Consent with invalid value in x-fapi-customer-ip-address header");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.addHeaderEntry("x-fapi-customer-ip-address", "ABCD");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Payment Consent");
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Errors[0].Message")),"Invalid value found in x-fapi-customer-ip-address header","Error message is correct");
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();	
	}
}
