package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/InstructedAmount block
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_054 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_054() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the status when MANDATORY Data/Initiation/InstructedAmount block present");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.submit();
		
		 testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.InstructedAmount")!=null ,
					"Instructed Amount block is present ");
	    testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.InstructedAmount.Amount")!=null ,
				"Amount field is present on Instructed Amount block "+internationalPayment.getResponseNodeStringByPath("Data.Initiation.InstructedAmount.Amount"));
	    testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.InstructedAmount.Currency")!=null ,
				"Amount field is present on Instructed Amount block "+internationalPayment.getResponseNodeStringByPath("Data.Initiation.InstructedAmount.Currency"));
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}

