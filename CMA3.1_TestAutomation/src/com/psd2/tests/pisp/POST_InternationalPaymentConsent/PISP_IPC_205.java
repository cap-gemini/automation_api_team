package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Risk/DeliveryAddress block haven't sent
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_205 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_205() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Risk/DeliveryAddress block haven't sent");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		String requestBody=internationalPayment.genRequestBody().replace(",\"DeliveryAddress\": {"
									+ "\"AddressLine\":"+ internationalPayment._riskAddressLine+","						
									+ "\"StreetName\": \""+internationalPayment._riskAddressStreetName+"\","
									+ "\"BuildingNumber\": \""+internationalPayment._riskAddressBuildingNumber+"\","
									+ "\"PostCode\": \""+internationalPayment._riskAddressPostCode+"\","
									+ "\"TownName\": \""+internationalPayment._riskAddressTown+"\","
									+ "\"CountrySubDivision\":[ \""+internationalPayment._riskCountrySubDivision+"\"],"
									+ "\"Country\": \""+internationalPayment._riskCountry+"\"}","");
		internationalPayment.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for internationalPayment Consent URI");
		
		testVP.verifyFalse(internationalPayment.getResponseString().contains("Risk.DeliveryAddress"), 
					"DeliveryAddress is not present");
		
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
