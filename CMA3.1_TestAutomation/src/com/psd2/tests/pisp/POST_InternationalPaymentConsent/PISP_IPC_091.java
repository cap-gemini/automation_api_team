package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/ExchangeRateInformation block
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_091 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_091() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Data/Initiation/CreditorAccount block");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for internationalPayment Consent URI");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency")).isEmpty(), 
				"Mandatory field SchemeName is present under ExchangeRateInformation");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.ExchangeRateInformation.RateType")).isEmpty(), 
				"Mandatory field Identification is present under ExchangeRateInformation");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.ExchangeRateInformation.ExchangeRate")).isEmpty(), 
				"Mandatory field OName is present under ExchangeRateInformation");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.ExchangeRateInformation.ContractIdentification")).isEmpty(), 
				"Optional field SecondaryIdentification is present under ExchangeRateInformation");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
