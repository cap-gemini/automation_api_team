package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of MANDATORY Initiation/InstructionIdentification field NOT sent or NULL or Invalid InstructionIdentification field or having length variation
 * @author : Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_038 extends TestBase{	
	@Test
	public void m_PISP_IPC_038() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
			
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Payment Consent without InstructionIdentification");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		String _reqBody = internationalPayment.genRequestBody().replace("\"InstructionIdentification\": \"ACME412ACME412ACME412ACME412ACME412\",","");
		internationalPayment.submit(_reqBody);
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400",
				"Response Code is correct for International Payment Consent URI when MANDATORY InstructedAmount/Amount field block haven't sent");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"),
				"UK.OBIE.Field.Missing", "Error Code are matched");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"),
				"Error validating JSON. Error: - Missing required field [InstructionIdentification]","Error Message are matched");

		
		TestLogger.logStep("[Step 3] : International Payment Consent with Blank value of InstructionIdentification");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setInstructionIdentification("");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400",
				"Response Code is correct for Domestic Scheduled Payment URL for InstructionIdentification fields which is set as NULL");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"),
				"UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"),
				"Error validating JSON. Error: - Expected min length 1 for field [InstructionIdentification], but got []","Error Message are matched");
		
		TestLogger.logStep("[Step 4] : International Payment Consent  having InstructionIdentification length variation");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setInstructionIdentification("ACME412ACME412ACME412ACME412ACME412AAA");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
					"Response Code is correct for International Payment Consent URL for InstructionIdentification fields which is greater than 35 char");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Expected max length 35 for field [InstructionIdentification], but got [ACME412ACME412ACME412ACME412ACME412AAA]", "Error Message are matched");
				
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();
	}
}
