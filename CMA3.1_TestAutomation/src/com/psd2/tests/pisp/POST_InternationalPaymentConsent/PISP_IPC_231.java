package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the values into MANDATORY Status field when consent has been cancelled from consent page
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_231 extends TestBase {	
	API_E2E_Utility apiUtility=new  API_E2E_Utility();
	@Test
	public void m_PISP_IPC_231() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Payment Consent");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);		
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for Post International Payment Consent");
		consentId = internationalPayment.getConsentId();
		TestLogger.logVariable("Consent Id : " + consentId);	
		
		TestLogger.logStep("[Step 3] : JWT Token Creation........");
		//reqObject.setBaseURL(apiConst.ro_endpoint);
		reqObject.setValueField(consentId);
		reqObject.setScopeField("payments");
		outId = reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Go to URL and authenticate consent");	
		redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		consentOps.renewAISPConsent(redirecturl,true,false);		
		closeDriverInstance();
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 5] : Verification of the values into MANDATORY Status field when consent has been cancelled from consent page");
		restRequest.setURL(apiConst.iPaymentConsent_endpoint+"/"+consentId);
		restRequest.setHeadersString("Authorization:Bearer "+cc_token+", Accept:application/json, x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for Get International Payment Consent URI");
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Data.Status")), "Rejected",
				"Status of the consent field is correct i.e. Rejected");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
