package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description :Verification of the values of OPTIONAL Initiation/ChargeBearer field
 * @author : Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_052 extends TestBase{	
	@Test
	public void m_PISP_IPC_052() throws Throwable{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
			
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Payment Consent with ChargeBearer");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.submit();
	    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201",
				"Response Code is correct for International Payment Consent URL for ChargeBearer fields ");
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.ChargeBearer").equals("BorneByDebtor") 
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.ChargeBearer").equals("BorneByCreditor")
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.ChargeBearer").equals("FollowingServiceLevel")
					|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.ChargeBearer").equals("Shared"), 
					"InstructionPriority field value is correct i.e. "+internationalPayment.getResponseNodeStringByPath("Data.Initiation.ChargeBearer"));
			
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();
	}
}
