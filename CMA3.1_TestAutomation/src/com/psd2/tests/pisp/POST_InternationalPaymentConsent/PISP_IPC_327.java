package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of response with ExpirationDateTime for the consent API when the RateType is provided as "Indicative" in the resquest payload
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_327 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_327() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Links block where Request has sent successfully and returned a HTTP Code 201 Created");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setRateType("Indicative");
		String responseBody= internationalPayment.genRequestBody().replace(",\"ExchangeRate\": "+internationalPayment._exchangeRate+",\"ContractIdentification\": \""+internationalPayment._contractIdentification+"\"", "");
		internationalPayment.submit(responseBody);
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for internationalPayment Consent URI with Indicative Rate Type");
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExpirationDateTime"))==null, 
				"Response Code is correct for internationalPayment Consent with Indicative Rate Type"+internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExpirationDateTime"));
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
