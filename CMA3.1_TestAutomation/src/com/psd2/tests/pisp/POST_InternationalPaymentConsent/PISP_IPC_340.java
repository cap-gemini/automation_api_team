package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of response for the consent API With ChargeAmount=1%, ChargeCurrency=GBP
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_340 extends TestBase {	
	@Test
	public void m_PISP_IPC_340() throws Throwable{	
		
        TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200","Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		API_Constant.setPisp_CC_AccessToken(cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Payment Consent....");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setAmount("300.00");
		String requestBody=internationalPayment.genRequestBody().replace("\"ChargeBearer\": \""+internationalPayment._chargeBearer+"\",", "");
		internationalPayment.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201",
				"Response Code is correct for internationalPayment consent URI ");
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Data.Charges.Amount[0].Amount")).substring(0, 3),
								String.valueOf(Misc.calculatePercentage(1D,Double.parseDouble(internationalPayment._amount))).substring(0, 3),"Response code is correct");
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
