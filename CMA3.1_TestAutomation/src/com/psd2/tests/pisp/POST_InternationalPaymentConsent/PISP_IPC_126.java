package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the value of OPTIONAL Creditor/PostalAddress/AddressType field
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_126 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_126() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of OPTIONAL Creditor/PostalAddress/AddressType field");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for internationalPayment Consent URI");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.AddressType").equals("Business")
				|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.AddressType").equals("Correspondence")
				|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.AddressType").equals("DeliveryTo")
				|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.AddressType").equals("MailTo")
				|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.AddressType").equals("POBox")
				|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.AddressType").equals("Postal")
				|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.AddressType").equals("Residential")
				|| internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.AddressType").equals("Statement"), 
				"Optional field AddressType is present under Creditor/PostalAddress and have correct value");
				
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
		
			}
}
