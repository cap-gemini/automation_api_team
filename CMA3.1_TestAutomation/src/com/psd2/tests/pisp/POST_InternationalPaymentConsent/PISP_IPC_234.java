package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into OPTIONAL ExpectedExecutionDateTime Field Where Request has sent successfully and returned HTTP Code 201 Created 
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_234 extends TestBase{	
	@Test
	public void m_PISP_IPC_234() throws Throwable{	
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
	"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();
	
    TestLogger.logStep("[Step 2] : International Payment Consent SetUp....");
	internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
	internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
	internationalPayment.submit();
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
    "Response Code is correct for Post international Payment Consents");
	
	testVP.verifyTrue(Misc.verifyDateTimeFormat(internationalPayment.getResponseNodeStringByPath("Data.ExpectedExecutionDateTime").split("T")[0], "yyyy-MM-dd") && 
	 (Misc.verifyDateTimeFormat(internationalPayment.getResponseNodeStringByPath("Data.ExpectedExecutionDateTime").split("T")[1], "HH:mm:ss+05:30") ||
	Misc.verifyDateTimeFormat(internationalPayment.getResponseNodeStringByPath("Data.ExpectedExecutionDateTime").split("T")[1], "HH:mm:ss+00:00")), 
		       "CutOffDateTime is as per expected format");
	TestLogger.logBlankLine();	
	testVP.testResultFinalize();
	}
}
