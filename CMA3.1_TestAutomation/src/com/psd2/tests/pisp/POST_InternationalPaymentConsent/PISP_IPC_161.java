package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/CreditorAgent/PostalAddress block 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_161 extends TestBase {	
	@Test
	public void m_PISP_IPC_161() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
			
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Payment Consent ");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
					"Response Code is correct ");
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress")!=null, 
				"field under postal address is not empty");
		testVP.verifyTrue(!internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType").isEmpty(), 
				"field under postal address is not empty");
		testVP.verifyTrue(!internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.Department").isEmpty(), 
				"field under postal address is not empty");
		testVP.verifyTrue(!internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.SubDepartment").isEmpty(), 
				"field under postal address is not empty");
		testVP.verifyTrue(!internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.StreetName").isEmpty(), 
				"field under postal address is not empty");
		testVP.verifyTrue(!internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.BuildingNumber").isEmpty(), 
				"field under postal address is not empty");
		testVP.verifyTrue(!internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.PostCode").isEmpty(), 
				"field under postal address is not empty");
		testVP.verifyTrue(!internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.TownName").isEmpty(), 
				"field under postal address is not empty");
		testVP.verifyTrue(!internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.CountrySubDivision").isEmpty(), 
				"field under postal address is not empty");
		testVP.verifyTrue(!internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.Country").isEmpty(), 
				"field under postal address is not empty");
		testVP.verifyTrue(!internationalPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressLine").isEmpty(), 
				"field under postal address is not empty");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
