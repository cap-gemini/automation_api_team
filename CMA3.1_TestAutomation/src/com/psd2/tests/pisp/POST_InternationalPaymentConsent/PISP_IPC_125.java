package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/Creditor block
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_125 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_125() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Data/Initiation/Creditor block");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.submit();
		
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor")).isEmpty(), 
				"Creditor block is present");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.Name")).isEmpty(), 
				"Optional field Name is present under PostalAddress");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.AddressType")).isEmpty(), 
				"Optional field AddressType is present under PostalAddress");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.Department")).isEmpty(), 
				"Optional field Department is present under PostalAddress");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.SubDepartment")).isEmpty(), 
				"Optional field SubDepartment is present under PostalAddress");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.StreetName")).isEmpty(), 
				"Optional field StreetName is present under PostalAddress");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.BuildingNumber")).isEmpty(), 
				"Optional field BuildingNumber is present under PostalAddress");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.PostCode")).isEmpty(), 
				"Optional field PostCode is present under PostalAddress");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.TownName")).isEmpty(), 
				"Optional field TownName is present under PostalAddress");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.CountrySubDivision")).isEmpty(), 
				"Optional field CountrySubDivision is present under PostalAddress");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.Country")).isEmpty(), 
				"Optional field Country is present under PostalAddress");
		testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.Creditor.PostalAddress.AddressLine")).isEmpty(), 
				"Optional field AddressLine is present under PostalAddress");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
		
			}
}
