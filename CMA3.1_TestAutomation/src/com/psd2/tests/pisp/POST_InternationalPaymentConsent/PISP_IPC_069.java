package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values of MANDATORY DebtorAccount/SchemeName field having OB defined values
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_069 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_069() throws Throwable{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values of MANDATORY DebtorAccount/SchemeName field having OB defined values");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.submit();

		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for internationalPayment Consent URI");
		
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.SortCodeAccountNumber")
				|| internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.IBAN")
				|| internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.PAN")
				|| internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.Paym")
				|| internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.BBAN")
				|| internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme1")
				|| internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme2"), 
				"MANDATORY DebtorAccount/SchemeName field has OB defined values");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

