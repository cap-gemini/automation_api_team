package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of response for the consent API when Unsupported Currency value is provided .Verification of response for the consent API when Unsupported Currency value is provided .
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_335 extends TestBase {	
	
	@Test
	public void m_PISP_IPC_335() throws Throwable{	
		
        TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200","Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		API_Constant.setPisp_CC_AccessToken(cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Payment Consent with Currency as AFN....");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setCurrency("AFN");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201",
				"Response Code is correct for internationalPayment consent URI when InstructedAmount/currency as AFN");
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Data.Status")), "Rejected",
				"Mandatory field Status as Rejected");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.MultiAuthorisation") == null,
				"Mandatory field MultiAuthorisation is not present under Initiation block"+internationalPayment.getResponseValueByPath("Data.MultiAuthorisation"));
		
		TestLogger.logStep("[Step 3] : International Payment Consent with Currency as IRR....");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		internationalPayment.setCurrency("IRR");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201",
				"Response Code is correct for internationalPayment consent URI when InstructedAmount/currency as IRR");
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Data.Status")), "Rejected",
				"Mandatory field Status as Rejected");
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.MultiAuthorisation") == null,
				"Mandatory field MultiAuthorisation is not present under Initiation block");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
