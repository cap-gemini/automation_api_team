package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of MANDATORY Initiation/CurrencyOfTransfer field NOT sent OR NULL or Invalid CurrencyOfTransfer field value having length variation 
 * @author : Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_042 extends TestBase{	
	@Test
	public void m_PISP_IPC_042() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
			
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Payment Consent without CurrencyOfTransfer");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		String _reqBody = internationalPayment.genRequestBody().replace("\"CurrencyOfTransfer\": \""+internationalPayment._currencyOfTransfer+"\",","");
		internationalPayment.submit(_reqBody);
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400",
				"Response Code is correct for International Payment Consent URI when MANDATORY CurrencyOfTransfer field block haven't sent");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"),
				"UK.OBIE.Field.Missing", "Error Code are matched");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"),
				"Error validating JSON. Error: - Missing required field [CurrencyOfTransfer]","Error Message are matched");
		
		TestLogger.logStep("[Step 3] : International Payment Consent with Blank value of CurrencyOfTransfer");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setCurrencyOfTransfer("");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400",
				"Response Code is correct for International Payment Consent URL for CurrencyOfTransfer fields which is set as NULL");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"),
				"UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"),
				"Error validating JSON. Error: - Invalid value ''. Expected ^[A-Z]{3,3}$ for CurrencyOfTransfer","Error Message are matched");
		
		TestLogger.logStep("[Step 4] : International Payment Consent  having CurrencyOfTransfer length variation as more than max length");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setCurrencyOfTransfer("GBPS");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
					"Response Code is correct for International Payment Consent URL for CurrencyOfTransfer fields which is greater than 35 char");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Invalid value 'GBPS'. Expected ^[A-Z]{3,3}$ for CurrencyOfTransfer", "Error Message are matched");
				
		
		TestLogger.logStep("[Step 5] : International Payment Consent  having CurrencyOfTransfer length variation as less than min length");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setCurrencyOfTransfer("GB");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
					"Response Code is correct for International Payment Consent URL for CurrencyOfTransfer fields which is greater than 35 char");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Invalid value 'GB'. Expected ^[A-Z]{3,3}$ for CurrencyOfTransfer", "Error Message are matched");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();
	}
}
