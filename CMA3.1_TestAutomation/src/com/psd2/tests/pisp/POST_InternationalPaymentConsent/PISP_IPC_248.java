package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description :Verification of the values into Mandatory Initiation Block Where request has Sent Successfully and retuened a HTTP Code 201 Created
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_248 extends TestBase{	
	@Test
	public void m_PISP_IPC_248() throws Throwable{	
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
	"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();
	
    TestLogger.logStep("[Step 2] : International Payment Consent SetUp....");
	internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
	internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
	internationalPayment.submit();
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
    "Response Code is correct for Post international Payment Consents");
	
    testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.InstructionIdentification"))!= null,
    "Mandatory field InstructionIdentification is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.InstructionIdentification."));
    
    testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.EndToEndIdentification"))!=null, 
    	"Mandatory field EndToEndIdentification is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.EndToEndIdentification"));
    
    testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.LocalInstrument"))!=null,
    	"Mandatory field LocalInstrument is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.LocalInstrument"));
    
    testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.InstructionPriority"))!=null, 
    	"Mandatory field InstructionPriority is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.InstructionPriority"));
    
    testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Purpose"))!=null,
    	"Optional field Purpose is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.Purpose"));
    
    testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.ChargeBearer"))!=null,
    	"Optional field ChargeBearer is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.ChargeBearer"));
    
    testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.RequestedExecutionDateTime"))==null,
    	"Optional field RequestedExecutionDateTime is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.RequestedExecutionDateTime"));
    
    testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CurrencyOfTransfer"))!=null,
    	"Optional field CurrencyOfTransfer is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.CurrencyOfTransfer"));
    TestLogger.logBlankLine();
    testVP.testResultFinalize();	
    		
   }
}	

