package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;

/**
 * Class Description : Verification of international-payment-consents URL that should be as per Open Banking standards as specified below  
 * @author : Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class PISP_IPC_001 extends TestBase{	
	@Test
	public void m_PISP_IPC_001() throws Throwable{	
			TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Payment Consent SetUp....");
			internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
			internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
			internationalPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
		    		"Response Code is correct for Post international Payment Consents");
			testVP.verifyEquals(internationalPayment.getURL(), apiConst.iPaymentConsent_endpoint,"Response Code is correct for Post International Payment Consent URL");
			
			testVP.verifyTrue(SignatureUtility.verifySignature(internationalPayment.getResponseString(), internationalPayment.getResponseHeader("x-jws-signature")), 
					"Response that created successfully with HTTP Code 201 MUST be digitally signed");
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
