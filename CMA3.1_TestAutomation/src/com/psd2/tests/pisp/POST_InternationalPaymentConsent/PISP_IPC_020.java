package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : 	Verification of the request with invalid value of scope for the flow into Authorization (Access Token) header

Access token having value of scope other than 'openid payments' or  'payments' 
 * @author : Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_020 extends TestBase{	
	@Test
	public void m_PISP_IPC_020() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
			
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST International Payment Consent without Authorization value");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+ cc_token);
		internationalPayment.submit();

		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"403",
				"Response Code is correct for International Payment Consent when scope is other than Payment");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();	
	}
}
