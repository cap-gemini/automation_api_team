package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description :Verification of the value of Mandatory Data/Initiation/DebtorAccount Block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_264 extends TestBase{	
	@Test
	public void m_PISP_IPC_264() throws Throwable{	
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
	"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();
	
    TestLogger.logStep("[Step 2] : International Payment Consent SetUp....");
	internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
	internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
	internationalPayment.submit();
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
    "Response Code is correct for Post international Payment Consents");
	
	testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.DebtorAccount")!=null,
			"DebtorAccount block is present and is not null");
	
	testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName")).isEmpty(), 
			" Mandatory SchemeName is present in DebtorAccount block");
	
	testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification")).isEmpty(), 
			"Mandatory Identification is present in DebtorAccount block");
	
	testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Name")).isEmpty(), 
			"Optional Name is present in DebtorAccount block");
	
	testVP.verifyTrue(!(internationalPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SecondaryIdentification")).isEmpty(), 
			"Optional SecondaryIdentification is present in DebtorAccount block");
	TestLogger.logBlankLine();
	testVP.testResultFinalize();
    }
}	

