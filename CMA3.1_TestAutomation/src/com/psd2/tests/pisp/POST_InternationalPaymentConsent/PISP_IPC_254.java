package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description :Verification of the values into Mandatory Initiation/ChargeBearer Field Where request has Sent Successfully and retuened a HTTP Code 201 Created
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_254 extends TestBase{	
	@Test
	public void m_PISP_IPC_254() throws Throwable{	
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
	"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();
	
    TestLogger.logStep("[Step 2] : International Payment Consent SetUp....");
	internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
	internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
	internationalPayment.submit();
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
    "Response Code is correct for Post international Payment Consents");
	
    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.ChargeBearer")), internationalPayment._chargeBearer,
    		"ChargeBearer Field is same in response as sent in request body");
    TestLogger.logBlankLine();
    testVP.testResultFinalize();
    		
    }
}	

