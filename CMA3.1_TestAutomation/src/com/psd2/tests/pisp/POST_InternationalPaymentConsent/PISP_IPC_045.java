package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/LocalInstrument field having NULL or Invalid value 
 * @author : Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_045 extends TestBase{	
	@Test
	public void m_PISP_IPC_045() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
			
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Payment Consent with Blank value of LocalInstrument");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setLocalInstrument("");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400",
				"Response Code is correct for International Payment Consent URL for LocalInstrument fields which is set as NULL");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"),
				"UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"),
				"Error validating JSON. Error: - Expected min length 1 for field [LocalInstrument], but got []","Error Message are matched");
		
		TestLogger.logStep("[Step 3] : International Payment Consent  having invalid value of LocalInstrument");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setLocalInstrument("UK.OBIE.IBAN");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
					"Response Code is correct for International Payment Consent URL for LocalInstrument fields which is greater than 35 char");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Unsupported.LocalInstrument", "Error Code are matched");
		testVP.verifyStringEquals(internationalPayment.getResponseNodeStringByPath("Errors[0].Message"), "invalid local instrument provided", "Error Message are matched");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();
	}
}
