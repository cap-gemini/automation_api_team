package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/LocalInstrument field having OB defined values 
 * @author : Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_046 extends TestBase{	
	@Test
	public void m_PISP_IPC_046() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
			
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();
		
			
		TestLogger.logStep("[Step 2] : International Payment Consents with Local Instrument as : UK.OBIE.SWIFT");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		internationalPayment.setLocalInstrument("UK.OBIE.SWIFT");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment Consent URI");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.SWIFT"),
					"LocalInstrument field value is as per OB defined values");		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3 : International Payment Consents with Local Instrument as : UK.OBIE.Euro1");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		internationalPayment.setLocalInstrument("UK.OBIE.Euro1");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment Consent URI");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Euro1"),
					"LocalInstrument field value is as per OB defined values");		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : International Payment Consents with Local Instrument as : UK.OBIE.SEPACreditTransfer");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		internationalPayment.setLocalInstrument("UK.OBIE.SEPACreditTransfer");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment Consent URI");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.SEPACreditTransfer"),
					"LocalInstrument field value is as per OB defined values");		
		TestLogger.logBlankLine();
			
		TestLogger.logStep("[Step 5] : International Payment Consents with Local Instrument as : UK.OBIE.Target2");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		internationalPayment.setLocalInstrument("UK.OBIE.Target2");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment Consent URI");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Target2"),
					"LocalInstrument field value is as per OB defined values");		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 6] : International Payment Consents with Local Instrument as : UK.OBIE.SEPAInstantCreditTransfer");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		internationalPayment.setLocalInstrument("UK.OBIE.SEPAInstantCreditTransfer");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Payment Consent URI");
		
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.SEPAInstantCreditTransfer"),
					"LocalInstrument field value is as per OB defined values");		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 7] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.BACS");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setLocalInstrument("UK.OBIE.BACS");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Scheduled Payment Consent URI");
		
		/*testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BACS"),
					"LocalInstrument field value is as per OB defined values");	*/	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 8] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.CHAPS");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		internationalPayment.setLocalInstrument("UK.OBIE.CHAPS");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Scheduled Payment Consent URI");
		
		/*testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.CHAPS"),
					"LocalInstrument field value is as per OB defined values");		*/
		TestLogger.logBlankLine(); 
		
		TestLogger.logStep("[Step 9] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.FPS");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		internationalPayment.setLocalInstrument("UK.OBIE.FPS");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Scheduled Payment Consent URI");
		
		/*testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.FPS"),
					"LocalInstrument field value is as per OB defined values");*/		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 10] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.BalanceTransfer");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		internationalPayment.setLocalInstrument("UK.OBIE.BalanceTransfer");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Scheduled Payment Consent URI");
		
		/*testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BalanceTransfer"),
					"LocalInstrument field value is as per OB defined values");	*/	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 11] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.MoneyTransfer");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		internationalPayment.setLocalInstrument("UK.OBIE.MoneyTransfer");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Scheduled Payment Consent URI");
		
		/*testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.MoneyTransfer"),
					"LocalInstrument field value is as per OB defined values");*/		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 12] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.Paym");
		
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		internationalPayment.setLocalInstrument("UK.OBIE.Paym");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Scheduled Payment Consent URI");
		
		/*testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Paym"),
					"LocalInstrument field value is as per OB defined values");*/		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}
