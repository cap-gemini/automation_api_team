package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through POST method with mandatory and optional fields.  
 * @author : Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_003 extends TestBase{	
	@Test
	public void m_PISP_IPC_003() throws Throwable{	
			TestLogger.logStep("[Step 1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Payment Consent SetUp....");
			internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
			internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
			internationalPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
		    		"Response Code is correct for Post international Payment Consents");
			testVP.verifyEquals(internationalPayment.getURL(), apiConst.iPaymentConsent_endpoint,"Response Code is correct for Post International Payment Consent URL");
			
			testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()), "201",
					"Response Code is correct for POST international Payment Submission");
			testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data") != null,
					"Mandatory field Data is present in response and is not empty");
			testVP.verifyTrue(internationalPayment.getResponseValueByPath("Links") != null,
					"Mandatory field Links is present in response and is not empty");
			testVP.verifyTrue((internationalPayment.getResponseNodeStringByPath("Links.Self"))!=null,
					"Mandatory field Self is present in response and is not empty");
			testVP.verifyStringEquals(internationalPayment.getResponseHeader("Content-Type"),
					"application/json;charset=utf-8",
					"Response is UTF-8 character encoded");
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
