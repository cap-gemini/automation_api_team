package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY ConsentId field where Request has sent successfully and returned a HTTP Code 201 Created (Consent ID Must be different for different idempotency key)
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_227 extends TestBase {	
	String consentId1;
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IPC_227() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalPayments, true, false);
		
		consentId1=consentDetails.get("consentId");
		TestLogger.logVariable("CCToken : "+ consentId1);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST International Payment Consent");
		internationalPayment.setTestData();
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		internationalPayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment URI");
		testVP.verifyTrue(!String.valueOf(internationalPayment.getResponseValueByPath("Data.ConsentId")).equalsIgnoreCase(consentId1), "consent IDs are different for different Idempotency key");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
