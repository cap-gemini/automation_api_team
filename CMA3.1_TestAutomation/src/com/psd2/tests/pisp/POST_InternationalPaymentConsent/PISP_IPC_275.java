package com.psd2.tests.pisp.POST_InternationalPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description :Verification of the value of Mandatory Data/Initiation/Creditor/PostalAddress Block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_IPC_275 extends TestBase{	
	@Test
	public void m_PISP_IPC_275() throws Throwable{	
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
	"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();
	
    TestLogger.logStep("[Step 2] : International Payment Consent SetUp....");
	internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
	internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
	internationalPayment.submit();
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
    "Response Code is correct for Post international Payment Consents");
	
   testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress")!=null, 
		"Optional field CreditorPostalAddress is present under Initiation block");
    
    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressType")!=null, 
			"Optional field AddressType is present under CreditorPostalAddress block");
    
    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.Department")!=null, 
			"Optional field Department is present under CreditorPostalAddress block");
    
    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.SubDepartment")!=null, 
			"Optional field SubDepartment is present under CreditorPostalAddress block");

    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.StreetName")!=null, 
			"Optional field StreetName is present under CreditorPostalAddress block");
    
    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.BuildingNumber")!=null, 
			"Optional field BuildingNumber is present under CreditorPostalAddress block");
    
    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.PostCode")!=null, 
			"Optional field PostCode is present under CreditorPostalAddress block");
    
    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.TownName")!=null, 
			"Optional field TownName is present under CreditorPostalAddress block");

    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.CountrySubDivision")!=null, 
			"Optional field CountrySubDivision is present under CreditorPostalAddress block");
    
    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.Country")!=null, 
			"Optional field Country is present under CreditorPostalAddress block");
    
    testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressLine")!=null, 
			"Optional field AddressLine is present under CreditorPostalAddress block");
    TestLogger.logBlankLine();
	testVP.testResultFinalize();
    
   }
}	

