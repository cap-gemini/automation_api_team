package com.psd2.tests.pisp.GET_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data block
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"FP"})
public class PISP_GFP_025 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFP_025() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Data block");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId());
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET File Payment URI");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Data")!=null, 
				"Mandatory block Data is present and is not nulll");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Data.Initiation")!=null, 
				"Initiation block under Data block is present and is not nulll");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.FilePaymentId")).isEmpty(), 
				"FilePaymentId field is present under Data block");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.ConsentId")).isEmpty(), 
				"ConsentId field is present under Data block");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.CreationDateTime")).isEmpty(), 
				"CreationDateTime field is present under Data block");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Status")).isEmpty(), 
				"Status field is present under Data block and is not empty");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.StatusUpdateDateTime")).isEmpty(), 
				"StatusUpdateDateTime field is present under Data block and is not empty");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}