package com.psd2.tests.pisp.GET_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Amount block where Request has sent successfully and returned a HTTP Code 200 Ok
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"FP"})
public class PISP_GFP_034 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFP_034() throws Throwable{	
		filePayment.setControlSum("222.00");
		TestLogger.logStep("[Step 1] : Generate FilePaymentId");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, true);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Amount block where Request has sent successfully and returned a HTTP Code 200 Ok");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"));
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET File Payment URI");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Data.Charges[0].Amount")!=null, 
				"Amount block under Data/Charges block is present and is not nulll");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Charges[0].Amount.Amount")).isEmpty(), 
				"Amount field is present under Data/Charges/Amount block");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Charges[0].Amount.Currency")).isEmpty(), 
				"Currency field is present under Data/Charges/Amount block");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}