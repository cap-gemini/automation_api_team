package com.psd2.tests.pisp.GET_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/DebtorAccount block
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"FP"})
public class PISP_GFP_045 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFP_045() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Data/Initiation/DebtorAccount block");
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId());
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET  File Payment URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName")).isEmpty(), 
				"Mandatory field SchemeName under DebtorAccount is present");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification")).isEmpty(), 
				"Mandatory field Identification under DebtorAccount is present");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Name")).isEmpty(), 
				"Optional field Name under DebtorAccount is present");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SecondaryIdentification")).isEmpty(), 
				"Optional field SecondaryIdentification under DebtorAccount is present");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}