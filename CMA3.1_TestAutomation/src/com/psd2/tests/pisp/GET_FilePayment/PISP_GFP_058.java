package com.psd2.tests.pisp.GET_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into OPTIONAL MultiAuthorisation/ExpirationDateTime where Request has sent successfully and returned a HTTP Code 200 Ok
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"FP"})
public class PISP_GFP_058 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFP_058() throws Throwable{	
		filePayment.setControlSum("300.00");
		TestLogger.logStep("[Step 1] : Generate FilePaymentId");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, true);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL MultiAuthorisation/ExpirationDateTime where Request has sent successfully and returned a HTTP Code 200 Ok");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"));
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		filePayment.setMethod("GET");
		filePayment.submit();
		
	    testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET File Payment URI");
		
	    testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.ExpirationDateTime")).isEmpty(),
	    		"ExpirationDateTime under MultiAuthorisation block is present and is not null");
	    
	    testVP.verifyTrue(Misc.verifyDateTimeFormat(filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.ExpirationDateTime").split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.ExpirationDateTime").split("T")[1], "HH:mm:ss+00:00"), 
				"ExpirationDateTime under MultiAuthorisation block is as per expected format");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}