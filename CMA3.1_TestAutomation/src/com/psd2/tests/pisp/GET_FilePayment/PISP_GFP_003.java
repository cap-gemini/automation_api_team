package com.psd2.tests.pisp.GET_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through GET method with mandatory and optional fields.
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"FP"})
public class PISP_GFP_003 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFP_003() throws Throwable{	
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Checking the request (UTF-8 character encoded) status through POST method with mandatory and optional fields");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId());
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET File Payment URI");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Data")!=null,
				"Mandatory field Data is present in response and is not empty");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Links")!=null, 
				"Mandatory field Links is present in response and is not empty");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Links.Self")).isEmpty(), 
				"Mandatory field Self is present in response and is not empty");
		
		testVP.verifyStringEquals(filePayment.getResponseHeader("Content-Type"), "application/json;charset=UTF-8", 
				"Response is UTF-8 character encoded");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}