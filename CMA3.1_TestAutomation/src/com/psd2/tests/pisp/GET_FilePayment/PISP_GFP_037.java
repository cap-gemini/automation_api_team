package com.psd2.tests.pisp.GET_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation block
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"FP"})
public class PISP_GFP_037 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFP_037() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Data/Initiation block");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId());
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET File Payment URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.FileType")).isEmpty(), 
				"Mandatory field FileType is present under Data/Initiation block");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.FileHash")).isEmpty(), 
				"Mandatory field FileHash is present under Data/Initiation block");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.FileReference")).isEmpty(), 
				"Optional field FileReference is present under Data/Initiation block and is not empty");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.NumberOfTransactions")).isEmpty(), 
				"Optional field NumberOfTransactions is present under Data/Initiation block and is not empty");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument")).isEmpty(), 
				"Optional field LocalInstrument is present under Data/Initiation block");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.ControlSum")).isEmpty(), 
				"Optional field ControlSum is present under Data/Initiation block");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.RequestedExecutionDateTime")).isEmpty(), 
				"Optional field RequestedExecutionDateTime is present under Data/Initiation block");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Data.Initiation.DebtorAccount")!=null, 
				"Optional field DebtorAccount is present under Data/Initiation block and is not empty");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Data.Initiation.RemittanceInformation")!=null, 
				"Optional field RemittanceInformation is present under Data/Initiation block and is not empty");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}