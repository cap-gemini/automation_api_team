package com.psd2.tests.pisp.GET_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of x-fapi-interaction-id value when NOT sent in the request with the response header that created successfully with HTTP Code 200 OK
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"FP"})
public class PISP_GFP_016 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFP_016() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
        
	    TestLogger.logStep("[Step 2] : Verification of x-fapi-interaction-id value when NOT sent in the request");
	    restRequest.setURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId());
	    restRequest.setHeadersString("Authorization:Bearer "+cc_token+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		
	    testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
	    		"Response Code is correct for GET File Payment URI");
	    testVP.verifyTrue(!(restRequest.getResponseHeader("x-fapi-interaction-id")).isEmpty(), 
	    		"x-fapi-interaction-id is present in the response");
	    
	    TestLogger.logBlankLine();
	    
	    TestLogger.logStep("[Step 3] : Verification of the request with Invalid value of x-fapi-interaction-id header");
		
	    filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId());
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.addHeaderEntry("x-fapi-interaction-id", "1234");
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"400", 
				"Response Code is correct for GET File Payment URI when invalid value is used for x-fapi-interaction-id header");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Errors[0].Message")).isEmpty(), 
				"Message for error code is '"+filePayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of the request with null value of x-fapi-interaction-id header");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId());
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.addHeaderEntry("x-fapi-interaction-id", "");
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET File Payment URI when null value is used for x-fapi-interaction-id header");
		
		TestLogger.logBlankLine();
        
		testVP.testResultFinalize();		
	}
}