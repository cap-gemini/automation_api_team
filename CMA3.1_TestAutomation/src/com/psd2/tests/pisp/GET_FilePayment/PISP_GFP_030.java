package com.psd2.tests.pisp.GET_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into MANDATORY StatusUpdateDateTime field where Request has sent successfully and returned a HTTP Code 200 Ok
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"FP"})
public class PISP_GFP_030 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFP_030() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of file-payment-consents URL that should be as per Open Banking standards");	
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId());
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET File Payment URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.StatusUpdateDateTime")).isEmpty(), 
				"StatusUpdateDateTime field is present under Data block");
		
		testVP.verifyTrue(Misc.verifyDateTimeFormat(filePayment.getResponseNodeStringByPath("Data.StatusUpdateDateTime").split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(filePayment.getResponseNodeStringByPath("Data.StatusUpdateDateTime").split("T")[1], "HH:mm:ss+00:00"), 
				"StatusUpdateDateTime is as per expected ISO Standard format");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
