package com.psd2.tests.pisp.GET_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without token value OR key-value pair into Authorization header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"FP"})
public class PISP_GFP_010 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFP_010() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with null value of Authorization header");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId());
		filePayment.setHeadersString("Authorization:Bearer ");
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"401", 
				"Response Code is correct for GET File Payment URI with no token value in the Authorization (Access Token) header");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the request without Authorization header");
		
		restRequest.setURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId());
		restRequest.setHeadersString("x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", 
				"Response Code is correct for GET File Payment URI without Authorization (Access Token) header");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

