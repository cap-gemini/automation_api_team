package com.psd2.tests.pisp.GET_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : 	Verification of the values into OPTIONAL Data/MultiAuthorisation block where Request has sent successfully and returned a HTTP Code 200 Ok
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GFP_053 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GFP_053() throws Throwable{	
		filePayment.setControlSum("300.00");
		TestLogger.logStep("[Step 1] : Generate FilePaymentId");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, true);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Data/MultiAuthorisation block where Request has sent successfully and returned a HTTP Code 200 Ok");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"));
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		filePayment.setMethod("GET");
		filePayment.submit();
		
	    testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET File Payment URI");
		
	    testVP.verifyTrue(filePayment.getResponseValueByPath("Data.MultiAuthorisation")!=null,
	    		"MultiAuthorisation block is present and is not null");
	    
	    testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.Status")).isEmpty(),
	    		"Status field under MultiAuthorisation block is present and is not null");
	    
	    testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.NumberRequired")).isEmpty(),
	    		"NumberRequired field under MultiAuthorisation block is present and is not null");
	    
	    testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.NumberReceived")).isEmpty(),
	    		"NumberReceived field under MultiAuthorisation block is present and is not null");
	    
	    testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.LastUpdateDateTime")).isEmpty(),
	    		"LastUpdateDateTime field under MultiAuthorisation block is present and is not null");
	    
	    testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.ExpirationDateTime")).isEmpty(),
	    		"ExpirationDateTime field under MultiAuthorisation block is present and is not null");
	    
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}