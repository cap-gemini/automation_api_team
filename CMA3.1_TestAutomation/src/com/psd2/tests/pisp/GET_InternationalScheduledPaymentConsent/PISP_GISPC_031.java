package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into OPTIONAL ExpectedExecutionDateTime field where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GISPC_031 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GISPC_031() throws Throwable{	
	TestLogger.logStep("[Step 1] : Generate Consent Id");
	consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, true, false);
	TestLogger.logBlankLine();	
	
	TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get("consentId"));
	iScheduledPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
	"Response Code is correct for GET International Scheduled Payment Consents");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExpectedExecutionDateTime"))!=null,
	"ExpectedExecutionDateTime field under Data is present in Get International scheduled Payment Consent response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ExpectedExecutionDateTime")));		
	testVP.verifyTrue(Misc.verifyDateTimeFormat(iScheduledPayment.getResponseNodeStringByPath("Data.ExpectedExecutionDateTime").split("T")[0], "yyyy-MM-dd") && 
		(Misc.verifyDateTimeFormat(iScheduledPayment.getResponseNodeStringByPath("Data.ExpectedExecutionDateTime").split("T")[1], "HH:mm:ss+00:00")), 
	"ExpectedExecutionDateTime is as per expected format");		
	TestLogger.logBlankLine();
	testVP.testResultFinalize();

}
	}

