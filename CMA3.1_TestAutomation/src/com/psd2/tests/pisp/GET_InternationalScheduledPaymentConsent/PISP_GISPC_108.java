package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the values into OPTIONAL Risk/DeliveryAddress block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GISPC_108 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GISPC_108() throws Throwable{	
	TestLogger.logStep("[Step 1] : Generate Consent Id");
    consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, true, false);
    TestLogger.logBlankLine();	 
    
	TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get("consentId"));
	iScheduledPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
	"Response Code is correct for GET International Scheduled Payment Consents");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Risk.DeliveryAddress"))!=null,
	"DeliveryAddress block is present");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Risk.DeliveryAddress.TownName"))!=null, 
	"Mandatory field TownName is present in DeliveryAddress block");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Risk.DeliveryAddress.Country"))!=null, 
	"Mandatory field Country is present in DeliveryAddress block");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Risk.DeliveryAddress.AddressLine"))!=null,
	"Optional field AddressLine is present in Risk block");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Risk.DeliveryAddress.StreetName"))!=null, 
	"Optional field Country is present in DeliveryAddress block");
    testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Risk.DeliveryAddress.BuildingNumber"))!=null, 
	"Optional field BuildingNumber is present in DeliveryAddress block");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Risk.DeliveryAddress.PostCode"))!=null, 
	"Optional field PostCode is present in DeliveryAddress block");
	/*testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Risk.DeliveryAddress.CountrySubDivision"))!=null, 
	"Optional field CountrySubDivision is present in DeliveryAddress block");*/
	TestLogger.logBlankLine();
    testVP.testResultFinalize();
				
	}

}	

