package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of response with x-jws-signature header
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GISPC_023 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GISPC_023() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get("consentId"));
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token")+", x-jws-signature:1234");
		iScheduledPayment.setMethod("GET");
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
		            "Response Code is correct for International Shedule Payment Consent URI");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}

