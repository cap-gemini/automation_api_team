package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the values into OPTIONAL Data/Initiation/Creditor/PostalAddress block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GISPC_073 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GISPC_073() throws Throwable{	
	TestLogger.logStep("[Step 1] : Generate Consent Id");
	consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, true, false);
	TestLogger.logBlankLine();	
	TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get("consentId"));
	iScheduledPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
	"Response Code is correct for GET International Scheduled Payment Consents");
	
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressType"))!=null , 
	"AddressType Field is present "+iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressType"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.Department"))!=null , 
    "Department field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.Department"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.SubDepartment"))!=null , 
	"SubDepartment field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.SubDepartment"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.StreetName"))!=null , 
	"StreetName field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.StreetName"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.BuildingNumber"))!=null , 
	"BuildingNumber field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.BuildingNumber"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.PostCode"))!=null , 
	"PostCode field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.PostCode"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.TownName"))!=null , 
	"TownName field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.TownName"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.CountrySubDivision"))!=null , 
	"CountrySubDivision field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.CountrySubDivision"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.Country"))!=null , 
	"Country field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.Country"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressLine"))!=null , 
	"AddressLine field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressLine"));
	TestLogger.logBlankLine();
    testVP.testResultFinalize();

}
}	

