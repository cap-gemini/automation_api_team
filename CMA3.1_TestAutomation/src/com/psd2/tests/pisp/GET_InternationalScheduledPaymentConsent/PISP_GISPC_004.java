package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status
 * through GET method with mandatory and optional fields
 * 
 * @author : Snehal Chaudhari
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_GISPC_004 extends TestBase {
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GISPC_004() throws Throwable {
	TestLogger.logStep("[Step 1] : Generate Consent Id");
	consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, true, false);
	TestLogger.logBlankLine();
	
	TestLogger.logStep("[Step 2] :  International Scheduled Payment Consent SetUp....");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get("consentId"));
	iScheduledPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();
		
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()), "200",
	    "Response Code is correct for International Scheduled Payment Consent URI");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data")) != null,
		"Mandatory field Data is present in response and is not empty");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation")) != null,
		"Mandatory field Initiation is present in response and is not empty");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Risk")) != null,
		"Mandatory field Risk is present in response and is not empty");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Links")) != null,
		"Mandatory field Links is present in response and is not empty");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Links.Self"))!= null,
		"Mandatory field Self is present in response and is not empty");
	testVP.verifyStringEquals(iScheduledPayment.getResponseHeader("Content-Type"), "application/json;charset=utf-8", 
		"Response is UTF-8 character encoded");
	TestLogger.logBlankLine();
	testVP.testResultFinalize();
	}
}
