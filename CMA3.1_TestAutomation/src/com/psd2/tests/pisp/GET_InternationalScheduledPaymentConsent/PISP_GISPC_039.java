package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the value of OPTIONAL ExchangeRateInformation Block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GISPC_039 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GISPC_039() throws Throwable{	
	TestLogger.logStep("[Step 1] : Generate Consent Id");
	consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, true, false);
	TestLogger.logBlankLine();	
	
	TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get("consentId"));
	iScheduledPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();
    testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
	"Response Code is correct for GET International Scheduled Payment Consents");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation"))!=null, 
	"Mandatory Block ExchangeRateInformation is present"+iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency"))!=null, 
	"Mandatory field UnitCurrency is present in ExchangeRateInformation block "+ iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.UnitCurrency"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ExchangeRate"))!=null, 
	"Mandatory field ExchangeRate is present in ExchangeRateInformation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ExchangeRate"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.RateType"))!=null, 
	"Optional field RateType is present in ExchangeRateInformation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.RateType"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ContractIdentification"))!=null, 
	"Optional field ContractIdentification is present in ExchangeRateInformation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.ContractIdentification"));
	TestLogger.logBlankLine();
	testVP.testResultFinalize();

}
}	

