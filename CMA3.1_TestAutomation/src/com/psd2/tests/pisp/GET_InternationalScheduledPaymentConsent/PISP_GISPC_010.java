package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without token value OR key-value pair into Authorization (Access Token) header
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GISPC_010 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GISPC_010() throws Throwable{			
	TestLogger.logStep("[Step 1] : Generate Consent Id");
	consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, true, false);
	TestLogger.logBlankLine();
	
	TestLogger.logStep("[Step 2] : All the above steps would remains same where request is having Blank value of Authorization header");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get("consentId"));
	iScheduledPayment.setHeadersString("Authorization:Bearer " + " ");
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();		
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"401", 
	  "Response Code is correct for International Scheduled Payment Consent URI when no token value or key value pair is given into Authorization (Access Token) header");
	
	TestLogger.logStep("[Step 3] : Verification of request without Authorization key in the header");
	restRequest.setURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get("consentId"));
    iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get(consentId));
    String requestBody=iScheduledPayment.genRequestBody();
    restRequest.setHeadersString("Content-Type:application/json, x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-fapi-interaction-id:"+PropertyUtils.getProperty("inter_id")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json");
	restRequest.setRequestBody(requestBody);
	restRequest.setMethod("GET");
	restRequest.submit();
	
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"401", 
		"Response Code is correct for GET International Scheduled Payment Consent URI when no token value or key value pair is given into Authorization (Access Token) header");
	TestLogger.logBlankLine();
	testVP.testResultFinalize();
}
}
