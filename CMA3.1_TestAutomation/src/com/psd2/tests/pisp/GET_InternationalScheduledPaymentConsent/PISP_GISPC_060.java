package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY RateType block where Request has sent successfully 
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GISPC_060 extends TestBase{ 
API_E2E_Utility apiUtility = new API_E2E_Utility();
@Test
public void m_PISP_GISPC_060() throws Throwable{ 
	TestLogger.logStep("[Step 1] : Generate Consent Id");
	consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, true, false);
	TestLogger.logBlankLine();	
	TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get("consentId"));
	iScheduledPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();
	
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
			"Response Code is correct for GET International Scheduled Payment Consents");
    testVP.verifyStringEquals(iScheduledPayment.getURL(),apiConst.iScheduledPaymentConsent_endpoint+"/"+consentDetails.get("consentId"), 
     "URI for GET International Schedule payment consent request is as per open banking standard");
    testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType"))!=null,
     "RateType field under ExchangeRateInformation block is present in Get International Schedule Payment Consent response body "+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType"))); 
    testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType")).equals("Actual")||
      (iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType")).equals("Agreed")||
      (iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType")).equals("Indicative"),
         "Mandatory field RateType is present in ExchangeRateInformation block and is not null");
      TestLogger.logBlankLine();
      testVP.testResultFinalize();
}
}