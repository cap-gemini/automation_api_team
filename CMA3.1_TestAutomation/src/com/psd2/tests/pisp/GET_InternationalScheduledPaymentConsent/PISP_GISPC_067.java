package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the value of OPTIONAL CreditorAccount Block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GISPC_067 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GISPC_067() throws Throwable{	
	TestLogger.logStep("[Step 1] : Generate Consent Id");
	consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, true, false);
	TestLogger.logBlankLine();	
			
	TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get("consentId"));
	iScheduledPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
	"Response Code is correct for GET International Scheduled Payment Consents");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount"))!=null, 
	"Mandatory field SchemeName is present in CreditorAccount block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName"))!=null, 
	"Mandatory field SchemeName is present in CreditorAccount block "+ iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.Identification"))!=null, 
	"Mandatory field Identification is present in CreditorAccount block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.Identification"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.Name"))!=null, 
	"Optional field Name is present in CreditorAccount block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.Name"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SecondaryIdentification"))!=null, 
	"Optional field SecondaryIdentification is present in CreditorAccount block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SecondaryIdentification"));
	TestLogger.logBlankLine();
	testVP.testResultFinalize();

}
}	

