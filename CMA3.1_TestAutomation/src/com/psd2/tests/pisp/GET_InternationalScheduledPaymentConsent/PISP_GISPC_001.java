package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of International Scheduled Payment Consents URI that should be as per Open Banking Standard 
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class PISP_GISPC_001 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GISPC_001() throws Throwable{	
    TestLogger.logStep("[Step 1] : Generate Consent Id");
	consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, true, false);
	TestLogger.logBlankLine();	
	
	TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get("consentId"));
	iScheduledPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
	"Response Code is correct for Post International Scheduled Payment Consents");
	testVP.verifyStringEquals(iScheduledPayment.getURL(),apiConst.iScheduledPaymentConsent_endpoint+"/"+consentDetails.get("consentId"),
	"POST International Scheduled Payment Consents URI is as per CMA Compliance");
	
	testVP.verifyTrue(SignatureUtility.verifySignature(iScheduledPayment.getResponseString(), iScheduledPayment.getResponseHeader("x-jws-signature")), 
			"Response that created successfully with HTTP Code 201 MUST be digitally signed");
	
	TestLogger.logBlankLine();	
	testVP.testResultFinalize();
	
	}
}
