package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GISPC_025 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GISPC_025() throws Throwable{	
	TestLogger.logStep("[Step 1] : Generate Consent Id");
	consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, true, false);
	TestLogger.logBlankLine();	
	
	TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get("consentId"));
	iScheduledPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();
	
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
	"Response Code is correct for GET International Scheduled Payment Consents");
	testVP.verifyStringEquals(iScheduledPayment.getURL(),apiConst.iScheduledPaymentConsent_endpoint+"/"+consentDetails.get("consentId"), 
	"URI for GET International Schedule payment consent request is as per open banking standard");
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ConsentId"))!=null,
	"ConsentId field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ConsentId")));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.CreationDateTime"))!=null,
	"CreationDateTime field under Data is present in Get International schedule Payment Consent response body"+(iScheduledPayment.getResponseValueByPath("Data.CreationDateTime")));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.StatusUpdateDateTime"))!=null,
	"StatusUpdateDateTime field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.StatusUpdateDateTime")));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Status"))!=null,
	"Status field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Status")));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.CutOffDateTime"))!=null,
	"CutOffDateTime field under Data is present in Get Domestic Payment Consent response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.CutOffDateTime")));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExpectedExecutionDateTime"))!=null,
	"ExpectedExecutionDateTime field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ExpectedExecutionDateTime")));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExpectedSettlementDateTime"))!=null,
	"ExpectedSettlementDateTime field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ExpectedSettlementDateTime")));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Charges"))!=null,
	"Charges field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Charges")));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation"))!=null,
	"Initiation field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation")));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Authorisation"))!=null,
	"Authorisation field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Authorisation")));
	TestLogger.logBlankLine();
	testVP.testResultFinalize();
	
	}
}
