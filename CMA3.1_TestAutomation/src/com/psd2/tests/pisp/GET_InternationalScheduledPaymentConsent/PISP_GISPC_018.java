package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with BLANK or Invalid value of OPTIONAL x-fapi-auth-date header
 * @author Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GISPC_018 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GISPC_018() throws Throwable{	
	TestLogger.logStep("[Step 1] : Generate Consent Id");
	consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, true, false);
	TestLogger.logBlankLine();
	
	TestLogger.logStep("[Step 2] : All the above steps would remains same where request is having Invalid value of OPTIONAL  x-fapi-auth-date header");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint + "/"+ apiConst.otherTPP_paymentConsentId);
	iScheduledPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
	iScheduledPayment.addHeaderEntry("x-fapi-customer-last-logged-time", "FSTYWFUD^%^");
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();		
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
	"Response Code is correct for International Scheduled Payment Consent URI when invalid x-fapi-auth-date is passed");
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
	"Response Error Code is correct");
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Errors[0].Message")),"Invalid value '[FSTYWFUD^%^]' for header x-fapi-customer-last-logged-time. Invalid value 'FSTYWFUD^%^'. Expected ^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), \\d{2} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \\d{4} \\d{2}:\\d{2}:\\d{2} (GMT|UTC)$", 
	"Error Message are matched");
	
	
	TestLogger.logStep("[Step 3] : All the above steps would remains same where request is having Blank value of OPTIONAL x-fapi-auth-date header");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint + "/"+consentDetails.get("consentId"));
	iScheduledPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
	iScheduledPayment.addHeaderEntry("x-fapi-customer-last-logged-time", "");
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();
	
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()), "400", 
	"Response Code is correct for GET International Scheduled Payment Consent URI when request is having Blank value of OPTIONAL x-fapi-auth-date is passed");                                              
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
         "Response Error Code is Correct");
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Errors[0].Message")), "Invalid value '[]' for header x-fapi-customer-last-logged-time. Invalid value ''. Expected ^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), \\d{2} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \\d{4} \\d{2}:\\d{2}:\\d{2} (GMT|UTC)$", 
			"Error Message are matched");
	
	TestLogger.logBlankLine();
	testVP.testResultFinalize();

	}
}