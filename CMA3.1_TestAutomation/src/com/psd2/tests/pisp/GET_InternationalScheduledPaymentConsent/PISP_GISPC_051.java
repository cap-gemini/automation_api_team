package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the values into Optional ChargeBearer Field Where request has Sent Successfully and retuened a HTTP Code 201 Created
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GISPC_051 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GISPC_051() throws Throwable{	
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
    createClientCred.submit();
    testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
    "Response Code is correct for client credetials");
    cc_token = createClientCred.getAccessToken();
    TestLogger.logVariable("AccessToken : " + cc_token);
    TestLogger.logBlankLine();
    
    TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
    iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
    iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
    String requestBody=iScheduledPayment.genRequestBody().replace("\"Purpose\": \"Test\",","\"Purpose\":\"Test\", \"ChargeBearer\":\"Shared\",");
    iScheduledPayment.submit(requestBody);
    testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201",
    "Response Code is correct for Post International Scheduled Payment Consents");
    
    TestLogger.logStep("[Step 3] :GET International Scheduled Payment Consent SetUp....");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+iScheduledPayment.getConsentId());
	iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
	"Response Code is correct for GET International Scheduled Payment Consents");
	
	testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer")),iScheduledPayment._chargeBearer, 
			"Mandatory field ChargeBearer is present in Initiation block");
			TestLogger.logBlankLine();
			testVP.testResultFinalize();

}
	}	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


