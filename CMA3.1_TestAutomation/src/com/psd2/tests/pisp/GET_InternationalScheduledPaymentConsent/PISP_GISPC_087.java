package com.psd2.tests.pisp.GET_InternationalScheduledPaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the values into OPTIONAL Data/Initiation/CreditorAgent/PostalAddress block
 * @author : Snehal Chaudhari
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GISPC_087 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GISPC_087() throws Throwable{	
	TestLogger.logStep("[Step 1] : Generate Consent Id");
	consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, true, false);
	TestLogger.logBlankLine();	
	TestLogger.logStep("[Step 2] : International Scheduled Payment Consent SetUp....");
	iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+ consentDetails.get("consentId"));
	iScheduledPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
	iScheduledPayment.setMethod("GET");
	iScheduledPayment.submit();
	
    testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType"))!=null , 
	"AddressType Field is present "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Department"))!=null , 
	"Department field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Department"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.SubDepartment"))!=null , 
	"SubDepartment field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.SubDepartment"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.StreetName"))!=null , 
	"StreetName field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.StreetName"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.BuildingNumber"))!=null , 
	"BuildingNumber field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.BuildingNumber"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.PostCode"))!=null , 
	"PostCode field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.PostCode"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.TownName"))!=null , 
	"TownName field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.TownName"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.CountrySubDivision"))!=null , 
	"CountrySubDivision field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.CountrySubDivision"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Country"))!=null , 
	"Country field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Country"));
	testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressLine"))!=null , 
	"AddressLine field is present under PostalAddress block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressLine"));
	TestLogger.logBlankLine();
	testVP.testResultFinalize();

}
}	

