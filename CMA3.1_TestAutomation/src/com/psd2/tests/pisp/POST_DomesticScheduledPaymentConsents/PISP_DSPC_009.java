package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request with same or different payload with same x-idempotency-key & TPP where previously a request has been set-up successfully 
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_009 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_009() throws Throwable{	
		String idemPotencyKey=PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3);
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
				
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Domestic Schedule Payment Consent SetUp....");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:" +idemPotencyKey);
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", "Response Code is correct for Domestic Payment Consent");
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-3] : Domestic Schedule Payment Consent SetUp....");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setCrAccountName("Credit");
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:" + idemPotencyKey);
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", "Response Code is correct for Domestic Payment Consent");
			
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Resource.ConsentMismatch", 
					"Error code for the response is correct i.e. '"+dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Errors[0].Message").equals("Payload comparison failed with the consent resource"), 
					"Message for error code is '"+dspConsent.getResponseNodeStringByPath("Errors[0].Message")+"'");
			
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();
}
}
