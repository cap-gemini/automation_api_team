package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the request with Null or Invalid  value of x-fapi-interaction-id header. 
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_026 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_026() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] :Verification of the request with Null value of x-fapi-interaction-id header.");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.addHeaderEntry("x-fapi-interaction-id", "");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct when  x-fapi-interaction-id header is removed for Domestic Schedule Payment Consent SetUp");
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-3] :Verification of the request with Invalid value of x-fapi-interaction-id header.");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.addHeaderEntry("x-fapi-interaction-id", "1234");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", "Response Code is correct when  x-fapi-interaction-id header is removed for Domestic Schedule Payment Consent SetUp");
			
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
					"Error code for the response is correct i.e. '"+dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].Message"), "invalid headers found in request",
					"Message for error code is correct i.e.  '"+dspConsent.getResponseNodeStringByPath("Errors[0].Message")+"'");
			
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
			
			
	}
	}
