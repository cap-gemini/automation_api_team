package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = any.bank.Scheme2
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_088 extends TestBase {	
	
	@Test
	public void m_PISP_DSPC_88() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = any.bank.Scheme2");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
		dspConsent.setDrAccountSchemeName("any.bank.scheme2");
		dspConsent.setDrAccountIdentification("scheme2");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic scheduled Payment Consent URI");
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName"), "any.bank.scheme2", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification"), "scheme2", 
				"Identification field value is correct");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
