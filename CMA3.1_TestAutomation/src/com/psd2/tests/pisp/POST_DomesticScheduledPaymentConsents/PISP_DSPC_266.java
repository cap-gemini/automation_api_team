package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY ChargeBearer field where Request has sent successfully and returned a HTTP Code 201 Created if InstructedAmount/Currency is USD
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_266 extends TestBase {	
	
	@Test
	public void m_PISP_DSPC_264() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY ChargeBearer field where Request has sent successfully and returned a HTTP Code 201 Created if InstructedAmount/Currency is USD");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
		dspConsent.setCurrency("USD");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic scheduled Payment Consent URI");
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Data.Status")), "Rejected", 
				"Status field value is rejected when currency is sent as USD in request");
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Data.Charges[0].ChargeBearer")), "BorneByCreditor", 
				"ChargeBearer field value is BorneByCreditor when currency is sent as USD in request");
				
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();
		
			}
}
