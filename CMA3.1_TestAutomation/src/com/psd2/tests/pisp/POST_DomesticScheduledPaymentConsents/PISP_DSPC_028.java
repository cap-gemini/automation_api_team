package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request where MANDATORY x-jws-signature header haven't sent 
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_028 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_028() throws Throwable{	
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
        
	    TestLogger.logStep("[Step 1-2] : Verification of the request where MANDATORY x-jws-signature header haven't sent");
	    restRequest.setURL(apiConst.dspConsent_endpoint);
	    dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
	    restRequest.setHeadersString("Authorization:Bearer "+cc_token+", Content-Type:application/json,  x-fapi-interaction-id:"+PropertyUtils.getProperty("inter_id")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json, x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		restRequest.setRequestBody(dspConsent.genRequestBody());
		restRequest.setMethod("POST");
		restRequest.submit();
		
	    testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
	    		"Response Code is correct for Post Domestic Scheduled Payment Consent");
	    
	    testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Missing", 
				"Error code for the response is correct i.e. '"+restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Errors[0].Message").equals("Required header x-jws-signature not specified"), 
				"Message for error code is '"+restRequest.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
        
		testVP.testResultFinalize();		
	}
	}
