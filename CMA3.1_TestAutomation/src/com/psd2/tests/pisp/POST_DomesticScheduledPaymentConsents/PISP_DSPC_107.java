package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the value of MANDATORY CreditorAccount/Identification field when SchemeName = UK.OBIE.PAN/PAN
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_107 extends TestBase {	
	
	@Test
	public void m_PISP_DSPC_107() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY CreditorAccount/Identification field when SchemeName = UK.OBIE.PAN/PAN");
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
		dspConsent.setCrAccountSchemeName("UK.OBIE.PAN");
		dspConsent.setCrAccountIdentification("1234567890123asdf@$#@#$");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic scheduled Payment Consent URI");
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Unsupported.AccountIdentifier",
				"Error code id correct i.e. "+dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"));
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].Message"), "invalid account identifier provided corresponding to scheme",
				"Error code id correct i.e. "+dspConsent.getResponseNodeStringByPath("Errors[0].Message"));
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
			}
}

