package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values of MANDATORY DebtorAccount/SchemeName field having OB defined values
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_069 extends TestBase {	
	
	@Test
	public void m_PISP_DSPC_069() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values of MANDATORY DebtorAccount/SchemeName field having OB defined values");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
		dspConsent.submit();
		

		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.SortCodeAccountNumber")
				|| dspConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.IBAN")
				|| dspConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.PAN")
				|| dspConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.Paym")
				|| dspConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.BBAN")
				|| dspConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme1")
				|| dspConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme2"), 
				"MANDATORY DebtorAccount/SchemeName field has OB defined values");
		
		testVP.testResultFinalize();		
	}
}

