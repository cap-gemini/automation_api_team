package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the request with valid value into Authorization (Access Token) header but generated by different TPP. 
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_020 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_020() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
				
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Verification of the request with valid value into Authorization (Access Token) header but generated by different TPP.");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+apiConst.othertpp_cc_access_token);
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"401", "Response Code is correct for expired token value into Authorization (Access Token) header for Domestic Schedule Payment Consent SetUp");
			
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();
}
}
