package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of domestic-scheduled-payment-consents URL having older version with 3.0 version payload 
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Invalid"})
public class PISP_DSPC_006 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_006() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Domestic-scheduled-payment-consents URL having older version with 3.0 version payload");
			
			//dspConsent.setBaseURL(apiConst.backcomp_dspc_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			String requestBody_Submission=dspConsent.genRequestBody().replace("\"CountrySubDivision\":[ \""+dspConsent._riskCountrySubDivision+"\"],", "");
			dspConsent.submit(requestBody_Submission);
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"404", 
					"Response Code is correct for Domestic-scheduled-payment-consents URL having older version with 3.0 version payload");
			
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
}
}
