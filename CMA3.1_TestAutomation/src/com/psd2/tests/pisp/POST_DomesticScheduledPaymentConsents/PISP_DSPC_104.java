package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the value of MANDATORY CreditorAccount/Identification field when SchemeName = UK.OBIE.IBAN/IBAN
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_104 extends TestBase {	
	
	@Test
	public void m_PISP_DSPC_104() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY CreditorAccount/Identification field when SchemeName = UK.OBIE.IBAN");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
		dspConsent.setCrAccountSchemeName("UK.OBIE.IBAN");
		dspConsent.setCrAccountIdentification("IE85BOFI90120412345679");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic scheduled Payment Consent URI");
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SchemeName"), "UK.OBIE.IBAN", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.Identification"), "IE85BOFI90120412345679", 
				"Identification field value is correct");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the value of MANDATORY CreditorAccount/Identification field when SchemeName = IBAN");
		
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		dspConsent.setCrAccountSchemeName("IBAN");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic scheduled Payment Consent URI");
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SchemeName"), "UK.OBIE.IBAN", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.Identification"), "IE85BOFI90120412345679", 
				"Identification field value is correct");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

