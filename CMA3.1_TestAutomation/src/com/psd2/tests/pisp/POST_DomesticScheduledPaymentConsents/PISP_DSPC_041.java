package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the value of MANDATORY Initiation/InstructionIdentification field
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_041 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_041() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
				
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Verification of MANDATORY Initiation/InstructionIdentification field while Domestic Scheduled Payment Consent SetUp");
			
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", "Response Code is correct for Domestic  scheduled Payment Consent");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.InstructionIdentification")).isEmpty(), 
					"Length of InstructionIdentification is not more than 35 characters");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Initiation.InstructionIdentification").length()<=35, 
					"Length of InstructionIdentification is not more than 35 characters");
			TestLogger.logBlankLine();
			testVP.testResultFinalize();	
	}
	}
