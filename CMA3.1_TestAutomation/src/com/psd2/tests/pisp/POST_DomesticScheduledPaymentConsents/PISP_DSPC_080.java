package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = UK.OBIE.PAN/PAN
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_080 extends TestBase {	
	
	@Test
	public void m_PISP_DSPC_80() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = UK.OBIE.PAN/PAN");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
		dspConsent.setDrAccountSchemeName("UK.OBIE.PAN");
		dspConsent.setDrAccountIdentification("123456789011211314");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Scheduled Payment Consent URI");
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName"), "UK.OBIE.PAN", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification"), "123456789011211314", 
				"Identification field value is correct");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName =PAN");
		
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		dspConsent.setDrAccountSchemeName("PAN");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic scheduled Payment Consent URI");
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName"), "UK.OBIE.PAN", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification"), "123456789011211314", 
				"Identification field value is correct");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
