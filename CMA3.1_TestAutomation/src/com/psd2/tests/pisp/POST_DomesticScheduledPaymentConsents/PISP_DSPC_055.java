package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values of MANDATORY InstructedAmount/Amount field
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_055 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_055() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Verification of the values of MANDATORY InstructedAmount/Amount field");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic  scheduled Payment Consent");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.InstructedAmount.Amount")).isEmpty(), 
					"Amount field under InstructedAmount is present and is not null");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Initiation.InstructedAmount.Amount").split("\\.")[0].length()<=13
					&& dspConsent.getResponseNodeStringByPath("Data.Initiation.InstructedAmount.Amount").split("\\.")[1].length()<=5, 
					"Amount field under InstructedAmount is present and is not null");
			
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();
	}
	}
