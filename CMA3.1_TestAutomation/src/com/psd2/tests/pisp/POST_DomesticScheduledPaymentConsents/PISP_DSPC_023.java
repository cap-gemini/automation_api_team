package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without MANDATORY x-fapi-financial-id header 
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_023 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_023() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
				
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] :Verification of the request without MANDATORY x-fapi-financial-id header");
			
			restRequest.setURL(apiConst.dspConsent_endpoint);
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			String requestBody=dspConsent.genRequestBody();
			restRequest.setHeadersString("Authorization:Bearer "+cc_token+", Content-Type:application/json, x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-fapi-interaction-id:"+PropertyUtils.getProperty("inter_id")+", Accept:application/json, x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
			restRequest.setRequestBody(requestBody);
			restRequest.setMethod("POST");	
			restRequest.submit();
			
			testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
					"Response Code is correct x-fapi-financial-id header is removed while Domestic Schedule Payment Consent SetUp");
			
			testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Missing", 
					"Response Error Code is correct");
			testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].Message")),"Required header x-fapi-financial-id not specified",
					"Error message is correct");
			
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();
	}
}