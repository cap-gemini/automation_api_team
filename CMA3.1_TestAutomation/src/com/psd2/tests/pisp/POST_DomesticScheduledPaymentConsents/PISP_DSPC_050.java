package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/LocalInstrument field having OB defined values
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_050 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_050() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.BACS");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.setLocalInstrument("UK.OBIE.BACS");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BACS"),
						"LocalInstrument field value is as per OB defined values");		
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 3] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.CHAPS");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
			dspConsent.setLocalInstrument("UK.OBIE.CHAPS");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.CHAPS"),
						"LocalInstrument field value is as per OB defined values");		
			TestLogger.logBlankLine(); 
			
			TestLogger.logStep("[Step 4] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.FPS");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
			dspConsent.setLocalInstrument("UK.OBIE.FPS");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.FPS"),
						"LocalInstrument field value is as per OB defined values");		
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 5] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.SWIFT");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
			dspConsent.setLocalInstrument("UK.OBIE.SWIFT");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Unsupported.LocalInstrument", 
					"Error code for the response is correct i.e. '"+dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Errors[0].Message").equals("invalid local instrument provided"), 
					"Message for error code is '"+dspConsent.getResponseNodeStringByPath("Errors[0].Message")+"'");		
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 6] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.BalanceTransfer");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
			dspConsent.setLocalInstrument("UK.OBIE.BalanceTransfer");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BalanceTransfer"),
						"LocalInstrument field value is as per OB defined values");		
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 7] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.MoneyTransfer");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
			dspConsent.setLocalInstrument("UK.OBIE.MoneyTransfer");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.MoneyTransfer"),
						"LocalInstrument field value is as per OB defined values");		
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 8] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.Paym");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
			dspConsent.setLocalInstrument("UK.OBIE.Paym");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Paym"),
						"LocalInstrument field value is as per OB defined values");		
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 9] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.Euro1");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
			dspConsent.setLocalInstrument("UK.OBIE.Euro1");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Unsupported.LocalInstrument", 
					"Error code for the response is correct i.e. '"+dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Errors[0].Message").equals("invalid local instrument provided"), 
					"Message for error code is '"+dspConsent.getResponseNodeStringByPath("Errors[0].Message")+"'");		
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 10] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.SEPACreditTransfer");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
			dspConsent.setLocalInstrument("UK.OBIE.SEPACreditTransfer");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Unsupported.LocalInstrument", 
					"Error code for the response is correct i.e. '"+dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Errors[0].Message").equals("invalid local instrument provided"), 
					"Message for error code is '"+dspConsent.getResponseNodeStringByPath("Errors[0].Message")+"'");		
			TestLogger.logBlankLine();
				
			TestLogger.logStep("[Step 11] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.Link");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
			dspConsent.setLocalInstrument("UK.OBIE.Link");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Link"),
						"LocalInstrument field value is as per OB defined values");		
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 12] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.Target2");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
			dspConsent.setLocalInstrument("UK.OBIE.Target2");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Unsupported.LocalInstrument", 
					"Error code for the response is correct i.e. '"+dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Errors[0].Message").equals("invalid local instrument provided"), 
					"Message for error code is '"+dspConsent.getResponseNodeStringByPath("Errors[0].Message")+"'");		
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 13] : Domestic Scheduled Payment Consents with Local Instrument as : UK.OBIE.SEPAInstantCreditTransfer");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
			dspConsent.setLocalInstrument("UK.OBIE.SEPAInstantCreditTransfer");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled Payment Consent URI");
			
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Unsupported.LocalInstrument", 
					"Error code for the response is correct i.e. '"+dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Errors[0].Message").equals("invalid local instrument provided"), 
					"Message for error code is '"+dspConsent.getResponseNodeStringByPath("Errors[0].Message")+"'");		
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();	
	}
}