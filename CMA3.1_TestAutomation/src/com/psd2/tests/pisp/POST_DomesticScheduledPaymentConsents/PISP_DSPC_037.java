package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of Permission 
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_037 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_037() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
				
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] :Verification of Permission while Domestic Scheduled Payment Consent SetUp");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.setMethod("POST");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", "Response Code is correct for Domestic Payment Consent");
			
			testVP.verifyStringEquals(dspConsent.getURL(),apiConst.dspConsent_endpoint, 
					"URL for POST Domestic Scheduled Payment consent is as per open banking standard");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data")).isEmpty(), 
					"Mandatory field Data block");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Permission").equals("Create"), 
					"Mandatory field permission is present under Data block and set as Create");
			
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();

			
	}
	}
