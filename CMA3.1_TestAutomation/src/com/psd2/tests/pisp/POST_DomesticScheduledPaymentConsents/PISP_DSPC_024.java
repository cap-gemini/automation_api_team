package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Comparison of x-fapi-interaction-id value sent in the request with the response header that created successfully with HTTP Code 201 Created 
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_024 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_024() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Comparison of x-fapi-interaction-id value sent in the request with the response header that created successfully with HTTP Code 201 Created");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.addHeaderEntry("x-fapi-interaction-id", PropertyUtils.getProperty("inter_id"));
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Payment SetUp URI");
			testVP.verifyStringEquals(dspConsent.getResponseHeader("x-fapi-interaction-id"),PropertyUtils.getProperty("inter_id"), 
					"x-fapi-interaction-id value in the response header is correct and matching with one sent in the request.");
			
		
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();
			
			
	}
	}
