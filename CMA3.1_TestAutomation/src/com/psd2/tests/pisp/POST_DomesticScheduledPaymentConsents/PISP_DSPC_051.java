package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description :Verification of RequestedExecutionDateTime
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_051 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_051() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Verification of RequestedExecutionDateTime");
			
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic  scheduled Payment Consent");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.RequestedExecutionDateTime")).isEmpty(), 
					"RequestedExecutionDateTime should be ISODateTime Format");
			
			testVP.verifyTrue(Misc.verifyDateTimeFormat(dspConsent.getResponseNodeStringByPath("Data.Initiation.RequestedExecutionDateTime").split("T")[0], "yyyy-MM-dd") && 
					Misc.verifyDateTimeFormat(dspConsent.getResponseNodeStringByPath("Data.Initiation.RequestedExecutionDateTime").split("T")[1], "HH:mm:ss+00:00"), 
					"RequestedExecutionDateTime under Initiation block is as per expected format");
			
			TestLogger.logBlankLine();
			testVP.testResultFinalize();	
	}
}
