package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;

/**
 * Class Description : Verification of CMA compliance for POST Domestic Scheduled Payment Consents URI 
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class PISP_DSPC_001 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_001() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
				
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Domestic Scheduled Payment Consent SetUp....");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
		    		"Response Code is correct for Post Domestic Scheduled Payment Consents");
			
		    testVP.verifyStringEquals(apiConst.dspConsent_endpoint,dspConsent.getURL(), 
		    		"POST Domestic Scheduled Payment Consents URI is as per CMA Compliance");
		    
		    testVP.verifyTrue(SignatureUtility.verifySignature(dspConsent.getResponseString(), dspConsent.getResponseHeader("x-jws-signature")), 
					"Response that created successfully with HTTP Code 201 MUST be digitally signed");
		    
		   TestLogger.logBlankLine();	
			
			testVP.testResultFinalize();
}
}
