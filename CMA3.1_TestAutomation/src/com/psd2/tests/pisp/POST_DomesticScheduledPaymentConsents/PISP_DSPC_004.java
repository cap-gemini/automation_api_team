package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through POST method with mandatory and optional fields. 
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_004 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_004() throws Throwable{	
		
TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Checking the request (UTF-8 character encoded) status through POST method with mandatory and optional fields");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic scheduled Payment Consent URI");
		
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data")!=null,
				"Mandatory field Data is present in response and is not empty");
		
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Links")!=null, 
				"Mandatory field Links is present in response and is not empty");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Links.Self")).isEmpty(), 
				"Mandatory field Self is present in response and is not empty");
		
		testVP.verifyStringEquals(dspConsent.getResponseHeader("Content-Type"), "application/json;charset=utf-8", 
				"Response is UTF-8 character encoded");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
