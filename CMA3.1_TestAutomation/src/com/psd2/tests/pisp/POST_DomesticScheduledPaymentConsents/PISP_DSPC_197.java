package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY ChargeBearer field where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_197 extends TestBase {	
	
	@Test
	public void m_PISP_DSPC_197() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY ChargeBearer field where Request has sent successfully and returned a HTTP Code 201 Created");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic scheduled Payment Consent URI");
		
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Charges[0].ChargeBearer").equals("FollowingServiceLevel") 
				|| dspConsent.getResponseValueByPath("Data.Charges[0].ChargeBearer").equals("BorneByCreditor") 
				|| dspConsent.getResponseValueByPath("Data.Charges[0].ChargeBearer").equals("BorneByDebtor") 
				|| dspConsent.getResponseValueByPath("Data.Charges[0].ChargeBearer").equals("Shared"), 
				"ChargeBearer under Charges block is correct i.e. "+dspConsent.getResponseValueByPath("Data.Charges[0].ChargeBearer"));
				
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();
		
			}
}
