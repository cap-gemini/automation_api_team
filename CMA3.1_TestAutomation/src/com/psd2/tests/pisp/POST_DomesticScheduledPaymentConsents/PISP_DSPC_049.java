package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/LocalInstrument field having length variation
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_049 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_049() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
				
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Verification of the value of MANDATORY Initiation/InstructionIdentification field having length variation");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.setLocalInstrument("UK.OBIE.LinkUK.OBIE.LinkUK.OBIE.LinkUK.OBIE.Link");
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic scheduled Payment Consent URI");
			
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
					"Error code for the response is correct i.e. '"+dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			
			testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 35 for field [LocalInstrument], but got [UK.OBIE.LinkUK.OBIE.LinkUK.OBIE.LinkUK.OBIE.Link]"), 
					"Message for error code is '"+dspConsent.getResponseNodeStringByPath("Errors[0].Message")+"'");
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();	
	}
}