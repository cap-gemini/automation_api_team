package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation block
 * @author : Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_038 extends TestBase{	
	
	@Test
	public void m_PISP_DSPC_038() throws Throwable{	
		
		 TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
				
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] :Verification of the values into MANDATORY Data/Initiation block is present while Domestic Scheduled Payment Consent SetUp");
			
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", "Response Code is correct for Domestic  scheduled Payment Consent");
			
			testVP.verifyStringEquals(dspConsent.getURL(),apiConst.dspConsent_endpoint, 
					"URL for POST Domestic Scheduled Payment consent is as per open banking standard");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data")).isEmpty(), 
					"Mandatory field Data block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation")).isEmpty(), 
					"Mandatory field Initiation block is present under Data block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.InstructionIdentification")).isEmpty(), 
					"Mandatory field InstructionIdentification  is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.EndToEndIdentification")).isEmpty(), 
					"Optional field EndToEndIdentification is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument")).isEmpty(), 
					"Optional field LocalInstrument is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.RequestedExecutionDateTime")).isEmpty(), 
					"Mandatory field RequestedExecutionDateTime is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.InstructedAmount")).isEmpty(), 
					"Mandatory field InstructedAmount is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.InstructedAmount.Amount")).isEmpty(), 
					"Mandatory field InstructedAmount/Amount is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.InstructedAmount.Currency")).isEmpty(), 
					"Mandatory field InstructedAmount/Currency is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount")).isEmpty(), 
					"Mandatory field CreditorAccount is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SchemeName")).isEmpty(), 
					"Mandatory field CreditorAccount/SchemeName is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.Identification")).isEmpty(), 
					"Mandatory field CreditorAccount/Identification is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.Name")).isEmpty(), 
					"Mandatory field CreditorAccount/Name is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SecondaryIdentification")).isEmpty(), 
					"Optional field CreditorAccount/SecondaryIdentification is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress")).isEmpty(), 
					"Optional field CreditorPostalAddress is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressType")).isEmpty(), 
					"Optional field CreditorPostalAddress/AddressType is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.Department")).isEmpty(), 
					"Optional field CreditorPostalAddress/Department is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.SubDepartment")).isEmpty(), 
					"Optional field CreditorPostalAddress/SubDepartment is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.StreetName")).isEmpty(), 
					"Optional field CreditorPostalAddress/StreetName is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.BuildingNumber")).isEmpty(), 
					"Optional field CreditorPostalAddress/BuildingNumber is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.PostCode")).isEmpty(), 
					"Optional field CreditorPostalAddress/PostCode is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.TownName")).isEmpty(), 
					"Optional field CreditorPostalAddress/TownName is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.CountrySubDivision")).isEmpty(), 
					"Optional field CreditorPostalAddress/CountrySubDivision is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.Country")).isEmpty(), 
					"Optional field CreditorPostalAddress/Country is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressLine")).isEmpty(), 
					"Optional field CreditorPostalAddress/AddressLine is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.RemittanceInformation")).isEmpty(), 
					"Optional field RemittanceInformation is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.RemittanceInformation.Unstructured")).isEmpty(), 
					"Optional field RemittanceInformation/Unstructured is present under Initiation block");
			
			testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.RemittanceInformation.Reference")).isEmpty(), 
					"Optional field RemittanceInformation/Reference is present under Initiation block");
			
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();

			
	}
	}
