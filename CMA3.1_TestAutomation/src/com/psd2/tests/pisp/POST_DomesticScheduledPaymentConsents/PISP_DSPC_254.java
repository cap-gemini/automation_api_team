package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of CompletionDateTime field  for without Seconds, without milliseconds and with 3 digit milliseconds  
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_254 extends TestBase {	
	
	@Test
	public void m_PISP_DSPC_254() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of date and time for below mentioend dates fields without giving seconds");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
		dspConsent.setCompletionDateTime("2021-03-20T06:06");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic  scheduled Payment Consent URI when CompletionDateTime field value is without seconds");
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].Message")),"Error validating JSON. Error: - Provided value 2021-03-20T06:06 is not compliant with the format datetime provided in rfc3339 for CompletionDateTime",
				"Error message is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of date and time for CompletionDateTime field without giving seconds");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dspConsent.setCompletionDateTime("2021-03-20T06:06Z");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic  scheduled Payment Consent URI when CompletionDateTime field value is without seconds");
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].Message")),"Error validating JSON. Error: - Provided value 2021-03-20T06:06Z is not compliant with the format datetime provided in rfc3339 for CompletionDateTime",
				"Error message is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of date and time for CompletionDateTime field without giving seconds");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dspConsent.setCompletionDateTime("2021-03-20T06:06+00:00");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic  scheduled Payment Consent URI when CompletionDateTime field value is without seconds");
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].Message")),"Error validating JSON. Error: - Provided value 2021-03-20T06:06+00:00 is not compliant with the format datetime provided in rfc3339 for CompletionDateTime",
				"Error message is correct");
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 5] : Verification of date and time for below mentioend dates fields without giving milliseconds");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dspConsent.setCompletionDateTime("2021-03-20T06:06:06");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic  scheduled Payment Consent URI when CompletionDateTime field value is without seconds");
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].Message")),"Error validating JSON. Error: - Provided value 2021-03-20T06:06:06 is not compliant with the format datetime provided in rfc3339 for CompletionDateTime",
				"Error message is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 6] : Verification of date and time for CompletionDateTime field without giving miliseconds");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dspConsent.setCompletionDateTime("2021-03-20T06:06:06Z");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic  scheduled Payment Consent URI when CompletionDateTime field value is without seconds");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 7] : Verification of date and time for CompletionDateTime field without giving miliseconds");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dspConsent.setCompletionDateTime("2021-03-20T06:06:06+00:00");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic  scheduled Payment Consent URI when CompletionDateTime field value is without seconds");
		TestLogger.logStep("[Step 8] : Verification of date and time for below mentioned dates fields  with 3 digit milliseconds");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dspConsent.setCompletionDateTime("2021-03-20T06:06:06.777");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic  scheduled Payment Consent URI when CompletionDateTime field value is without seconds");
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Errors[0].Message")),"Error validating JSON. Error: - Provided value 2021-03-20T06:06:06.777 is not compliant with the format datetime provided in rfc3339 for CompletionDateTime",
				"Error message is correct");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 9] : Verification of date and time for CompletionDateTime field  with 3 digit milliseconds");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dspConsent.setCompletionDateTime("2021-03-20T06:06:06.777Z");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic  scheduled Payment Consent URI when CompletionDateTime field value is without seconds");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 10] : Verification of date and time for CompletionDateTime field  with 3 digit milliseconds");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dspConsent.setCompletionDateTime("2021-03-20T06:06:06.777+00:00");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic  scheduled Payment Consent URI when CompletionDateTime field value is without seconds");
		
		testVP.testResultFinalize();
		
			}
}
