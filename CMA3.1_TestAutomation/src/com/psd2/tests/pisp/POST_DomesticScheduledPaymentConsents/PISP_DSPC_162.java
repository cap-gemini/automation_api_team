package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Risk/DeliveryAddress block.
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_162 extends TestBase {	
	
	@Test
	public void m_PISP_DSPC_162() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Risk/DeliveryAddress block.");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic scheduled Payment Consent URI");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Risk.DeliveryAddress.TownName")).isEmpty(), 
				"Mandatory field TownName is present under DeliveryAddress block");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Risk.DeliveryAddress.Country")).isEmpty(), 
				"Mandatory field Country is present under DeliveryAddress block");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Risk.DeliveryAddress.AddressLine")).isEmpty(), 
				"Optional field AddressLine is present under DeliveryAddress block");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Risk.DeliveryAddress.BuildingNumber")).isEmpty(), 
				"Optional field BuildingNumber is present under DeliveryAddress block");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Risk.DeliveryAddress.StreetName")).isEmpty(), 
				"Optional field StreetName is present under DeliveryAddress block");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Risk.DeliveryAddress.PostCode")).isEmpty(), 
				"Optional field PostCode is present under DeliveryAddress block");
		
		/*testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Risk.DeliveryAddress.CountrySubDivision")).isEmpty(), 
				"Optional field CountrySubDivision is present under DeliveryAddress block");*/
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();
	}
}
