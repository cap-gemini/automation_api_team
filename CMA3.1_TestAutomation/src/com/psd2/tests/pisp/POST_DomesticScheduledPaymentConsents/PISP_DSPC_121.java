package com.psd2.tests.pisp.POST_DomesticScheduledPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/CreditorPostalAddress block
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSPC_121 extends TestBase {	
	
	@Test
	public void m_PISP_DSPC_121() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Data/Initiation/CreditorPostalAddress block");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
		dspConsent.submit();
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressType")).isEmpty(), 
				"Optional field AddressType is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.Department")).isEmpty(), 
				"Optional field Department is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.SubDepartment")).isEmpty(), 
				"Optional field SubDepartment is present under CreditorPostalAddress");
	
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.StreetName")).isEmpty(), 
				"Optional field StreetName is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.BuildingNumber")).isEmpty(), 
				"Optional field BuildingNumber is present under CreditorPostalAddress");

		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.PostCode")).isEmpty(), 
				"Optional field PostCode is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.TownName")).isEmpty(), 
				"Optional field TownName is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.CountrySubDivision")).isEmpty(), 
				"Optional field CountrySubDivision is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.Country")).isEmpty(), 
				"Optional field Country is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressLine")).isEmpty(), 
				"Optional field AddressLine is present under CreditorPostalAddress");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
		
			}
}
