package com.psd2.tests.pisp.GET_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request NOT having any value into OPTIONAL Accept header
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDPC_020 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDPC_020() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request NOT having any value into OPTIONAL Accept header");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"));
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		paymentConsent.addHeaderEntry("Accept", "");
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Payment Consent URI when optional Accept header is passed with an invalid value");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}

