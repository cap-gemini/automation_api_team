package com.psd2.tests.pisp.GET_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Initiation block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDPC_040 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDPC_040() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Initiation block where Request has sent successfully and returned a HTTP Code 200 OK");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"));
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Payment Consent URI");
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.InstructionIdentification"))!=null, 
				"Mandatory field InstructionIdentification is present in Initiation block"+paymentConsent.getResponseValueByPath("Data.Initiation.InstructionIdentification"));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.EndToEndIdentification"))!=null, 
				"Mandatory field EndToEndIdentification is present in Initiation block"+paymentConsent.getResponseValueByPath("Data.Initiation.EndToEndIdentification"));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.InstructedAmount"))!=null,
				"Mandatory field InstructedAmount is present in Initiation block"+paymentConsent.getResponseValueByPath("Data.Initiation.InstructedAmount"));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount"))!=null, 
				"Mandatory field EndToEndIdentification is present in Initiation block"+paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount"));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.LocalInstrument"))!=null,
				"Optional field LocalInstrument is present in Initiation block"+paymentConsent.getResponseValueByPath("Data.Initiation.LocalInstrument"));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.DebtorAccount"))!=null,
				"Optional field LocalInstrument is present in Initiation block"+paymentConsent.getResponseValueByPath("Data.Initiation.DebtorAccount"));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress"))!=null,
				"Optional field CreditorPostalAddress is present in Initiation block"+paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress"));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation"))!=null,
				"Optional field CreditorPostalAddress is present in Initiation block"+paymentConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation"));
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}

