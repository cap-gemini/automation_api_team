package com.psd2.tests.pisp.GET_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/CreditorPostalAddress block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDPC_057 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDPC_057() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Data/Initiation/CreditorPostalAddress block where Request has sent successfully and returned a HTTP Code 200 OK");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"));
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress"))!=null, 
				"Optional field AddressType is present in CreditorPostalAddress block "+ paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress"));
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.AddressType"))!=null, 
				"Optional field AddressType is present in CreditorPostalAddress block "+paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.AddressType"));
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.Department"))!=null, 
				"Optional field Department is present in CreditorPostalAddress block "+ paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.Department"));
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.SubDepartment"))!=null, 
				"Optional field SubDepartment is present in CreditorPostalAddress block "+ paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.SubDepartment"));
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.StreetName"))!=null, 
				"Optional field StreetName is present in CreditorPostalAddress block "+paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.StreetName"));
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.BuildingNumber"))!=null, 
				"Optional field BuildingNumber is present in CreditorPostalAddress block "+paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.BuildingNumber"));
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.PostCode"))!=null, 
				"Optional field PostCode is present in CreditorPostalAddress block "+paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.PostCode"));
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}

