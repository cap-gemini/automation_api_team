package com.psd2.tests.pisp.GET_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDPC_026 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDPC_026() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 200 OK");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"));
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(paymentConsent.getURL(),apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"), 
				"URI for GET domestic payment consent request is as per open banking standard");
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.ConsentId"))!=null,
				"ConsentId field under Data is present in Get Domestic Payment Consent response body"+String.valueOf(paymentConsent.getResponseValueByPath("Data.ConsentId")));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.CreationDateTime"))!=null,
				"CreationDateTime field under Data is present in Get Domestic Payment Consent response body"+(paymentConsent.getResponseValueByPath("Data.CreationDateTime")));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.StatusUpdateDateTime"))!=null,
				"StatusUpdateDateTime field under Data is present in Get Domestic Payment Consent response body"+String.valueOf(paymentConsent.getResponseValueByPath("Data.StatusUpdateDateTime")));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Status"))!=null,
				"Status field under Data is present in Get Domestic Payment Consent response body"+String.valueOf(paymentConsent.getResponseValueByPath("Data.Status")));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.CutOffDateTime"))!=null,
				"CutOffDateTime field under Data is present in Get Domestic Payment Consent response body"+String.valueOf(paymentConsent.getResponseValueByPath("Data.CutOffDateTime")));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.ExpectedExecutionDateTime"))!=null,
				"ExpectedExecutionDateTime field under Data is present in Get Domestic Payment Consent response body"+String.valueOf(paymentConsent.getResponseValueByPath("Data.ExpectedExecutionDateTime")));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.ExpectedSettlementDateTime"))!=null,
				"ExpectedSettlementDateTime field under Data is present in Get Domestic Payment Consent response body"+String.valueOf(paymentConsent.getResponseValueByPath("Data.ExpectedSettlementDateTime")));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Charges"))!=null,
				"Charges field under Data is present in Get Domestic Payment Consent response body"+String.valueOf(paymentConsent.getResponseValueByPath("Data.Charges")));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation"))!=null,
				"Initiation field under Data is present in Get Domestic Payment Consent response body"+String.valueOf(paymentConsent.getResponseValueByPath("Data.Initiation")));
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Authorisation"))!=null,
				"Authorisation field under Data is present in Get Domestic Payment Consent response body"+String.valueOf(paymentConsent.getResponseValueByPath("Data.Authorisation")));
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}

