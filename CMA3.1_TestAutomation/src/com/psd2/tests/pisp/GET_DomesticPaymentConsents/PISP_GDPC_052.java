package com.psd2.tests.pisp.GET_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/CreditorAccount block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDPC_052 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDPC_052() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Data/Initiation/CreditorAccount block where Request has sent successfully and returned a HTTP Code 200 OK");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"));
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount"))!=null, 
				"Mandatory field SchemeName is present in CreditorAccount block "+paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount"));
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName"))!=null, 
				"Mandatory field SchemeName is present in CreditorAccount block "+ paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName"));
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.Identification"))!=null, 
				"Mandatory field Identification is present in CreditorAccount block "+paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.Identification"));
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.Name"))!=null, 
				"Optional field Name is present in CreditorAccount block "+paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.Name"));
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.SecondaryIdentification"))!=null, 
				"Optional field SecondaryIdentification is present in CreditorAccount block "+paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount.SecondaryIdentification"));
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}

