package com.psd2.tests.pisp.GET_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Risk/DeliveryAddress block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDPC_078 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDPC_078() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Risk/DeliveryAddress block where Request has sent successfully and returned a HTTP Code 200 OK");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"));
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Risk.DeliveryAddress"))!=null,
				"DeliveryAddress block is present");
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Risk.DeliveryAddress.AddressLine"))!=null,
				"Optional field AddressLine is present in Risk block");
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Risk.DeliveryAddress.TownName"))!=null, 
				"Mandatory field TownName is present in DeliveryAddress block");
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Risk.DeliveryAddress.Country"))!=null, 
				"Mandatory field Country is present in DeliveryAddress block");
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Risk.DeliveryAddress.StreetName"))!=null, 
				"Optional field Country is present in DeliveryAddress block");
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Risk.DeliveryAddress.BuildingNumber"))!=null, 
				"Optional field BuildingNumber is present in DeliveryAddress block");
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Risk.DeliveryAddress.PostCode"))!=null, 
				"Optional field PostCode is present in DeliveryAddress block");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}

