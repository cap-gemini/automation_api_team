package com.psd2.tests.pisp.GET_DomesticPaymentConsents;

import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with expired token value into Authorization header
 * @author Kiran Dewangan
 *
 */
public class PISP_GDPC_013 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDPC_013() throws Throwable{	

		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with expired token value into Authorization header");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"));
		paymentConsent.setHeadersString("Authorization:Bearer "+apiConst.othertpp_cc_access_token);
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"401", 
				"Response Code is correct for Domestic Payment Consent URI when expired value of access token is used");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
