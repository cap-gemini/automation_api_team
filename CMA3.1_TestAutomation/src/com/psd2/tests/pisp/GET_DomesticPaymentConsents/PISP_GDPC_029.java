package com.psd2.tests.pisp.GET_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Status field where Request has sent successfully and returned a HTTP Code 200 OK
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDPC_029 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDPC_029() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY ConsentId field where Request has sent successfully and returned a HTTP Code 200 OK");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"));
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
	
		testVP.verifyTrue(String.valueOf(paymentConsent.getResponseValueByPath("Data.Status")).equals("Authorised")||
				String.valueOf(paymentConsent.getResponseValueByPath("Data.Status")).equals("AwaitingAuthorisation")||
				String.valueOf(paymentConsent.getResponseValueByPath("Data.Status")).equals("Consumed")||
				String.valueOf(paymentConsent.getResponseValueByPath("Data.Status")).equals("Rejected"), 
				"Status is correct"+String.valueOf(paymentConsent.getResponseValueByPath("Data.Status")));
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}

