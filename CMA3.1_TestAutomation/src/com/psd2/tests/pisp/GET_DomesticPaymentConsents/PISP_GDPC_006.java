package com.psd2.tests.pisp.GET_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Valid ConsentId but not associated with TPP originated the request
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDPC_006 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDPC_006() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of valid ConsentId but not associated with TPP originated the request");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint+"/"+apiConst.otherTPP_paymentConsentId);
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Payment Consent URI when consentId is not passed");
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Resource.NotFound", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].Message")),"payment setup id is not found in platform","Error message is correct");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}

