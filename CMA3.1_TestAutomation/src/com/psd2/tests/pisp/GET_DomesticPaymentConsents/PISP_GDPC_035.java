package com.psd2.tests.pisp.GET_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY ChargeBearer field where Request has sent successfully and returned a HTTP Code 200 OK
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDPC_035 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDPC_035() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Charges block where Request has sent successfully and returned a HTTP Code 200 OK");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"));
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Payment Consent URI");
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Charges[0]"))!=null,  
				"Charges block is present");
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Data.Charges[0].ChargeBearer")).equals("BorneByCreditor")||
				(paymentConsent.getResponseValueByPath("Data.Charges[0].ChargeBearer")).equals("BorneByDebtor")||
				(paymentConsent.getResponseValueByPath("Data.Charges[0].ChargeBearer")).equals("FollowingServiceLevel")||
				(paymentConsent.getResponseValueByPath("Data.Charges[0].ChargeBearer")).equals("Shared")
				, "Mandatory field ChargeBearer is present in Charges block");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}

