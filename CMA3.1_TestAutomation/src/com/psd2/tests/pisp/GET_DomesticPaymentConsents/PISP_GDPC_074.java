package com.psd2.tests.pisp.GET_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Risk block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDPC_074 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDPC_074() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Risk block where Request has sent successfully and returned a HTTP Code 200 OK");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"));
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Risk.PaymentContextCode"))!=null, 
				"Risk block is present");
		
		testVP.verifyTrue(String.valueOf(paymentConsent.getResponseValueByPath("Risk.PaymentContextCode"))!=null, 
				"Optional field PaymentContextCode is present in Risk block");
		
		testVP.verifyTrue(String.valueOf(paymentConsent.getResponseValueByPath("Risk.MerchantCategoryCode"))!=null, 
				"Optional field MerchantCategoryCode is present in Risk block");
		
		testVP.verifyTrue(String.valueOf(paymentConsent.getResponseValueByPath("Risk.MerchantCustomerIdentification"))!=null, 
				"Optional field MerchantCustomerIdentification is present in Risk block");
		
		testVP.verifyTrue((paymentConsent.getResponseValueByPath("Risk.DeliveryAddress"))!=null, 
				"Optional field DeliveryAddress is present in Risk block");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}

