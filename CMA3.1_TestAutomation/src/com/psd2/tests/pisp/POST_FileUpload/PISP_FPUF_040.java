package com.psd2.tests.pisp.POST_FileUpload;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with BLANK or Invalid value of OPTIONAL x-fapi-customer-last-logged-time header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_FPUF_040 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_FPUF_040() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with Invalid value of OPTIONAL x-fapi-customer-last-logged-time header");
		
		uploadFile.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		uploadFile.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		uploadFile.addHeaderEntry("x-fapi-customer-last-logged-time", "1234");
		uploadFile.setFileType(filePayment.getFileType());
		uploadFile.setIdempotencyKey(consentDetails.get("idempotency_key"));
		uploadFile.submit();
		
		testVP.verifyStringEquals(String.valueOf(uploadFile.getResponseStatusCode()),"400", 
				"Response Code is correct for Upload File URI when invalid value is used for x-fapi-customer-last-logged-time header");
		
		testVP.verifyStringEquals(uploadFile.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+uploadFile.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(!(uploadFile.getResponseNodeStringByPath("Errors[0].Message")).isEmpty(), 
				"Message for error code is '"+uploadFile.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the request with BLANK value of OPTIONAL x-fapi-customer-last-logged-time header");
		
		uploadFile.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		uploadFile.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		uploadFile.addHeaderEntry("x-fapi-customer-last-logged-time", "");
		uploadFile.setFileType(filePayment.getFileType());
		uploadFile.setIdempotencyKey(consentDetails.get("idempotency_key"));
		uploadFile.submit();
		
		testVP.verifyStringEquals(String.valueOf(uploadFile.getResponseStatusCode()),"400", 
				"Response Code is correct for Upload File URI when invalid value is used for x-fapi-customer-last-logged-time header");
		
		testVP.verifyStringEquals(uploadFile.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+uploadFile.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(!(uploadFile.getResponseNodeStringByPath("Errors[0].Message")).isEmpty(), 
				"Message for error code is '"+uploadFile.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
