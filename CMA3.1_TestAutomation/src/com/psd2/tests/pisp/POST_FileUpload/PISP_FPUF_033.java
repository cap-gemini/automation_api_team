package com.psd2.tests.pisp.POST_FileUpload;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with invalid value of scope for the flow into Authorization header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_FPUF_033 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_FPUF_033() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Creating client credetials with scope as accounts");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("accounts");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Upload File with invalid value of scope");
		
		uploadFile.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		uploadFile.setHeadersString("Authorization:Bearer "+cc_token);
		uploadFile.setFileType(filePayment.getFileType());
		uploadFile.setIdempotencyKey(consentDetails.get("idempotency_key"));
		uploadFile.submit();
		
		testVP.verifyStringEquals(String.valueOf(uploadFile.getResponseStatusCode()),"403", 
				"Response Code is correct for Upload File URI");

		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
