package com.psd2.tests.pisp.POST_FileUpload;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request without x-idempotency-key value OR key-value pair in the header
 * @author : Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_FPUF_023 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_FPUF_023() throws Throwable{
	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
        
        TestLogger.logStep("[Step 2] : Upload file with null value of x-idempotency-key");	
		uploadFile.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		uploadFile.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		uploadFile.setFileType(filePayment.getFileType());
		uploadFile.setIdempotencyKey("");
		uploadFile.submit();
		
		testVP.verifyStringEquals(String.valueOf(uploadFile.getResponseStatusCode()),"400", 
				"Response Code is correct for POST Upload File URI");	
		testVP.verifyStringEquals(uploadFile.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+uploadFile.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(uploadFile.getResponseNodeStringByPath("Errors[0].Message").equals("Invalid value '' for header x-idempotency-key. Invalid value ''. Expected ^(?!\\s)(.*)(\\S)$"), 
				"Message for error code is '"+uploadFile.getResponseNodeStringByPath("Errors[0].Message")+"'");
        TestLogger.logBlankLine();
        
        TestLogger.logStep("[Step 3] : Upload file without x-idempotency-key");	
        restRequest.setURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token")+", x-jws-signature:"+SignatureUtility.generateFileSignature(API_Constant.xmlFilePath)+", Accept:application/json");
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.setFileUploadPath(API_Constant.xmlFilePath);
		restRequest.setFileUploadType("application/xml");
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for POST Upload File URI");	

		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Missing", 
				"Error code for the response is correct i.e. '"+restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Errors[0].Message").equals("Required header x-idempotency-key not specified"), 
				"Message for error code is '"+restRequest.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
        
		testVP.testResultFinalize();
	}
}