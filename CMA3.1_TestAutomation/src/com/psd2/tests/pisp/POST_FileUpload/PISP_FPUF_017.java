package com.psd2.tests.pisp.POST_FileUpload;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of file-payment-consents/{consentId}/file URL sending with the json file in body and File Type as "UK.OBIE.PaymentInitiation.3.0"
 * @author : Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_FPUF_017 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_FPUF_017() throws Throwable{
	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Generate consent id");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.setFileType("UK.OBIE.PaymentInitiation.3.0");
		filePayment.setFileHash(Misc.generateFileHash(API_Constant.jsonFilePath));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for File Payment Consents URI");
		consentId=filePayment.getConsentId();
		TestLogger.logVariable("Consent Id : " + consentId);
		TestLogger.logBlankLine();
        
        TestLogger.logStep("[Step 2] : Upload file");	
		restRequest.setURL(apiConst.fPaymentConsent_endpoint+"/"+consentId+"/file");
		restRequest.setHeadersString("Authorization:Bearer "+cc_token+", x-jws-signature:"+SignatureUtility.generateFileSignature(API_Constant.jsonFilePath)+", x-idempotency-key:"+filePayment.getHeaderEntry("x-idempotency-key")+", Accept:application/json");
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.setFileUploadPath(API_Constant.jsonFilePath);
		restRequest.setFileUploadType("application/json");
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for POST Upload File URI");	

		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}