package com.psd2.tests.pisp.POST_FileUpload;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without token value OR key-value pair into Authorization header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_FPUF_030 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_FPUF_030() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with null value of Authorization header");
		
		uploadFile.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
		uploadFile.setHeadersString("Authorization:Bearer ");
		uploadFile.addHeaderEntry("x-fapi-interaction-id", PropertyUtils.getProperty("inter_id"));
		uploadFile.setFileType(filePayment.getFileType());
		uploadFile.setIdempotencyKey(consentDetails.get("idempotency_key"));
		uploadFile.submit();
		
		testVP.verifyStringEquals(String.valueOf(uploadFile.getResponseStatusCode()),"401", 
				"Response Code is correct for Upload File URI with no token value in the Authorization (Access Token) header");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the request without Authorization header");
		restRequest.setURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
	    restRequest.setHeadersString("Accept:application/json, x-idempotency-key:"+consentDetails.get("idempotency_key"));
		restRequest.setFileUploadPath(API_Constant.xmlFilePath);
		restRequest.setFileUploadType("application/xml");
	    restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", 
				"Response Code is correct for Upload File URI without Authorization (Access Token) header");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

