package com.psd2.tests.pisp.POST_FileUpload;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request with NULL or Invalid value of MANDATORY x-jws-signature header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","JWT"})
public class PISP_FPUF_037 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_FPUF_037() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with Invalid value of x-jws-signature header");
		restRequest.setURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
	    restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token")+", Accept:application/json, x-idempotency-key:"+consentDetails.get("idempotency_key"));
	    restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
	    restRequest.addHeaderEntry("x-jws-signature", "1234");
	    restRequest.setFileUploadPath(API_Constant.xmlFilePath);
		restRequest.setFileUploadType("application/xml");
	    restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for Upload File URI");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].ErrorCode")), "UK.OBIE.Signature.Malformed", 
				"Response Error Code is correct");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].Message")),"Unable to parse x-jws-signature header", "Error Code is matching");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the request with null value of x-jws-signature header");
		
		restRequest.setURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId")+"/file");
	    restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token")+", Accept:application/json, x-idempotency-key:"+consentDetails.get("idempotency_key"));
	    restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
	    restRequest.addHeaderEntry("x-jws-signature", "");
	    restRequest.setFileUploadPath(API_Constant.xmlFilePath);
		restRequest.setFileUploadType("application/xml");
	    restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for Upload File URI");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Signature.Missing", 
				"Response Error Code is correct");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].Message")),"x-jws-signature missing in request headers", "Error Message is matching");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
