package com.psd2.tests.pisp.POST_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the MANDATORY DebtorAccount/SchemeName field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_FPC_064 extends TestBase {	
	
	@Test
	public void m_PISP_FPC_064() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY DebtorAccount/SchemeName field");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.submit();

		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName").isEmpty()), 
				"Mandatory field SchemeName is present and is not null");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

