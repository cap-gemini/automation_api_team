package com.psd2.tests.pisp.POST_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Status field where Request has sent for already existing ConsentId having any of the values as Authorised, Consumed, Rejected
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_FPC_103 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_FPC_103() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Reject consent");
        consentDetails=apiUtility.generatePayments(true, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of status field when consent is rejected");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		filePayment.setMethod("GET");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for File Payment Consent URI");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Data.Status").equals("Rejected"), 
				"Status field is having correct status i.e. "+filePayment.getResponseNodeStringByPath("Data.Status"));
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
