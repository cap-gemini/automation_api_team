package com.psd2.tests.pisp.POST_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of date format for optional field Authorisation/CompletionDateTime
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_FPC_056 extends TestBase {	
	
	@Test
	public void m_PISP_FPC_056() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of date format for optional field Authorisation/CompletionDateTime");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for File Payment Consent URI");
		
		testVP.verifyTrue(Misc.verifyDateTimeFormat(filePayment.getResponseNodeStringByPath("Data.Authorisation.CompletionDateTime").split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(filePayment.getResponseNodeStringByPath("Data.Authorisation.CompletionDateTime").split("T")[1], "HH:mm:ss+00:00"), 
				"CompletionDateTime is as per expected ISO Standard format");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
