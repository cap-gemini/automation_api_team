package com.psd2.tests.pisp.POST_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/DebtorAccount block
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_FPC_063 extends TestBase {	
	
	@Test
	public void m_PISP_FPC_063() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Data/Initiation/DebtorAccount block");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.submit();
		

		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for File Payment Consent URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName")).isEmpty(), 
				"Mandatory field SchemeName under DebtorAccount is present");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification")).isEmpty(), 
				"Mandatory field Identification under DebtorAccount is present");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Name")).isEmpty(), 
				"Optional field Name under DebtorAccount is present");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SecondaryIdentification")).isEmpty(), 
				"Optional field SecondaryIdentification under DebtorAccount is present");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

