package com.psd2.tests.pisp.POST_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the request with BLANK or Invalid value of OPTIONAL x-fapi-customer-ip-address header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_FPC_022 extends TestBase {	
	
	@Test
	public void m_PISP_FPC_022() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with BLANK value of OPTIONAL x-fapi-customer-ip-address header");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.addHeaderEntry("x-fapi-customer-ip-address", "");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"400", 
				"Response Code is correct for File Payment Consents URI when invalid value is used for x-fapi-customer-last-logged-time header");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Errors[0].Message").equals("Invalid value found in x-fapi-customer-ip-address header"), 
				"Message for error code is '"+filePayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the request with invalid value of OPTIONAL x-fapi-customer-ip-address header");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.addHeaderEntry("x-fapi-customer-ip-address", "abcd");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"400", 
				"Response Code is correct for File Payment Consents URI when invalid value is used for x-fapi-customer-last-logged-time header");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Errors[0].Message").equals("Invalid value found in x-fapi-customer-ip-address header"), 
				"Message for error code is '"+filePayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
