package com.psd2.tests.pisp.POST_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY Status field where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_FPC_102 extends TestBase {	
	
	@Test
	public void m_PISP_FPC_102() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Status field where Request has sent successfully and returned a HTTP Code 201 Created");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for File Payment Consent URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Status")).isEmpty(), 
				"Status field is present under Data block and is not empty");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Data.Status").equals("AwaitingUpload")
				|| filePayment.getResponseNodeStringByPath("Data.Status").equals("Authorised")
				|| filePayment.getResponseNodeStringByPath("Data.Status").equals("AwaitingAuthorisation")
				|| filePayment.getResponseNodeStringByPath("Data.Status").equals("Consumed")
				|| filePayment.getResponseNodeStringByPath("Data.Status").equals("Rejected"), 
				"Status field is having correct status i.e. "+filePayment.getResponseNodeStringByPath("Data.Status"));
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
