package com.psd2.tests.pisp.POST_FilePaymentConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request without x-idempotency-key value OR key-value pair in the header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_FPC_006 extends TestBase {	
	
	@Test
	public void m_PISP_FPC_006() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request with null value of x-idempotency-key value in the header");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.addHeaderEntry("x-idempotency-key", "");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"400", 
				"Response Code is correct for File Payment Consents when x-idempotency-key value in header is not present");

	    testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Errors[0].Message").equals("Invalid value '' for header x-idempotency-key. Invalid value ''. Expected ^(?!\\s)(.*)(\\S)$"), 
				"Message for error code is '"+filePayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of request without x-idempotency-key in the header");
		restRequest.setURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		String requestBody=filePayment.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+cc_token+", Content-Type:application/json, x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-fapi-interaction-id:"+PropertyUtils.getProperty("inter_id")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json");
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for File Payment Consents when x-idempotency-key in header is not present");

	    testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Missing", 
				"Error code for the response is correct i.e. '"+restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Errors[0].Message").equals("Required header x-idempotency-key not specified"), 
				"Message for error code is '"+restRequest.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
