package com.psd2.tests.pisp.GET_Download_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request with BLANK or Invalid value of OPTIONAL x-fapi-customer-ip-address header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DFP"})
public class PISP_GDFP_031 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDFP_031() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with BLANK value of OPTIONAL x-fapi-customer-ip-address header");
		
		restRequest.setURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId()+"/report-file");
		restRequest.setHeadersString("Authorization:Bearer "+cc_token+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("x-fapi-customer-ip-address", "");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for GET Download File Payment URI when invalid value is used for x-fapi-customer-last-logged-time header");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Errors[0].Message").equals("Invalid value found in x-fapi-customer-ip-address header"), 
				"Message for error code is '"+restRequest.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of the request with invalid value of OPTIONAL x-fapi-customer-ip-address header");
		
		restRequest.setURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId()+"/report-file");
		restRequest.setHeadersString("Authorization:Bearer "+cc_token+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("x-fapi-customer-ip-address", "abcd");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for GET Download File Payment URI when invalid value is used for x-fapi-customer-last-logged-time header");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Errors[0].Message").equals("Invalid value found in x-fapi-customer-ip-address header"), 
				"Message for error code is '"+restRequest.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}