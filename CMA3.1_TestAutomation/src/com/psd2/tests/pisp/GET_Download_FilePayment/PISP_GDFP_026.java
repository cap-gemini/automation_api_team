package com.psd2.tests.pisp.GET_Download_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request without x-fapi-financial-id value or key-value pair into header
 * @author : Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DFP"})
public class PISP_GDFP_026 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDFP_026() throws Throwable{	
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request without x-fapi-financial-id value or key-value pair into header");	
		restRequest.setURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId()+"/report-file");
		restRequest.setHeadersString("Authorization:Bearer "+cc_token);
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for GET Download File Payment URI");	
        TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
}