package com.psd2.tests.pisp.GET_Download_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of file-payment/{FilePaymentId}/report-file URL that should be as per Open Banking standards
 * @author : Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity","DFP"})
public class PISP_GDFP_001 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDFP_001() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate File Payment Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, true);
		API_Constant.setFp_PaymentId(consentDetails.get("paymentId"));
		TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Verification of file-payment/{FilePaymentId}/report-file URL that should be as per Open Banking standards");	
		restRequest.setURL(apiConst.fPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId")+"/report-file");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for GET Download File Payment URI is as per Open Banking Standard");	
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
}	