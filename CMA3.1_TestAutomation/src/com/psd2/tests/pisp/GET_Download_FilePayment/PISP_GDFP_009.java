package com.psd2.tests.pisp.GET_Download_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of GET file-payment/{FilePaymentId}/report-file URL sending with FilePaymentId having status InitiationPending
 * @author : Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DFP"})
public class PISP_GDFP_009 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDFP_009() throws Throwable{
		filePayment.setControlSum("400.00");
		TestLogger.logStep("[Step 1] : Generate File Payment Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, true);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of GET file-payment/{FilePaymentId}/report-file URL sending with FilePaymentId having status InitiationPending");	
		restRequest.setURL(apiConst.fPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId")+"/report-file");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for GET Download File Payment URI");	
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}