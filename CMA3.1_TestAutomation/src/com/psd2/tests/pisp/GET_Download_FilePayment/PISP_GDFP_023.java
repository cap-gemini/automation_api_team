package com.psd2.tests.pisp.GET_Download_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request with expired token value into Authorization header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DFP"})
public class PISP_GDFP_023 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDFP_023() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with expired token value into Authorization header");
		
		restRequest.setURL(apiConst.fPaymentSubmission_endpoint+"/"+API_Constant.getFp_PaymentId()+"/report-file");
		restRequest.setHeadersString("Authorization:Bearer "+ apiConst.expiredPispCCAccessToken+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", 
				"Response Code is correct for GET Download File Payment URI with expired token value in the Authorization (Access Token) header");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}