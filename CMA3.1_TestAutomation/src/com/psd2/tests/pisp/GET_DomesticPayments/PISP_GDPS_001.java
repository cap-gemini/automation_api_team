package com.psd2.tests.pisp.GET_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of domestic-payments URL that should be as per Open Banking standards 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity","DP"})
public class PISP_GDPS_001 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDPS_001() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, false, true);
		API_Constant.setDp_PaymentId(consentDetails.get("paymentId"));
        TestLogger.logBlankLine();
		
        
		TestLogger.logStep("[Step 2] : Get Domestic Payment Submission");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint+"/"+consentDetails.get("paymentId"));
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Get Domestic Payment Submission");
		testVP.verifyEquals(paymentConsent.getURL(), apiConst.dps_endpoint+"/"+consentDetails.get("paymentId"), "Response Code is correct for Get Domestic Payment Submission URL");
		
		testVP.verifyTrue(SignatureUtility.verifySignature(paymentConsent.getResponseString(), paymentConsent.getResponseHeader("x-jws-signature")), 
				"Response that created successfully with HTTP Code 201 MUST be digitally signed");
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();
	}
}