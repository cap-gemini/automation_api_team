package com.psd2.tests.pisp.GET_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Initiation block where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DP"})
public class PISP_GDPS_041 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDPS_041() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Get Domestic Payment Submission ");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint+"/"+API_Constant.getDp_PaymentId());
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Get Domestic Payment Submission");	
		TestLogger.logBlankLine();	
		
		testVP.verifyTrue(String.valueOf(paymentConsent.getResponseValueByPath("Data.Initiation"))!=null,
				"Initiation field under Data is present in Get Domestic Payment submission response body");
		testVP.verifyTrue(String.valueOf(paymentConsent.getResponseValueByPath("Data.Initiation.InstructionIdentification"))!=null,
				"InstructionIdentification field present in Get Domestic Payment submission response body");
		testVP.verifyTrue(String.valueOf(paymentConsent.getResponseValueByPath("Data.Initiation.EndToEndIdentification"))!=null,
				"EndToEndIdentification field is present in Get Domestic Payment submission response body");
		testVP.verifyTrue(String.valueOf(paymentConsent.getResponseValueByPath("Data.Initiation.InstructedAmount"))!=null,
				"InstructedAmount block is present in Get Domestic Payment submission response body");
		testVP.verifyTrue(String.valueOf(paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount"))!=null,
				"CreditorAccount block is present in Get Domestic Payment submission response body");
		testVP.verifyTrue(String.valueOf(paymentConsent.getResponseValueByPath("Data.Initiation.LocalInstrument"))!=null,
				"LocalInstrument field is present in Get Domestic Payment submission response body");
		testVP.verifyTrue(String.valueOf(paymentConsent.getResponseValueByPath("Data.Initiation.DebtorAccount"))!=null,
				"DebtorAccount block is present in Get Domestic Payment submission response body");
		testVP.verifyTrue(String.valueOf(paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress"))!=null,
				"CreditorPostalAddress block is present in Get Domestic Payment submission response body");
		testVP.verifyTrue(String.valueOf(paymentConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation"))!=null,
				"RemittanceInformation block is present in Get Domestic Payment submission response body");
		
		testVP.testResultFinalize();		
	}
}
