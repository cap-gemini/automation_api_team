package com.psd2.tests.pisp.GET_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of OPTIONAL CreditorAccount/PostCode where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DP"})
public class PISP_GDPS_064 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDPS_064() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Get Domestic Payment Submission ");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint+"/"+API_Constant.getDp_PaymentId());
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Get Domestic Payment Submission");
		
		testVP.verifyTrue(!String.valueOf(paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.PostCode")).isEmpty(),
				"PostCode field is present in Get Domestic Payment submission response body");
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.PostCode")),paymentConsent._crPostCode,
				"Response is same as sent in request body");
		testVP.testResultFinalize();		
	}
}
