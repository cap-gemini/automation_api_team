package com.psd2.tests.pisp.GET_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description :Verification of the values into OPTIONAL MultiAuthorisation/ExpirationDateTime where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DP"})
public class PISP_GDPS_077 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDPS_077() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Get Domestic Payment Submission");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint+"/"+API_Constant.getDp_PaymentId());
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Get Domestic Payment Submission");	
		TestLogger.logBlankLine();	
		
		testVP.verifyTrue(!String.valueOf(paymentConsent.getResponseValueByPath("Data.MultiAuthorisation.ExpirationDateTime")).isEmpty(),
				"ExpirationDateTime field is present in Get Domestic Payment submission response body");
		testVP.verifyTrue(Misc.verifyDateTimeFormat(paymentConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.ExpirationDateTime").split("T")[0], "yyyy-MM-dd") && 
				(Misc.verifyDateTimeFormat(paymentConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.ExpirationDateTime").split("T")[1], "HH:mm:ss+00:00")), 
				"ExpirationDateTime is as per expected format");
		testVP.testResultFinalize();		
	}
}
