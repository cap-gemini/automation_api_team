package com.psd2.tests.pisp.GET_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without token value OR key-value pair into Authorization header 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DP"})
public class PISP_GDPS_012 extends TestBase{	

	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_GDPS_012() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : Get Domestic Payment Submission ");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint+"/"+API_Constant.getDp_PaymentId());
		paymentConsent.setHeadersString("Authorization:Bearer ");
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"401", 
				"Response Code is correct for Get Domestic Payment Submission");
		
		TestLogger.logStep("[Step 3] : Get Domestic Payment Submission ");	
		restRequest.setURL(apiConst.dps_endpoint+"/"+API_Constant.getDp_PaymentId());
		restRequest.setHeadersString("x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", 
				"Response Code is correct for Get Domestic Payment Submission");	
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();		
	}
}
