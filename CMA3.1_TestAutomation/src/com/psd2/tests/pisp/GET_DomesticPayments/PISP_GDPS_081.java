package com.psd2.tests.pisp.GET_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of response for the payments API when the Amount starts with "700" in the POST resquest payload 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"DP"})
public class PISP_GDPS_081 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GDPS_080() throws Throwable{	
		paymentConsent.setAmount("700.00");
		TestLogger.logStep("[Step 1] : Generate Payment Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, false, true);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Get Domestic Schedule Payment ");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint+"/"+consentDetails.get("paymentId"));
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
			
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"500", 
				"Response Code is correct for POST Domestic schedule Payment Submission");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}