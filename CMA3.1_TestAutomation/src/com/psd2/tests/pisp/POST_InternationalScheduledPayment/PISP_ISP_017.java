package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without token value OR key-value pair into Authorization (Authorization Code) header 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_017 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_017() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer ");
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();

			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"401",
					"Response Code is correct for International Scheduled Payment Consent when request without token value");

			TestLogger.logStep("[Step 3] : POST International Scheduled Payment Submission without Authorization");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			restRequest.setURL(apiConst.iScheduledPaymentSubmission_endpoint);
			String requestBody= iScheduledPayment.genRequestBody();
			restRequest.setHeadersString("Accept:application/json, x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3)+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
			restRequest.setRequestBody(requestBody);
			restRequest.setMethod("POST");
			restRequest.submit();
			testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", 
					"Response Code is correct for without x-idempotency-key key-value pair into header for International scheduled Payment Submission");
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
