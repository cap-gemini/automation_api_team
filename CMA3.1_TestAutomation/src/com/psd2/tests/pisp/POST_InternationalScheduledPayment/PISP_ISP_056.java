package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/ChargeBearer field
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_056 extends TestBase {	
	
	@Test
	public void m_PISP_ISP_056() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", "Response Code is correct for client credentials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);    
        TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 2] : Verification of the values of OPTIONAL Initiation/ChargeBearer field");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		String requestBody=iScheduledPayment.genRequestBody().replace("\"Purpose\": \"Test\",","\"Purpose\":\"Test\", \"ChargeBearer\":\"Shared\",");
		iScheduledPayment.submit(requestBody);
		
		String consentId=iScheduledPayment.getConsentId();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Consent URI");
		
		testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer").equals("BorneByCreditor")
				|| iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer").equals("BorneByDebtor")
				|| iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer").equals("FollowingServiceLevel")
				|| iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer").equals("Shared"),
				"ChargeBearer field value is as per OB defined values");
		
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 1-3] : JWT Token Creation........");
		
		//reqObject.setBaseURL(apiConst.ro_endpoint);
		reqObject.setValueField(consentId);
		reqObject.setScopeField("payments");
		outId = reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-4] : Go to URL and authenticate consent");	
		redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		authCode = consentOps.authorisePISPConsent(redirecturl+"##"+iScheduledPayment._drAccountIdentification,iScheduledPayment.addDebtorAccount);	
		closeDriverInstance();
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 1-5] : Get access and refresh token");	
		accesstoken.setBaseURL(apiConst.at_endpoint);
		//accesstoken.setAuthentication(PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		accesstoken.setAuthCode(authCode);
		accesstoken.submit();
		
		testVP.verifyStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
				"Response Code is correct for get access token request");	
		access_token = accesstoken.getAccessToken();
		refresh_token = accesstoken.getRefreshToken();
		TestLogger.logVariable("Access Token : " + access_token);
		
		API_Constant.setPisp_AccessToken(access_token);		
		
		TestLogger.logStep("[Step 1-6] : Verification of the values of OPTIONAL Initiation/ChargeBearer field");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+access_token);
		iScheduledPayment.setConsentId(consentId);
		requestBody=iScheduledPayment.genRequestBody().replace("\"Purpose\": \"Test\",","\"Purpose\":\"Test\", \"ChargeBearer\":\"Shared\",");
		iScheduledPayment.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment URI");
		
		testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer").equals("BorneByCreditor")
				|| iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer").equals("BorneByDebtor")
				|| iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer").equals("FollowingServiceLevel")
				|| iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer").equals("Shared"),
				"ChargeBearer field value is as per OB defined values");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
