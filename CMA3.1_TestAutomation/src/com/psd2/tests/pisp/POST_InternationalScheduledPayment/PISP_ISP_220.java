package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL ExchangeRateInformation block where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_220 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_220() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment SetUp....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for POST International Scheduled Payment");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation"))!=null, 
					"Mandatory Block ExchangeRateInformation is present"+iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation"));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.UnitCurrency"))!=null, 
					"Mandatory field UnitCurrency is present in ExchangeRateInformation block "+ iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.UnitCurrency"));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExchangeRate"))!=null, 
					"Mandatory field ExchangeRate is present in ExchangeRateInformation block "+iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExchangeRate"));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType"))!=null, 
					"Optional field RateType is present in ExchangeRateInformation block "+iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType"));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ContractIdentification"))!=null, 
					"Optional field ContractIdentification is present in ExchangeRateInformation block "+iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ContractIdentification"));
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
	}
}
