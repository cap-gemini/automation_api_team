package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY CreditorAccount/Identification field when SchemeName = any.bank.scheme2
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_352 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_ISP_352() throws Throwable{	
		iScheduledPayment.setCrAccountSchemeName("any.bank.scheme2");
		iScheduledPayment.setCrAccountIdentification("scheme2");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		apiUtility.pispAccessToken=true;
		consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Scheduled Payment ");	
	    iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		iScheduledPayment.setCrAccountSchemeName("any.bank.scheme2");
		iScheduledPayment.setCrAccountIdentification("scheme2561546as1");
		iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Scheduled Payment Submission URI ");			
        testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), 
	    		"UK.OBIE.Resource.ConsentMismatch", "Error Code are matched");
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message"),
				"Payload comparison failed with the consent resource", "Error Message are matched");
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();
	}
}
