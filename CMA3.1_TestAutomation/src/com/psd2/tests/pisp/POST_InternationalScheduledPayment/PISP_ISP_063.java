package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/InstructedAmount block
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_063 extends TestBase {	
	
   API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_063() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Data/Initiation/InstructedAmount block");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint	);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment URI");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.InstructedAmount.Amount")).isEmpty(), 
				"Amount field under InstructedAmount array is present and is not null");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.InstructedAmount.Currency")).isEmpty(), 
				"Currency field under InstructedAmount array is present and is not null");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
