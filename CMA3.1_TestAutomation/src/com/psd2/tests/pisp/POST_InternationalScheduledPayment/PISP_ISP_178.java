package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Risk/PaymentContextCode field having other values
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_178 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_ISP_178() throws Throwable{	
		iScheduledPayment.setRiskPaymentContextCode("EcommerceGoods");
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		apiUtility.pispAccessToken=true;
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
            TestLogger.logBlankLine();
	
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Risk/PaymentContextCode field having other values");                          
        iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
        iScheduledPayment.setConsentId(consentDetails.get("consentId"));
        iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Submission URI ");			
		testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Risk")==null, 
				"PaymentContextCode is not present in response body");
		
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
