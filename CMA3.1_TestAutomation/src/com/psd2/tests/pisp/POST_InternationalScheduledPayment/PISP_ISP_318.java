package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY ChargeBearer field where Request has sent successfully and returned a HTTP Code 201 Created if InstructedAmount/Currency is USD
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_318 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_ISP_318() throws Throwable{	
		iScheduledPayment.setCurrency("USD");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Scheduled Payment Submission");	
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
	    iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
	    iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
			"Response Code is correct for International Scheduled Payment Submission URI");
		    
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Data.Status"), "InitiationCompleted",
			"Status field value is correct");
		
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Data.Charges[0].ChargeBearer"), "BorneByCreditor",
				"ChargeBearer field value is correct");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
