package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation block 
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_035 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_035() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate access token");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();

			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()), "201",
					"Response Code is correct for Domestic Scheduled Payment URI");
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data") != null,
					"Mandatory block Data is present and is not null");
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.ConsentId")).isEmpty(),
					"Mandatory field ConsentId is present under Data");
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.CreationDateTime")).isEmpty(),
					"Mandatory field CreationDateTime is present under Data");
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Status")).isEmpty(),
					"Mandatory field Status is present under Data");
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.StatusUpdateDateTime")).isEmpty(),
					"Mandatory field StatusUpdateDateTime is present under Data");
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation") != null,
					"Mandatory block Initiation is present and is not null");
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.InstructionIdentification")).isEmpty(),
					"Mandatory field InstructionIdentification is present under Initiation");
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument")).isEmpty(),
					"Mandatory field LocalInstrument is present under Initiation block");
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.EndToEndIdentification")).isEmpty(),
					"Mandatory field EndToEndIdentification is present under Initiation block");
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructedAmount") != null,
					"Mandatory field InstructedAmount is present under Initiation block");
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount") != null,
					"Mandatory field CreditorAccount is present under Initiation block");
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.DebtorAccount") != null,
					"Optional field DebtorAccount is present under Initiation block");
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Risk") == null,
					"Optional field Risk block is not present under Data block");
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation") != null,
					"Optional field RemittanceInformation is present under Initiation block");
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Risk") == null,
					"Mandatory field Risk is not present under Initiation block");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
