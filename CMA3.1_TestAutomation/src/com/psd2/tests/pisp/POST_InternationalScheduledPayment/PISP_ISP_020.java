package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with invalid value of scope for the flow into Authorization (Access Token) header 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_020 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_020() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		consentDetails=apiUtility.generateAISPConsent(null,false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Scheduled Payment Submission");
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("api_access_token"));
		iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.submit();

		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"403", 
				"Response Code is correct for International Scheduled Payment Consent URI");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
