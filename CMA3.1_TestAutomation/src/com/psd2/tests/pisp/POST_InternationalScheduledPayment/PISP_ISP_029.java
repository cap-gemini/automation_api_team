package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without Content-Type header OR Invalid format values 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_029 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_029() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");
			
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			restRequest.setURL(apiConst.iScheduledPaymentSubmission_endpoint);
			String requestBody= iScheduledPayment.genRequestBody();
			restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", Accept:application/json, x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3)+", x-jws-signature:"+SignatureUtility.generateSignature(requestBody));
			restRequest.setRequestBody(requestBody);
			restRequest.setMethod("POST");
			restRequest.submit();
			
			testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"415", 
					"Response Code is correct without Content-Type header");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
