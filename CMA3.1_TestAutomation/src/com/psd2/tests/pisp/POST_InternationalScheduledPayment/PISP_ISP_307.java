package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of response for the consent API when the RateType is provided as "Agreed" in the request payload 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_307 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_ISP_307() throws Throwable{	
		iScheduledPayment.setRateType("Agreed"); 
		 
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
	    TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : Verification of response when the RateType is provided as \"Agreed\" in the request payload");                          
        iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
        iScheduledPayment.setConsentId(consentDetails.get("consentId"));
        iScheduledPayment.setRateType("Agreed");
        iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Submission URI ");			
		testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExpirationDateTime"))==null,
				"ExchangeRate field under ExchangeRateInformation block is NOT present in International Payment Submission response body ");		
           
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
