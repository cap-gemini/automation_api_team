package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of date and time for RequestedExecutionDateTime field without giving seconds
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_060 extends TestBase {	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_060() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
        String key = iScheduledPayment.getHeaderEntry("x-idempotency-key");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of date and time for RequestedExecutionDateTime field without giving seconds");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.setRequestedExecutionDateTime("2021-08-06T06:06");
		iScheduledPayment.submit();
		

		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Scheduled Payment Submission URI when RequestedExecutionDateTime field value is without seconds");
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Provided value 2021-08-06T06:06 is not compliant with the format datetime provided in rfc3339 for RequestedExecutionDateTime", "Error Message are matched");
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of date and time for RequestedExecutionDateTime field without giving seconds");
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-idempotency-key:"+key);
		iScheduledPayment.setRequestedExecutionDateTime("2021-03-20T06:06Z");
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Scheduled Payment Submission URI when RequestedExecutionDateTime field value is without seconds");
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Provided value 2021-03-20T06:06Z is not compliant with the format datetime provided in rfc3339 for RequestedExecutionDateTime", "Error Message are matched");
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of date and time for RequestedExecutionDateTime field without giving seconds");
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-idempotency-key:"+key);
		iScheduledPayment.setRequestedExecutionDateTime("2021-03-20T06:06+00:00");
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Scheduled Payment Submission URI when RequestedExecutionDateTime field value is without seconds");
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Provided value 2021-03-20T06:06+00:00 is not compliant with the format datetime provided in rfc3339 for RequestedExecutionDateTime", "Error Message are matched");
        TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
