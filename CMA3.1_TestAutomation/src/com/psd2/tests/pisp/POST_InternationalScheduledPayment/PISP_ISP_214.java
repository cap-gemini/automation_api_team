package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Charges block where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_214 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_214() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment SetUp....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for POST International Scheduled Payment");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Charges"))!=null,
					"Charges Block under Data is present in POST International scheduled Payment response body");			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Charges.ChargeBearer"))!=null,
					"ChargeBearer is present in POST International scheduled Payment response body");		
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Charges.Type"))!=null,
					"Type is present in POST International scheduled Payment response body");			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Charges.Amount"))!=null,
					"Amount  is present in POST International scheduled Payment response body");					
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
	}
}
