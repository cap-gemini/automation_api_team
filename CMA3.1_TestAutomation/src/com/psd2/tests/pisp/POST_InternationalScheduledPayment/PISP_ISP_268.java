package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/CreditorAgent/PostalAddress block 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_268 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_268() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
            iScheduledPayment.submit();
			
            testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment URI");		
            testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent"))!=null, 
					"CreditorAgentAgent block is present International Payment Submission Response Body and is not null");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType"))!=null, 
					"Mandatory field AddressType is present in CreditorAgent.PostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Department"))!=null, 
					"Mandatory field Department is present in CreditorAgent.PostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.SubDepartment"))!=null, 
					"Optional field SubDepartment is present in CreditorAgent.PostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.StreetName"))!=null, 
					"Optional field StreetName is present in CreditorAgent.PostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.BuildingNumber"))!=null, 
					"Optional field BuildingNumber is present in CreditorAgent.PostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.PostCode"))!=null, 
					"Optional field PostCode is present in CreditorAgent.PostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.TownName"))!=null, 
					"Optional field TownName is present in CreditorAgent.PostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.CountrySubDivision"))!=null, 
					"Optional field CountrySubDivision is present in CreditorAgent.PostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Country"))!=null, 
					"Optional field Country is present in CreditorAgent.PostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressLine"))!=null, 
					"Optional field AddressLine is present in CreditorAgent.PostalAddress block");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
