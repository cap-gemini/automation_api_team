package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Initiation/RequestedExecutionDateTime
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_058 extends TestBase {
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_058() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Initiation/RequestedExecutionDateTime");
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment URI");
		
		testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.RequestedExecutionDateTime")).isEmpty(), 
				"RequestedExecutionDateTime is not null");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the values into MANDATORY Initiation/RequestedExecutionDateTime field not followed ISO 8601 date-time format");
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.setRequestedExecutionDateTime("02-25-2021T06:06:06+00:00");
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Schedduled payment URI when RequestedExecutionDateTime is not followed ISO date-time");
		
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Provided value 02-25-2021T06:06:06+00:00 is not compliant with the format datetime provided in rfc3339 for RequestedExecutionDateTime"), 
				"Message for error code is '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
