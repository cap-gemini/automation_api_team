package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/LocalInstrument field having OB defined values
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_051 extends TestBase {	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_051() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Scheduled Payment Consents....");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment URI");
		
		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.FPS")
				|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BACS")
				|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.CHAPS")
				|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Paym")
				|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BalanceTransfer")
				|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.MoneyTransfer")
				|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.SWIFT")
				|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Euro1")
				|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.SEPACreditTransfer")
				|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.SEPAInstantCreditTransfer")
				|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Target2")
				|| iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Link"), 
				"LocalInstrument field value is as per OB defined values");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
