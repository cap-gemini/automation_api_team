package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification SCA page without debtor account block
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_326 extends TestBase{	
	
	
	@Test
	public void m_PISP_ISP_326() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST International Scheduled Payment");
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.removeDebtorAccount();
		iScheduledPayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for Post International Scheduled Payment");
		consentId = iScheduledPayment.getConsentId();	
		TestLogger.logVariable("Consent Id : " + consentId);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : JWT Token Creation........");
		
		//reqObject.setBaseURL(apiConst.ro_endpoint);
		reqObject.setValueField(consentId);
		reqObject.setScopeField("payments");
		outId = reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Go to URL and authenticate consent");	
		redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		
		authCode = consentOps.authorisePISPConsent(redirecturl+"##"+iScheduledPayment._drAccountIdentification,iScheduledPayment.removeDebtorAccount);
		closeDriverInstance();
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 5] : Get access token");	
		accesstoken.setBaseURL(apiConst.at_endpoint);
		accesstoken.setAuthCode(authCode);
		accesstoken.submit();
		
		testVP.verifyStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
				"Response Code is correct for get access token request");	
		access_token = accesstoken.getAccessToken();
		TestLogger.logVariable("Access Token : " + access_token);		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 6] : POST International Scheduled Payment Submission");	
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+access_token);
		iScheduledPayment.setConsentId(consentId);
		iScheduledPayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
			"Response Code is correct for International Scheduled Payment when Debtor Account is not sent in request");
		    
		testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.DebtorAccont")==null,
			"Optional Debtor Account field is not present");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
