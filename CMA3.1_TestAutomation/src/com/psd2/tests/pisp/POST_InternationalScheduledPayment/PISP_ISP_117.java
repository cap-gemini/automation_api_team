package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/Creditor.PostalAddress block 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_117 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_117() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International Scheduled Payment URI");			
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress"))!=null,
					"Creditor.PostalAddress block present in POST International Scheduled Payment Submission response body ");
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressType"))!=null,
					"AddressType field present in POST International Scheduled Payment Submission response body ");
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.Department"))!=null,
					"Department field present in POST International Scheduled Payment Submission response body ");
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.SubDepartment"))!=null,
					"SubDepartment field present in POST International Scheduled Payment Submission response body ");
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.StreetName"))!=null,
					"StreetName field present in POST International Scheduled Payment Submission response body ");
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.BuildingNumber"))!=null,
					"BuildingNumber field present in POST International Scheduled Payment Submission response body ");
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.PostCode"))!=null,
					"PostCode field present in POST International Scheduled Payment Submission response body ");
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.TownName"))!=null,
					"TownName field present in POST International Scheduled Payment Submission response body ");
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.CountrySubDivision"))!=null,
					"CountrySubDivision field present in POST International Scheduled Payment Submission response body ");
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.Country"))!=null,
					"Country field present in POST International Scheduled Payment Submission response body ");
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressLine"))!=null,
					"AddressLine field present in POST International Scheduled Payment Submission response body ");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
