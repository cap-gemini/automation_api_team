package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of OPTIONAL DeliveryAddress/BuildingNumber field having length variation
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_188 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_188() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
	    TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL DeliveryAddress/BuildingNumber field");                          
        iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
        iScheduledPayment.setConsentId(consentDetails.get("consentId"));
        iScheduledPayment.setRiskAddressBuildingNumber("ABCDEF1234567890");
        iScheduledPayment.submit();
		
        testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment URI");	
        testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Risk")==null, 
				"Risk block under Data is not present");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
