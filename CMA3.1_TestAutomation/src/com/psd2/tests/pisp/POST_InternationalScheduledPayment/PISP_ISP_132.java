package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of OPTIONAL CreditorPostalAddress/CountrySubDivision field 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_132 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_132() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
					
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
			"Response Code is correct for POST International Scheduled Payment where CountrySubDivision field having length in between 1-35 characters ");			
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.CountrySubDivision")!=null, 
			"Optional field CountrySubDivision is present under CreditorPostalAddress block");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.CountrySubDivision").toString().length()<=35), 
			"Optional field CountrySubDivision value is between 1 and 35");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
