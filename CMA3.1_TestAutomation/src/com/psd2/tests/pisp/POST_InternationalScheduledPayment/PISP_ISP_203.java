package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_203 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_203() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment SetUp....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment URI");		
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ConsentId"))!=null,
					"ConsentId field under Data is present in POST International schedule Payment response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ConsentId")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.InternationalScheduledPaymentId"))!=null,
					"InternationalScheduledPaymentId field under Data is present in POST Domestic Payment response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.InternationalScheduledPaymentId")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.CreationDateTime"))!=null,
					"CreationDateTime field under Data is present in POST International schedule Payment response body"+(iScheduledPayment.getResponseValueByPath("Data.CreationDateTime")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.StatusUpdateDateTime"))!=null,
					"StatusUpdateDateTime field under Data is present in POST International schedule Payment response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.StatusUpdateDateTime")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Status"))!=null,
					"Status field under Data is present in POST International schedule Payment response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Status")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExpectedExecutionDateTime"))!=null,
					"ExpectedExecutionDateTime field under Data is present in POST International schedule Payment response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ExpectedExecutionDateTime")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExpectedSettlementDateTime"))!=null,
					"ExpectedSettlementDateTime field under Data is present in POST International schedule Payment response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ExpectedSettlementDateTime")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Charges"))!=null,
					"Charges field under Data is present in POST International schedule Payment response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Charges")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation"))!=null,
					"ExchangeRateInformation field under Data is present in POST International schedule Payment response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation"))!=null,
					"Initiation field under Data is present in POST International schedule Payment response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.MultiAuthorisation"))!=null,
					"MultiAuthorisation field under Data is present in POST International schedule Payment response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.MultiAuthorisation")));
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
