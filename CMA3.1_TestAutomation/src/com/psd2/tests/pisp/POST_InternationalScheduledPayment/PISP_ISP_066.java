package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the status when MANDATORY InstructedAmount/Amount field block haven't sent 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_066 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_066() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");

				iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
				iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
				iScheduledPayment.setConsentId(consentDetails.get("consentId"));
				String _reqBody = iScheduledPayment.genRequestBody().replace("\"Amount\": \"300.12\",","");
				iScheduledPayment.submit(_reqBody);

			    testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
						"Response Code is correct for internationalScheduledPayments URI when MANDATORY InstructedAmount/Amount field block haven't sent");
			    testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Missing", "Error Code are matched");
				testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Missing required field [Amount]", "Error Message are matched");
				
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
