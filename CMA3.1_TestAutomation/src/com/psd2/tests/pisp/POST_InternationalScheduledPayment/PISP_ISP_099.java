package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of OPTIONAL ExchangeRateInformation/ExchangeRate field 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_099 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_099() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International Payment Consent URI");				
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExchangeRate")).isEmpty(), 
					"ExchangeRate field under ExchangeRateInformation is present and is not null");
			testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExchangeRate").length()>=1
					&&iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExchangeRate").length()<=70, 
					"ExchangeRate field under ExchangeRateInformation is of 1 to 70 characters");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
