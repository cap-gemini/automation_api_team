package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of Currency field underInstructedAmount/Currency having values less or greater than 3 characters 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_074 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_074() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment ....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setCurrency("EURA");
			iScheduledPayment.submit();
			
		    testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
					"Response Code is correct for International Scheduled Payment URI when mandatory Data/Initiation/InstructedAmount/Currency having values less or greater than 3 characters");
		    testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
			testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Invalid value 'EURA'. Expected ^[A-Z]{3,3}$ for Currency", "Error Message are matched");
			
			TestLogger.logBlankLine();	
			
			String key = iScheduledPayment.getHeaderEntry("x-idempotency-key");	
			
			TestLogger.logStep("[Step 3] : International Scheduled Payment");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+key);
			iScheduledPayment.setCurrency("EU");
			iScheduledPayment.submit();
			
		    testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
					"Response Code is correct for International Scheduled Payment URI when mandatory Data/Initiation/InstructedAmount/Currency having values less or greater than 3 characters");
		    testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
			testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Invalid value 'EU'. Expected ^[A-Z]{3,3}$ for Currency", "Error Message are matched");
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
