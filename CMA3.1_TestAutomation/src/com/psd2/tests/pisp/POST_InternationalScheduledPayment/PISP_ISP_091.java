package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY ExchangeRateInformation/UnitCurrency field 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_091 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_091() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International Scheduled Payment Consent URI");			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.UnitCurrency"))!=null,
					"UnitCurrency field under ExchangeRateInformation block is present in Get International Payment Consent response body"+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.UnitCurrency")));			
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.UnitCurrency")).length()==3  
					,"UnitCurrency value is present and is of 3 characters");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
