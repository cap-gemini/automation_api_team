package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into OPTIONAL ExpirationDateTime where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_225 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_225() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credentials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);    
        TestLogger.logBlankLine();
        
        TestLogger.logStep("[Step 2] : Verification of response when the RateType is provided as \"Actual\" in the request payload");                          
        iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
        iScheduledPayment.setRateType("Actual");
        String requestBody=iScheduledPayment.genRequestBody().replace("\"ExchangeRate\": "+iScheduledPayment._exchangeRate+",","");
        requestBody = requestBody.replace(",\"ContractIdentification\": \"123identification\"","");
        iScheduledPayment.submit(requestBody);
       
		String consentId=iScheduledPayment.getConsentId();
		
        TestLogger.logStep("[Step 3] : JWT Token Creation........");
		
		//reqObject.setBaseURL(apiConst.ro_endpoint);
		reqObject.setValueField(consentId);
		reqObject.setScopeField("payments");
		outId = reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Go to URL and authenticate consent");	
		redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		
		authCode = consentOps.authorisePISPConsent(redirecturl+"##"+iScheduledPayment._drAccountIdentification,iScheduledPayment.removeDebtorAccount);
		closeDriverInstance();
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 5] : Get access token");	
		accesstoken.setBaseURL(apiConst.at_endpoint);
		accesstoken.setAuthCode(authCode);
		accesstoken.submit();
		
		testVP.verifyStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
				"Response Code is correct for get access token request");	
		access_token = accesstoken.getAccessToken();
		TestLogger.logVariable("Access Token : " + access_token);		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 6] : Verification of response when the RateType is provided as \"Actual\" in the request payload");                          
        iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+access_token);
        iScheduledPayment.setConsentId(consentId);
        iScheduledPayment.setRateType("Actual");
        requestBody=iScheduledPayment.genRequestBody().replace("\"ExchangeRate\": "+iScheduledPayment._exchangeRate+",","");
        requestBody = requestBody.replace(",\"ContractIdentification\": \"123identification\"","");
        iScheduledPayment.submit(requestBody);

		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for POST International Scheduled Payment");
		testVP.verifyTrue(Misc.verifyDateTimeFormat(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExpirationDateTime").split("T")[0], "yyyy-MM-dd") && 
				(Misc.verifyDateTimeFormat(iScheduledPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExpirationDateTime").split("T")[1], "HH:mm:ss+00:00")), 
				"ExpirationDateTime is as per expected format");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
