package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of international-payments URL having older version with 3.1 version payload 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Invalid"})
public class PISP_ISP_006 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_006() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment SetUp....");
			//iScheduledPayment.setBaseURL(apiConst.backcomp_isp_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			//String _reqBody = iScheduledPayment.genRequestBody().replace("\"CountrySubDivision\":[ \""+iScheduledPayment._riskCountrySubDivision+"\"],","\"CountrySubDivision\": \""+iScheduledPayment._riskCountrySubDivision+"\",");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"404",
					"Response Code is correct for POST International Scheduled Payment Submission for version 3.0 in PISP");
			
			/*testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400",
					"Response Code is correct for POST International Scheduled Payment Submission for version 3.0 in PISP");
			testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Resource.NotFound", 
						"Error code for the response is correct i.e. '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message").equals("payment setup id is not found in platform"), 
						"Message for error code is '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");*/
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
