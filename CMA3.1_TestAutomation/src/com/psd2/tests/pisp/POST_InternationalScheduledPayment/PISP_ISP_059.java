package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of MANDATORY Initiation/RequestedExecutionDateTime field NOT sent
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_059 extends TestBase {
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_059() throws Throwable{	
		

		TestLogger.logStep("[Step 1] : Generate access token");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of MANDATORY Initiation/RequestedExecutionDateTime field NOT sent");
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		String requestBody=iScheduledPayment.genRequestBody().replace("\"RequestedExecutionDateTime\": \"2021-08-06T06:06:06.666+00:00\",","");
		iScheduledPayment.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for International Scheduled Payment URI");
		
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Missing", 
				"Error code for the response is correct i.e. '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Missing required field [RequestedExecutionDateTime]"), 
				"Message for error code is '"+iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
