package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/InstructedAmount block where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_235 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_235() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
            iScheduledPayment.submit();
			
            testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment URI");		
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructedAmount"))!=null,
					"InstructedAmount block under Initiation block is present in POST International scheduled Payment response body and is not null");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructedAmount.Amount"))!=null,
					"Amount block under InstructedAmount block is present in POST International scheduled Payment response body and is not null");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructedAmount.Currency"))!=null,
					"Currency block under InstructedAmount block is present in POST International scheduled Payment response body and is not null");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
