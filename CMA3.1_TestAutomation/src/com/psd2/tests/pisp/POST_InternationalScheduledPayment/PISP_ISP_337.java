package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the Invalid value of MANDATORY DebtorAccount/Identification field when SchemeName = UK.OBIE.PAN/PAN
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Blocked"})
public class PISP_ISP_337 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_ISP_337() throws Throwable{	
		iScheduledPayment.setDrAccountSchemeName("UK.OBIE.PAN");
		iScheduledPayment.setDrAccountIdentification("123456789011211314");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		apiUtility.pispAccessToken=true;
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Scheduled Payment Submission ");	
        iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
        iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.setDrAccountSchemeName("UK.OBIE.PAN");
		iScheduledPayment.setDrAccountIdentification("123456789011211314555");
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400",
				"Response Code is correct for Domestic Payment URI when DebtorAccount Identification value is not valid");

		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Resource.ConsentMismatch",
				"Error code for the response is correct i.e. '"+ iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+ "'");

		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message").equals("Payload comparison failed with the consent resource"),"Message for error code is '"
						+ iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message")+ "'");
	
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
