package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/MultiAuthorisation block where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_282 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_282() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
            iScheduledPayment.submit();
			
            testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment URI");		
            testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.MultiAuthorisation"))!=null, 
					"MultiAuthorisation block is present");			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.MultiAuthorisation.Status"))!=null, 
					"Mandatory field Status is present in Authorisation block and is not null");			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.MultiAuthorisation.NumberRequired"))!=null,
					"Optional field NumberRequired is present in Authorisation block and is not null");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.MultiAuthorisation.LastUpdateDateTime"))!=null,
					"Optional field LastUpdateDateTime is present in Authorisation block and is not null");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.MultiAuthorisation.ExpirationDateTime"))!=null,
					"Optional field ExpirationDateTime is present in Authorisation block and is not null");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
