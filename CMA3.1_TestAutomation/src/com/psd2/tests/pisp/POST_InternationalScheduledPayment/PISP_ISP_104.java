package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of MANDATORY CreditorAccount/SchemeName field having OB defined values 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_104 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_104() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International Scheduled Payment URI");		
			testVP.verifyTrue(iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.SortCodeAccountNumber")
					|| iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("SortCodeAccountNumber")
					|| iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.IBAN")
					|| iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("IBAN")
					|| iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.PAN")
					|| iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("PAN")
					|| iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.Paym")
					|| iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("Paym")
					|| iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.BBAN")
					|| iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("BBAN")
					|| iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme1")
					|| iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme2"), 
					"MANDATORY CreditorAccount/SchemeName field has OB defined values");		
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
