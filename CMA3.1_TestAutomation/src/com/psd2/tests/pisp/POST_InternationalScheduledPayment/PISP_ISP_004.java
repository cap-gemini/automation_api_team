package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through POST method with mandatory and optional fields. 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_004 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_004() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()), "201",
					"Response Code is correct for POST International Scheduled Payment Submission");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data")) != null,
					"Mandatory field Data is present in response and is not empty");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Charges")) != null,
					"Optional field Charges is present in response and is not empty");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation")) != null,
					"Mandatory field Initiation is present in response and is not empty");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.MultiAuthorisation")) != null,
					"Optional field MultiAuthorisation is present in response and is not empty");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Links")) != null,
					"Mandatory field Links is present in response and is not empty");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Links.Self")) != null,
					"Mandatory field Self is present in response and is not empty");
			testVP.verifyStringEquals(iScheduledPayment.getResponseHeader("Content-Type"), "application/json;charset=utf-8", 
					"Response is UTF-8 character encoded");
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
