package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = any.bank.Scheme1
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Blocked"})
public class PISP_ISP_338 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_ISP_338() throws Throwable{	
		iScheduledPayment.setDrAccountSchemeName("any.bank.scheme1");
		iScheduledPayment.setDrAccountIdentification("scheme1");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Scheduled Payment Submission ");	
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
        iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for POST International Scheduled Payment Submission");
		
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName"), "any.bank.scheme1", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification"), "scheme1", 
				"Identification field value is correct");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
