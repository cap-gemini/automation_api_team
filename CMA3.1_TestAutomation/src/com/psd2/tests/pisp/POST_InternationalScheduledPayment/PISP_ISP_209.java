package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Status field where Request has sent successfully and returned a HTTP Code 201 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_209 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_209() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment SetUp....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for POST International Scheduled Payment");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Status"))!=null,
					"Status field under Data is present in POST International Scheduled Payment response body"+(iScheduledPayment.getResponseValueByPath("Data.Status")));				
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Status")).equals("InitiationPending")||
						String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Status")).equals("InitiationFailed")||
						String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Status")).equals("InitiationCompleted"), 
					"Status is correct "+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Status")));		
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 3] : Verification of the values into Status field of consent API where Request has sent successfully and returned a HTTP Code 201 Created");
			
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
			iScheduledPayment.setMethod("GET");
			iScheduledPayment.submit();
				
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
					"Response Code is correct for International Scheduled Payment Orders URL");
			
			testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Data.Status").equals("Consumed"),
					"Status field value is correct i.e. "+iScheduledPayment.getResponseNodeStringByPath("Data.Status"));
				
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
	}
}
