package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request where same  request payload has been sent into BODY with same x-idempotency-key by same TPP within 24 hrs
 * @author Rama Arora
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_ISP_010 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_ISP_010() throws Throwable {

		String idemPotencyKey=PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3);
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST International Scheduled Payment Submission");

		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",x-idempotency-key:"+idemPotencyKey);
		iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.submit();

		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment when x-idempotency-key value in header is "+idemPotencyKey+"");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of request with same x-idempotency-key i.e. "+idemPotencyKey+" and same payload");
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",x-idempotency-key:"+idemPotencyKey);
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment when x-idempotency-key value is same i.e. "+idemPotencyKey+" and payload is same");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
