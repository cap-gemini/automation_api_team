package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/InstructedAmount/Amount field where Amount starts with 300
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_315 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_ISP_315() throws Throwable{	
		iScheduledPayment.setAmount("300.00");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
        TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : POST International Scheduled Payment Submission");	
	    iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
        iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
			"Response Code is correct for International Scheduled Payment Submission URI");
		    
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Data.Status"), "InitiationCompleted",
			"Status field value is correct");
		
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Data.MultiAuthorisation.Status"), "Authorised",
	    		"Status field value under MultiAuthorisation block is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the values into Status field of consent API where Request has sent successfully and returned a HTTP Code 201 Created");
		
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		iScheduledPayment.setMethod("GET");
		iScheduledPayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for International Scheduled Payment Orders URL");
		
		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Data.Status").equals("Consumed"),
				"Status field value is correct i.e. "+iScheduledPayment.getResponseNodeStringByPath("Data.Status"));
			
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
