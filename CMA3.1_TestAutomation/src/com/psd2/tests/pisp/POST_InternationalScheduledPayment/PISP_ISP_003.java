package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :  Verification of domestic-scheduled-payments URL that should be as per Open Banking standards as specified format 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_003 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_003() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");
			iScheduledPayment.setBaseURL(apiConst.otherpispapi_dso_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
										
			testVP.verifyStringEquals(iScheduledPayment.getURL(),apiConst.otherpispapi_dso_endpoint,
					"URL for POST International Scheduled Payment Submission is as per open banking standard");
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()), "400",
					"Response Code is correct for POST International Scheduled Payment Submission");
			testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].ErrorCode"),
					"UK.OBIE.Field.Missing", "Error Code are matched");		
			testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Errors[0].Message"),
					"Error validating JSON. Error: - Missing required field [Frequency]","Error Message are matched");		
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
