package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY CreditorAccount/SchemeName field 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_103 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_103() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International Scheduled Payment Submission URI");			
			testVP.verifyTrue(!(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SchemeName")).isEmpty(), 
					"SchemeName field under CreditorAccount is present and is not null");
			testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SchemeName").length()>=1
					&&iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SchemeName").length()<=40, 
					"SchemeName field under CreditorAccount is of 1 to 40 characters");		
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
