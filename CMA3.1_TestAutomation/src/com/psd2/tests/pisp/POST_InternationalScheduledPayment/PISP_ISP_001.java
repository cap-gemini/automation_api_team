package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of CMA compliance for POST International Scheduled Payment URI 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class PISP_ISP_001 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_001() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
		    		"Response Code is correct for Post International Scheduled Payment");
			
			testVP.verifyStringEquals(apiConst.iScheduledPaymentSubmission_endpoint,apiConst.iScheduledPaymentSubmission_endpoint, 
		    		"POST International Scheduled Payment URI is as per CMA Compliance");
			
			testVP.verifyTrue(SignatureUtility.verifySignature(iScheduledPayment.getResponseString(), iScheduledPayment.getResponseHeader("x-jws-signature")), 
					"Response that created successfully with HTTP Code 201 MUST be digitally signed");
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
