package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY CreditorAgent/SchemeName field 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_265 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_265() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
            iScheduledPayment.submit();
			
            testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International scheduled Payment URI");		
            testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent"))!=null, 
					"CreditorAgent block is present International scheduled Payment Response Body and is not null");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.SchemeName"))!=null, 
					"MANDATORY field SchemeName is present in International scheduled Payment Response Body and is not null");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.Identification"))!=null,
					"MANDATORY field Identification is present in International scheduled Payment Response Body and is not null");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.Name"))!=null,
					"Optional field Name is present in International scheduled Payment Response Body and is not null");
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.SchemeName")), 
					iScheduledPayment._crAgentSchemeName, "SchemeName is same as sent in the request");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
