package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/CreditorAgent/Name block
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_147 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_147() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment SetUp....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			iScheduledPayment.submit();
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International Scheduled Payment URI");		
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.Name")), 
					iScheduledPayment._crAgentName, "Name field value is correct");			
			testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.Name").length()>=1&&
					iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAgent.Name").length()<=140, 
					"Length of Name is between 1 to 140 characters");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
