package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY CreditorAccount/Identification field when SchemeName = any.bank.scheme2
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_351 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_ISP_351() throws Throwable{	
		iScheduledPayment.setCrAccountSchemeName("any.bank.scheme2");
		iScheduledPayment.setCrAccountIdentification("scheme2");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Scheduled Payment ");	
	    iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		iScheduledPayment.setCrAccountSchemeName("any.bank.scheme2");
		iScheduledPayment.setCrAccountIdentification("scheme2");
		iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for POST International Scheduled Payment Submission");		
		
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SchemeName"), "any.bank.scheme2", 
				"SchemeName field value is correct");	
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.Identification"), "scheme2", 
				"Identification field value is correct");
		testVP.verifyTrue(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.Identification").toString().length()<=256, 
				"Identification field length is correct");
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
