package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = UK.OBIE.IBAN/IBAN
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_335 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_ISP_335() throws Throwable{	
		iScheduledPayment.setDrAccountSchemeName(PropertyUtils.getProperty("DrAccount_SchemeName"));
		iScheduledPayment.setDrAccountIdentification(PropertyUtils.getProperty("DrAccount_Identification"));
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		apiUtility.pispAccessToken=true;
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Scheduled Payment Submission ");	
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
		 iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		 iScheduledPayment.setConsentId(consentDetails.get("consentId"));
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for POST International Scheduled Payment Submission");		
		
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName"), "UK.OBIE.IBAN", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(iScheduledPayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification"), iScheduledPayment._drAccountIdentification, 
				"Identification field value is correct");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
