package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/ExchangeRateInformation block 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_090 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_090() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");

			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
	        iScheduledPayment.submit();	     
	        
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"201", 
					"Response Code is correct for International Scheduled Payment Submission URI");						
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation"))!=null,
					"ExchangeRateInformation block under Data is present in Get International Scheduled Payment Submission response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation")));			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.UnitCurrency"))!=null,
					"UnitCurrency field under ExchangeRateInformation block is present in Get International Scheduled Payment Submission response body"+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.UnitCurrency")));			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExchangeRate"))!=null,
					"ExchangeRate field under ExchangeRateInformation block is present in Get International Scheduled Payment Submission response body"+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExchangeRate")));			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType"))!=null,
					"RateType field under ExchangeRateInformation block is present in Get International Scheduled Payment Submission response body"+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType")));			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ContractIdentification"))!=null,
					"ContractIdentification field under ExchangeRateInformation block is present in Get International Scheduled Payment Submission response body"+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ContractIdentification")));			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExpirationDateTime"))==null,
					"ExpirationDateTime field under ExchangeRateInformation block is not present in Get International Scheduled Payment Submission response body"+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExpirationDateTime")));		
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
