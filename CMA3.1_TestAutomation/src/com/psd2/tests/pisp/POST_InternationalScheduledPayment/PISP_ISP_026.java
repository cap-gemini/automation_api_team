package com.psd2.tests.pisp.POST_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request where MANDATORY x-jws-signature header haven't sent
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_ISP_026 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_ISP_026() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, false);
		    TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment Submission....");
			
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
			restRequest.setURL(apiConst.iScheduledPaymentSubmission_endpoint);
			iScheduledPayment.setConsentId(consentDetails.get("consentId"));
			restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			restRequest.addHeaderEntry("x-idempotency-key:" , PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
			restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
			restRequest.addHeaderEntry("Content-Type", "application/json");
			restRequest.addHeaderEntry("x-fapi-interaction-id:" , PropertyUtils.getProperty("inter_id"));
			restRequest.addHeaderEntry("Accept", "application/json");
			restRequest.setRequestBody(iScheduledPayment.genRequestBody());
			restRequest.setMethod("POST");
			restRequest.submit();
			
			testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
					"Response Code is correct for internationalScheduledPayments");			
			testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Missing", 
					"Response Error Code is correct");			
			testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].Message")),"Required header x-jws-signature not specified","Error message is correct");
				
	}
}
