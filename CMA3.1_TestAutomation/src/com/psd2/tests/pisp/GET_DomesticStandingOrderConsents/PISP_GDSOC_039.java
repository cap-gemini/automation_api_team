package com.psd2.tests.pisp.GET_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Initiation block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSOC_039 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSOC_039() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Standing Order Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Initiation block where Request has sent successfully and returned a HTTP Code 200 OK");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint+"/"+consentDetails.get("consentId"));
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation.Frequency"))!=null, 
				"Mandatory field Frequency is present in Initiation block"+dStandingOrder.getResponseValueByPath("Data.Initiation.Frequency"));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation.Reference"))!=null, 
				"Optional field Reference is present in Initiation block"+dStandingOrder.getResponseValueByPath("Data.Initiation.Reference"));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation.FirstPaymentDateTime"))!=null,
				"Mandatory field FirstPaymentDateTime is present in Initiation block"+dStandingOrder.getResponseValueByPath("Data.Initiation.FirstPaymentDateTime"));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation.RecurringPaymentDateTime"))!=null, 
				"Optional field RecurringPaymentDateTime is present in Initiation block"+dStandingOrder.getResponseValueByPath("Data.Initiation.RecurringPaymentDateTime"));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation.FinalPaymentDateTime"))!=null,
				"Optional field FinalPaymentDateTime is present in Initiation block"+dStandingOrder.getResponseValueByPath("Data.Initiation.FinalPaymentDateTime"));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation.FirstPaymentAmount"))!=null,
				"Mandatory field FirstPaymentAmount is present in Initiation block"+dStandingOrder.getResponseValueByPath("Data.Initiation.FirstPaymentAmount"));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation.RecurringPaymentAmount"))!=null,
				"Optional field RecurringPaymentAmount is present in Initiation block"+dStandingOrder.getResponseValueByPath("Data.Initiation.RecurringPaymentAmount"));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation.FinalPaymentAmount"))!=null,
				"Optional field FinalPaymentAmount is present in Initiation block"+dStandingOrder.getResponseValueByPath("Data.Initiation.FinalPaymentAmount"));
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
}

