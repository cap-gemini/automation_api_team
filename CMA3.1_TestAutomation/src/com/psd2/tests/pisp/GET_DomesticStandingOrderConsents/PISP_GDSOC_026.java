package com.psd2.tests.pisp.GET_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSOC_026 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSOC_026() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Standing Order Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 200 OK");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint+"/"+consentDetails.get("consentId"));
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		testVP.verifyStringEquals(dStandingOrder.getURL(),apiConst.dsoConsent_endpoint+"/"+consentDetails.get("consentId"), 
				"URI for GET Domestic Standing Order Consent request is as per open banking standard");
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.ConsentId"))!=null,
				"ConsentId field under Data is present in Get Domestic Standing Order Consent response body"+String.valueOf(dStandingOrder.getResponseValueByPath("Data.ConsentId")));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.CreationDateTime"))!=null,
				"CreationDateTime field under Data is present in Get Domestic Standing Order Consent response body"+(dStandingOrder.getResponseValueByPath("Data.CreationDateTime")));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.StatusUpdateDateTime"))!=null,
				"StatusUpdateDateTime field under Data is present in Get Domestic Standing Order Consent response body"+String.valueOf(dStandingOrder.getResponseValueByPath("Data.StatusUpdateDateTime")));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Status"))!=null,
				"Status field under Data is present in Get Domestic Standing Order Consent response body"+String.valueOf(dStandingOrder.getResponseValueByPath("Data.Status")));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Permission"))!=null,
				"Permission field under Data is present in Get Domestic Standing Order Consent response body"+String.valueOf(dStandingOrder.getResponseValueByPath("Data.Permission")));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.CutOffDateTime"))!=null,
				"CutOffDateTime field under Data is present in Get Domestic Standing Order Consent response body"+String.valueOf(dStandingOrder.getResponseValueByPath("Data.CutOffDateTime")));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Charges"))!=null,
				"Charges field under Data is present in Get Domestic Standing Order Consent response body"+String.valueOf(dStandingOrder.getResponseValueByPath("Data.Charges")));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation"))!=null,
				"Initiation field under Data is present in Get Domestic Standing Order Consent response body"+String.valueOf(dStandingOrder.getResponseValueByPath("Data.Initiation")));
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Authorisation"))!=null,
				"Authorisation field under Data is present in Get Domestic Standing Order Consent response body"+String.valueOf(dStandingOrder.getResponseValueByPath("Data.Authorisation")));
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}

