package com.psd2.tests.pisp.GET_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of response that created successfully with HTTP Code 201 MUST be UTF-8 character encoded
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSOC_023 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSOC_023() throws Throwable{	
		TestLogger.logStep("[Step 1] : Create Domestic Standing Order Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of response that created successfully with HTTP Code 201 MUST be UTF-8 character encoded");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint+"/"+consentDetails.get("consentId"));
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseHeader("Content-Type")), "application/json;charset=UTF-8", 
				"Content-Type header response is correct");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}

