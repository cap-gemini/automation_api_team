package com.psd2.tests.pisp.GET_DomesticStandingOrderConsents;

import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with expired token value into Authorization header
 * @author Mohit Patidar
 *
 */
public class PISP_GDSOC_013 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSOC_013() throws Throwable{	

		TestLogger.logStep("[Step 1] : Create Domestic Standing Order Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with expired token value into Authorization header");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint+"/"+consentDetails.get("consentId"));
		dStandingOrder.setHeadersString("Authorization:Bearer "+apiConst.othertpp_cc_access_token);
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"401", 
				"Response Code is correct for Domestic Standing Order Consent URI when expired value of access token is used");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
