package com.psd2.tests.pisp.GET_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/CreditorAccount block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSOC_060 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSOC_060() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Standing Order Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Data/Initiation/CreditorAccount block where Request has sent successfully and returned a HTTP Code 200 OK");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint+"/"+consentDetails.get("consentId"));
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount"))!=null, 
				"Mandatory field SchemeName is present in CreditorAccount block "+dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount"));
		
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName"))!=null, 
				"Mandatory field SchemeName is present in CreditorAccount block "+ dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.SchemeName"));
		
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.Identification"))!=null, 
				"Mandatory field Identification is present in CreditorAccount block "+dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.Identification"));
		
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.Name"))!=null, 
				"Optional field Name is present in CreditorAccount block "+dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.Name"));
		
		testVP.verifyTrue((dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.SecondaryIdentification"))!=null, 
				"Optional field SecondaryIdentification is present in CreditorAccount block "+dStandingOrder.getResponseValueByPath("Data.Initiation.CreditorAccount.SecondaryIdentification"));
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}

