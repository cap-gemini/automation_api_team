package com.psd2.tests.pisp.GET_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY permission field where Request has sent successfully and returned a HTTP Code 200 ok
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSOC_031 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSOC_031() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Standing Order Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY permission field where Request has sent successfully and returned a HTTP Code 200 ok");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint+"/"+consentDetails.get("consentId"));
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Permission").equals("Create"), 
				"Permission field value is correct");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}

