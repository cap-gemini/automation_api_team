package com.psd2.tests.pisp.GET_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of domestic-standing-order-consents URL that should be as per Open Banking standards as specified
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class PISP_GDSOC_001 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSOC_001() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Standing Order Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, true, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : GET Domestic Standing Order Consent");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint+"/"+consentDetails.get("consentId"));
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		
		testVP.verifyStringEquals(dStandingOrder.getURL(),apiConst.dsoConsent_endpoint+"/"+consentDetails.get("consentId"), 
				"URI for GET domestic standing order consent request is as per open banking standard");
		
		testVP.verifyTrue(SignatureUtility.verifySignature(dStandingOrder.getResponseString(), dStandingOrder.getResponseHeader("x-jws-signature")), 
				"Response that created successfully with HTTP Code 201 MUST be digitally signed");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}