package com.psd2.tests.pisp.GET_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of request when ConsentId is not sent in the request
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSOC_006 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSOC_005() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Standing Order Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request when ConsentId is not sent in the request");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint+"/"+" ");
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"405", 
				"Response Code is correct for Domestic Standing Order Consent URI when consentId is not passed");
	
		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}

