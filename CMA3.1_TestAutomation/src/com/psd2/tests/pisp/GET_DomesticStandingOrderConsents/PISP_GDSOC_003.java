package com.psd2.tests.pisp.GET_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of domestic-standing-order-consents URL that is should as per Open Banking standards with different PISP/Other Url
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSOC_003 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSOC_003() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Standing Order Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, true, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : Verification of domestic-standing-order-consents URL that is should as per Open Banking standards with different PISP/Other Url");
		
		dStandingOrder.setBaseURL(apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"));
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Resource.NotFound", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseValueByPath("Errors[0].Message")),"payment setup id is not found in platform",
				"Error message is correct");

		TestLogger.logBlankLine();

		testVP.testResultFinalize();
	}
}