package com.psd2.tests.pisp.GET_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into OPTIONAL CutOffDateTime field where Request has sent successfully and returned a HTTP Code 200 OK
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GDSOC_032 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_GDSOC_032() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Standing Order Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticStandingOrders, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL CutOffDateTime field where Request has sent successfully and returned a HTTP Code 200 OK");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint+"/"+consentDetails.get("consentId"));
		dStandingOrder.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dStandingOrder.setMethod("GET");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		
		testVP.verifyTrue(Misc.verifyDateTimeFormat(dStandingOrder.getResponseNodeStringByPath("Data.CutOffDateTime").split("T")[0], "yyyy-MM-dd") && 
				(Misc.verifyDateTimeFormat(dStandingOrder.getResponseNodeStringByPath("Data.CutOffDateTime").split("T")[1], "HH:mm:ss+00:00")), 
				"CutOffDateTime is as per expected format");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}

