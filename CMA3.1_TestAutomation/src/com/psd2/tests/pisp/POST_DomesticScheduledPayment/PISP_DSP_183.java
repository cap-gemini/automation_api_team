package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Amount field where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_183 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DSP_183() throws Throwable{	
	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
			
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Scheduled Payment Consent URI");
		
		testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Charges[0].Amount.Amount").split("\\.")[0].length()<=13 
				&& dspConsent.getResponseNodeStringByPath("Data.Charges[0].Amount.Amount").split("\\.")[1].length()<=5, 
				"Amount under Charges block consists of Amount field which is equal to 13 digits and 5 precisions");
			    
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}

