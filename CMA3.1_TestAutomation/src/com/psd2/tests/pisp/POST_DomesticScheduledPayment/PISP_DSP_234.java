package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of date and time for below mentioned dates fields without giving seconds
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_234 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DSP_234() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
        String key = dspConsent.getHeaderEntry("x-idempotency-key");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of date and time for RequestedExecutionDateTime field without giving seconds");
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-idempotency-key:"+key);
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.setRequestedExecutionDateTime("2021-03-20T06:06");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Scheduled Payment Consent URI when RequestedExecutionDateTime field value is without seconds");
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Provided value 2021-03-20T06:06 is not compliant with the format datetime provided in rfc3339 for RequestedExecutionDateTime", "Error Message are matched");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of date and time for RequestedExecutionDateTime field without giving seconds");
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-idempotency-key:"+key);
		dspConsent.setRequestedExecutionDateTime("2021-03-20T06:06Z");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Scheduled Payment Consent URI when RequestedExecutionDateTime field value is without seconds");
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Provided value 2021-03-20T06:06Z is not compliant with the format datetime provided in rfc3339 for RequestedExecutionDateTime", "Error Message are matched");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of date and time for RequestedExecutionDateTime field without giving seconds");
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-idempotency-key:"+key);
		dspConsent.setRequestedExecutionDateTime("2021-03-20T06:06+00:00");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Scheduled Payment Consent URI when RequestedExecutionDateTime field value is without seconds");
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Provided value 2021-03-20T06:06+00:00 is not compliant with the format datetime provided in rfc3339 for RequestedExecutionDateTime", "Error Message are matched");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}