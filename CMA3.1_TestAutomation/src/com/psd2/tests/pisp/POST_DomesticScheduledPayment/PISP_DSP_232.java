package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/InstructedAmount/Amount field starts with 700 for Currency as EUR/GBP
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_232 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DSP_232() throws Throwable{	
		dspConsent.setAmount("700.12");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Domestic Scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201",
				"Response Code is correct for Domestic Scheduled Payment URI when InstructedAmount/Amount as 700");

		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Data.Status")), "InitiationCompleted",
				"status is correct");
         
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the values into Status field of consent API where Request has sent successfully and returned a HTTP Code 201 Created");
		
		dspConsent.setBaseURL(apiConst.dspConsent_endpoint+"/"+consentDetails.get("consentId"));
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		dspConsent.setMethod("GET");
		dspConsent.submit();
			
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Scheduled Payment Orders URL");
		
		testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Status").equals("Consumed"),
				"Status field value is correct i.e. "+dspConsent.getResponseNodeStringByPath("Data.Status"));
			
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}