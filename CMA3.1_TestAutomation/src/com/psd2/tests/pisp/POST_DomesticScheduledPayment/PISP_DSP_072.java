package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = UK.OBIE.IBAN/IBAN
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_072 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DSP_72() throws Throwable{	
		dspConsent.setDrAccountSchemeName(PropertyUtils.getProperty("DrAccount_SchemeName"));
		dspConsent.setDrAccountIdentification(PropertyUtils.getProperty("DrAccount_Identification"));
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Domestic Scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Scheduled Payment URI");
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName")),"UK.OBIE.IBAN",
				"SchemeName value is correct");
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount.Identification")),"IE85BOFI90120412345679",
				"Identification value is correct");
	}
}