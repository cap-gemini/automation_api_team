package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of Amount field having both values more than 13 non-decimal and 5 decimals digits under InstructedAmount/Amount
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_056 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_056() throws Throwable{	
	
				
			TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
	        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");	
			
			dspConsent.setBaseURL(apiConst.dsp_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dspConsent.setConsentId(consentDetails.get("consentId"));
			dspConsent.setAmount("3123456789012111.1234511111");
			dspConsent.submit();
		
		    TestLogger.logStep("[Step 3] : Verification of the status when MANDATORY InstructedAmount/Amount field having both values more than 13 non-decimal and 5 decimals");
		    testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled Payment URI when MANDATORY InstructedAmount/Amount field having both values more than 13 non-decimal and 5 decimals");
			
		    testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Invalid value '3123456789012111.1234511111'. Expected ^\\d{1,13}\\.\\d{1,5}$ for Amount", "Error Message are matched");
			
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
