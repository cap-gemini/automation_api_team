package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without token value OR
 * key-value pair into Authorization (Authorization Code) header
 * 
 * @author Kiran Dewangan
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_DSP_018 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_DSP_018() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission without token value");
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer ");
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();

		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"401",
				"Response Code is correct for Domestic Scheduled Payment Consent when request without token value");

		TestLogger.logStep("[Step 3] : POST Domestic Scheduled Payment Submission without Authorization");
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		restRequest.setURL(apiConst.dsp_endpoint);
		String requestBody= dspConsent.genRequestBody();
		restRequest.setHeadersString("Accept:application/json, x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3)+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", 
				"Response Code is correct for without Authorization key-value pair into header for Domestic schedule Payment Submission");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
