package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of domestic-scheduled-payments URL that is NOT as per Open Banking standards as specified format
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_002 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_002() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
		consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
	
		TestLogger.logStep("[Step 2] : Domestic Schedule Payment ");	
		dspConsent.setBaseURL(apiConst.invalid_payment_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"404", 
				"Response Code is correct for NON domestic- scheduled payment Submission");		
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
