package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of domestic-schedule-payments URL that should be as per Open Banking standards as specified format 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class PISP_DSP_001 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_DSP_001() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : POST Domestic scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic scheduled Payment Submission");
		
		testVP.verifyEquals(dspConsent.getURL(), apiConst.dsp_endpoint,"Response Code is correct for Get Domestic scheduled Payment Submission URL");
		
		testVP.verifyTrue(SignatureUtility.verifySignature(dspConsent.getResponseString(), dspConsent.getResponseHeader("x-jws-signature")), 
				"Response that created successfully with HTTP Code 201 MUST be digitally signed");
		
		String paymentId=dspConsent.getPaymentId();
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Data.Status"), "InitiationCompleted",
				"Status field value is correct");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : POST Domestic scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic scheduled Payment Submission");
		
		testVP.assertStringEquals(dspConsent.getResponseNodeStringByPath("Data.DomesticScheduledPaymentId"), paymentId, "PaymentId is same as the request sent previously");
		
		TestLogger.logStep("[Step 4] : POST Domestic scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic scheduled Payment Submission");
		
		testVP.assertStringEquals(dspConsent.getResponseNodeStringByPath("Data.DomesticScheduledPaymentId"), paymentId, "PaymentId is same as the request sent previously");
		
		TestLogger.logStep("[Step 5] : POST Domestic scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic scheduled Payment Submission");
		
		testVP.assertStringEquals(dspConsent.getResponseNodeStringByPath("Data.DomesticScheduledPaymentId"), paymentId, "PaymentId is same as the request sent previously");
		
		TestLogger.logStep("[Step 6] : POST Domestic scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic scheduled Payment Submission");
		
		testVP.assertStringEquals(dspConsent.getResponseNodeStringByPath("Data.DomesticScheduledPaymentId"), paymentId, "PaymentId is same as the request sent previously");
		
		testVP.testResultFinalize();	
	}
}
