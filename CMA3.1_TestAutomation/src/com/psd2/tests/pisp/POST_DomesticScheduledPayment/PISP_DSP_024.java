package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of x-fapi-interaction-id value when NOT sent in the request with the response header 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_024 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_024() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Domestic Scheduled Payment ");
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setConsentId(consentDetails.get("consentId"));
		restRequest.setURL(apiConst.dsp_endpoint);
		String requestBody= dspConsent.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", Content-Type:application/json, x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Scheduled Payments");
		testVP.verifyTrue(restRequest.getResponseHeader("x-fapi-interaction-id")!=null, 
				"x-fapi-interaction-id value in the response header is provided by ASPSP if not sent in the request"+(accountSetup.getResponseHeader("x-fapi-interaction-id")));
	
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
