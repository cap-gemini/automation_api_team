package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the status when MANDATORY InstructedAmount/Amount field  haven't sent
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_053 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_053() throws Throwable{	
	
			
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");	
			
		    dspConsent.setBaseURL(apiConst.dsp_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dspConsent.setConsentId(consentDetails.get("consentId"));
			String _reqBody = dspConsent.genRequestBody().replace("\"Amount\": \"300.12\",","");
			dspConsent.submit(_reqBody);

		TestLogger.logStep("[Step 3] : Verification of the status when MANDATORY InstructedAmount/Amount field haven't sent");
		    testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled Payment URI when MANDATORY InstructedAmount/Amount field block haven't sent");
		    testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Missing", "Error Code are matched");
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Missing required field [Amount]", "Error Message are matched");
			
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
