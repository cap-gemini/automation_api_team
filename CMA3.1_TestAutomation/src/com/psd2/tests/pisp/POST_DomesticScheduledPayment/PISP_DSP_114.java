package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/CreditorPostalAddress block
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_114 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_114() throws Throwable{	
	
			
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for POST Domestic Scheduled Payment Submission");
		
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data")!=null, 
				"Mandatory block Data is present and is not null");
	    
	    testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.ConsentId")).isEmpty(), 
				"Mandatory field ConsentId is present under Data");
	    
	    testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress")!=null, 
			"Optional field CreditorPostalAddress is present under Initiation block");
	    
	    testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.AddressType")!=null, 
				"Optional field AddressType is present under CreditorPostalAddress block");
	    
	    testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.Department")!=null, 
				"Optional field Department is present under CreditorPostalAddress block");
	    
	    testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.SubDepartment")!=null, 
				"Optional field SubDepartment is present under CreditorPostalAddress block");
	
	    testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.StreetName")!=null, 
				"Optional field StreetName is present under CreditorPostalAddress block");
	    
	    testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.BuildingNumber")!=null, 
				"Optional field BuildingNumber is present under CreditorPostalAddress block");
	    
	    testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.PostCode")!=null, 
				"Optional field PostCode is present under CreditorPostalAddress block");
	    
	    testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.TownName")!=null, 
				"Optional field TownName is present under CreditorPostalAddress block");
	
	    testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.CountrySubDivision")!=null, 
				"Optional field CountrySubDivision is present under CreditorPostalAddress block");
	    
	    testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.Country")!=null, 
				"Optional field Country is present under CreditorPostalAddress block");
	    
	    testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.AddressLine")!=null, 
				"Optional field AddressLine is present under CreditorPostalAddress block");
		
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();		
	}
}
