package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with malformed token value
 * into Authorization (Authorization Code) header
 * 
 * @author kiran dewangan
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_DSP_017 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_DSP_017() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+"123");
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();

		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"401",
				"Response Code is correct for Domestic Scheduled Payment Consent when for malformed access token");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
