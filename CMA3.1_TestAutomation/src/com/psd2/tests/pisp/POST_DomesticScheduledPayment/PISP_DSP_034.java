package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation
 * block
 * 
 * @author Kiran Dewangan
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_DSP_034 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_DSP_034() throws Throwable {

		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();

		TestLogger.logStep("[Step 3] : Verification of the values into MANDATORY Initiation block where Request has sent successfully and returned a HTTP Code 201 Created");
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()), "201",
				"Response Code is correct for Domestic Scheduled Payment URI");
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data") != null,
				"Mandatory block Data is present and is not null");
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.ConsentId")).isEmpty(),
				"Mandatory field ConsentId is present under Data");
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.CreationDateTime")).isEmpty(),
				"Mandatory field CreationDateTime is present under Data");
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Status")).isEmpty(),
				"Mandatory field Status is present under Data");
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.StatusUpdateDateTime")).isEmpty(),
				"Mandatory field StatusUpdateDateTime is present under Data");
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation") != null,
				"Mandatory block Initiation is present and is not null");
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.InstructionIdentification")).isEmpty(),
				"Mandatory field InstructionIdentification is present under Initiation");
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument")).isEmpty(),
				"Mandatory field LocalInstrument is present under Initiation block");
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.EndToEndIdentification")).isEmpty(),
				"Mandatory field EndToEndIdentification is present under Initiation block");
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.InstructedAmount") != null,
				"Mandatory field InstructedAmount is present under Initiation block");
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorAccount") != null,
				"Mandatory field CreditorAccount is present under Initiation block");
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount") != null,
				"Optional field DebtorAccount is present under Initiation block");
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress") != null,
				"Optional field CreditorPostalAddress is present under Initiation block");
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation") != null,
				"Optional field RemittanceInformation is present under Initiation block");
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Risk") == null,
				"Mandatory field Risk is not present under Initiation block");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
