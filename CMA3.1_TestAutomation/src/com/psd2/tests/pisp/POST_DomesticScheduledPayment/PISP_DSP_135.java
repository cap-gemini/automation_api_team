package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/RemittanceInformation block
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_135 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_DSP_135() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
        dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
		"Response Code is correct for Domestic Scheduled Payment URL for AddressLine fields having length in between 1-70 characters");		
		
		 testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation.Unstructured")!=null, 
		 " Unstructured field is present under RemittanceInformation block");
		 
		 testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation.Reference")!=null, 
		 " Reference field is present under RemittanceInformation block");
		 
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation.Unstructured").toString().length()<=140), 
		"Optional field Unstructured value is between 1 and 140");
					
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation.Reference").toString().length()<=35), 
		"Optional field Reference value is between 1 and 35");
				
		  TestLogger.logBlankLine();
		  testVP.testResultFinalize();  
	}
}
