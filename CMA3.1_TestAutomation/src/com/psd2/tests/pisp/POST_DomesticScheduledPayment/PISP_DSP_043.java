package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of MANDATORY Initiation/EndToEndIdentification field in between 1-35 characters
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_043 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_043() throws Throwable{	
	
			TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment");
	        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");	
			dspConsent.setBaseURL(apiConst.dsp_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dspConsent.setConsentId(consentDetails.get("consentId"));
			dspConsent.submit();
		
		    TestLogger.logStep("[Step 3] : Verification of the values of MANDATORY Initiation/EndToEndIdentification field");
				
		    testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Scheduled Payment");
		    testVP.verifyTrue(String.valueOf(dspConsent.getResponseValueByPath("Data.Initiation.EndToEndIdentification")).length()<=35,
					"InstructionIdentification field length up to 35 characters");
		    TestLogger.logBlankLine();
		
		    testVP.testResultFinalize();		
	}
}
