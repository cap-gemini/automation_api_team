package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into OPTIONAL MultiAuthorisation/LastUpdateDateTime where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_220 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DSP_220() throws Throwable{	
	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
			
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment URI");
		testVP.verifyTrue(Misc.verifyDateTimeFormat(dspConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.LastUpdateDateTime").split("T")[0], "yyyy-MM-dd") && 
				(Misc.verifyDateTimeFormat(dspConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.LastUpdateDateTime").split("T")[1], "HH:mm:ss+05:30") ||
				Misc.verifyDateTimeFormat(dspConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.LastUpdateDateTime").split("T")[1], "HH:mm:ss+00:00")), 
				"LastUpdateDateTime is as per expected format");
			    
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}

