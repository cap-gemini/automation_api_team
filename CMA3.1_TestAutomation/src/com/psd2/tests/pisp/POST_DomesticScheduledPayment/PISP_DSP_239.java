package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verify SCA page without debtor account
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_239 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DSP_239() throws Throwable{	
			
			TestLogger.logStep("[Step 1-1] : Creating client credetials....");
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200","Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + cc_token);
			API_Constant.setPisp_CC_AccessToken(cc_token);	
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-2] : Domestic Scheduled Payment Consent SetUp....");
			dspConsent.setBaseURL(apiConst.dspConsent_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+cc_token);
			dspConsent.removeDebtorAccount();
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", "Response Code is correct for Domestic Scheduled Payment Consent");
			consentId = dspConsent.getConsentId();
			TestLogger.logVariable("Consent Id : " + consentId);
			TestLogger.logBlankLine();
		
			TestLogger.logStep("[Step 1-3] : JWT Token Creation........");
			//reqObject.setBaseURL(apiConst.ro_endpoint);
			reqObject.setValueField(consentId);
			reqObject.setScopeField("payments");
			outId = reqObject.submit();
			
			TestLogger.logVariable("JWT Token : " + outId);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-4] : Go to URL and authenticate consent");	
			redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
			startDriverInstance();
			authCode = consentOps.authorisePISPConsent(redirecturl+"##"+dspConsent._drAccountIdentification,true);		
			closeDriverInstance();
			TestLogger.logBlankLine();

			TestLogger.logStep("[Step 1-5] : Get access token");	
			accesstoken.setBaseURL(apiConst.at_endpoint);
			accesstoken.setAuthCode(authCode);
			accesstoken.submit();
			
			testVP.verifyStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
					"Response Code is correct for get access token request");	
			access_token = accesstoken.getAccessToken();
			TestLogger.logVariable("Access Token : " + access_token);		
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 1-6] : Domestic Scheduled Payment ");	
			dspConsent.setBaseURL(apiConst.dsp_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+access_token);
			dspConsent.setConsentId(consentId);
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201",
					"Response Code is correct for Domestic Scheduled Payment");

			TestLogger.logBlankLine();		
			testVP.testResultFinalize();	
	}
}