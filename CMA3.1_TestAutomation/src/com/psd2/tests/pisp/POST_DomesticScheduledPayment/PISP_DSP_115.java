package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of OPTIONAL CreditorPostalAddress/AddressType field
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_115 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DSP_115() throws Throwable{	
	
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
			
		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for POST Domestic Scheduled Payment Submission");	
		
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.AddressType")!=null, 
					"Optional field AddressType is present under CreditorPostalAddress block");
		 
		testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressType").equals("Business") 
					|| dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressType").equals("Correspondence") 
					|| dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressType").equals("DeliveryTo")
					|| dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressType").equals("MailTo") 
					|| dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressType").equals("POBox") 
					|| dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressType").equals("Postal")
					|| dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressType").equals("Residential")
					|| dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressType").equals("Statement"),
					"AddressType field value is correct i.e. "+dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressType"));

		TestLogger.logBlankLine();	
		
		testVP.testResultFinalize();		
	}
}

