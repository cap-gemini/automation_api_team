package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of MANDATORY Initiation/EndToEndIdentification field having length greater than 35 characters
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_045 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_045() throws Throwable{	
	
			
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");
			
			dspConsent.setBaseURL(apiConst.dsp_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dspConsent.setEndToEndIdentification("FRESCO.21302.GFX.20FRESCO.21302.GFAAAA");
			dspConsent.setConsentId(consentDetails.get("consentId"));
			dspConsent.submit();
		
		 TestLogger.logStep("[Step 3] : Verification of the values of MANDATORY Initiation/EndToEndIdentification field having length variation");
				
		    testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Scheduled Payment URL for EndToEndIdentification fields which is greater than 35 char");
		
		    testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Expected max length 35 for field [EndToEndIdentification], but got [FRESCO.21302.GFX.20FRESCO.21302.GFAAAA]", "Error Message are matched");
			
			TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
