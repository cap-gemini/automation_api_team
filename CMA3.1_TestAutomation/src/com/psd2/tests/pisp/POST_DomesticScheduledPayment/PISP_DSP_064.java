package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of MANDATORY DebtorAccount/SchemeName field having OB defined values
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_064 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_064() throws Throwable{	
	
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");
				
			dspConsent.setBaseURL(apiConst.dsp_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dspConsent.setConsentId(consentDetails.get("consentId"));
			dspConsent.submit();
			
			testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic Scheduled Payment URI");
			
			testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.SortCodeAccountNumber")
					|| dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.IBAN")
					|| dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.PAN")
					|| dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.Paym")
					|| dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.BBAN")
					|| dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme1")
					|| dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme2"), 
					"MANDATORY DebtorAccount/SchemeName field has OB defined values");
			
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
