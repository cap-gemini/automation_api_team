package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the value of OPTIONAL CreditorAccount/Country field
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_131 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_DSP_131() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setCountry("US");
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
		"Response Code is correct for POST Domestic Scheduled Payment Submission where Country field having length having length of 2 characters");
		
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.Country")!=null, 
		"Optional field Country is present under CreditorAccount block");
		
		testVP.verifyTrue((dspConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress.Country").toString().length()<=16), 
		"Optional field Country value is in between 16");
				
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
}
}
