package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_168 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_DSP_168() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
        dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Scheduled Payment URI");
		testVP.verifyTrue(!String.valueOf(dspConsent.getResponseValueByPath("Data.DomesticScheduledPaymentId")).isEmpty(),  
				"Mandatory field ConsentId is present and not empty");
		testVP.verifyTrue(!String.valueOf(dspConsent.getResponseValueByPath("Data.ConsentId")).isEmpty(),  
				"Mandatory field ConsentId is present and not empty");
		testVP.verifyTrue(!String.valueOf(dspConsent.getResponseValueByPath("Data.CreationDateTime")).isEmpty(), 
				"Mandatory field CreationDateTime is present and not empty");
		testVP.verifyTrue(!String.valueOf(dspConsent.getResponseValueByPath("Data.Status")).isEmpty(), 
				"Mandatory field Status is present and not empty");
		testVP.verifyTrue(!String.valueOf(dspConsent.getResponseValueByPath("Data.ExpectedExecutionDateTime")).isEmpty(), 
				"Mandatory field StatusUpdateDateTime is present and not empty");
		testVP.verifyTrue(!String.valueOf(dspConsent.getResponseValueByPath("Data.ExpectedSettlementDateTime")).isEmpty(), 
				"Mandatory field StatusUpdateDateTime is present and not empty");
		testVP.verifyTrue(!String.valueOf(dspConsent.getResponseValueByPath("Data.Initiation")).isEmpty(), 
				"Mandatory field Initiation is present and not empty");
				
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
		
	}
	}

