package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status
 * through POST method with mandatory and optional fields
 * 
 * @author kiran dewangan
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_DSP_004 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_DSP_004() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();

		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()), "201",
				"Response Code is correct for POST Domestic Scheduled Payment Submission");
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data") != null,
				"Mandatory field Data is present in response and is not empty");
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Links") != null,
				"Mandatory field Links is present in response and is not empty");
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Links.Self")).isEmpty(),
				"Mandatory field Self is present in response and is not empty");
		testVP.verifyStringEquals(dspConsent.getResponseHeader("Content-Type"),
				"application/json;charset=utf-8",
				"Response is UTF-8 character encoded");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
