package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with invalid value of scope for the flow into Authorization (Access Token) header
 * 
 * @author Kiran Dewangan
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_DSP_021 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_DSP_021() throws Throwable {

       TestLogger.logStep("[Step 1] : Creating client credetials....");
		consentDetails=apiUtility.generateAISPConsent(null,false,false,false,false,null,false,false);
		access_token= API_Constant.getAisp_AccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);
		
		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+ access_token);
		dspConsent.submit();

		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"403", 
				"Response Code is correct for Domestic Scheduled Payment Consent URI");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
