package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the Invalid value of MANDATORY CreditorAccount/Identification field having length variation
 * 
 * @author Kiran Dewangan
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_DSP_094 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_DSP_094() throws Throwable {

		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
		consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.setCrAccountSchemeName("UK.OBIE.PAN");
		dspConsent.setCrAccountIdentification("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ12345678ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ123456781ABABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ123456781CDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABXYZ12345678");
		dspConsent.submit();

		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400",
				"Response Code is correct for Domestic Scheduled Payment URI when CreditorAccount Identification is more than 256 characters");
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Field.Invalid",
				"Error code for the response is correct i.e. '"+ dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode")+ "'");
		testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 256 for field [Identification], but got [ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ12345678ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ123456781ABABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ123456781CDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABXYZ12345678]"),"Message for error code is '"
						+ dspConsent.getResponseNodeStringByPath("Errors[0].Message")+ "'");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
