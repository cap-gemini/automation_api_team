package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request with BLANK, NULL or Invalid value of MANDATORY x-jws-signature header
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","JWT"})
public class PISP_DSP_025 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	String requestBody;
	@Test
	public void m_PISP_DSP_025() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
		consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Domestic Scheduled Payment with invalid signature");
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setConsentId(consentDetails.get("consentId"));
		restRequest.setURL(apiConst.dsp_endpoint);
		requestBody= dspConsent.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", Content-Type:application/json, Accept:application/json, x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", x-jws-signature:1234"+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Scheduled Payments");
		
		TestLogger.logStep("[Step 3] : Domestic Scheduled Payment with Blank signature");
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setConsentId(consentDetails.get("consentId"));
		restRequest.setURL(apiConst.dsp_endpoint);
		requestBody= dspConsent.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", Content-Type:application/json, Accept:application/json, x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key"));
		restRequest.addHeaderEntry("x-jws-signature","");
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Payments");
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Missing", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].Message")),"Required header x-jws-signature not specified","Error message is correct");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();			
	}
}
