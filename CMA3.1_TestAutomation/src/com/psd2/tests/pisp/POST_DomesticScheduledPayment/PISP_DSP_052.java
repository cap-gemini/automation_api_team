package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of MANDATORY InstructedAmount/Amount field having values in between 13 non-decimal and 5 decimals
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_052 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_052() throws Throwable{	
	
			
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
			
	        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
			TestLogger.logBlankLine();
			
		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");	
			dspConsent.setBaseURL(apiConst.dsp_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dspConsent.setConsentId(consentDetails.get("consentId"));
			dspConsent.submit();
		
		TestLogger.logStep("[Step 3] : Verification of the values of MANDATORY InstructedAmount/Amount field having values in between 13 non-decimal and 5 decimals");
				
		    testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic Scheduled Payment URI when mandatory Data/Initiation/InstructedAmount/Amount field having values in between 13 non-decimal and 5 decimals");
			testVP.verifyTrue((String.valueOf(dspConsent.getResponseValueByPath("Data.Initiation.InstructedAmount.Amount")).split("\\.")[0].length()) <= 13 &&(String.valueOf(dspConsent.getResponseValueByPath("Data.Initiation.InstructedAmount.Amount")).split("\\.")[1].length() <=5),"Amount field having values in between 13 non-decimal and 5 decimals");
			
			TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
