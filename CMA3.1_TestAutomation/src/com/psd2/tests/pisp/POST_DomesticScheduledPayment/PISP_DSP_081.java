package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the Invalid value of MANDATORY DebtorAccount/Identification field when SchemeName = any.bank.Scheme1
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Blocked"})
public class PISP_DSP_081 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DSP_81() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
		apiUtility.pispAccessToken=true;
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Domestic Scheduled Payment ");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.setDrAccountSchemeName("any.bank.scheme1");
		dspConsent.setDrAccountIdentification("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCD");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400",
				"Response Code is correct for Invalid value of MANDATORY DebtorAccount/Identification field");
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Field.Invalid",
				"Error code for the response is correct i.e. '"+ dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode")+ "'");
		testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 256"),
				"Message for error code is '"+ dspConsent.getResponseNodeStringByPath("Errors[0].Message")+ "'");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}