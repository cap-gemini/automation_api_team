package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY CreditorAccount/Name field having length variations
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_112 extends TestBase {	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_112() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY CreditorAccount/Name field having length variations");
		
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.setCrAccountName("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567834782469723054");
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Scheduled Payment URI when CreditorAccount Name is more than 70 characters");
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 70 for field [Name], but got [ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567834782469723054]"), 
				"Message for error code is '"+dspConsent.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();	
		
	}
}
