package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/MultiAuthorisation block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_216 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DSP_216() throws Throwable{	
	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
			
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Scheduled Payment Consent URI");
		testVP.verifyTrue(dspConsent.getResponseValueByPath("Data.MultiAuthorisation")!=null,
				"Optional field Authorisation is present and is not null");
		testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.Status")!=null,
				"Mandatory field Status is present under MultiAuthorisation");
		testVP.verifyTrue((dspConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.NumberRequired"))!=null,
				"Optional field NumberRequired is present under Authorisation"+dspConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.NumberRequired"));
		testVP.verifyTrue((dspConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.NumberReceived"))!=null,
				"Optional field NumberReceived is present under Authorisation"+dspConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.NumberReceived"));
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.LastUpdateDateTime")).isEmpty(),
				"Optional field LastUpdatedDateTime is present under Authorisation"+dspConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.LastUpdateDateTime"));
		testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.ExpirationDateTime")).isEmpty(),
				"Optional field ExpirationDateTime is present under Authorisation"+dspConsent.getResponseNodeStringByPath("Data.MultiAuthorisation.ExpirationDateTime"));
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}

