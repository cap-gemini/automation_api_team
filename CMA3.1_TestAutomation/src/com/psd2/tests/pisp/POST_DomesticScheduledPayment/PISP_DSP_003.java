package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of domestic-scheduled-payments URL that should be as
 * per Open Banking standards as specified format
 * 
 * @author Kiran Dewangan
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression" })
public class PISP_DSP_003 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_DSP_003() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");
		dspConsent.setBaseURL(apiConst.otherpispapi_dso_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();

		testVP.verifyStringEquals(dspConsent.getURL(),apiConst.otherpispapi_dso_endpoint,
				"URL for POST Domestic Scheduled Payment Submission is as per open banking standard");

		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()), "400",
				"Response Code is correct for POST Domestic Scheduled Payment Submission");

		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"),
				"UK.OBIE.Field.Missing", "Error Code are matched");
		
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].Message"),
				"Error validating JSON. Error: - Missing required field [Frequency]","Error Message are matched");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
