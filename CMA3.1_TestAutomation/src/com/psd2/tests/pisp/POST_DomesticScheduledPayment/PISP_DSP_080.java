package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = any.bank.Scheme1
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Blocked"})
public class PISP_DSP_080 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DSP_80() throws Throwable{	
		dspConsent.setDrAccountSchemeName("any.bank.scheme1");
		dspConsent.setDrAccountIdentification("scheme1");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Domestic Scheduled Payment Submission");	
		dspConsent.setBaseURL(apiConst.dsp_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		dspConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Scheduled Payment URI");
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName")),"any.bank.scheme1",
				"SchemeName value is correct");
		
		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseValueByPath("Data.Initiation.DebtorAccount.Identification")),"scheme1",
				"Identification value is correct");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();
	}	
}