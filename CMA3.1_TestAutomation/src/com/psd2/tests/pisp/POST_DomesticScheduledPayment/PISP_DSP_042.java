package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of MANDATORY Initiation/EndToEndIdentification field NOT sent
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_042 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_042() throws Throwable{	
	
			TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment");
	        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
			TestLogger.logBlankLine();
			
		    TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");
			
		    dspConsent.setBaseURL(apiConst.dps_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dspConsent.setConsentId(consentDetails.get("consentId"));
			String _reqBody = dspConsent.genRequestBody().replace("\"EndToEndIdentification\": \"FRESCO.21302.GFX.20FRESCO.21302.GF\",","");
			dspConsent.submit(_reqBody);
            
		    TestLogger.logStep("[Step 3] : Verification of MANDATORY Initiation/EndToEndIdentification field NOT sent");

		    testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled Payment URI when EndToEndIdentification field haven't sent");
			
		    testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Missing", "Error Code are matched");
			testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Missing required field [EndToEndIdentification]", "Error Message are matched");
			
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
