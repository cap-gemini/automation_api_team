package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/LocalInstrument field having OB defined values
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_046 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_046() throws Throwable{	
	
			
			TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment");
	        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
			TestLogger.logBlankLine();
				
			TestLogger.logStep("[Step 2] : Verification of the values of MANDATORY Initiation/LocalInstrument field" );
			
			dspConsent.setBaseURL(apiConst.dsp_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dspConsent.setConsentId(consentDetails.get("consentId"));
			dspConsent.submit();
		
		    testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic Scheduled Payment URI");
		    
		    testVP.verifyTrue(dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Link") 
					|| dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.FPS") 
					|| dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BACS")
					|| dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.CHAPS") 
					|| dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Paym") 
					|| dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BalanceTransfer")
					|| dspConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.MoneyTransfer"),
					"LocalInstrument field value is correct i.e. "+dspConsent.getResponseNodeStringByPath("Data.Initiation[0].LocalInstrument"));
			
		    TestLogger.logBlankLine();
		
		    testVP.testResultFinalize();		
	}
}
