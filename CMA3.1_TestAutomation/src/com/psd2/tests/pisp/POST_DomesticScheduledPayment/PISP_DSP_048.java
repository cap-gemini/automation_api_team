package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of NULL value in RequestedExecutionDateTime field
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_048 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_048() throws Throwable{	
	
			
			TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment");
	        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
			TestLogger.logBlankLine();
				
			TestLogger.logStep("[Step 2] : Verification of the values of MANDATORY Initiation/RequestedExecutionDateTime field");
			
			dspConsent.setBaseURL(apiConst.dsp_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dspConsent.setConsentId(consentDetails.get("consentId"));
			dspConsent.setRequestedExecutionDateTime("");
			dspConsent.submit();
		
		    testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Scheduled Payment URI for NULL value in RequestedExecutionDateTime");
		
		    testVP.testResultFinalize();		
	}
}
