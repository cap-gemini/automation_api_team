package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of domestic-payments URL having older
 * version with 1.1 version payload
 * 
 * @author Kiran Dewangan
 *
 */

@Listeners({ TestListener.class })
@Test(groups = { "Regression","Invalid" })
public class PISP_DSP_006 extends TestBase {

	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_PISP_DSP_006() throws Throwable {

		TestLogger.logStep("[Step 1] : Generate Consent Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");
		//dspConsent.setBaseURL(apiConst.backcomp_dsps_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setConsentId(consentDetails.get("consentId"));
		String _reqBody = dspConsent.genRequestBody().replace("\"CountrySubDivision\":[ \""+dspConsent._riskCountrySubDivision+"\"],","\"CountrySubDivision\": \""+dspConsent._riskCountrySubDivision+"\",");
		dspConsent.submit(_reqBody);

		testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"404",
				"Response Code is correct for POST Domestic Scheduled Payment Submission for version 3.0 in PISP");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
