package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY CreditorAccount/SchemeName field
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_088 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility(); 
	
	@Test
	public void m_PISP_DSP_088() throws Throwable{	
	
			
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
        	consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
        	TestLogger.logBlankLine();
			
			dspConsent.setBaseURL(apiConst.dsp_endpoint);
			dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
			dspConsent.setConsentId(consentDetails.get("consentId"));
			dspConsent.submit();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY CreditorAccount/SchemeName field");
		    testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"201", 
					"Response Code is correct for Domestic Scheduled Payment URI when mandatory Data/Initiation/CreditorAccount/SchemeName field");
		    testVP.verifyTrue(!(dspConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SchemeName")).isEmpty(), 
					"Mandatory field SchemeName is present under CreditorAccount");
		    
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
