package com.psd2.tests.pisp.POST_DomesticScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of Currency field underInstructedAmount/Currency having BLANK value.
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSP_062 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_DSP_062() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create Domestic Scheduled Payment Consent");
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticScheduledPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Scheduled Payment Submission");
		dspConsent.setBaseURL(apiConst.dps_endpoint);
		dspConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		dspConsent.setCurrency("");
		dspConsent.submit();
		
	    testVP.verifyStringEquals(String.valueOf(dspConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Scheduled Payment URI when mandatory Data/Initiation/InstructedAmount/Currency having BLANK value");
	    testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", "Error Code are matched");
		testVP.verifyStringEquals(dspConsent.getResponseNodeStringByPath("Errors[0].Message"), "Error validating JSON. Error: - Invalid value ''. Expected ^[A-Z]{3,3}$ for Currency", "Error Message are matched");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}

