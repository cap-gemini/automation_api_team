package com.psd2.tests.pisp.GET_InternationalPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through GET method 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GIPC_004 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GIPC_004() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, true, false);
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : GET International Payments");	
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET International Payment Consent");	
		testVP.verifyEquals(internationalPayment.getURL(), apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"), 
				"URL is correct and matching with Get Domestic Standing Order Submission URL");
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data")) != null,
				"Mandatory field Data is present in response and is not empty");
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Charges")) != null,
				"Optional field Charges is present in response and is not empty");
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation")) != null,
				"Optional field ExchangeRateInformation is present in response and is not empty");
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation")) != null,
				"Mandatory field Initiation is present in response and is not empty");
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Authorisation")) != null,
				"Optional field Authorisation is present in response and is not empty");		
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Risk")) != null,
				"Mandatory field Risk is present in response and is not empty");
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Risk.DeliveryAddress")) != null,
				"Optional field DeliveryAddress is present in response and is not empty");
		testVP.verifyStringEquals(internationalPayment.getResponseHeader("Content-Type"), "application/json;charset=utf-8", 
				"Response is UTF-8 character encoded");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}

