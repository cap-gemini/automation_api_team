package com.psd2.tests.pisp.GET_InternationalPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL ContractIdentification block where Request has sent successfully 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GIPC_044 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GIPC_044() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, true, false);
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : GET International Payments");	
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for International Payment Consent URI");			
		testVP.verifyStringEquals(internationalPayment.getURL(),apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"), 
				"URI for GET International payment consent request is as per open banking standard");
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ContractIdentification"))!=null,
				"ContractIdentification field under ExchangeRateInformation block is present in Get International Payment Consent response body"+(internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ContractIdentification")));		
		testVP.verifyTrue(String.valueOf((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ContractIdentification"))).length()>=1&&String.valueOf((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ContractIdentification"))).length()<=256 
				,"ContractIdentification value is present and is of range 1 to 256");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}

