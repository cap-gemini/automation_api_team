package com.psd2.tests.pisp.GET_InternationalPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Status field where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GIPC_029 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GIPC_029() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, true, false);
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : GET International Payments");	
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET International Payment Consent");
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Status"))!=null,
				"Status field under Data is present in Get International Payment Consent response body"+(internationalPayment.getResponseValueByPath("Data.Status")));				
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.Status")).equals("Authorised")||
				String.valueOf(internationalPayment.getResponseValueByPath("Data.Status")).equals("AwaitingAuthorisation")||
				String.valueOf(internationalPayment.getResponseValueByPath("Data.Status")).equals("Consumed")||
				String.valueOf(internationalPayment.getResponseValueByPath("Data.Status")).equals("Rejected"), 
				"Status is correct "+String.valueOf(internationalPayment.getResponseValueByPath("Data.Status")));		
		testVP.verifyEquals(internationalPayment.getURL(), apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"), 
				"URL is correct and matching with GET International Payment Consent URL");
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();
	}
}

