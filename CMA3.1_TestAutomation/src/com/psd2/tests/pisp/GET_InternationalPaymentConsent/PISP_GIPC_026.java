package com.psd2.tests.pisp.GET_InternationalPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GIPC_026 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GIPC_026() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, true, false);
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : GET International Payments");	
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for International Payment Consent URI");			
		testVP.verifyStringEquals(internationalPayment.getURL(),apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"), 
				"URI for GET International payment consent request is as per open banking standard");
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ConsentId"))!=null,
				"ConsentId field under Data is present in Get International Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Data.ConsentId")));
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Charges"))!=null,
				"Charges field under Data is present in Get International Payment Consent response body"+(internationalPayment.getResponseValueByPath("Data.Charges")));
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation"))!=null,
				"ExchangeRateInformation field under Data is present in Get International Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation")));
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation"))!=null,
				"Initiation field under Data is present in Get International Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation")));
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Authorisation"))!=null,
				"Authorisation field under Data is present in Get International Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Data.Authorisation")));
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Risk"))!=null,
				"Risk field under Data is present in Get International Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Risk")));
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Risk.DeliveryAddress"))!=null,
				"DeliveryAddress field under Data is present in Get International Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Risk.DeliveryAddress")));		
		testVP.verifyEquals(internationalPayment.getURL(), apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"), 
				"URL is correct and matching with GET International Payment Consent URL");
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();
	}
}

