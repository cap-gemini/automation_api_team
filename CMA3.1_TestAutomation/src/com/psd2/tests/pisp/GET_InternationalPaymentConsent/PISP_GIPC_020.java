package com.psd2.tests.pisp.GET_InternationalPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request NOT having any value into OPTIONAL Accept header 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GIPC_020 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GIPC_020() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, true, false);
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : GET International Payments");	
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		internationalPayment.addHeaderEntry("Accept", " ");
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET International Payment Consent when request is NOT having any value into OPTIONAL Accept header");	
		testVP.verifyEquals(internationalPayment.getURL(), apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"), 
				"URL is correct and matching with GET International Payment Consent URL");
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();
	}
}

