package com.psd2.tests.pisp.GET_InternationalPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with BLANK or Invalid value of OPTIONAL x-fapi-customer-last-logged-time header 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GIPC_019 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GIPC_019() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, true, false);
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : GET International Payments");	
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		internationalPayment.addHeaderEntry("x-fapi-customer-last-logged-time", "FSTYWFUD^%^");
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
				"Response Code is correct for GET International Payment Consent URI when invalid x-fapi-customer-last-logged-time is passed");
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Errors[0].Message")),"Invalid value 'FSTYWFUD^%^' for header x-fapi-customer-last-logged-time. Invalid value 'FSTYWFUD^%^'. Expected ^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), \\d{2} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \\d{4} \\d{2}:\\d{2}:\\d{2} (GMT|UTC)$", 
				"Error Message are matched");
		testVP.verifyEquals(internationalPayment.getURL(), apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"), 
				"URL is correct and matching with GET International Payment Consent URL");
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();
	}
}

