package com.psd2.tests.pisp.GET_InternationalPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Risk/DeliveryAddress block where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GIPC_108 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GIPC_108() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, true, false);
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : GET International Payments");	
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		
			testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
					"Response Code is correct for International Payment Consent URI");			
			testVP.verifyStringEquals(internationalPayment.getURL(),apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"), 
					"URI for GET International payment consent request is as per open banking standard");			
			testVP.verifyTrue((internationalPayment.getResponseValueByPath("Risk.DeliveryAddress"))!=null,
					"DeliveryAddress block is present");
			
			testVP.verifyTrue((internationalPayment.getResponseValueByPath("Risk.DeliveryAddress.AddressLine"))!=null,
					"Optional field AddressLine is present in Risk block");
			
			testVP.verifyTrue((internationalPayment.getResponseValueByPath("Risk.DeliveryAddress.TownName"))!=null, 
					"Mandatory field TownName is present in DeliveryAddress block");
			
			testVP.verifyTrue((internationalPayment.getResponseValueByPath("Risk.DeliveryAddress.Country"))!=null, 
					"Mandatory field Country is present in DeliveryAddress block");
			
//			testVP.verifyTrue((internationalPayment.getResponseValueByPath("Risk.DeliveryAddress.CountrySubDivision"))!=null, 
//					"Mandatory field CountrySubDivision is present in DeliveryAddress block");
			
			testVP.verifyTrue((internationalPayment.getResponseValueByPath("Risk.DeliveryAddress.StreetName"))!=null, 
					"Optional field Country is present in DeliveryAddress block");
			
			testVP.verifyTrue((internationalPayment.getResponseValueByPath("Risk.DeliveryAddress.BuildingNumber"))!=null, 
					"Optional field BuildingNumber is present in DeliveryAddress block");
			
			testVP.verifyTrue((internationalPayment.getResponseValueByPath("Risk.DeliveryAddress.PostCode"))!=null, 
					"Optional field PostCode is present in DeliveryAddress block");
			TestLogger.logBlankLine();
	        testVP.testResultFinalize();
	}
}

