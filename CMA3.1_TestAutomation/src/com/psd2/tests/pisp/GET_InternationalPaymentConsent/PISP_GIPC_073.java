package com.psd2.tests.pisp.GET_InternationalPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/Creditor/PostalAddress block where Request has sent successfully and returned a HTTP Code 200 Created 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GIPC_073 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GIPC_073() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, true, false);
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : GET International Payments");	
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for International Payment Consent URI");			
		testVP.verifyStringEquals(internationalPayment.getURL(),apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"), 
				"URI for GET International payment consent request is as per open banking standard");
		
				testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressType"))!=null, 
				"Mandatory field AddressType is present in Creditor.PostalAddress block");
				testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.Department"))!=null, 
				"Mandatory field Department is present in Creditor.PostalAddress block");
				testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.SubDepartment"))!=null, 
				"Optional field SubDepartment is present in Creditor.PostalAddress block");
				testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.StreetName"))!=null, 
				"Optional field StreetName is present in Creditor.PostalAddress block");
				testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.BuildingNumber"))!=null, 
				"Optional field BuildingNumber is present in Creditor.PostalAddress block");
				testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.PostCode"))!=null, 
				"Optional field PostCode is present in Creditor.PostalAddress block");
				testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.TownName"))!=null, 
				"Optional field TownName is present in Creditor.PostalAddress block");
				testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.CountrySubDivision"))!=null, 
				"Optional field CountrySubDivision is present in Creditor.PostalAddress block");
				testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.Country"))!=null, 
				"Optional field Country is present in Creditor.PostalAddress block");
				testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Creditor.PostalAddress.AddressLine"))!=null, 
				"Optional field AddressLine is present in Creditor.PostalAddress block");
				TestLogger.logBlankLine();
				testVP.testResultFinalize();
	}
}

