package com.psd2.tests.pisp.GET_InternationalPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with invalid value of scope for the flow into Authorization (Access Token) header 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GIPC_015 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GIPC_015() throws Throwable{	
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("accounts");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("CC token : " + cc_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : GET International Payment Consent with invalid value of scope");
		
		consentDetails = apiUtility.generatePayments(false, apiConst.internationalPayments, true, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Domestic Payment Submission with invalid value of scope");
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		internationalPayment.setHeadersString("Authorization:Bearer " +cc_token);
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"403", 
				"Response Code is correct for invalid value of scope for the flow into Authorization header for GET International Payment Consent");	
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();
	}
}

