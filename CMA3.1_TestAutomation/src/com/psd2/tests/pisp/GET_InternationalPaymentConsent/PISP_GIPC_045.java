package com.psd2.tests.pisp.GET_InternationalPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into OPTIONAL ExpirationDateTime block where Request has sent successfully 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GIPC_045 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GIPC_045() throws Throwable{	


		TestLogger.logStep("[Step 1] : Creating client credetials....");
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", "Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);    
        TestLogger.logBlankLine();
        
        TestLogger.logStep("[Step 2] : Verification of response when the RateType is provided as \"Actual\" in the request payload");                          
        internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
        internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
        internationalPayment.setRateType("Actual");
        String requestBody=internationalPayment.genRequestBody().replace("\"ExchangeRate\": 1.2,","");
        requestBody = requestBody.replace(",\"ContractIdentification\": \"123identification\"","");
        internationalPayment.submit(requestBody);
        
        
		String consentId=internationalPayment.getConsentId();
		
		TestLogger.logStep("[Step 2] : GET International Payments");	
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentId);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for International Payment Consent URI");			
		testVP.verifyStringEquals(internationalPayment.getURL(),apiConst.iPaymentConsent_endpoint+"/"+consentId, 
				"URI for GET International payment consent request is as per open banking standard");
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExpirationDateTime"))!=null,
				"ExpirationDateTime field under ExchangeRateInformation block is present in Get International Payment Consent response body"+(internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExpirationDateTime")));
		testVP.verifyTrue(Misc.verifyDateTimeFormat(internationalPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExpirationDateTime").split("T")[0], "yyyy-MM-dd") && 
				(Misc.verifyDateTimeFormat(internationalPayment.getResponseNodeStringByPath("Data.ExchangeRateInformation.ExpirationDateTime").split("T")[1], "HH:mm:ss+00:00")), 
				"ExpirationDateTime is as per expected format");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}

