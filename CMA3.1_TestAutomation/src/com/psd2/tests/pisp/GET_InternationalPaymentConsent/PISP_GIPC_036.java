package com.psd2.tests.pisp.GET_InternationalPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Type field 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GIPC_036 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GIPC_036() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, true, false);
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : GET International Payments");	
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for International Payment Consent URI");			
		testVP.verifyStringEquals(internationalPayment.getURL(),apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"), 
				"URI for GET International payment consent request is as per open banking standard");
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Charges[0].Type"))!=null,  
				"Type is present in charges block");	
		testVP.verifyTrue((String.valueOf(internationalPayment.getResponseValueByPath("Data.Charges[0].Type")).length()<=40)&&(String.valueOf(internationalPayment.getResponseValueByPath("Data.Charges[0].Type")).length()>=1),  
				"Type having length up to 1-40 characters");				
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}

