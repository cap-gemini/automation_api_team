package com.psd2.tests.pisp.GET_InternationalPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into MANDATORY StatusUpdateDateTime field where Request has sent successfully and returned a HTTP Code 200 OK 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GIPC_030 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_GIPC_030() throws Throwable{	
		TestLogger.logStep("[Step 1] : Generate Consent Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, true, false);
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : GET International Payments");	
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET International Payment Consent");	
		testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.StatusUpdateDateTime")!=null, 
				 "StatusUpdateDateTime field is present under Data");	 
		testVP.verifyTrue(Misc.verifyDateTimeFormat(internationalPayment.getResponseNodeStringByPath("Data.StatusUpdateDateTime").split("T")[0], "yyyy-MM-dd") && 
				(Misc.verifyDateTimeFormat(internationalPayment.getResponseNodeStringByPath("Data.StatusUpdateDateTime").split("T")[1], "HH:mm:ss+00:00")), 
				"StatusUpdateDateTime is as per expected format");	
		testVP.verifyEquals(internationalPayment.getURL(), apiConst.iPaymentConsent_endpoint+"/"+consentDetails.get("consentId"), 
				"URL is correct and matching with GET International Payment Consent URL");
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();
	}
}

