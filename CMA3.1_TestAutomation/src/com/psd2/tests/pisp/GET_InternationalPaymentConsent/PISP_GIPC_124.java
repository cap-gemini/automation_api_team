package com.psd2.tests.pisp.GET_InternationalPaymentConsent;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of GET response for the consent API when the Amount starts with "700" in the POST request payload 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_GIPC_124 extends TestBase{	
	
	@Test
	public void m_PISP_GIPC_124() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
                                     "Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);    
        TestLogger.logBlankLine();
        
        TestLogger.logStep("[Step 2] : Verification of response for the consent API when the amount starts with 700");                          
        internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
        internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
        internationalPayment.setAmount("700.123");
        internationalPayment.submit();
        
		String consentId=internationalPayment.getConsentId();
		
		TestLogger.logStep("[Step 2] : GET International Payments");	
		internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint+"/"+consentId);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		;
			testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"500", 
					"Response Code is correct for International Payment Consent URI");			
			testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.UnexpectedError", 
					"Response Error Code is correct");
			testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Errors[0].Message")),"Internal error occurred","Error message is correct");		
									
			TestLogger.logBlankLine();
	        testVP.testResultFinalize();
	}
}

