package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of domestic-Standing-order-consents URL having older version with 3.0 version payload
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_006 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_006() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of domestic-payment-consents URL having older version with 3.0 version payload");
		
		dStandingOrder.setBaseURL(apiConst.invalid_dsoc_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"404", 
				"Response Code is correct for Old Domestic Standing Order Consent URI");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

