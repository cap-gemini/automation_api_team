
package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of response that created successfully with HTTP Code 201 MUST be UTF-8 character encoded
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_187 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_187() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of response that created successfully with HTTP Code 201 MUST be UTF-8 character encoded");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseHeader("Content-Type")), "application/json;charset=UTF-8", 
				"Value of response header Content-Type is correct");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
