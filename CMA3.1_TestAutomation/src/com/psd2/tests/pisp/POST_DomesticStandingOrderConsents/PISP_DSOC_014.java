package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of response when different request payload has been sent into BODY with different x-idempotency-key by same TPP within 24 hrs.
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_014 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_014() throws Throwable{	
		String idemPotencyKey=PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3);
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
        
	    TestLogger.logStep("[Step 1-2] : Verification response when different request payload has been sent into BODY with different x-idempotency-key by same TPP within 24 hrs.");
	    dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
	    dStandingOrder.removeDebtorAccount();
	    dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey);      
	    dStandingOrder.submit();
	    testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
	    		"Response Code is correct for Post Domestic Standing Order Consent when different request payload has been sent into BODY with different x-idempotency-key by same TPP within 24 hrs");
        TestLogger.logBlankLine();
        
        TestLogger.logStep("[Step 1-3] : Verification response when different request payload has been sent into BODY with different x-idempotency-key by same TPP within 24 hrs.");
        dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
        dStandingOrder.removeDebtorAccount();
        dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey+"123");
        dStandingOrder.setCrAccountName("abc");
        dStandingOrder.submit();
	    testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
	    		"Response Code is correct for Post Domestic Standing Order Consent when different request payload has been sent into BODY with different x-idempotency-key by same TPP within 24 hrs");
        TestLogger.logBlankLine();
        
		testVP.testResultFinalize();		
	}
}
