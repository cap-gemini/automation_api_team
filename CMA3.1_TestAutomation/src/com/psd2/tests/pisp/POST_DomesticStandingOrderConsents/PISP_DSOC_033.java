package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without Content-Type header OR Invalid format values
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_033 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_033() throws Throwable{	
		 
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request without Content-Type header OR Invalid format values");
		
		restRequest.setURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		String requestBody=dStandingOrder.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+cc_token+", x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-fapi-interaction-id:"+PropertyUtils.getProperty("inter_id")+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+", Accept:application/json, x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id"));
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"415", 
				"Response Code is correct for Domestic Standing Order Consent when Content Type header is blank");
		
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
