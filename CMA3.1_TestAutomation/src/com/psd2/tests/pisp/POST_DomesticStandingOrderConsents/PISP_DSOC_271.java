package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of response for the consent API when Unsupported Currency USD/INR value is provided .
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_271 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_271() throws Throwable{	
		
		String idemPotencyKey=PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3);
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of response for the consent API when Unsupported Currency USD/INR value is provided");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey);
		dStandingOrder.setFirstPaymentCurrency("USD");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseValueByPath("Data.Status")), "Rejected", 
				"Status field value is rejected when invalid currency is sent in request");
		
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 2] : Verification of response for the consent API when Unsupported Currency USD/INR value is provided");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey + "123");
		dStandingOrder.setFirstPaymentCurrency("INR");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseValueByPath("Data.Status")), "Rejected", 
				"Status field value is rejected when invalid currency is sent in request");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
