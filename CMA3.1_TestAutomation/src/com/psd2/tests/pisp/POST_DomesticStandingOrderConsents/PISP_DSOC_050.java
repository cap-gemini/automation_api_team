package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values of MANDATORY Initiation/FirstPaymentDateTime field
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_050 extends TestBase{
	
	@Test
	public void m_PISP_DSOC_050() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 2] : Verification of the values of MANDATORY Initiation/FirstPaymentDateTime field");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI when FirstPaymentDateTime is present");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.FirstPaymentDateTime")).isEmpty(), 
				"Mandatory field FirstPaymentDateTime is present and is not null");
		
		testVP.verifyTrue(Misc.verifyDateTimeFormat(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.FirstPaymentDateTime").split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.FirstPaymentDateTime").split("T")[1], "HH:mm:ss+00:00"), 
				"FirstPaymentDateTime under Initiation block is as per expected format");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}

}
