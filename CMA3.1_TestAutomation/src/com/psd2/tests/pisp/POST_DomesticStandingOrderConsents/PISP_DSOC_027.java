package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request with NULL or Invalid value of MANDATORY x-jws-signature header
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","JWT"})
public class PISP_DSOC_027 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_027() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with Invalid value of x-jws-signature header");
		restRequest.setURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+cc_token+", Content-Type:application/json,  x-fapi-interaction-id:"+PropertyUtils.getProperty("inter_id")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json, x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		restRequest.addHeaderEntry("x-jws-signature", "1234");
		restRequest.setRequestBody(dStandingOrder.genRequestBody());
		restRequest.setMethod("Post");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].ErrorCode")), "UK.OBIE.Signature.Malformed", 
				"Response Error Code is correct");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].Message")),"Unable to parse x-jws-signature header", "Error Code is matching");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the request with null value of x-jws-signature header");
		
		restRequest.setURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+cc_token+", Content-Type:application/json,  x-fapi-interaction-id:"+PropertyUtils.getProperty("inter_id")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json, x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		restRequest.addHeaderEntry("x-jws-signature", "");
		restRequest.setRequestBody(dStandingOrder.genRequestBody());
		restRequest.setMethod("Post");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Signature.Missing", 
				"Response Error Code is correct");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors[0].Message")),"x-jws-signature missing in request headers", "Error Message is matching");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
