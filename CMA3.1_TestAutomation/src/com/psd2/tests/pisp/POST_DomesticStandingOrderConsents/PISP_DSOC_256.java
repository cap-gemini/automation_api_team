package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of date & time for RecurringPaymentDateTime fields with Invalid dates i.e Invalid,NULL or BLANK , Removing Hour,Minutes &seconds
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_256 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_256() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of date and time for RecurringPaymentDateTime field without giving hours");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.setRecurringPaymentDateTime("2021-03-25T:06+00:00");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when RecurringPaymentDateTime field value is without hours");

		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Provided value 2021-03-25T:06+00:00 is not compliant with the format datetime provided in rfc3339 for RecurringPaymentDateTime"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of date and time for RecurringPaymentDateTime field without giving minutes");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dStandingOrder.setRecurringPaymentDateTime("2021-03-25T06+06Z");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when RecurringPaymentDateTime field value is without minutes");

		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Provided value 2021-03-25T06+06Z is not compliant with the format datetime provided in rfc3339 for RecurringPaymentDateTime"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of date and time for RecurringPaymentDateTime field without giving seconds");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dStandingOrder.setRecurringPaymentDateTime("2021-03-26T06:06:06+00:00");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI when RecurringPaymentDateTime field value is without seconds");

		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 5] : Verification of date and time for RecurringPaymentDateTime field with invalid value");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dStandingOrder.setRecurringPaymentDateTime("ABCD");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when RecurringPaymentDateTime field value is with invalid value");

		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Provided value ABCD is not compliant with the format datetime provided in rfc3339 for RecurringPaymentDateTime"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 6] : Verification of date and time for RecurringPaymentDateTime field with null value");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dStandingOrder.setRecurringPaymentDateTime("");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when RecurringPaymentDateTime field value is with null value");

		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Provided value  is not compliant with the format datetime provided in rfc3339 for RecurringPaymentDateTime"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 7] : Verification of date and time for RecurringPaymentDateTime field with invalid date");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dStandingOrder.setRecurringPaymentDateTime("1981-03-20T06:06:06+00:00");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when RecurringPaymentDateTime field value is with invalid date");

		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.InvalidDate", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("provided date-time expired"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 8] : Verification of date and time for RecurringPaymentDateTime field without date");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dStandingOrder.setRecurringPaymentDateTime("T06:06:06+00:00");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when RecurringPaymentDateTime field value is without date");

		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Provided value T06:06:06+00:00 is not compliant with the format datetime provided in rfc3339 for RecurringPaymentDateTime"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
