package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of date and time for FirstPaymentDateTime field with 3 digit milliseconds
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_263 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_263() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of date and time for FirstPaymentDateTime field with 3 digit milliseconds");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.setFirstPaymentDateTime("2021-03-25T06:06:06.777");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when FirstPaymentDateTime field with 3 digit milliseconds");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of date and time for FirstPaymentDateTime field with 3 digit milliseconds and Z");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dStandingOrder.setFirstPaymentDateTime("2021-03-25T06:06:06.777Z");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI when FirstPaymentDateTime field with 3 digit milliseconds and Z");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of date and time for FirstPaymentDateTime field with 3 digit milliseconds and offset");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		dStandingOrder.setFirstPaymentDateTime("2021-03-25T06:06:06.777+00:00");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI when FirstPaymentDateTime field with 3 digit milliseconds and offset");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
