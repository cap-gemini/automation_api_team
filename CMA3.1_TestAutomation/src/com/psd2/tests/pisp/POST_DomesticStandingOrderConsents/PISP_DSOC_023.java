package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the request with Null or Invalid value of x-fapi-financial-id header
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_023 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_023() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with Null value of x-fapi-financial-id header.");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.addHeaderEntry("x-fapi-financial-id", "");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"403", 
				"Response Code is correct for Domestic Standing Order Consent when x-fapi-financial-id is null in header");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the request with Invalid value of x-fapi-financial-id header.");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.addHeaderEntry("x-fapi-financial-id", "ABCD");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"403", 
				"Response Code is correct for Domestic Standing Order Consent when x-fapi-financial-id is invalid in header");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
