package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/Reference field
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_046 extends TestBase{
	
	@Test
	public void m_PISP_DSOC_046() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 2] : Verification of MANDATORY Initiation/Reference field value set as till 1 to 35 char");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.setReference("ASASASBDDWDB212312@@@@SSSS");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI when Reference is sent with characters between 1-35 characters");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Reference")).isEmpty(), 
				"Mandatory field Reference is present and is not null");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Reference").length()<=35, 
				"Reference field length is less than or equal to 35 characters");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}

}
