package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the Invalid value of MANDATORY DebtorAccount/Identification field when SchemeName = UK.OBIE.PAN/PAN
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_113 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_113() throws Throwable{	
		
        TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = UK.OBIE.SortCodeAccountNumber");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.setDrAccountSchemeName("UK.OBIE.PAN");
		dStandingOrder.setDrAccountIdentification("12345678901121131!@#");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400",
				"Response Code is correct for Domestic Standing Order URI when DebtorAccount Identification value is not valid");

		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Unsupported.AccountIdentifier",
				"Error code for the response is correct i.e. '"+ dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+ "'");

		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("invalid account identifier provided corresponding to scheme"),"Message for error code is '"
						+ dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+ "'");
	
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}
