package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY Risk block
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_160 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_160() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Risk block");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Risk.PaymentContextCode")).isEmpty(), 
				"PaymentContextCode field is present under Risk");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Risk.MerchantCategoryCode")).isEmpty(), 
				"MerchantCategoryCode field is present under Risk");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Risk.MerchantCustomerIdentification")).isEmpty(),  
				"MerchantCustomerIdentification field is present under Risk");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Risk.DeliveryAddress")!=null,  
				"DeliveryAddress field is present under Risk");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
