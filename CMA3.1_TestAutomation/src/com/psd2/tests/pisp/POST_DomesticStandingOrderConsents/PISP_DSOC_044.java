package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the value of MANDATORY Initiation/frequency field set as EvryDay, EvryWorkgDay, IntrvlDay etc.
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_044 extends TestBase{
	
	@Test
	public void m_PISP_DSOC_044() throws Throwable{	
		
		String idemPotencyKey=PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3);
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of MANDATORY Initiation/Frequency field value set as EvryWorkgDay");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.setFrequency("EvryWorkgDay");
		dStandingOrder.submit();
		
        testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
        		"Response Code is correct for Domestic Standing Order Consent");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency")).isEmpty(), 
				"Mandatory field Frequency field");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency").equals("EvryWorkgDay"), 
				"Mandatory field Frequency is present under Data block and set as EvryWorkgDay");
		
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 3] : Verification of MANDATORY Initiation/Frequency field value set as IntrvlDay");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey + "1223");
		dStandingOrder.setFrequency("IntrvlDay:15");
		dStandingOrder.submit();
		
        testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
        		"Response Code is correct for Domestic Standing Order Consent");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Unsupported.Frequency", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Invalid value 'IntrvlDay:15'. Expected ^(EvryDay)$|^(EvryWorkgDay)$|^(IntrvlWkDay:0[1-9]:0[1-7])$|^(WkInMnthDay:0[1-5]:0[1-7])$|^(IntrvlMnthDay:(0[1-6]|12|24):(-0[1-5]|0[1-9]|[12][0-9]|3[01]))$|^(QtrDay:(ENGLISH|SCOTTISH|RECEIVED))$ for Frequency"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3-1] : Verification of MANDATORY Initiation/Frequency field value set Invalid IntrvlDay");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey + "1222");
		dStandingOrder.setFrequency("IntrvlDay:01");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when Frequency is passed as invalid");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Unsupported.Frequency", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Invalid value 'IntrvlDay:01'. Expected ^(EvryDay)$|^(EvryWorkgDay)$|^(IntrvlWkDay:0[1-9]:0[1-7])$|^(WkInMnthDay:0[1-5]:0[1-7])$|^(IntrvlMnthDay:(0[1-6]|12|24):(-0[1-5]|0[1-9]|[12][0-9]|3[01]))$|^(QtrDay:(ENGLISH|SCOTTISH|RECEIVED))$ for Frequency"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 4] : Verification of MANDATORY Initiation/Frequency field value set as IntrvlWkDay");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey + "122211");
		dStandingOrder.setFrequency("IntrvlWkDay:01:07");
		dStandingOrder.submit();
		
        testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
        		"Response Code is correct for Domestic Standing Order Consent");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency")).isEmpty(), 
				"Mandatory field Frequency field");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency").equals("IntrvlWkDay:01:07"), 
				"Mandatory field Frequency is present under Data block and set as IntrvlWkDay");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4-1] : Verification of MANDATORY Initiation/Frequency field value set invalid IntrvlWkDay");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey + "122223");
		dStandingOrder.setFrequency("IntrvlWkDay:09:08");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when Frequency is passed as invalid");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Unsupported.Frequency", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Invalid value 'IntrvlWkDay:09:08'. Expected ^(EvryDay)$|^(EvryWorkgDay)$|^(IntrvlWkDay:0[1-9]:0[1-7])$|^(WkInMnthDay:0[1-5]:0[1-7])$|^(IntrvlMnthDay:(0[1-6]|12|24):(-0[1-5]|0[1-9]|[12][0-9]|3[01]))$|^(QtrDay:(ENGLISH|SCOTTISH|RECEIVED))$ for Frequency"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 5] : Verification of MANDATORY Initiation/Frequency field value set as WkInMnthDay");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey + "122332");
		dStandingOrder.setFrequency("WkInMnthDay:05:07");
		dStandingOrder.submit();
		
        testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
        		"Response Code is correct for Domestic Standing Order Consent");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency")).isEmpty(), 
				"Mandatory field Frequency field");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency").equals("WkInMnthDay:05:07"), 
				"Mandatory field Frequency is present under Data block and set as WkInMnthDay");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 5-1] : Verification of MANDATORY Initiation/Frequency field value set invalid WkInMnthDay");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey + "1223452");
		dStandingOrder.setFrequency("WkInMnthDay:06:08");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when Frequency is passed as invalid");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Unsupported.Frequency", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Invalid value 'WkInMnthDay:06:08'. Expected ^(EvryDay)$|^(EvryWorkgDay)$|^(IntrvlWkDay:0[1-9]:0[1-7])$|^(WkInMnthDay:0[1-5]:0[1-7])$|^(IntrvlMnthDay:(0[1-6]|12|24):(-0[1-5]|0[1-9]|[12][0-9]|3[01]))$|^(QtrDay:(ENGLISH|SCOTTISH|RECEIVED))$ for Frequency"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 6] : Verification of MANDATORY Initiation/Frequency field value set as IntrvlMnthDay");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey + "122256");
		dStandingOrder.setFrequency("IntrvlMnthDay:06:-01");
		dStandingOrder.submit();
		
        testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
        		"Response Code is correct for Domestic Standing Order Consent");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency")).isEmpty(), 
				"Mandatory field Frequency field");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency").equals("IntrvlMnthDay:06:-01"), 
				"Mandatory field Frequency is present under Data block and set as IntrvlMnthDay");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 6-1] : Verification of MANDATORY Initiation/Frequency field value set invalid IntrvlMnthDay");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey + "1222657");
		dStandingOrder.setFrequency("IntrvlMnthDay:07:-32");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when Frequency is passed as invalid");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Unsupported.Frequency", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Invalid value 'IntrvlMnthDay:07:-32'. Expected ^(EvryDay)$|^(EvryWorkgDay)$|^(IntrvlWkDay:0[1-9]:0[1-7])$|^(WkInMnthDay:0[1-5]:0[1-7])$|^(IntrvlMnthDay:(0[1-6]|12|24):(-0[1-5]|0[1-9]|[12][0-9]|3[01]))$|^(QtrDay:(ENGLISH|SCOTTISH|RECEIVED))$ for Frequency"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		TestLogger.logBlankLine();
		
		 TestLogger.logStep("[Step 7] : Verification of MANDATORY Initiation/Frequency field value set as QtrDay");
			
			dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
			dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey + "12229887");
			dStandingOrder.setFrequency("QtrDay:ENGLISH");
			dStandingOrder.submit();
			
	        testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
	        		"Response Code is correct for Domestic Standing Order Consent");
			
			testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency")).isEmpty(), 
					"Mandatory field Frequency field");
			
			testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.Frequency").equals("QtrDay:ENGLISH"), 
					"Mandatory field Frequency is present under Data block and set as QtrDay");
			
			TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}

}
