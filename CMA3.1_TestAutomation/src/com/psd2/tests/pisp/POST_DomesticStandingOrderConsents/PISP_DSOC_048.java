package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/NumberOfPayment field
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_048 extends TestBase{
	
	@Test
	public void m_PISP_DSOC_048() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 2] : Verification of the values of OPTIONAL Initiation/NumberOfPayment field");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.addNumberOfPayments();
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI when NumberOfPayments added & removed FinalPaymentDateTime");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.NumberOfPayments")).isEmpty(), 
				"Mandatory field NumberOfPayments is present and is not null");
		
		testVP.verifyTrue((dStandingOrder.getResponseNodeStringByPath("Data.Initiation.FinalPaymentDateTime"))==null, 
				"Mandatory field FinalPaymentDateTime is null");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.NumberOfPayments").length()<=35, 
				"NumberOfPayments field length is less than or equal to 35 characters");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}

}
