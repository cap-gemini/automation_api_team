package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/FinalPaymentDateTime field
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_056 extends TestBase{
	
	@Test
	public void m_PISP_DSOC_056() throws Throwable{	
		String idemPotencyKey=PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3);
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 2] : Verification of the values of OPTIONAL Initiation/FinalPaymentDateTime field");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI when FinalPaymentDateTime is present");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.FinalPaymentDateTime")).isEmpty(), 
				"Mandatory field FinalPaymentDateTime is present and is not null");
		
		testVP.verifyTrue(Misc.verifyDateTimeFormat(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.FinalPaymentDateTime").split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.FinalPaymentDateTime").split("T")[1], "HH:mm:ss+00:00"), 
				"FinalPaymentDateTime under Initiation block is as per expected format");
		
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 3] : Verification of the values of OPTIONAL Initiation/FinalPaymentDateTime & NumberOfPayments");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey+"1212");
		dStandingOrder.setFinalPaymentDateTime("2021-03-27T06:06:06+00:00");
		dStandingOrder.addNumberOfPayments();
		dStandingOrder.setNumberOfPayments("AAAAA");
		String requestBody=dStandingOrder.genRequestBody().replace("\"NumberOfPayments\": \"AAAAA\"", "\"NumberOfPayments\": \"AAAAA\","+"\"FinalPaymentDateTime\": \"2021-03-27T06:06:06+00:00\"");
		dStandingOrder.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when Initiation/FinalPaymentDateTime & NumberOfPayments");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Unexpected", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("invalid combination of field in request body"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		 TestLogger.logBlankLine();
		
		 TestLogger.logStep("[Step 4] : Verification of the values of OPTIONAL Initiation/FinalPaymentDateTime field is less than or equal to FirstPaymentDateTime");
			
			dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
			dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey+"2343");
			dStandingOrder.setFinalPaymentDateTime("2021-03-25T06:06:06+00:00");
			requestBody=dStandingOrder.genRequestBody().replace("\"NumberOfPayments\": \"AAAAA\"", "\"FinalPaymentDateTime\": \"2021-03-25T06:06:06+00:00\"");
			dStandingOrder.submit(requestBody);
			
			testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Standing Order Consent URI when FinalPaymentDateTime is less than or equal to FirstPaymentDateTime");
			
			testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.InvalidDate", 
					"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			
			testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("provided date-time expired"), 
					"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
			
			TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}