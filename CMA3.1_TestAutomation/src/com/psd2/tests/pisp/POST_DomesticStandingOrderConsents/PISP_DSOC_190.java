package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_190 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_190() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 201 Created");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		
		testVP.verifyTrue(!String.valueOf(dStandingOrder.getResponseValueByPath("Data.ConsentId")).isEmpty(),  
				"Mandatory field ConsentId is present and not empty");
		
		testVP.verifyTrue(!String.valueOf(dStandingOrder.getResponseValueByPath("Data.CreationDateTime")).isEmpty(), 
				"Mandatory field CreationDateTime is present and not empty");
		
		testVP.verifyTrue(!String.valueOf(dStandingOrder.getResponseValueByPath("Data.Status")).isEmpty(), 
				"Mandatory field Status is present and not empty");
		
		testVP.verifyTrue(!String.valueOf(dStandingOrder.getResponseValueByPath("Data.StatusUpdateDateTime")).isEmpty(), 
				"Mandatory field StatusUpdateDateTime is present and not empty");
		
		testVP.verifyTrue(!String.valueOf(dStandingOrder.getResponseValueByPath("Data.Initiation")).isEmpty(), 
				"Mandatory field Initiation is present and not empty");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
