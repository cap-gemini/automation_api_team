package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the value of OPTIONAL CreditorAccount/SecondaryIdentification field
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_152 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_152() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of OPTIONAL CreditorAccount/SecondaryIdentification field");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SecondaryIdentification")).isEmpty(), 
				"SecondaryIdentification field under CreditorAccount is present and is not null");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SecondaryIdentification").length()<=34, 
				"SecondaryIdentification field under CreditorAccount is less than 34 characters");
		
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 3] : Verification of NULL value of MANDATORY CreditorAccount/SecondaryIdentification field");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		dStandingOrder.setCrAccountSrIdentification("");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when CreditorAccount SecondaryIdentification is NULL");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected min length 1 for field [SecondaryIdentification], but got []"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of the value of MANDATORY CreditorAccount/SecondaryIdentification field having length variations");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		dStandingOrder.setCrAccountSrIdentification("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567834782469723054");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when CreditorAccount SecondaryIdentification is more than 34 characters");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 34 for field [SecondaryIdentification], but got [ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567834782469723054]"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
