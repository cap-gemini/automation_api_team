package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/RecurringPaymentDateTime field
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_054 extends TestBase{
	
	@Test
	public void m_PISP_DSOC_054() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 2] : Verification of the values of OPTIONAL Initiation/RecurringPaymentDateTime field");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI when RecurringPaymentDateTime is present");
		
		testVP.verifyTrue(!(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.RecurringPaymentDateTime")).isEmpty(), 
				"Mandatory field RecurringPaymentDateTime is present and is not null");
		
		testVP.verifyTrue(Misc.verifyDateTimeFormat(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.RecurringPaymentDateTime").split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.RecurringPaymentDateTime").split("T")[1], "HH:mm:ss+00:00"), 
				"RecurringPaymentDateTime under Initiation block is as per expected format");
		
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 3] : Verification of the values of OPTIONAL Initiation/RecurringPaymentDateTime is equal to FirstPaymentDateTime");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.setRecurringPaymentDateTime("2021-03-25T06:06:06+00:00");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Standing Order Consent URI when FirstPaymentDateTime is not followed ISO date-time");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Unexpected", 
				"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("both first date-time and recurring date are equal"), 
				"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
		
		 TestLogger.logBlankLine();
		
		 TestLogger.logStep("[Step 3] : Verification of the values of OPTIONAL Initiation/RecurringPaymentDateTime field is less than FirstPaymentDateTime");
			
			dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
			dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
			dStandingOrder.setRecurringPaymentDateTime("2021-03-24T06:06:06+00:00");
			dStandingOrder.submit();
			
			testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"400", 
					"Response Code is correct for Domestic Standing Order Consent URI when FirstPaymentDateTime is not followed ISO date-time");
			
			testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.InvalidDate", 
					"Error code for the response is correct i.e. '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			
			testVP.verifyTrue(dStandingOrder.getResponseNodeStringByPath("Errors[0].Message").equals("final payment date can not be prior to first payment date"), 
					"Message for error code is '"+dStandingOrder.getResponseNodeStringByPath("Errors[0].Message")+"'");
			
			TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}

}
