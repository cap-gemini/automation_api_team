package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request would remains same where same  request payload has been sent into BODY with same x-idempotency-key by same TPP within 24 hrs.
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_011 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_011() throws Throwable{	
		String idemPotencyKey=PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3);
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request with x-idempotency-key value as "+idemPotencyKey+" in the header");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey);
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent when x-idempotency-key value in header is "+idemPotencyKey+"");
		
		String consentId=dStandingOrder.getConsentId();
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.Status"), "AwaitingAuthorisation",
				"Status field value is correct");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of request with same x-idempotency-key i.e. "+idemPotencyKey+" and same payload");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey);
		dStandingOrder.submit();
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent when x-idempotency-key value is same i.e. "+idemPotencyKey+" and payload is same");

		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.Status"), "AwaitingAuthorisation",
				"Status field value is correct");
		
		testVP.assertStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.ConsentId"), consentId, 
				"ConsentId is same as the request sent previously");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
