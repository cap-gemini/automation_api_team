package com.psd2.tests.pisp.POST_DomesticStandingOrderConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the value of MANDATORY CreditorAccount/Identification field when SchemeName = UK.OBIE.IBAN/IBAN
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DSOC_136 extends TestBase {	
	
	@Test
	public void m_PISP_DSOC_136() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY CreditorAccount/Identification field when SchemeName = UK.OBIE.IBAN");
		
		dStandingOrder.setBaseURL(apiConst.dsoConsent_endpoint);
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token);
		dStandingOrder.setCrAccountSchemeName("UK.OBIE.IBAN");
		dStandingOrder.setCrAccountIdentification("IE85BOFI90120412345679");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SchemeName"), "UK.OBIE.IBAN", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.Identification"), "IE85BOFI90120412345679", 
				"Identification field value is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the value of MANDATORY CreditorAccount/Identification field when SchemeName = IBAN");
		
		dStandingOrder.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		dStandingOrder.setCrAccountSchemeName("IBAN");
		dStandingOrder.submit();
		
		testVP.verifyStringEquals(String.valueOf(dStandingOrder.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Standing Order Consent URI");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SchemeName"), "UK.OBIE.IBAN", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(dStandingOrder.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.Identification"), "IE85BOFI90120412345679", 
				"Identification field value is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
