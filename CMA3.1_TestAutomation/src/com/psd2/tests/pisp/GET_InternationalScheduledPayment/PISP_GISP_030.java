package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 200 OK  
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"ISP"})
public class PISP_GISP_030 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_GISP_030() throws Throwable{	
		
			TestLogger.logStep("[Step 1-1] : Creating client credetials....");
	        
	        createClientCred.setBaseURL(apiConst.cc_endpoint);
	        createClientCred.setScope("payments");
	        createClientCred.submit();
	        
	        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	        cc_token = createClientCred.getAccessToken();
	        TestLogger.logVariable("AccessToken : " + cc_token);
	        TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+API_Constant.getIsp_PaymentId());
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.setMethod("GET");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
					"Response Code is correct for Get International Scheduled Payment Submission URI");			
			testVP.verifyStringEquals(iScheduledPayment.getURL(),apiConst.iScheduledPaymentSubmission_endpoint+"/"+API_Constant.getIsp_PaymentId(), 
					"URI for Get International Scheduled Payment Submission is as per open banking standard");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.InternationalScheduledPaymentId"))!=null,
					"InternationalScheduledPaymentId field under Data is present in Get International Scheduled Payment  response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.InternationalScheduledPaymentId")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ConsentId"))!=null,
					"ConsentId field under Data is present in Get International Scheduled Payment  response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ConsentId")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.CreationDateTime"))!=null,
					"CreationDateTime field under Data is present in Get International Scheduled Payment  response body"+(iScheduledPayment.getResponseValueByPath("Data.CreationDateTime")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.StatusUpdateDateTime"))!=null,
					"StatusUpdateDateTime field under Data is present in Get International Scheduled Payment  response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.StatusUpdateDateTime")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Status"))!=null,
					"Status field under Data is present in Get International Scheduled Payment  response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.Status")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExpectedExecutionDateTime"))!=null,
					"ExpectedExecutionDateTime field under Data is present in Get International Scheduled Payment  response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ExpectedExecutionDateTime")));
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExpectedSettlementDateTime"))!=null,
					"ExpectedSettlementDateTime block under Data is present in Get International Scheduled Payment  response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ExpectedSettlementDateTime")));						
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
