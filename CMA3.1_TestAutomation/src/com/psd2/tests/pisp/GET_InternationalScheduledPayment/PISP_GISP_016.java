package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with invalid value of scope for the flow into Authorization (Access Token) header  
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"ISP"})
public class PISP_GISP_016 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_GISP_016() throws Throwable{	
					
			TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("accounts");
			createClientCred.submit();
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
					"Response Code is correct for client credetials");
			cc_token = createClientCred.getAccessToken();
			TestLogger.logVariable("CC token : " + cc_token);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment with invalid value of scope");
			
			consentDetails = apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, true);
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 3] : International Scheduled Payment Submission with invalid value of scope");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+API_Constant.getIsp_PaymentId());
			iScheduledPayment.setHeadersString("Authorization:Bearer " +cc_token);
			iScheduledPayment.setMethod("GET");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"403", 
					"Response Code is correct for invalid value of scope for the flow into Authorization header for Get International Scheduled Payment");	
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();	
	}
}
