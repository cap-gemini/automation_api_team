package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of international-scheduled-payments URL that should be as per Open Banking standards 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity","ISP"})
public class PISP_GISP_001 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_GISP_001() throws Throwable{	
		
		    TestLogger.logStep("[Step 1] : Generate Payment Id");
            consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, true);
            API_Constant.setIsp_PaymentId(consentDetails.get("paymentId"));
            TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment ....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"));
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
			iScheduledPayment.setMethod("GET");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
		    		"Response Code is correct for GET International Scheduled Payment URI");
			
			testVP.verifyStringEquals(iScheduledPayment.getURL(), apiConst.iScheduledPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"),
					"GET International Scheduled Payment URI is as per CMA Compliance");
							
			testVP.verifyTrue(SignatureUtility.verifySignature(iScheduledPayment.getResponseString(), iScheduledPayment.getResponseHeader("x-jws-signature")), 
					"Response that created successfully with HTTP Code 201 MUST be digitally signed");
			
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
