package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with BLANK or Invalid value of OPTIONAL x-fapi-auth-date header  
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"ISP"})
public class PISP_GISP_023 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_GISP_023() throws Throwable{	
		
			TestLogger.logStep("[Step 1-1] : Creating client credetials....");
	        
	        createClientCred.setBaseURL(apiConst.cc_endpoint);
	        createClientCred.setScope("payments");
	        createClientCred.submit();
	        
	        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	        cc_token = createClientCred.getAccessToken();
	        TestLogger.logVariable("AccessToken : " + cc_token);
	        TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+API_Constant.getIsp_PaymentId());
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.addHeaderEntry("x-fapi-customer-last-logged-time", "FSTYWFUD^%^");
			iScheduledPayment.setMethod("GET");
			iScheduledPayment.submit();		
				testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
						"Response Code is correct for International Scheduled Payment Submission URI when invalid x-fapi-customer-ip-address is passed");
				testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
						"Response Error Code is correct");
				testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Errors[0].Message")),
						"Invalid value '[FSTYWFUD^%^]' for header x-fapi-customer-last-logged-time. Invalid value 'FSTYWFUD^%^'. Expected ^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), \\d{2} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \\d{4} \\d{2}:\\d{2}:\\d{2} (GMT|UTC)$","Error message is correct");		
									
			TestLogger.logStep("[Step 3] : Get International Scheduled Payment ");	
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+API_Constant.getIsp_PaymentId());
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.addHeaderEntry("x-fapi-customer-last-logged-time", "");
			iScheduledPayment.setMethod("GET");
			iScheduledPayment.submit();		
				testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"400", 
						"Response Code is correct for International Scheduled Payment Submission URI when blank x-fapi-customer-ip-address is passed");
				testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
						"Response Error Code is correct");
				testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Errors[0].Message")),
						"Invalid value '[]' for header x-fapi-customer-last-logged-time. Invalid value ''. Expected ^(Mon|Tue|Wed|Thu|Fri|Sat|Sun), \\d{2} (Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) \\d{4} \\d{2}:\\d{2}:\\d{2} (GMT|UTC)$","Error message is correct");		
				TestLogger.logBlankLine();
				testVP.testResultFinalize();	
	}
}
