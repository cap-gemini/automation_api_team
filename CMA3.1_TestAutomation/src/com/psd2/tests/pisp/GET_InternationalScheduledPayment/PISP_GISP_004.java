package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through GET method with mandatory and optional fields. 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"ISP"})
public class PISP_GISP_004 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_GISP_004() throws Throwable{	
		
			TestLogger.logStep("[Step 1-1] : Creating client credetials....");
	        
	        createClientCred.setBaseURL(apiConst.cc_endpoint);
	        createClientCred.setScope("payments");
	        createClientCred.submit();
	        
	        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	        cc_token = createClientCred.getAccessToken();
	        TestLogger.logVariable("AccessToken : " + cc_token);
	        TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment SetUp....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+API_Constant.getIsp_PaymentId());
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.setMethod("GET");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()), "200",
					"Response Code is correct for International Scheduled Payment URI");
			testVP.verifyEquals(iScheduledPayment.getURL(), apiConst.iScheduledPaymentSubmission_endpoint+"/"+API_Constant.getIsp_PaymentId(), 
					"URL is correct and matching with Get International Scheduled Payment URL");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data")) != null,
					"Mandatory field Data is present in response and is not empty");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Charges")) != null,
					"Optional field Charges is present in response and is not empty");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation")) != null,
					"Mandatory field Initiation is present in response and is not empty");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.MultiAuthorisation")) != null,
					"Optional field MultiAuthorisation is present in response and is not empty");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation")) != null,
					"Optional field ExchangeRateInformation is present in response and is not empty");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Links")) != null,
					"Mandatory field Links is present in response and is not empty");
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Links.Self")) != null,
					"Mandatory field Self is present in response and is not empty");
			testVP.verifyStringEquals(iScheduledPayment.getResponseHeader("Content-Type"), "application/json;charset=utf-8", 
					"Response is UTF-8 character encoded");						
			TestLogger.logBlankLine();	
			testVP.testResultFinalize();
	}
}
