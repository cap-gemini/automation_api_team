package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of response for the payments API when the RateType is provided as "Indicative" in the request payload  
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"ISP"})
public class PISP_GISP_119 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_GISP_119() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", "Response Code is correct for client credentials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);    
        TestLogger.logBlankLine();
        
        TestLogger.logStep("[Step 2] : Verification of response when the RateType is provided as \"Indicative\" in the request payload");                          
        iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
        iScheduledPayment.setRateType("Indicative");
        String requestBody=iScheduledPayment.genRequestBody().replace("\"ExchangeRate\": "+iScheduledPayment._exchangeRate+",","");
        requestBody = requestBody.replace(",\"ContractIdentification\": \"123identification\"","");
        iScheduledPayment.submit(requestBody);
       
		String consentId=iScheduledPayment.getConsentId();
		
		TestLogger.logStep("[Step 1-3] : JWT Token Creation........");
		
		//reqObject.setBaseURL(apiConst.ro_endpoint);
		reqObject.setValueField(consentId);
		reqObject.setScopeField("payments");
		outId = reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-4] : Go to URL and authenticate consent");	
		redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		authCode = consentOps.authorisePISPConsent(redirecturl+"##"+iScheduledPayment._drAccountIdentification,iScheduledPayment.removeDebtorAccount);	
		closeDriverInstance();
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 1-5] : Get access and refresh token");	
		accesstoken.setBaseURL(apiConst.at_endpoint);
		accesstoken.setAuthCode(authCode);
		accesstoken.submit();
		
		testVP.verifyStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
				"Response Code is correct for get access token request");	
		access_token = accesstoken.getAccessToken();
		refresh_token = accesstoken.getRefreshToken();
		TestLogger.logVariable("Access Token : " + access_token);
		TestLogger.logVariable("Refresh Token : " + refresh_token);
		
		API_Constant.setPisp_AccessToken(access_token);		
		
		TestLogger.logStep("[Step 2] : Verification of response when the RateType is provided as \"Indicative\" in the request payload");                          
        iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+access_token);
        iScheduledPayment.setConsentId(consentId);
        iScheduledPayment.setRateType("Indicative");
        requestBody = iScheduledPayment.genRequestBody().replace("\"ExchangeRate\": "+iScheduledPayment._exchangeRate+",","");
        requestBody = requestBody.replace(",\"ContractIdentification\": \"123identification\"","");
        iScheduledPayment.submit(requestBody);
		    
        paymentId=iScheduledPayment.getPaymentId();
    
		TestLogger.logStep("[Step 2] : International Scheduled Payment ....");
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+paymentId);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.setMethod("GET");
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for International Scheduled Payment URI");			
		testVP.verifyStringEquals(iScheduledPayment.getURL(),apiConst.iScheduledPaymentSubmission_endpoint+"/"+paymentId, 
				"URI for GET International Scheduled Payment request is as per open banking standard");			
		testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType"))!=null,
				"RateType field under ExchangeRateInformation block is present in International Payment Submission response body "
						+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType"))); 
		testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType")).equals("Indicative"),
				"RateType field under ExchangeRateInformation block is present in International Payment Submission response body and has same value as sent in request");
		   
		TestLogger.logBlankLine();
        testVP.testResultFinalize();
	}
}
