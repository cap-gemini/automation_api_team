package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of response for the payments API when the RateType is provided as "Agreed" in the request payload  
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"ISP"})
public class PISP_GISP_120 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_GISP_120() throws Throwable{	
		iScheduledPayment.setRateType("Agreed");
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, true);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : International Scheduled Payment ....");
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"));
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		iScheduledPayment.setMethod("GET");
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for International Scheduled Payment URI");			
		testVP.verifyStringEquals(iScheduledPayment.getURL(),apiConst.iScheduledPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"), 
				"URI for GET International Scheduled Payment request is as per open banking standard");			
		testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType"))!=null,
				"RateType field under ExchangeRateInformation block is present in International Payment Submission response body "
						+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType"))); 
		testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType")).equals("Agreed"),
				"RateType field under ExchangeRateInformation block is present in International Payment Submission response body and has same value as sent in request");
		   
		TestLogger.logBlankLine();
        testVP.testResultFinalize();
	}
}