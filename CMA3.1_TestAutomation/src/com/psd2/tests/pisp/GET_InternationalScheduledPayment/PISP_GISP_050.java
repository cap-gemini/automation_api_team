package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Initiation block where Request has sent successfully and returned a HTTP Code 200 OK  
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"ISP"})
public class PISP_GISP_050 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_GISP_050() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credentials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);    
        TestLogger.logBlankLine();
        
        TestLogger.logStep("[Step 1-2] : RateType is provided as \"Actual\" in the request payload");                          
        iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
        String requestBody=iScheduledPayment.genRequestBody().replace("\"Purpose\": \"Test\",","\"Purpose\":\"Test\", \"ChargeBearer\":\"Shared\","); 
        iScheduledPayment.submit(requestBody);
       
		String consentId=iScheduledPayment.getConsentId();
		
		TestLogger.logStep("[Step 1-3] : JWT Token Creation........");
		
		//reqObject.setBaseURL(apiConst.ro_endpoint);
		reqObject.setValueField(consentId);
		reqObject.setScopeField("payments");
		outId = reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-4] : Go to URL and authenticate consent");	
		redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		authCode = consentOps.authorisePISPConsent(redirecturl+"##"+iScheduledPayment._drAccountIdentification,iScheduledPayment.removeDebtorAccount);	
		closeDriverInstance();
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 1-5] : Get access and refresh token");	
		accesstoken.setBaseURL(apiConst.at_endpoint);
		accesstoken.setAuthCode(authCode);
		accesstoken.submit();
		
		testVP.verifyStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
				"Response Code is correct for get access token request");	
		access_token = accesstoken.getAccessToken();
		refresh_token = accesstoken.getRefreshToken();
		TestLogger.logVariable("Access Token : " + access_token);
		TestLogger.logVariable("Refresh Token : " + refresh_token);
		
		API_Constant.setPisp_AccessToken(access_token);		
		
		TestLogger.logStep("[Step 1-6] : RateType is provided as \"Actual\" in the request payload");                          
        iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+access_token);
        iScheduledPayment.setConsentId(consentId);
        requestBody=iScheduledPayment.genRequestBody().replace("\"Purpose\": \"Test\",","\"Purpose\":\"Test\", \"ChargeBearer\":\"Shared\","); 
        iScheduledPayment.submit(requestBody);
        paymentId = iScheduledPayment.getPaymentId();
        			
		TestLogger.logStep("[Step 2] : International Scheduled Payment SetUp....");
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+paymentId);
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.setMethod("GET");
		iScheduledPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for International Scheduled Payment URI");			
		testVP.verifyStringEquals(iScheduledPayment.getURL(),apiConst.iScheduledPaymentSubmission_endpoint+"/"+paymentId, 
				"URI for GET International Scheduled Payment request is as per open banking standard");
		testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructionIdentification"))!=null, 
				"Mandatory field InstructionIdentification is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructionIdentification"));
		testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.EndToEndIdentification"))!=null, 
				"Mandatory field EndToEndIdentification is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.EndToEndIdentification"));
		testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.LocalInstrument"))!=null,
				"Optional field LocalInstrument is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.LocalInstrument"));
		testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructionPriority"))!=null,
				"Optional field InstructionPriority is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.InstructionPriority"));
		testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.Purpose"))!=null,
				"Optional field Purpose is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.Purpose"));
		testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer"))!=null,
				"Optional field ChargeBearer is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.ChargeBearer"));
		testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.RequestedExecutionDateTime"))!=null,
				"Optional field RequestedExecutionDateTime is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.RequestedExecutionDateTime"));
		testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.Initiation.CurrencyOfTransfer"))!=null,
				"Mandatory field CurrencyOfTransfer is present in Initiation block "+iScheduledPayment.getResponseValueByPath("Data.Initiation.CurrencyOfTransfer"));
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
