package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;

/**
 * Class Description : Verification of the values into OPTIONAL ExchangeRateInformation block where Request has sent successfully and returned a HTTP Code 201 Created  
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"ISP"})
public class PISP_GISP_044 extends TestBase{	
	
	@Test
	public void m_PISP_GISP_044() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credentials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);    
        TestLogger.logBlankLine();
        
        TestLogger.logStep("[Step 1-2] : Verification of response when the RateType is provided as \"Actual\" in the request payload");                          
        iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentConsent_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
        iScheduledPayment.setRateType("Actual");
        String requestBody=iScheduledPayment.genRequestBody().replace("\"ExchangeRate\": "+iScheduledPayment._exchangeRate+",","");
        requestBody = requestBody.replace(",\"ContractIdentification\": \"123identification\"","");
        iScheduledPayment.submit(requestBody);
       
		String consentId=iScheduledPayment.getConsentId();
		
		TestLogger.logStep("[Step 1-3] : JWT Token Creation........");
		
		//reqObject.setBaseURL(apiConst.ro_endpoint);
		reqObject.setValueField(consentId);
		reqObject.setScopeField("payments");
		outId = reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-4] : Go to URL and authenticate consent");	
		redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		authCode = consentOps.authorisePISPConsent(redirecturl+"##"+iScheduledPayment._drAccountIdentification,iScheduledPayment.removeDebtorAccount);	
		closeDriverInstance();
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 1-5] : Get access and refresh token");	
		accesstoken.setBaseURL(apiConst.at_endpoint);
		accesstoken.setAuthCode(authCode);
		accesstoken.submit();
		
		testVP.verifyStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
				"Response Code is correct for get access token request");	
		access_token = accesstoken.getAccessToken();
		refresh_token = accesstoken.getRefreshToken();
		TestLogger.logVariable("Access Token : " + access_token);
		TestLogger.logVariable("Refresh Token : " + refresh_token);
		
		API_Constant.setPisp_AccessToken(access_token);		
		
		TestLogger.logStep("[Step 1-6] : Verification of response when the RateType is provided as \"Actual\" in the request payload");                          
        iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint);
        iScheduledPayment.setHeadersString("Authorization:Bearer "+access_token);
        iScheduledPayment.setConsentId(consentId);
        iScheduledPayment.setRateType("Actual");
        requestBody = iScheduledPayment.genRequestBody().replace("\"ExchangeRate\": "+iScheduledPayment._exchangeRate+",","");
        requestBody = requestBody.replace(",\"ContractIdentification\": \"123identification\"","");
        iScheduledPayment.submit(requestBody);
			
			TestLogger.logStep("[Step 1-7] : International Scheduled Payment ....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+iScheduledPayment.getPaymentId());
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.setMethod("GET");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
					"Response Code is correct for International Scheduled Payment URI");			
			testVP.verifyStringEquals(iScheduledPayment.getURL(),apiConst.iScheduledPaymentSubmission_endpoint+"/"+iScheduledPayment.getPaymentId(), 
					"URI for GET International payment consent request is as per open banking standard");
			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation"))!=null,
					"ExchangeRateInformation block under Data is present in Get International Scheduled Payment response body"+String.valueOf(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation")));
			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.UnitCurrency"))!=null,
					"UnitCurrency field under ExchangeRateInformation block is present in Get International Scheduled Payment response body"+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.UnitCurrency")));
			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExchangeRate"))!=null,
					"ExchangeRate field under ExchangeRateInformation block is present in Get International Scheduled Payment response body"+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExchangeRate")));
			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType"))!=null,
					"RateType field under ExchangeRateInformation block is present in Get International Scheduled Payment response body"+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.RateType")));
			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ContractIdentification"))!=null,
					"ContractIdentification field under ExchangeRateInformation block is present in Get International Scheduled Payment response body"+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ContractIdentification")));
			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExpirationDateTime"))!=null,
					"ExpirationDateTime field under ExchangeRateInformation block is present in Get International Scheduled Payment response body"+(iScheduledPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExpirationDateTime")));
		
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
	}
}
