package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of GET response for the payment API when the Amount starts with "700" in the POST request payload  
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"ISP"})
public class PISP_GISP_123 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_GISP_123() throws Throwable{	
		iScheduledPayment.setAmount("700.123");
		TestLogger.logStep("[Step 1] : Generate Payment Id");
        consentDetails=apiUtility.generatePayments(false, apiConst.internationalScheduledPayments, false, true);
        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : GET International Scheduled Payment");	
		iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"));
		iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		iScheduledPayment.setMethod("GET");
		iScheduledPayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"500", 
				"Response Code is correct for GET International Scheduled Payment URI");			
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Errors[0].ErrorCode")),
				"UK.OBIE.UnexpectedError", "Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseValueByPath("Errors[0].Message")),
				"Internal error occurred","Error message is correct");		
									
		TestLogger.logBlankLine();
	    testVP.testResultFinalize();
	}
}
