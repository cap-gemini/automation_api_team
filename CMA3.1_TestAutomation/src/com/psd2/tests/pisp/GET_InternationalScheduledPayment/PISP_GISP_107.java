package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY MultiAuthorisation/Status where Request has sent successfully and returned a HTTP Code 200 OK  
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"ISP"})
public class PISP_GISP_107 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_GISP_107() throws Throwable{	
		
			TestLogger.logStep("[Step 1-1] : Creating client credetials....");
	        
	        createClientCred.setBaseURL(apiConst.cc_endpoint);
	        createClientCred.setScope("payments");
	        createClientCred.submit();
	        
	        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	        cc_token = createClientCred.getAccessToken();
	        TestLogger.logVariable("AccessToken : " + cc_token);
	        TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : International Scheduled Payment ....");
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+API_Constant.getIsp_PaymentId());
			iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
			iScheduledPayment.setMethod("GET");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
					"Response Code is correct for International Payment Consent URI");			
			testVP.verifyStringEquals(iScheduledPayment.getURL(),apiConst.iScheduledPaymentSubmission_endpoint+"/"+API_Constant.getIsp_PaymentId(), 
					"URI for GET International payment consent request is as per open banking standard");			
			testVP.verifyTrue((iScheduledPayment.getResponseValueByPath("Data.MultiAuthorisation.Status"))!=null, 
					"Mandatory field Status is present in MultiAuthorisation block and is not null");
			testVP.verifyTrue(String.valueOf(iScheduledPayment.getResponseValueByPath("Data.MultiAuthorisation.Status")).equals("AwaitingFurtherAuthorisation")
					||String.valueOf(iScheduledPayment.getResponseValueByPath("Data.MultiAuthorisation.Status")).equals("Rejected")
					||String.valueOf(iScheduledPayment.getResponseValueByPath("Data.MultiAuthorisation.Status")).equals("Authorised"),
					"Status field in response is same as that sent in request and has same value as request");
			TestLogger.logBlankLine();
	        testVP.testResultFinalize();
	}
}
