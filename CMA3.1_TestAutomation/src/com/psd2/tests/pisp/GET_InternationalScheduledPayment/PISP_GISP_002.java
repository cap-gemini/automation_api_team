package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of international-scheduled-payments URL that is NOT as per Open Banking standards 
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"ISP"})
public class PISP_GISP_002 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_GISP_002() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
			
		TestLogger.logStep("[Step 2] : International Scheduled Payment ....");
		iScheduledPayment.setBaseURL(apiConst.invalid_payment_endpoint+"/"+API_Constant.getIsp_PaymentId());
		iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
		iScheduledPayment.setMethod("GET");
		iScheduledPayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"404", 
				"Response Code is correct for Get international-scheduled-payments URL is NOT as per Open Banking standards");							
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();
	}
}
