package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with BLANK or Invalid value of OPTIONAL x-fapi-financial-id header  
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"ISP"})
public class PISP_GISP_019 extends TestBase{

API_E2E_Utility apiUtility = new API_E2E_Utility();

@Test
public void m_PISP_GISP_019() throws Throwable{
 
	TestLogger.logStep("[Step 1-1] : Creating client credetials....");
    createClientCred.setBaseURL(apiConst.cc_endpoint);
    createClientCred.setScope("payments");
    createClientCred.submit();
    testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200",
            "Response Code is correct for client credetials");
    cc_token = createClientCred.getAccessToken();
    TestLogger.logVariable("AccessToken : " + cc_token);
    TestLogger.logBlankLine();
 
    TestLogger.logStep("[Step 2] : Get International Scheduled Payment Submission with BLANK value of OPTIONAL x-fapi-financial-id header");
    iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+API_Constant.getIsp_PaymentId());
    iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
    iScheduledPayment.addHeaderEntry("x-fapi-financial-id","");
    iScheduledPayment.setMethod("GET");
    iScheduledPayment.submit();
    testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"403",
      "Response Code is correct for Blank value of x-fapi-financial-id header Get International Scheduled Payment Submission");
    TestLogger.logBlankLine();
    TestLogger.logStep("[Step 3] : Get International Scheduled Payment Submission with Invalid value of OPTIONAL x-fapi-financial-id header");
    iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+API_Constant.getIsp_PaymentId());
    iScheduledPayment.setHeadersString("Authorization:Bearer "+cc_token);
    iScheduledPayment.addHeaderEntry("x-fapi-financial-id","aa$$$");
    iScheduledPayment.setMethod("GET");
    iScheduledPayment.submit();
    testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"403",
      "Response Code is correct for Invalid value of x-fapi-financial-id header Get International Scheduled Payment Submission");
    TestLogger.logBlankLine();
    testVP.testResultFinalize();
  }
}