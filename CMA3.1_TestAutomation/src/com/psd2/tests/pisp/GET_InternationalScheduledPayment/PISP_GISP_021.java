package com.psd2.tests.pisp.GET_InternationalScheduledPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of x-fapi-interaction-id value when NOT sent in the request with the response header that created successfully with HTTP Code 200 OK  
 * @author : Soumya Banerjee
 *
 */

@Listeners( { TestListener.class })
//@Test(groups={"Regression"},dependsOnGroups={"ISP"})
public class PISP_GISP_021 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_PISP_GISP_021() throws Throwable{	
				
			TestLogger.logStep("[Step 1] : Generate Consent Id");
			consentDetails = apiUtility.generatePayments(false,apiConst.internationalScheduledPayments, false, true);
			TestLogger.logBlankLine();	
			
			TestLogger.logStep("[Step 2] : GET International Scheduled Payments");	
			iScheduledPayment.setBaseURL(apiConst.iScheduledPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"));
			iScheduledPayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
			iScheduledPayment.addHeaderEntry("x-fapi-interaction-id","");
			iScheduledPayment.setMethod("GET");
			iScheduledPayment.submit();
			
			testVP.verifyStringEquals(String.valueOf(iScheduledPayment.getResponseStatusCode()),"200", 
					"Response Code is correct for GET International Payment Consent URI when x-fapi-interaction-id value is not sent in request");	
			testVP.verifyEquals(iScheduledPayment.getURL(), apiConst.iScheduledPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"), 
					"URL is correct and matching with GET International Scheduled Payment URL");
			TestLogger.logBlankLine();
			testVP.testResultFinalize();
	}
}