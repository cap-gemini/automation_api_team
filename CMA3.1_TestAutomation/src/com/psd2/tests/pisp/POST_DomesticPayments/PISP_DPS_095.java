package com.psd2.tests.pisp.POST_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY CreditorAccount/Identification field when SchemeName = UK.OBIE.SortCodeAccountNumber/SortCodeAccountNumber
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPS_095 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DPS_95() throws Throwable{	
		paymentConsent.setCrAccountSchemeName("UK.OBIE.SortCodeAccountNumber");
		paymentConsent.setCrAccountIdentification("12345678901234");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");

		consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, false, false);
		TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : POST Domestic Payment Submission");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		paymentConsent.setConsentId(consentDetails.get("consentId"));
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for POST Domestic Payment Submission");		
		
		testVP.verifyStringEquals(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SchemeName"), "UK.OBIE.SortCodeAccountNumber", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.Identification"), "12345678901234", 
				"Identification field value is correct");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}