package com.psd2.tests.pisp.POST_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Status field where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPS_174 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DPS_174() throws Throwable{	
	
			
		TestLogger.logStep("[Step 1] : Create Domestic Payment Consent");
		
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : POST Domestic Payment Submission");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		paymentConsent.setConsentId(consentDetails.get("consentId"));
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(paymentConsent.getResponseNodeStringByPath("Data.Status").equals("AcceptedSettlementInProcess"),				
				"PaymentContextCode under Data block has correct value");		
			    
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the values into Status field of consent API where Request has sent successfully and returned a HTTP Code 201 Created");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint+"/"+consentDetails.get("consentId"));
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		paymentConsent.setMethod("GET");
		paymentConsent.submit();
			
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"200", 
				"Response Code is correct for Domestic Payment URL");
		
		testVP.verifyTrue(paymentConsent.getResponseNodeStringByPath("Data.Status").equals("Consumed"),
				"Status field value is correct i.e. "+paymentConsent.getResponseNodeStringByPath("Data.Status"));
			
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}

