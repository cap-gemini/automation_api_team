package com.psd2.tests.pisp.POST_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/InstructedAmount/Amount field starts with 200 for Currency as EUR/GBP
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPS_226 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DPS_226() throws Throwable{	
		paymentConsent.setAmount("200.12");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");

		consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, false, false);
		TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : POST Domestic Payment Submission");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		paymentConsent.setConsentId(consentDetails.get("consentId"));
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201",
				"Response Code is correct for Domestic Payment URI when InstructedAmount/Amount as 200");

		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Data.Status")), "Rejected",
				"Mandatory field Status as Rejected");
		
		testVP.verifyTrue(paymentConsent.getResponseValueByPath("MultiAuthorisation") == null,
				"Mandatory field MultiAuthorisation is not present under Initiation block");
		
		TestLogger.logStep("[Step 3] : POST Domestic Payment Submission");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		paymentConsent.setConsentId(consentDetails.get("consentId"));
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201",
				"Response Code is correct for Domestic Payment URI when InstructedAmount/Amount as 200");

		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Data.Status")), "Rejected",
				"Mandatory field Status as Rejected");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}