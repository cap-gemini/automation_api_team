package com.psd2.tests.pisp.POST_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the Invalid value of MANDATORY DebtorAccount/Identification field when SchemeName = any.bank.scheme2
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Blocked"})
public class PISP_DPS_083 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DPS_83() throws Throwable{	
		paymentConsent.setDrAccountSchemeName("any.bank.scheme2");
		paymentConsent.setDrAccountIdentification("scheme2");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		apiUtility.pispAccessToken=true;
		consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, false, false);
		TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Domestic Payment Submission");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		paymentConsent.setDrAccountIdentification("scheme2334455");
		paymentConsent.setConsentId(consentDetails.get("consentId"));
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"400",
				"Response Code is correct for Domestic Payment URI when DebtorAccount Identification value is not valid");

		testVP.verifyStringEquals(paymentConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Resource.ConsentMismatch",
				"Error code for the response is correct i.e. '"+ paymentConsent.getResponseNodeStringByPath("Errors[0].ErrorCode")+ "'");

		testVP.verifyTrue(paymentConsent.getResponseNodeStringByPath("Errors[0].Message").equals("Payload comparison failed with the consent resource"),
				"Message for error code is '" + paymentConsent.getResponseNodeStringByPath("Errors[0].Message") + "'");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}