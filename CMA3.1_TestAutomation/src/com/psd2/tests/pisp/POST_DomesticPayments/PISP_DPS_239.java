package com.psd2.tests.pisp.POST_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of Payments 1.1 API
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Sanity","Regression"})
public class PISP_DPS_239 extends TestBase {
	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DPS_239() throws Throwable{	
		TestLogger.logStep("[Step 1] : Post Payment Submission v1.1");
		consentDetails=apiUtility.generatePayments(false, apiConst.paymentSetup, false, false);
		TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : POST Payment Submission v1.1");	
		paymentSetup.setBaseURL(apiConst.paymentSubmission_endpoint);
		paymentSetup.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		paymentSetup.setPaymentId(consentDetails.get("consentId"));
		paymentSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentSetup.getResponseStatusCode()),"201",
				"Response Code is correct forPost Payment Submission v1.1");

		TestLogger.logBlankLine();		
		
		testVP.testResultFinalize();		
	}
}