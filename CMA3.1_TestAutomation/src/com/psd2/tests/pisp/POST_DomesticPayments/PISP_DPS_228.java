package com.psd2.tests.pisp.POST_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data/Initiation/InstructedAmount/Amount field starts with 300 for Currency as EUR/GBP
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPS_228 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DPS_228() throws Throwable{	
		paymentConsent.setAmount("300.12");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");

		consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, false, false);
		TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : POST Domestic Payment Submission");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		paymentConsent.setConsentId(consentDetails.get("consentId"));
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201",
				"Response Code is correct for Domestic Payment URI when InstructedAmount/Amount as 300");

		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Data.Status")), "AcceptedSettlementInProcess",
				"Mandatory field Status as AcceptedSettlementInProcess");
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Data.MultiAuthorisation.Status")),"Authorised",
				"Mandatory field MultiAuthorisation is present and status as Authorised");
		
		String paymentId=paymentConsent.getPaymentId();
		
		TestLogger.logBlankLine();		
		
		TestLogger.logStep("[Step 3] : POST Domestic Payment Submission");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		paymentConsent.setConsentId(consentDetails.get("consentId"));
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201",
				"Response Code is correct for Domestic Payment URI when InstructedAmount/Amount as 300");

		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Data.Status")), "AcceptedSettlementInProcess",
				"Mandatory field Status as AcceptedSettlementInProcess");
		
		testVP.assertStringEquals(paymentConsent.getResponseNodeStringByPath("Data.DomesticPaymentId"), paymentId, 
				"PaymentId is same as the request sent previously");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}