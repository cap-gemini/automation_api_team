package com.psd2.tests.pisp.POST_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the value of MANDATORY CreditorAccount/Identification field when SchemeName = any.bank.scheme2
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPS_107 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DPS_107() throws Throwable{	
		paymentConsent.setCrAccountSchemeName("any.bank.scheme2");
		paymentConsent.setCrAccountIdentification("scheme2");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");

		consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, false, false);
		TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : POST Domestic Payment Submission");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		paymentConsent.setConsentId(consentDetails.get("consentId"));
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyStringEquals(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.SchemeName"), "any.bank.scheme2", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorAccount.Identification"), "scheme2", 
				"Identification field value is correct");
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}