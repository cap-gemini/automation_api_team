package com.psd2.tests.pisp.POST_DomesticPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request without Name field under DebtorAccount
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class PISP_DPS_240 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_DPS_240() throws Throwable{	
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200","Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		API_Constant.setPisp_CC_AccessToken(cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-2] : Domestic Payment Consent SetUp....");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		String requestBody=paymentConsent.genRequestBody().replace("\"Name\": \""+paymentConsent._drAccountName+"\",", "");
		paymentConsent.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", "Response Code is correct for Domestic Payment Consent");
		consentId = paymentConsent.getConsentId();
		TestLogger.logVariable("Consent Id : " + consentId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-3] : JWT Token Creation........");
		
		//reqObject.setBaseURL(apiConst.ro_endpoint);
		reqObject.setValueField(consentId);
		reqObject.setScopeField("payments");
		outId = reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-4] : Go to URL and authenticate consent");	
		redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		authCode = consentOps.authorisePISPConsent(redirecturl+"##"+paymentConsent._drAccountIdentification,paymentConsent.addDebtorAccount);		
		closeDriverInstance();
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 1-5] : Get access token");	
		accesstoken.setBaseURL(apiConst.at_endpoint);
		accesstoken.setAuthCode(authCode);
		accesstoken.submit();
		
		testVP.verifyStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
				"Response Code is correct for get access token request");	
		access_token = accesstoken.getAccessToken();
		TestLogger.logVariable("Access Token : " + access_token);		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-6] : Domestic Payment Submission request wihout Name field");	
		paymentConsent.setBaseURL(apiConst.dps_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+access_token);
		paymentConsent.setConsentId(consentId);
		String requestBody_Submission=paymentConsent.genRequestBody().replace("\"Name\": \""+paymentConsent._drAccountName+"\",", "");
		paymentConsent.submit(requestBody_Submission);
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201",
				"Response Code is correct for Domestic Payment Submission when request is sent without Name field");
		
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}