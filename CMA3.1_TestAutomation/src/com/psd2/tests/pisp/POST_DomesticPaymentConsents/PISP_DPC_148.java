package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into OPTIONAL Authorisation/CompletionDateTime field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPC_148 extends TestBase {	
	
	@Test
	public void m_PISP_DPC_148() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Authorisation/CompletionDateTime field");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Post Domestic Payment Consents");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Authorisation.CompletionDateTime")).isEmpty(), 
				"Optional field CompletionDateTime is present and is not null");
		
		testVP.verifyTrue(Misc.verifyDateTimeFormat(paymentConsent.getResponseNodeStringByPath("Data.Authorisation.CompletionDateTime").split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(paymentConsent.getResponseNodeStringByPath("Data.Authorisation.CompletionDateTime").split("T")[1], "HH:mm:ss+00:00"), 
				"CompletionDateTime under Authorisation block is as per expected format");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
