package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/LocalInstrument field having OB defined values
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPC_050 extends TestBase {	
	
	@Test
	public void m_PISP_DPC_050() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Domestic Payment Consents with Local Instrument as : UK.OBIE.BACS");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		paymentConsent.setLocalInstrument("UK.OBIE.BACS");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(paymentConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BACS"),
					"LocalInstrument field value is as per OB defined values");		
		TestLogger.logBlankLine(); 
		
		TestLogger.logStep("[Step 3] : Domestic Payment Consents with Local Instrument as : UK.OBIE.CHAPS");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		paymentConsent.setLocalInstrument("UK.OBIE.CHAPS");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(paymentConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.CHAPS"),
					"LocalInstrument field value is as per OB defined values");		
		TestLogger.logBlankLine(); 
		
		TestLogger.logStep("[Step 4] : Domestic Payment Consents with Local Instrument as : UK.OBIE.FPS");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		paymentConsent.setLocalInstrument("UK.OBIE.FPS");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(paymentConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.FPS"),
					"LocalInstrument field value is as per OB defined values");		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 5] : Domestic Payment Consents with Local Instrument as : UK.OBIE.SWIFT");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		paymentConsent.setLocalInstrument("UK.OBIE.SWIFT");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Unsupported.LocalInstrument", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].Message")),"invalid local instrument provided",
				"Error message is correct");	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 6] : Domestic Payment Consents with Local Instrument as : UK.OBIE.BalanceTransfer");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		paymentConsent.setLocalInstrument("UK.OBIE.BalanceTransfer");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(paymentConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BalanceTransfer"),
					"LocalInstrument field value is as per OB defined values");		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 7] : Domestic Payment Consents with Local Instrument as : UK.OBIE.MoneyTransfer");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		paymentConsent.setLocalInstrument("UK.OBIE.MoneyTransfer");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(paymentConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.MoneyTransfer"),
					"LocalInstrument field value is as per OB defined values");		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 8] : Domestic Payment Consents with Local Instrument as : UK.OBIE.Paym");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		paymentConsent.setLocalInstrument("UK.OBIE.Paym");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(paymentConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Paym"),
					"LocalInstrument field value is as per OB defined values");		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 9] : Domestic Payment Consents with Local Instrument as : UK.OBIE.Euro1");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		paymentConsent.setLocalInstrument("UK.OBIE.Euro1");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Unsupported.LocalInstrument", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].Message")),"invalid local instrument provided",
				"Error message is correct");			
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 10] : Domestic Payment Consents with Local Instrument as : UK.OBIE.SEPACreditTransfer");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		paymentConsent.setLocalInstrument("UK.OBIE.SEPACreditTransfer");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Unsupported.LocalInstrument", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].Message")),"invalid local instrument provided",
				"Error message is correct");			
		TestLogger.logBlankLine();
			
		TestLogger.logStep("[Step 11] : Domestic Payment Consents with Local Instrument as : UK.OBIE.Link");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		paymentConsent.setLocalInstrument("UK.OBIE.Link");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(paymentConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Link"),
					"LocalInstrument field value is as per OB defined values");		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 12] : Domestic Payment Consents with Local Instrument as : UK.OBIE.Target2");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		paymentConsent.setLocalInstrument("UK.OBIE.Target2");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Unsupported.LocalInstrument", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].Message")),"invalid local instrument provided",
				"Error message is correct");			
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 13] : Domestic Payment Consents with Local Instrument as : UK.OBIE.SEPAInstantCreditTransfer");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		paymentConsent.setLocalInstrument("UK.OBIE.SEPAInstantCreditTransfer");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Unsupported.LocalInstrument", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].Message")),"invalid local instrument provided",
				"Error message is correct");		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}