package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/CreditorPostalAddress block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPC_215 extends TestBase {	
	
	@Test
	public void m_PISP_DPC_215() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Data/Initiation/CreditorPostalAddress block where Request has sent successfully and returned a HTTP Code 201 Created");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress")!=null,
				"Optional Block CreditorPostalAddress is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressType")).isEmpty(), 
				"Optional field AddressType is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.Department")).isEmpty(), 
				"Optional field Department is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.SubDepartment")).isEmpty(), 
				"Optional field SubDepartment is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.StreetName")).isEmpty(),
				"Optional field StreetName is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.BuildingNumber")).isEmpty(),
				"Optional field BuildingNumber is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.PostCode")).isEmpty(), 
				"Optional field PostCode is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.TownName")).isEmpty(), 
				"Optional field TownName is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.Country")).isEmpty(),
				"Optional field Country is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.CountrySubDivision")).isEmpty(),
				"Optional field CountrySubDivision is present under CreditorPostalAddress");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Initiation.CreditorPostalAddress.AddressLine[0]")).isEmpty(), 
				"Optional field AddressLine is present under CreditorPostalAddress");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
