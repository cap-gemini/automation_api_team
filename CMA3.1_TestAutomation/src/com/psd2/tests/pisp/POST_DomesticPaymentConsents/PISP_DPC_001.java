package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;

/**
 * Class Description : Verification of CMA compliance for POST Domestic Payment Consents URI 
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class PISP_DPC_001 extends TestBase {	
	
	@Test
	public void m_PISP_DPC_001() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
        
	    TestLogger.logStep("[Step 1-2] : Verification of CMA compliance for POST Domestic Payment Consents URI");
	    paymentConsent.setBaseURL(apiConst.dpc_endpoint);
	    paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
	    paymentConsent.submit();
	    testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
	    		"Response Code is correct for Post Domestic Payment Consent");
	    testVP.verifyStringEquals(paymentConsent.getURL(),apiConst.dpc_endpoint, 
	    		"POST Domestic Payment Consents URI is as per CMA Compliance");
	    
	    testVP.verifyTrue(SignatureUtility.verifySignature(paymentConsent.getResponseString(), paymentConsent.getResponseHeader("x-jws-signature")), 
				"Response that created successfully with HTTP Code 201 MUST be digitally signed");
	    
	    TestLogger.logBlankLine();
        
		testVP.testResultFinalize();		
	}
}
