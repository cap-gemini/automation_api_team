package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY Initiation block where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPC_198 extends TestBase {	
	
	@Test
	public void m_PISP_DPC_198() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Initiation block where Request has sent successfully and returned a HTTP Code 201 Created");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(paymentConsent.getResponseValueByPath("Data.Initiation")!=null, 
				"Mandatory block Initiation is present and is not null");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Initiation.InstructionIdentification")).isEmpty(), 
				"Mandatory field InstructionIdentification is present under Initiation");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Initiation.LocalInstrument")).isEmpty(), 
				"Mandatory field LocalInstrument is present under Initiation block");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Data.Initiation.EndToEndIdentification")).isEmpty(), 
				"Mandatory field EndToEndIdentification is present under Initiation block");
		
		testVP.verifyTrue(paymentConsent.getResponseValueByPath("Data.Initiation.InstructedAmount")!=null, 
				"Mandatory field InstructedAmount is present under Initiation block");
		
		testVP.verifyTrue(paymentConsent.getResponseValueByPath("Data.Initiation.CreditorAccount")!=null, 
				"Mandatory field CreditorAccount is present under Initiation block");
		
		testVP.verifyTrue(paymentConsent.getResponseValueByPath("Data.Initiation.DebtorAccount")!=null, 
				"Optional field DebtorAccount is present under Initiation block");
		
		testVP.verifyTrue(paymentConsent.getResponseValueByPath("Data.Initiation.CreditorPostalAddress")!=null, 
				"Optional field CreditorPostalAddress is present under Initiation block");
		
		testVP.verifyTrue(paymentConsent.getResponseValueByPath("Data.Initiation.RemittanceInformation")!=null, 
				"Optional field RemittanceInformation is present under Initiation block");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
