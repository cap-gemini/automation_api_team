package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of date and time for CompletionDateTime field without giving seconds
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPC_249 extends TestBase {	
	
	@Test
	public void m_PISP_DPC_249() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of date and time for CompletionDateTime field without giving seconds");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		paymentConsent.setCompletionDateTime("2021-03-20T06:06");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Payment Consent URI when CompletionDateTime field value is without seconds");
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].Message")),"Error validating JSON. Error: - Provided value 2021-03-20T06:06 is not compliant with the format datetime provided in rfc3339 for CompletionDateTime",
				"Error message is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of date and time for CompletionDateTime field without giving seconds");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		paymentConsent.setCompletionDateTime("2021-03-20T06:06Z");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Payment Consent URI when CompletionDateTime field value is without seconds");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of date and time for CompletionDateTime field without giving seconds");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+", x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(4));
		paymentConsent.setCompletionDateTime("2021-03-20T06:06+00:00");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Payment Consent URI when CompletionDateTime field value is without seconds");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}