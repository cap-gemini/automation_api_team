package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL Risk/DeliveryAddress block haven't sent
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPC_160 extends TestBase {	
	
	@Test
	public void m_PISP_DPC_160() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Risk/DeliveryAddress block haven't sent");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		String requestBody=paymentConsent.genRequestBody().replace(",\"DeliveryAddress\": {"
									+ "\"AddressLine\":[\"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ\",\"12345678\"],"					
									+ "\"StreetName\": \"ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ12345678\","
									+ "\"BuildingNumber\": \"ABCDEF1234567890\","
									+ "\"PostCode\": \"ABCDEF 123456789\","
									+ "\"TownName\": \"ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789\","
									//+ "\"CountrySubDivision\":[ \"test\"],"
									+ "\"Country\": \"GB\"}", "");
		paymentConsent.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(paymentConsent.getResponseValueByPath("Risk.DeliveryAddress")==null, 
				"DeliveryAddress is not present");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}