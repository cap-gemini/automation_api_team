package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through POST method with mandatory and optional fields.
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPC_004 extends TestBase {	
	
	@Test
	public void m_PISP_PSU_004() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Checking the request (UTF-8 character encoded) status through POST method with mandatory and optional fields");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(paymentConsent.getResponseValueByPath("Data")!=null,
				"Mandatory field Data is present in response and is not empty");
		
		testVP.verifyTrue(paymentConsent.getResponseValueByPath("Links")!=null, 
				"Mandatory field Links is present in response and is not empty");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Links.Self")).isEmpty(), 
				"Mandatory field Self is present in response and is not empty");
		
		testVP.verifyStringEquals(paymentConsent.getResponseHeader("Content-Type"), "application/json;charset=UTF-8", 
				"Response is UTF-8 character encoded");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
