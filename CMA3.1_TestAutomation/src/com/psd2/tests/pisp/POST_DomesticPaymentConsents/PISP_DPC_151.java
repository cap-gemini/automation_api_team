package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into MANDATORY Risk block
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPC_151 extends TestBase {	
	
	@Test
	public void m_PISP_DPC_151() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY Risk block");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Risk.PaymentContextCode")).isEmpty(), 
				"PaymentContextCode field is present under Risk");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Risk.MerchantCategoryCode")).isEmpty(), 
				"MerchantCategoryCode field is present under Risk");
		
		testVP.verifyTrue(!(paymentConsent.getResponseNodeStringByPath("Risk.MerchantCustomerIdentification")).isEmpty(),  
				"MerchantCustomerIdentification field is present under Risk");
		
		testVP.verifyTrue(paymentConsent.getResponseNodeStringByPath("Risk.DeliveryAddress")!=null,  
				"DeliveryAddress field is present under Risk");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
