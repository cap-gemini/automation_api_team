package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of domestic-payment-consents URL that is NOT as per Open Banking standards as specified format
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPC_002 extends TestBase {	
	
	@Test
	public void m_PISP_DPC_002() throws Throwable{	
		
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
        
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);
        TestLogger.logBlankLine();
        
	    TestLogger.logStep("[Step 1-2] : Verification of domestic-payment-consents URL that is NOT as per Open Banking standards as specified format");
	    paymentConsent.setBaseURL(apiConst.invalid_paymentconsent_endpoint);
	    paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);      
	    paymentConsent.submit();
	    testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"404", 
	    		"Response Code is correct when Post Domestic Payment Consent URL is NOT as per Open Banking standards");
	    TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

