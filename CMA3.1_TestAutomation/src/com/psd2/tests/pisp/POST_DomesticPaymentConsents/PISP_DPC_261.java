package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the values into OPTIONAL SecondaryIdentification field where Request has sent successfully and returned a HTTP Code 201 Created if SecondaryIdentification starts with 333
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPC_261 extends TestBase {	
	
	@Test
	public void m_PISP_DPC_261() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL SecondaryIdentification field where Request has sent successfully and returned a HTTP Code 201 Created if SecondaryIdentification starts with 333");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		paymentConsent.setCrAccountSrIdentification("333");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Payment Consent URI");
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseValueByPath("Errors[0].ErrorCode")), "UK.OBIE.Unsupported.AccountSecondaryIdentifier", 
				"Error code is correct when CreditorAccount/SecondaryIdentification field value is 333");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
