package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request with different payload with same x-idempotency-key where previously a request has been set-up successfully  by same TPP within 24 hrs
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPC_011 extends TestBase {	
	
	@Test
	public void m_PISP_DPC_011() throws Throwable{	
		String idemPotencyKey=PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3);
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request with x-idempotency-key value as "+idemPotencyKey+" in the header");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey);
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent when x-idempotency-key value in header is "+idemPotencyKey+"");
		
		String consentId=paymentConsent.getConsentId();
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of request with same x-idempotency-key i.e. "+idemPotencyKey+" and same payload");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token+",x-idempotency-key:"+idemPotencyKey);
		paymentConsent.submit();
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"201", 
				"Response Code is correct for Domestic Payment Consent when x-idempotency-key value is same i.e. "+idemPotencyKey+" and payload is same");

		testVP.assertStringEquals(paymentConsent.getResponseNodeStringByPath("Data.ConsentId"), consentId, 
				"ConsentId is same as the request sent previously");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
