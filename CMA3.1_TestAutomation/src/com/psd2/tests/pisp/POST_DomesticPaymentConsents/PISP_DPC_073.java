package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of Identification field under DebtorAccount/Identification having length variation
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPC_073 extends TestBase {	
	
	@Test
	public void m_PISP_DPC_073() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of Identification field under DebtorAccount/Identification having length variation");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		paymentConsent.setDrAccountIdentification("ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567891241244JO94CBJO001000000000013100030213571263581263912749udhkJO94CBJO001000000000013100030213571263581263912749udhkJO94CBJO001000000000013100030213571263581263912749udhkJO94CBJO001000000000013100030213571263581263912749udhk");
		paymentConsent.submit();
		

		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for Domestic Payment Consent URI when DebtorAccount Identification is more than 35 characters");
		
		testVP.verifyStringEquals(paymentConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Invalid", 
				"Error code for the response is correct i.e. '"+paymentConsent.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(paymentConsent.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Expected max length 256 for field [Identification], but got [ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567891241244JO94CBJO001000000000013100030213571263581263912749udhkJO94CBJO001000000000013100030213571263581263912749udhkJO94CBJO001000000000013100030213571263581263912749udhkJO94CBJO001000000000013100030213571263581263912749udhk]"), 
				"Message for error code is '"+paymentConsent.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

