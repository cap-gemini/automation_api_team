package com.psd2.tests.pisp.POST_DomesticPaymentConsents;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of the response when MANDATORY Data/Initiation/InstructedAmount/Amount field value is 800
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_DPC_256 extends TestBase {	
	
	@Test
	public void m_PISP_DPC_256() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the response when MANDATORY Data/Initiation/InstructedAmount/Amount field value is 800");
		
		paymentConsent.setBaseURL(apiConst.dpc_endpoint);
		paymentConsent.setHeadersString("Authorization:Bearer "+cc_token);
		paymentConsent.setAmount("800.00");
		paymentConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(paymentConsent.getResponseStatusCode()),"500", 
				"Response Code is correct for Domestic Payment Consent URI when amount set to 800");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
