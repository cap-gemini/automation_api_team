package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the MANDATORY DebtorAccount/SchemeName field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_059 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_059() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY DebtorAccount/SchemeName field");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();

		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for File Payment URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName").isEmpty()), 
				"Mandatory field SchemeName is present and is not null");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.SortCodeAccountNumber")
				|| filePayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.IBAN")
				|| filePayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.PAN")
				|| filePayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.Paym")
				|| filePayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.BBAN")
				|| filePayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme1")
				|| filePayment.getResponseValueByPath("Data.Initiation.DebtorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme2"), 
				"MANDATORY DebtorAccount/SchemeName field has OB defined values");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

