package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of response for the API when ControlSum starts with 200
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_124 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_124() throws Throwable{	
		filePayment.setControlSum("200.00");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of response for the API when ControlSum starts with 200");	
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for POST File Payment URI");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Data.Status"),"InitiationFailed", 
				"Status field value is correct when controlsum starts with 200");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the values into Status field of consent API where Request has sent successfully and returned a HTTP Code 201 Created");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentDetails.get("consentId"));
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		filePayment.setMethod("GET");
		filePayment.submit();
			
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"200", 
				"Response Code is correct for File Payment URL");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Data.Status").equals("Consumed"),
				"Status field value is correct i.e. "+filePayment.getResponseNodeStringByPath("Data.Status"));
			
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}