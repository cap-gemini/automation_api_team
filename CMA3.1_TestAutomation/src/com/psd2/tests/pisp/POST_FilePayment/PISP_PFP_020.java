package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without x-fapi-financial-id header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_020 extends TestBase {
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_020() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request without MANDATORY x-fapi-financial-id header");
		restRequest.setURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setConsentId(consentDetails.get("consentId"));
		String requestBody=filePayment.genRequestBody();
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", Content-Type:application/json, x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-fapi-interaction-id:"+PropertyUtils.getProperty("inter_id")+", Accept:application/json, x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for File Payment URI without x-fapi-financial-id header");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
