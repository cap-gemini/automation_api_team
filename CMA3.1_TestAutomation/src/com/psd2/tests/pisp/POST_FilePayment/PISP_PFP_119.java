package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into OPTIONAL MultiAuthorisation/LastUpdateDateTime where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_119 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_119() throws Throwable{
		filePayment.setControlSum("300.00");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 7] : Verification of the values into OPTIONAL MultiAuthorisation/LastUpdateDateTime where Request has sent successfully and returned a HTTP Code 201 Created");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
	    testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for Post File Payment URI");
		
	    testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.LastUpdateDateTime")).isEmpty(),
	    		"LastUpdateDateTime under MultiAuthorisation block is present and is not null");
	    
	    testVP.verifyTrue(Misc.verifyDateTimeFormat(filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.LastUpdateDateTime").split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.LastUpdateDateTime").split("T")[1], "HH:mm:ss+00:00"), 
				"LastUpdateDateTime under MultiAuthorisation block is as per expected format");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}