package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of x-fapi-interaction-id value when NOT sent in the request 
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_022 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_022() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
        
	    TestLogger.logStep("[Step 2] : Verification of x-fapi-interaction-id value when NOT sent in the request");
	    restRequest.setURL(apiConst.fPaymentSubmission_endpoint);
	    filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
	    filePayment.setConsentId(consentDetails.get("consentId"));
	    String requestBody=filePayment.genRequestBody();
	    restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", Content-Type:application/json, x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json, x-idempotency-key:"+PropertyUtils.getProperty("idempotency_key")+"."+Misc.generateString(3));
		restRequest.setRequestBody(requestBody);
		restRequest.setMethod("POST");
		restRequest.submit();
		
	    testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"201", 
	    		"Response Code is correct for Post File Payment URI");
	    
	    testVP.verifyTrue(!(restRequest.getResponseHeader("x-fapi-interaction-id")).isEmpty(), 
	    		"x-fapi-interaction-id is present in the response");
	    
	    TestLogger.logBlankLine();
	    
	    TestLogger.logStep("[Step 3] : Verification of the request with Invalid value of x-fapi-interaction-id header");
		
	    filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.addHeaderEntry("x-fapi-interaction-id", "1234");
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"400", 
				"Response Code is correct for File Payment URI when invalid value is used for x-fapi-interaction-id header");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Errors[0].Message")).isEmpty(), 
				"Message for error code is '"+filePayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of the request with null value of x-fapi-interaction-id header");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+", x-idempotency-key:"+restRequest.getHeaderEntry("x-idempotency-key"));
		filePayment.addHeaderEntry("x-fapi-interaction-id", "");
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for File Payment URI when null value is used for x-fapi-interaction-id header");
		
		TestLogger.logBlankLine();
        
		testVP.testResultFinalize();		
	}
}
