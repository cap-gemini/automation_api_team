package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL Data/Initiation/DebtorAccount block
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_107 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_107() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into OPTIONAL Data/Initiation/DebtorAccount block");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		

		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for File Payment URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName")).isEmpty(), 
				"Mandatory field SchemeName under DebtorAccount is present");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification")).isEmpty(), 
				"Mandatory field Identification under DebtorAccount is present");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Name")).isEmpty(), 
				"Optional field Name under DebtorAccount is present");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SecondaryIdentification")).isEmpty(), 
				"Optional field SecondaryIdentification under DebtorAccount is present");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

