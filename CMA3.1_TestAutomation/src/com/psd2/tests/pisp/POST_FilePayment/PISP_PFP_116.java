package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY MultiAuthorisation/Status where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_116 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_116() throws Throwable{	
		filePayment.setControlSum("300.00");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY MultiAuthorisation/Status where Request has sent successfully and returned a HTTP Code 201 Created");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
	    testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for Post File Payment URI");
		
	    testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.Status").equals("AwaitingFurtherAuthorisation")
	    		|| filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.Status").equals("Authorised") 
	    		|| filePayment.getResponseNodeStringByPath("Data.MultiAuthorisation.Status").equals("Rejected"),
	    		"Status field under MultiAuthorisation block is present and value of field is correct");
	    
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}