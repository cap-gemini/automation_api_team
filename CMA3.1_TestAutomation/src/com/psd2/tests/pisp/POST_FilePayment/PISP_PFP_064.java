package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of MANDATORY DebtorAccount/Identification haven't sent
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_064 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_064() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of MANDATORY DebtorAccount/Identification haven't sent");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		String requestBody=filePayment.genRequestBody().replace("\"Identification\": \""+filePayment._drAccountIdentification+"\",","");
		filePayment.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"400", 
				"Response Code is correct for File Payment URI when DebtorAccount Identification is not sent");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Field.Missing", 
				"Error code for the response is correct i.e. '"+filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Errors[0].Message").equals("Error validating JSON. Error: - Missing required field [Identification]"), 
				"Message for error code is '"+filePayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

