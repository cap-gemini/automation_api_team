package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/RequestedExecutionDateTime field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_055 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_055() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values of OPTIONAL Initiation/RequestedExecutionDateTime field");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for File Payment URI");
		
		testVP.verifyTrue(Misc.verifyDateTimeFormat(filePayment.getResponseNodeStringByPath("Data.Initiation.RequestedExecutionDateTime").split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(filePayment.getResponseNodeStringByPath("Data.Initiation.RequestedExecutionDateTime").split("T")[1], "HH:mm:ss+00:00"), 
				"RequestedExecutionDateTime is as per expected ISO Standard format");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
