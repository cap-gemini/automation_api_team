package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request without Name field under DebtorAccount
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class PISP_PFP_143 extends TestBase {	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_143() throws Throwable{	
		TestLogger.logStep("[Step 1-1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200","Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		API_Constant.setPisp_CC_AccessToken(cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-2] : File Payment Consent SetUp....");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		String requestBody=filePayment.genRequestBody().replace("\"Name\": \""+filePayment._drAccountName+"\",", "");
		filePayment.submit(requestBody);
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", "Response Code is correct for File Payment Consent");
		consentId = filePayment.getConsentId();
		TestLogger.logVariable("Consent Id : " + consentId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-3] : Upload file");	
		uploadFile.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentId+"/file");
		uploadFile.setHeadersString("Authorization:Bearer "+cc_token);
		uploadFile.setFileType(filePayment.getFileType());
		uploadFile.setIdempotencyKey(filePayment.getHeaderEntry("x-idempotency-key"));
		uploadFile.submit();
		
		testVP.assertStringEquals(String.valueOf(uploadFile.getResponseStatusCode()),"200", 
				"Response Code is correct for POST File Payment Consent file upload");	

        TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-4] : JWT Token Creation........");
		
		//reqObject.setBaseURL(apiConst.ro_endpoint);
		reqObject.setValueField(consentId);
		reqObject.setScopeField("payments");
		outId = reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-5] : Go to URL and authenticate consent");	
		redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		authCode = consentOps.authorisePISPConsent(redirecturl+"##"+filePayment._drAccountIdentification,filePayment.addDebtorAccount);		
		closeDriverInstance();
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 1-6] : Get access token");	
		accesstoken.setBaseURL(apiConst.at_endpoint);
		accesstoken.setAuthCode(authCode);
		accesstoken.submit();
		
		testVP.verifyStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
				"Response Code is correct for get access token request");	
		access_token = accesstoken.getAccessToken();
		TestLogger.logVariable("Access Token : " + access_token);		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-7] : File Payment Submission request wihout Name field");	
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+access_token);
		filePayment.setConsentId(consentId);
		String requestBody_Submission=filePayment.genRequestBody().replace("\"Name\": \""+filePayment._drAccountName+"\",", "");
		filePayment.submit(requestBody_Submission);
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201",
				"Response Code is correct for File Payment Submission when request is sent without Name field");
		
		
		TestLogger.logBlankLine();		
		testVP.testResultFinalize();		
	}
}