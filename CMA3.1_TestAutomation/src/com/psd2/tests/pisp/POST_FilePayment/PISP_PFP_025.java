package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with BLANK or Invalid value of OPTIONAL x-fapi-customer-ip-address header
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_025 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_025() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the request with BLANK value of OPTIONAL x-fapi-customer-ip-address header");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.addHeaderEntry("x-fapi-customer-ip-address", "");
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"400", 
				"Response Code is correct for File Payment URI when null value is used for x-fapi-customer-ip-address header");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Errors[0].Message").equals("Invalid value found in x-fapi-customer-ip-address header"), 
				"Message for error code is '"+filePayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the request with invalid value of OPTIONAL x-fapi-customer-ip-address header");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.addHeaderEntry("x-fapi-customer-ip-address", "abcd");
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"400", 
				"Response Code is correct for File Payment URI when invalid value is used for x-fapi-customer-ip-address header");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
				"Error code for the response is correct i.e. '"+filePayment.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Errors[0].Message").equals("Invalid value found in x-fapi-customer-ip-address header"), 
				"Message for error code is '"+filePayment.getResponseNodeStringByPath("Errors[0].Message")+"'");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
