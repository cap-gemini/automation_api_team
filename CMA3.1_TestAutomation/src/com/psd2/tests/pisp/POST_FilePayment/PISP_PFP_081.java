package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY FilePaymentId field where Request has sent successfully and returned a HTTP Code 201 Created
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_081 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_081() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values into MANDATORY FilePaymentId field where Request has sent successfully and returned a HTTP Code 201 Created");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for File Payment URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.FilePaymentId")).isEmpty(), 
				"FilePaymentId field is present under Data block");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Data.FilePaymentId").length()<=40, 
				"FilePaymentId length is correct i.e. less than or equal to 40 chracters");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
