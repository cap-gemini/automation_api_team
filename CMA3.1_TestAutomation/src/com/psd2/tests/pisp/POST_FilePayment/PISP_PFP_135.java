package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of text on SCA page when Invalid payer account is provided
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_135 extends TestBase {	
	
	@Test
	public void m_PISP_PFP_135() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Generate consent id");
		
		filePayment.setBaseURL(apiConst.fPaymentConsent_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+cc_token);
		filePayment.setDrAccountIdentification("IE86BOFI90575812345678");
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for File Payment Consent URI");
		
		consentId = filePayment.getConsentId();
		TestLogger.logVariable("Consent Id : " + consentId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Upload file");	
		uploadFile.setBaseURL(apiConst.fPaymentConsent_endpoint+"/"+consentId+"/file");
		uploadFile.setHeadersString("Authorization:Bearer "+cc_token);
		uploadFile.setFileType(filePayment.getFileType());
		uploadFile.setIdempotencyKey(filePayment.getHeaderEntry("x-idempotency-key"));
		uploadFile.submit();
		
		testVP.verifyStringEquals(String.valueOf(uploadFile.getResponseStatusCode()),"200", 
				"Response Code is correct for POST Upload File URI");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : JWT Token Creation........");
		
		//reqObject.setBaseURL(apiConst.ro_endpoint);
		reqObject.setValueField(consentId);
		reqObject.setScopeField("payments");
		outId = reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 5] : Verification of text on SCA page when Invalid payer account is provided");	
		redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		
		String errorMessage = consentOps.invalidPayerAccont(redirecturl);
		closeDriverInstance();
		TestLogger.logBlankLine();
		
		testVP.verifyStringEquals(errorMessage, "Invalid payer account provided. Request cannot be processed.", 
				"Error message is correct when invalid payer account is provided");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}