package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of response for the API when ControlSum starts with 111
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_128 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_128() throws Throwable{	
		filePayment.setControlSum("111.00");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of response for the API when ControlSum starts with 111");	
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for POST File Payment URI");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Data.Charges[0].ChargeBearer"),"BorneByCreditor", 
				"ChargeBearer field value is correct when controlsum starts with 111");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}