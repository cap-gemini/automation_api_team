package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of file-payments URL that is NOT as per Open Banking standards
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_002 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_002() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
        
	    TestLogger.logStep("[Step 1-2] : Verification of file-payments URL that is NOT as per Open Banking standards");
	    filePayment.setBaseURL(apiConst.invalid_payment_endpoint);
	    filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
	    filePayment.setConsentId(consentDetails.get("consentId"));
	    filePayment.submit();
	    testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"404", 
	    		"Response Code is correct when Post File Payment URL is NOT as per Open Banking standards");
	    TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

