package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through POST method with mandatory and optional fields.
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_003 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_003() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Checking the request (UTF-8 character encoded) status through POST method with mandatory and optional fields");
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for Post File Payment URI");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Data")!=null,
				"Mandatory field Data is present in response and is not empty");
		
		testVP.verifyTrue(filePayment.getResponseValueByPath("Links")!=null, 
				"Mandatory field Links is present in response and is not empty");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Links.Self")).isEmpty(), 
				"Mandatory field Self is present in response and is not empty");
		
		testVP.verifyStringEquals(filePayment.getResponseHeader("Content-Type"), "application/json;charset=UTF-8", 
				"Response is UTF-8 character encoded");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
