package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of response for the POST File Payment API With ChargeAmount=10.33%, ChargeCurrency=GBP
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_132 extends TestBase {
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_109() throws Throwable{	
		filePayment.setControlSum("444.00");
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of response for the API when ControlSum starts with 444");	
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for POST File Payment URI");
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseValueByPath("Data.Charges.Amount[0].Amount")).substring(0, 3),
				String.valueOf(Misc.calculatePercentage(1D,Double.parseDouble(filePayment._controlSum))).substring(0, 3),
				"ChargeAmount is correct i.e. 10.33% of controlsum value");
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseValueByPath("Data.Charges.Amount[0].Currency")), "GBP",
				"ChargeCurrency is correct i.e. GBP");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}