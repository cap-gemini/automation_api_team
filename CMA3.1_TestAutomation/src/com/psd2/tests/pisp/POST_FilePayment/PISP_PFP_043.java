package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values of OPTIONAL Initiation/LocalInstrument field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class PISP_PFP_043 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_043() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate access token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the values of OPTIONAL Initiation/LocalInstrument field");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for File Payment URI");
		
		testVP.verifyTrue(!(filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").isEmpty()),
				"LocalInstrument is present and is not null");
		
		testVP.verifyTrue(filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BACS")
				|| filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.CHAPS")
				|| filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.FPS")
				|| filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.SWIFT")
				|| filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.BalanceTransfer")
				|| filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.MoneyTransfer")
				|| filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Paym")
				|| filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Euro1")
				|| filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.SEPACreditTransfer")
				|| filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.SEPAInstantCreditTransfer")
				|| filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Link")
				|| filePayment.getResponseNodeStringByPath("Data.Initiation.LocalInstrument").equals("UK.OBIE.Target2"), 
				"LocalInstrument field value is correct");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
