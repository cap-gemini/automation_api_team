package com.psd2.tests.pisp.POST_FilePayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = UK.OBIE.IBAN/IBAN
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Blocked"})
public class PISP_PFP_137 extends TestBase {	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_PFP_137() throws Throwable{	
		filePayment.setDrAccountSchemeName(PropertyUtils.getProperty("DrAccount_SchemeName"));
		filePayment.setDrAccountIdentification(PropertyUtils.getProperty("DrAccount_Identification"));
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = UK.OBIE.IBAN");
		
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for POST File Payment Submission URI");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName"), "UK.OBIE.IBAN", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification"), "IE85BOFI90120412345679", 
				"Identification field value is correct");
		TestLogger.logBlankLine();
		
		filePayment.setTestData();
		filePayment.setDrAccountSchemeName("IBAN");
		filePayment.setDrAccountIdentification(PropertyUtils.getProperty("DrAccount_Identification"));
		
		TestLogger.logStep("[Step 1] : Generate Access Token");
		consentDetails=apiUtility.generatePayments(false, apiConst.filePayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of the value of MANDATORY DebtorAccount/Identification field when SchemeName = IBAN");
		filePayment.setBaseURL(apiConst.fPaymentSubmission_endpoint);
		filePayment.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		filePayment.setConsentId(consentDetails.get("consentId"));
		filePayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(filePayment.getResponseStatusCode()),"201", 
				"Response Code is correct for POST File Payment Submission URI");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.SchemeName"), "UK.OBIE.IBAN", 
				"SchemeName field value is correct");
		
		testVP.verifyStringEquals(filePayment.getResponseNodeStringByPath("Data.Initiation.DebtorAccount.Identification"), "IE85BOFI90120412345679", 
				"Identification field value is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}