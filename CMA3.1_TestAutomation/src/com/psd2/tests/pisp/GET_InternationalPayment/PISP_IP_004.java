package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status
 * through GET method with mandatory and optional fields
 * 
 * @author : Rama Arora
 *
 */

@Listeners({ TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_004 extends TestBase {
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IP_004() throws Throwable {
	TestLogger.logStep("[Step 1-1] : Creating client credetials....");
    createClientCred.setBaseURL(apiConst.cc_endpoint);
    createClientCred.setScope("payments");
    createClientCred.submit();
        
    testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
        		"Response Code is correct for client credetials");
    cc_token = createClientCred.getAccessToken();
    TestLogger.logVariable("AccessToken : " + cc_token);
    TestLogger.logBlankLine();
	
	TestLogger.logStep("[Step 2] :  Get International Payment Submission");
	internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId());
	internationalPayment.setHeadersString("Authorization:Bearer "+ cc_token);
	internationalPayment.setMethod("GET");
	internationalPayment.submit();
		
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()), "200",
	    "Response Code is correct for International Payment Submission URI");
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data")) != null,
		"Mandatory field Data is present in response and is not empty");
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation")) != null,
		"Mandatory field Initiation is present in response and is not empty");
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.MultiAuthorisation")) != null,
		"Mandatory field Risk is not present in response");
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Links")) != null,
		"Mandatory field Links is present in response and is not empty");
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Links.Self"))!= null,
		"Mandatory field Self is present in response and is not empty");
	testVP.verifyStringEquals(internationalPayment.getResponseHeader("Content-Type"), "application/json;charset=utf-8", 
		"Response is UTF-8 character encoded");
	TestLogger.logBlankLine();
	testVP.testResultFinalize();
	}
}
