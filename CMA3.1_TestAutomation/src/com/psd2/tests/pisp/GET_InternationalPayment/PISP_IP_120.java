package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of response for the payments API when the Amount starts with "700" in the POST request payload
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_120 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IP_120() throws Throwable{
		internationalPayment.setAmount("700.00");
		TestLogger.logStep("[Step 1] : Generate Payment Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, false, true);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Get Domestic Schedule Payment ");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"500", 
				"Response Code is correct for POST International Scheduled Payment Submission");
		
	    TestLogger.logBlankLine();
	    testVP.testResultFinalize();		
	}
}	