package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of International Payment Submission URI that should be as per Open Banking Standard 
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity","IP"})
public class PISP_IP_001 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IP_001() throws Throwable{	
    TestLogger.logStep("[Step 1] : Generate Payment Id");
	consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, false, true);
	API_Constant.setIp_PaymentId(consentDetails.get("paymentId"));
	TestLogger.logBlankLine();	
	
	TestLogger.logStep("[Step 2] : Get International Payment Submission");
	internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"));
	internationalPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
	internationalPayment.setMethod("GET");
	internationalPayment.submit();
	
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
	"Response Code is correct for Post International Payment Submission");
	testVP.verifyStringEquals(internationalPayment.getURL(),apiConst.iPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"),
	"POST International Payment Submission URI is as per CMA Compliance");
	
	testVP.verifyTrue(SignatureUtility.verifySignature(internationalPayment.getResponseString(), internationalPayment.getResponseHeader("x-jws-signature")), 
			"Response that created successfully with HTTP Code 201 MUST be digitally signed");
	
	TestLogger.logBlankLine();	
	testVP.testResultFinalize();
	
	}
}