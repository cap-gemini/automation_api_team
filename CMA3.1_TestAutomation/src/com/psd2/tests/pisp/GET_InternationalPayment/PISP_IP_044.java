package com.psd2.tests.pisp.GET_InternationalPayment;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY ExchangeRate field where Request has sent successfully 
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_044 extends TestBase{ 
API_E2E_Utility apiUtility = new API_E2E_Utility();
    @Test
    public void m_PISP_IP_044() throws Throwable{ 
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	        
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();
	
    TestLogger.logStep("[Step 2] : Get International Payment Submission");
    internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId());
	internationalPayment.setHeadersString("Authorization:Bearer "+ cc_token);
	internationalPayment.setMethod("GET");
	internationalPayment.submit(); 
    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
    "Response Code is correct for GET International Payment Submission");
    testVP.verifyStringEquals(internationalPayment.getURL(),apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId(), 
    "URI for GET International Schedule payment consent request is as per open banking standard");
    testVP.verifyTrue((String.valueOf(internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExchangeRate")))!=null,
    "ExchangeRate field under ExchangeRateInformation block is present in Get International Schedule Payment Consent response body"+(String.valueOf(internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExchangeRate"))));
    testVP.verifyEquals(String.valueOf(String.valueOf(internationalPayment.getResponseValueByPath(("Data.ExchangeRateInformation.ExchangeRate")))), internationalPayment._exchangeRate,
    "ExchangeRate has same value as sent in request");
    testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.ExchangeRateInformation.ExchangeRate")).matches("-?\\d+(\\.\\d+)?"),
    	    "ExchangeRate field value is in float numbers");
    TestLogger.logBlankLine();
    testVP.testResultFinalize();
}
}
