package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the values into OPTIONAL Data/Initiation/CreditorAgent/PostalAddress block
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_089 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IP_089() throws Throwable{	
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	        
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();
		
	TestLogger.logStep("[Step 2] : Get International Payment Submission");
	internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId());
	internationalPayment.setHeadersString("Authorization:Bearer "+ cc_token);
	internationalPayment.setMethod("GET");
	internationalPayment.submit();
	
    testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType"))!=null , 
	"AddressType Field is present "+internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressType"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Department"))!=null , 
	"Department field is present under PostalAddress block "+internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Department"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.SubDepartment"))!=null , 
	"SubDepartment field is present under PostalAddress block "+internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.SubDepartment"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.StreetName"))!=null , 
	"StreetName field is present under PostalAddress block "+internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.StreetName"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.BuildingNumber"))!=null , 
	"BuildingNumber field is present under PostalAddress block "+internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.BuildingNumber"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.PostCode"))!=null , 
	"PostCode field is present under PostalAddress block "+internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.PostCode"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.TownName"))!=null , 
	"TownName field is present under PostalAddress block "+internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.TownName"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.CountrySubDivision"))!=null , 
	"CountrySubDivision field is present under PostalAddress block "+internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.CountrySubDivision"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Country"))!=null , 
	"Country field is present under PostalAddress block "+internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.Country"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressLine"))!=null , 
	"AddressLine field is present under PostalAddress block "+internationalPayment.getResponseValueByPath("Data.Initiation.CreditorAgent.PostalAddress.AddressLine"));
	TestLogger.logBlankLine();
	testVP.testResultFinalize();

}
}	

