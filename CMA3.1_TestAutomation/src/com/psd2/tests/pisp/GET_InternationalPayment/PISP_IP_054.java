package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the values into Optional ChargeBearer Field Where request has Sent Successfully and returned a HTTP Code 201 Created
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_054 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_IP_054() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate Payment Id");
		consentDetails = apiUtility.generatePayments(false,apiConst.internationalPayments, false, true);
		TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Get International Payment Submission");
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+consentDetails.get("paymentId"));
		internationalPayment.setHeadersString("Authorization:Bearer "+ consentDetails.get("cc_access_token"));
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for GET International Payment Submission");
				
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.ChargeBearer")),internationalPayment._chargeBearer, 
						"Mandatory field ChargeBearer is present in Initiation block");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}