package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of response for the consent  API when the rate is provided  as "Indicative" in the request payload
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_116 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IP_116() throws Throwable{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
        createClientCred.setBaseURL(apiConst.cc_endpoint);
        createClientCred.setScope("payments");
        createClientCred.submit();
        testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", "Response Code is correct for client credentials");
        cc_token = createClientCred.getAccessToken();
        TestLogger.logVariable("AccessToken : " + cc_token);    
        TestLogger.logBlankLine();
        
        TestLogger.logStep("[Step 2] : Verification of response when the RateType is provided as \"Actual\" in the request payload");                          
        internationalPayment.setBaseURL(apiConst.iPaymentConsent_endpoint);
        internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
        internationalPayment.setRateType("Indicative");
        String requestBody=internationalPayment.genRequestBody().replace("\"ExchangeRate\": 1.2,","");
        requestBody = requestBody.replace(",\"ContractIdentification\": \"123identification\"","");
        internationalPayment.submit(requestBody);
       
		String consentId=internationalPayment.getConsentId();
		
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Consent URI");
		
		TestLogger.logStep("[Step 1-3] : JWT Token Creation........");
		
		reqObject.setValueField(consentId);
		reqObject.setScopeField("payments");
		outId = reqObject.submit();		
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 1-4] : Go to URL and authenticate consent");	
		redirecturl = apiConst.pispconsent_URL.replace("#token_RequestGeneration#", outId);
		startDriverInstance();
		authCode = consentOps.authorisePISPConsent(redirecturl+"##"+internationalPayment._drAccountIdentification,internationalPayment.removeDebtorAccount);	
		closeDriverInstance();
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 1-5] : Get access and refresh token");	
		accesstoken.setBaseURL(apiConst.at_endpoint);
		//accesstoken.setAuthentication(PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		accesstoken.setAuthCode(authCode);
		accesstoken.submit();
		
		testVP.verifyStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
				"Response Code is correct for get access token request");	
		access_token = accesstoken.getAccessToken();
		refresh_token = accesstoken.getRefreshToken();
		TestLogger.logVariable("Access Token : " + access_token);
		TestLogger.logVariable("Refresh Token : " + refresh_token);
		
		API_Constant.setPisp_AccessToken(access_token);		
		
		TestLogger.logStep("[Step 1-6] : Verification of response when the RateType is provided as \"Actual\" in the request payload");                          
        internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
        internationalPayment.setHeadersString("Authorization:Bearer "+access_token);
        internationalPayment.setConsentId(consentId);
        internationalPayment.setRateType("Indicative");
        requestBody = internationalPayment.genRequestBody().replace("\"ExchangeRate\": 1.2,","");
        requestBody = requestBody.replace(",\"ContractIdentification\": \"123identification\"","");
        internationalPayment.submit(requestBody);
		
        String paymentId = internationalPayment.getPaymentId();
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"201", 
				"Response Code is correct for International Scheduled Payment Submission URI ");	
		
		TestLogger.logStep("[Step 1-7] :Get International Payment Submission");
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+paymentId);
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
		"Response Code is correct for GET International Payment Submission");
		
	     testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.ExchangeRateInformation.RateType")),"Indicative", 
		"RateType field in response is same as that sent in request");
	     
	TestLogger.logBlankLine();
	testVP.testResultFinalize();		
		
}
}	

