package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the values into Mandatory Initiation Block Where request has Sent Successfully and retuened a HTTP Code 201 Created
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_048 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
    @Test
    public void m_PISP_IP_048() throws Throwable{ 
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	        
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine(); 
    
    TestLogger.logStep("[Step 3] :Get International Payment Submission");
	internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId());
	internationalPayment.setHeadersString("Authorization:Bearer "+ cc_token);
	internationalPayment.setMethod("GET");
	internationalPayment.submit();
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
	"Response Code is correct for GET International Payment Submission");
	
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.InstructionIdentification"))!=null, 
	   "Mandatory field InstructionIdentification is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.InstructionIdentification"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.EndToEndIdentification"))!=null, 
	    "Mandatory field EndToEndIdentification is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.EndToEndIdentification"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.LocalInstrument"))!=null,
	 "Mandatory field LocalInstrument is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.LocalInstrument"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.InstructionPriority"))!=null, 
	"Mandatory field InstructionPriority is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.InstructionPriority"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.Purpose"))!=null,
	"Optional field Purpose is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.Purpose"));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.CurrencyOfTransfer"))!=null,
	"Optional field CurrencyOfTransfer is present in Initiation block"+internationalPayment.getResponseValueByPath("Data.Initiation.CurrencyOfTransfer"));	
	TestLogger.logBlankLine();
	testVP.testResultFinalize();	
	
		
}
}	

