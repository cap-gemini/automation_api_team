package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of the values into MANDATORY StatusUpdateDateTime field where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_033 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
    public void m_PISP_IP_033() throws Throwable{	
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	        
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();	
    
    TestLogger.logStep("[Step 2] : Get International Payment Submission");
    internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId());
	internationalPayment.setHeadersString("Authorization:Bearer "+ cc_token);
	internationalPayment.setMethod("GET");
	internationalPayment.submit();
    testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
	"Response Code is correct for GET International Payment Submission");
	testVP.verifyTrue(internationalPayment.getResponseValueByPath("Data.StatusUpdateDateTime")!=null, 
	"Data field is present under StatusUpdateDateTime field");	 
	testVP.verifyTrue(Misc.verifyDateTimeFormat(internationalPayment.getResponseNodeStringByPath("Data.StatusUpdateDateTime").split("T")[0], "yyyy-MM-dd") && 
	(Misc.verifyDateTimeFormat(internationalPayment.getResponseNodeStringByPath("Data.StatusUpdateDateTime").split("T")[1], "HH:mm:ss+00:00")), 
	"StatusUpdateDateTime is as per expected format");				
	TestLogger.logBlankLine();
	testVP.testResultFinalize();
}
	}

