package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.signature.SignatureUtility;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the request without token value OR key-value pair into Authorization (Access Token) header
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_013 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IP_013() throws Throwable{			
	TestLogger.logStep("[Step 1-1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	        
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();
	
	TestLogger.logStep("[Step 2] : All the above steps would remains same where request is having Blank value of Authorization header");
	internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId());
	internationalPayment.setHeadersString("Authorization:Bearer " + " ");
	internationalPayment.setMethod("GET");
	internationalPayment.submit();		
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"401", 
	  "Response Code is correct for International Payment Submission URI when no token value or key value pair is given into Authorization (Access Token) header");
	
	TestLogger.logStep("[Step 3] : Verification of request without Authorization key in the header");
	restRequest.setURL(apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId());
    internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+ API_Constant.getIp_PaymentId());
    String requestBody=internationalPayment.genRequestBody();
    restRequest.setHeadersString("Content-Type:application/json, x-jws-signature:"+SignatureUtility.generateSignature(requestBody)+", x-fapi-interaction-id:"+PropertyUtils.getProperty("inter_id")+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json");
	restRequest.setRequestBody(requestBody);
	restRequest.setMethod("GET");
	restRequest.submit();
	
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"401", 
		"Response Code is correct for GET International Payment Submission URI when no token value or key value pair is given into Authorization (Access Token) header");
	TestLogger.logBlankLine();
	testVP.testResultFinalize();
}
}
