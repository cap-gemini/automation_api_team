package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the value of MANDATORY DebtorAccount/Identification  where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_066 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IP_066() throws Throwable{	
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	        
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();
	
    TestLogger.logStep("[Step 2] : Get International Payment Submission");
    internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId());
	internationalPayment.setHeadersString("Authorization:Bearer "+ cc_token);
	internationalPayment.setMethod("GET");
	internationalPayment.submit();
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
	"Response Code is correct for GET International Payment Submission");
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation.DebtorAccount.Identification")),internationalPayment._drAccountIdentification,
	"Mandatory field Identification is present in DebtorAccount block");
	TestLogger.logBlankLine();
	testVP.testResultFinalize();

}
}	

