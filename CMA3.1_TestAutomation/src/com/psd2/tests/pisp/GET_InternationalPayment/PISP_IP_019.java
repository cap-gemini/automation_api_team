package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of x-fapi-interaction-id value when NOT sent in the request with the response header that created successfully with HTTP Code 200 OK 
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_019 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IP_019() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		        
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
		        		"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Get International Schedule Payment Consent ");	
		restRequest.setURL(apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId());
		restRequest.setHeadersString("Authorization:Bearer "+cc_token+", x-fapi-financial-id:"+PropertyUtils.getProperty("fin_id")+", Accept:application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct if x-fapi-interaction-id value NOT sent in the request");
		testVP.verifyTrue(!(restRequest.getResponseHeader("x-fapi-interaction-id")).isEmpty(), 
				"x-fapi-interaction-id value is present in response header");
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();		
	}
}
