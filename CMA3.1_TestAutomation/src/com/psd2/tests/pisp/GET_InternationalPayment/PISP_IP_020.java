package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the request with BLANK or Invalid value of OPTIONAL x-fapi-customer-ip-address header
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_020 extends TestBase{
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_IP_020() throws Throwable{	
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	        
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();
	
    TestLogger.logStep("[Step 2] : All the above steps would remains same where request is having Invalid value of OPTIONAL x-fapi-customer-ip-address header");
	internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint + "/"+API_Constant.getIp_PaymentId());
	internationalPayment.setHeadersString("Authorization:Bearer "+ cc_token);
	internationalPayment.addHeaderEntry("x-fapi-customer-ip-address", "UUXGUISGC^%");
	internationalPayment.setMethod("GET");
	internationalPayment.submit();	
	
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"400", 
	"Response Code is correct for GET International Payment Submission URI when invalid x-fapi-customer-ip-address is passed");
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
	"Response Error Code is correct");
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Errors[0].Message")),
			"Invalid value found in x-fapi-customer-ip-address header","Error message is correct");	
	
	TestLogger.logStep("[Step 3] : All the above steps would remains same where request is having Blank value of OPTIONAL x-fapi-customer-ip-address header");
	internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint + "/"+API_Constant.getIp_PaymentId());
	internationalPayment.setHeadersString("Authorization:Bearer "+ cc_token);
	internationalPayment.addHeaderEntry("x-fapi-customer-ip-address", "");
	internationalPayment.setMethod("GET");
	internationalPayment.submit();
	
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()), "400", 
	"Response Code is correct for GET International Payment Submission URI when request is having Blank value of OPTIONAL x-fapi-customer-ip-address is passed");                                              
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
         "Response Error Code is Correct");
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseValueByPath("Errors[0].Message")), 
			"Invalid value found in x-fapi-customer-ip-address header", 
        "Error msg is correct");
	TestLogger.logBlankLine();
	testVP.testResultFinalize();

	}
}

