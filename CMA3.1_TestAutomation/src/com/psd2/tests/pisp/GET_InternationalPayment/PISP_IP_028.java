package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into MANDATORY Data block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_028 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IP_028() throws Throwable{	
	TestLogger.logStep("[Step 1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	        
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();
	
	TestLogger.logStep("[Step 2] : Get International Payment Submission");
	internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId());
	internationalPayment.setHeadersString("Authorization:Bearer "+ cc_token);
	internationalPayment.setMethod("GET");
	internationalPayment.submit();
	
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
	"Response Code is correct for GET International Payment Submission");
	testVP.verifyStringEquals(internationalPayment.getURL(),apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId(), 
	"URI for GET International Schedule payment consent request is as per open banking standard");
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.InternationalPaymentId"))!=null,
	"paymentId field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Data.paymentId")));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.CreationDateTime"))!=null,
	"CreationDateTime field under Data is present in Get International schedule Payment Consent response body"+(internationalPayment.getResponseValueByPath("Data.CreationDateTime")));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.StatusUpdateDateTime"))!=null,
	"StatusUpdateDateTime field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Data.StatusUpdateDateTime")));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Status"))!=null,
	"Status field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Data.Status")));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.CutOffDateTime"))==null,
	"CutOffDateTime field under Data is not present in Get Domestic Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Data.CutOffDateTime")));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExpectedExecutionDateTime"))!=null,
	"ExpectedExecutionDateTime field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Data.ExpectedExecutionDateTime")));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.ExpectedSettlementDateTime"))!=null,
	"ExpectedSettlementDateTime field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Data.ExpectedSettlementDateTime")));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Charges"))!=null,
	"Charges field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Data.Charges")));
	testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation"))!=null,
	"Initiation field under Data is present in Get International schedule Payment Consent response body"+String.valueOf(internationalPayment.getResponseValueByPath("Data.Initiation")));
	TestLogger.logBlankLine();
	testVP.testResultFinalize();
	
	}
}
