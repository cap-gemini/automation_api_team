package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of request when paymentId is not sent in the request
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_006 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IP_006() throws Throwable{	
	TestLogger.logStep("[Step 1-1] : Creating client credetials....");
	createClientCred.setBaseURL(apiConst.cc_endpoint);
	createClientCred.setScope("payments");
	createClientCred.submit();
	        
	testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
	        		"Response Code is correct for client credetials");
	cc_token = createClientCred.getAccessToken();
	TestLogger.logVariable("AccessToken : " + cc_token);
	TestLogger.logBlankLine();
	
	TestLogger.logStep("[Step 2] : Get International Payment Submission");
	internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint);
	internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
	internationalPayment.setMethod("GET");
	internationalPayment.submit();
	testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"405", 
		"Response Code is correct for International Payment Submission URI when paymentId is not sent in request");
	TestLogger.logBlankLine();
    testVP.testResultFinalize();
}
}
