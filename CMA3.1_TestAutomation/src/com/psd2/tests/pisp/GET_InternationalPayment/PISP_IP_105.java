package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of the values into OPTIONAL MultiAuthorisation/NumberRequired where Request has sent successfully 
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_105 extends TestBase{	
	API_E2E_Utility apiUtility=new API_E2E_Utility();
	@Test
	public void m_PISP_IP_105() throws Throwable{
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		        
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
		        		"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();
        
		TestLogger.logStep("[Step 2] : Get International Payment Submission");	
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId());
		internationalPayment.setHeadersString("Authorization:Bearer "+cc_token);
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
		testVP.verifyStringEquals(String.valueOf(internationalPayment.getResponseStatusCode()),"200", 
				"Response Code is correct for Get International Payment Submission");	
		testVP.verifyEquals(internationalPayment.getURL(), apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId(), 
				"URL is correct and matching with Get International Payment Submission URL");
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.MultiAuthorisation"))!=null,
				"OPTIONAL MultiAuthorisation block present in Get International Payment Submission response body ");
		testVP.verifyTrue(String.valueOf(internationalPayment.getResponseValueByPath("Data.MultiAuthorisation.NumberRequired"))!=null,
				"MANDATORY NumberRequired field present in Get International Payment Submission response body ");
		testVP.verifyTrue((String.valueOf(internationalPayment.getResponseValueByPath("Data.MultiAuthorisation.NumberRequired"))).matches("\\d"),"response is integer");
		
		TestLogger.logBlankLine();	
		testVP.testResultFinalize();		
	}
}
