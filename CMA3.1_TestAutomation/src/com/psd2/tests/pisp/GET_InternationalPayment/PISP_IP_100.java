package com.psd2.tests.pisp.GET_InternationalPayment;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description :Verification of the values into OPTIONAL Data/Initiation/RemittanceInformation block where Request has sent successfully and returned a HTTP Code 200 OK
 * @author : Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"IP"})
public class PISP_IP_100 extends TestBase{	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_PISP_IP_100() throws Throwable{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("payments");
		createClientCred.submit();
		        
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
		        		"Response Code is correct for client credetials");
		cc_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + cc_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Get International Payment Submission");
		internationalPayment.setBaseURL(apiConst.iPaymentSubmission_endpoint+"/"+API_Constant.getIp_PaymentId());
		internationalPayment.setHeadersString("Authorization:Bearer "+ cc_token);
		internationalPayment.setMethod("GET");
		internationalPayment.submit();
	    testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation.Unstructured"))!=null, 
		"Mandatory field Unstructured is present in RemittanceInformation block");
		testVP.verifyTrue((internationalPayment.getResponseValueByPath("Data.Initiation.RemittanceInformation.Reference"))!=null, 
		"Mandatory field Reference is present in RemittanceInformation block");
		testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.RemittanceInformation.Unstructured").length()<=140, 
		"Unstructured field length is less than or equal to 140 characters");
	      testVP.verifyTrue(internationalPayment.getResponseNodeStringByPath("Data.Initiation.RemittanceInformation.Reference").length()<=35, 
		"Reference field length is less than or equal to 35 characters");
		TestLogger.logBlankLine();
	     testVP.testResultFinalize();
	}


}	

