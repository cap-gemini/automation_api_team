package com.psd2.tests.aisp.SingleAccountProducts;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request where permissions are as required to access the Single Account Products API i.e. "ReadProducts" in submitted POST Account Request & consent
 * @author Priya Chauhan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_016 extends TestBase
{
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_AISP_SAP_016() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsBasic;ReadProducts",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request where permissions are as required to access the Single Account product API i.e. ReadProducts");	
		
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/"+"product");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account product request");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Product[0]")!=null),"Mandatory fields are present in the response  "+restRequest.getResponseValueByPath("Data.Product"));
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
