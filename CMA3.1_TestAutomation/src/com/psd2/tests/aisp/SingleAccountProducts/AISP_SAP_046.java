package com.psd2.tests.aisp.SingleAccountProducts;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description: Verification of the field TierBandMethod in the response when the "TierBandMethod" is "Whole"
 * @author Priya Chauhan
 * 
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_046 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAP_046() throws Throwable{	

		TestLogger.logStep("[Step 1] : Verification of the field TierBandMethod in the response when the TierBandMethod is Whole");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/"+"product");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client-id")+",client_secret:"+PropertyUtils.getProperty("client-secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for single account product uri");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.CreditInterest.TierBandSet").isEmpty()), 
				"Mandatory field i.e TierBandSet is present and is not null");
		
		testVP.verifyStringEquals((restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.CreditInterest.TierBandSet.TierBandMethod")), 
				"[Whole]", "The value of TierBandMethod of CreditInterest is WHOLE and matches with the response");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].AccountId").isEmpty()), 
				"Mandatory field i.e AccountId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].ProductType").isEmpty()), 
				"Mandatory field i.e ProductType is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Links.Self").isEmpty()), 
				"Mandatory field i.e Self in Links field is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Meta.TotalPages").isEmpty()), 
				"Mandatory field i.e TotalPages in Meta field is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].ProductId").isEmpty()), 
				"Optional field i.e ProductId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].ProductName").isEmpty()), 
				"Optional field i.e ProductName is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].SecondaryProductId").isEmpty()), 
				"Optional field i.e SecondaryProductId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].MarketingStateId").isEmpty()), 
				"Optional field i.e MarketingStateId is present and is not null");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
