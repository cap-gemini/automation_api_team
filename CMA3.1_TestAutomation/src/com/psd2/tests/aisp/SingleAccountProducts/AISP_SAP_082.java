package com.psd2.tests.aisp.SingleAccountProducts;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description: Verification of the fields in the OtherFeeCategory array
 * @author Priya Chauhan
 * 
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_082 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAP_082() throws Throwable{	

		TestLogger.logStep("[Step 1] : Verification of the fields in the OtherFeeCategory array");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/"+"product");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client-id")+",client_secret:"+PropertyUtils.getProperty("client-secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for single account product uri");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.OtherFeesCharges.FeeChargeDetail.OtherFeeCategoryType.Name").isEmpty()), 
				"Mandatory field i.e Name is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.OtherFeesCharges.FeeChargeDetail.OtherFeeCategoryType.Description").isEmpty()), 
				"Mandatory field i.e Description is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.OtherFeesCharges.FeeChargeDetail.OtherFeeCategoryType.Code").isEmpty()), 
				"Optional field i.e Code is present and is not null");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
