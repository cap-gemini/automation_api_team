package com.psd2.tests.aisp.SingleAccountProducts;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description: Checking the request status without Authorization (Access Token)
 * @author Priya Chauhan
 * 
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_026 extends TestBase
{
	
	@Test
	public void m_AISP_SADD_016() throws Throwable{
		
	
		TestLogger.logStep("[Step 1] : Get Single account product with no Authorization header");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/product");
		restRequest.setHeadersString("client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for get single account products request with no Authorization header");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
