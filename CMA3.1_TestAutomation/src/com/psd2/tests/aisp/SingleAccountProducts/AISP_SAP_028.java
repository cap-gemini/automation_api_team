package com.psd2.tests.aisp.SingleAccountProducts;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description: Checking the request when consent status is Expired
 * @author Priya Chauhan
 * 
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_028 extends TestBase
{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_AISP_SAP_028() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		
		consentDetails = apiUtility.generateAISPConsent(null,true,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 2] :  Verify the response when the consent is expired and access token is still valid");	
		
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/product");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"403", 
				"Response Code is correct for expired consent while access token is still valid");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
