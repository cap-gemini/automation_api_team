package com.psd2.tests.aisp.SingleAccountProducts;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the fields in the response
 * @author Priya Chauhan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_043 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAP_043() throws Throwable{	
		TestLogger.logStep("[Step 1] : Verification of the fields in the response");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/"+"product");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client-id")+",client_secret:"+PropertyUtils.getProperty("client-secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account product request");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].CreditInterest.TierBandSet")).isEmpty(), 
				"Mandatory field 'TierbandSet' is present under CreditInterest");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].CreditInterest.TierBandSet[0].TierBandMethod")).isEmpty(), 
				"Mandatory field TierBandMethod is present under CreditInterest");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].CreditInterest.TierBandSet[0].TierBand[]")).isEmpty(), 
				"Mandatory field TierBand is present under CreditInterest");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].CreditInterest.TierBandSet[0].TierValueMinimum")).isEmpty(), 
				"Mandatory field TierValueMinimum is present under CreditInterest");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].CreditInterest.TierBandSet[0].ApplicationFrequency")).isEmpty(), 
				"Mandatory field ApplicationFrequency is present under CreditInterest");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].CreditInterest.TierBandSet[0].FixedVariableInterestRateType")).isEmpty(), 
				"Mandatory field FixedVariableInterestRateType is present under CreditInterest");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].CreditInterest.TierBandSet[0].AER")).isEmpty(), 
				"Mandatory field AER is present under CreditInterest");
		
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].ProductId")).isEmpty(), 
				"Optional field ProductId is present");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].ProductName")).isEmpty(), 
				"Optional field ProductName is present");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
