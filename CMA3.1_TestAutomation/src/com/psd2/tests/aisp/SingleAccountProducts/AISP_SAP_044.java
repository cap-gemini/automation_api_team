package com.psd2.tests.aisp.SingleAccountProducts;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the field TierBandMethod in the response when the "TierBandMethod" is "Tiered"
 * @author Priya Chauhan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Database"}, dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_044 extends TestBase{	

	@BeforeClass
	public void loadTestData() throws Throwable{
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token !=null, "Access token is not null");
	}

	@Test
	public void m_AISP_SAP_044() throws Throwable
	{
		TestLogger.logStep("[Step 1] : Get account number");
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "200", "Response code is correct for request");
		TestLogger.logBlankLine();
		
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+API_Constant.getAisp_AccountId(),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number = "+accountNumber);
		TestLogger.logStep("[Step 2] : Update TierBandMethod to Tiered in database for account number "+accountNumber);
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_product"));
		mongo.updateArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"BCA.creditInterest.tierBandSet", 0, "tierBandMethod", "TIERED");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of the field TierBandMethod in the response when the TierBandMethod is Tiered");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/"+"product");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for single account product uri");
		
		testVP.verifyStringEquals((restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.CreditInterest.TierBandSet.TierBandMethod")), 
				"[Tiered]", "The value of TierBandMethod of CreditInterest is TIERED and matches with the response");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].AccountId").isEmpty()), 
				"Mandatory field i.e AccountId is present and is not null - " +restRequest.getResponseNodeStringByPath("Data.Product[0].AccountId"));
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].ProductType").isEmpty()), 
				"Mandatory field i.e ProductType is present and is not null - " +restRequest.getResponseNodeStringByPath("Data.Product[0].ProductType"));
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Links.Self").isEmpty()), 
				"Mandatory field i.e Self in Links field is present and is not null - " +restRequest.getResponseNodeStringByPath("Links.Self"));
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Meta.TotalPages").isEmpty()), 
				"Mandatory field i.e TotalPages in Meta field is present and is not null - " +restRequest.getResponseNodeStringByPath("Meta.TotalPages"));
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].ProductId").isEmpty()), 
				"Optional field i.e ProductId is present and is not null - " +restRequest.getResponseNodeStringByPath("Data.Product[0].ProductId"));
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].ProductName").isEmpty()), 
				"Optional field i.e ProductName is present and is not null - " +restRequest.getResponseNodeStringByPath("Data.Product[0].ProductName"));
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].SecondaryProductId").isEmpty()), 
				"Optional field i.e SecondaryProductId is present and is not null - " +restRequest.getResponseNodeStringByPath("Data.Product[0].SecondaryProductId"));
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].MarketingStateId").isEmpty()), 
				"Optional field i.e MarketingStateId is present and is not null - " +restRequest.getResponseNodeStringByPath("Data.Product[0].MarketingStateId"));
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		
		mongo.updateArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"BCA.creditInterest.tierBandSet", 0, "tierBandMethod", "WHOLE");
	}
}
