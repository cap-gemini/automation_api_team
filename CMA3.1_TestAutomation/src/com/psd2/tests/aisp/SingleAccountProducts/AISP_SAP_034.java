package com.psd2.tests.aisp.SingleAccountProducts;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status with invalid value of Accept header.
 * @author Priya Chauhan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_034 extends TestBase{	
		
		@BeforeClass
		public void loadTestData() throws Throwable{
			
			access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
					PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
			testVP.verifyTrue(access_token != null,	"Access token is not null");
		}
		
		@Test
		public void m_AISP_SAP_034() throws Throwable{
			
			TestLogger.logStep("[Step 1] : Get Single account products with invalid value of Accept header");	
			restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/product");
			restRequest.setHeadersString("Authorization:Bearer "+access_token);
			restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
			restRequest.addHeaderEntry("Accept", "application/xml");
			restRequest.setMethod("GET");
			restRequest.submit();
			testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"406", 
					"Response Code is correct for get single account request with invalid value of Accept header");
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();		
		}	
	}
