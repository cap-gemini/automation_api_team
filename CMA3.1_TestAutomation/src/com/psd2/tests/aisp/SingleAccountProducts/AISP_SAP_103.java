package com.psd2.tests.aisp.SingleAccountProducts;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description: Verification of the below fields in the BCA/Overdraft/OverdraftTierBandSet/OverdraftTierBand/OverdraftFeeCharges/OverdraftFeeChargeDetail/OtherFeeType array
 * @author Soumya Banerjee
 * 
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_103 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAP_103() throws Throwable{	

		TestLogger.logStep("[Step 1] : Verification of the below fields in the OtherFeeType array");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/product");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.addHeaderEntry("x-fapi-customer-ip-address",PropertyUtils.getProperty("cust_ip_add"));
		restRequest.addHeaderEntry("x-fapi-customer-last-logged-time",PropertyUtils.getProperty("cust_last_log_time"));
		restRequest.addHeaderEntry("x-fapi-interaction-id",PropertyUtils.getProperty("inter_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for single account product uri");		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.Overdraft.OverdraftTierBandSet.OverdraftTierBand.OverdraftFeesCharges.OverdraftFeeChargeDetail.OtherFeeType.Name").isEmpty()), 
				"Name is present and is not null");		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.Overdraft.OverdraftTierBandSet.OverdraftTierBand.OverdraftFeesCharges.OverdraftFeeChargeDetail.OtherFeeType.Description").isEmpty()), 
				"Description is present and is not null");		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.Overdraft.OverdraftTierBandSet.OverdraftTierBand.OverdraftFeesCharges.OverdraftFeeChargeDetail.OtherFeeType.Code").isEmpty()), 
				"Code is present and is not null");	
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
