package com.psd2.tests.aisp.SingleAccountProducts;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description: Verification of the "Description" field in the OtherCalculationFrequency when its greater that 350 chaacters
 * @author Atishay Bansal
 * 
 */

@Listeners({TestListener.class})
@Test(groups={"Regression","Database"}, dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_065 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAP_065() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Get Account Number");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId());
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client-id")+",client_secret:"+PropertyUtils.getProperty("client-secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account request");
		
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+API_Constant.getAisp_AccountId(),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 2] : Update the value of Description having length more than 350 chars for the account number  : "+accountNumber);
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_product"));
		mongo.updateArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber, "BCA.creditInterest.tierBandSet", 0, "tierBand.0.otherCalculationFrequency.description", "kjahsdkjhaskjdhaskjdhaskjdhkasjdhaksjdhkasjhdkasjhdkasjdhkasjdhkasjhdkasjdhkasjdhkasjdhaskjdhaskjdhkasjhdkasjhdkajshdkjashdkjashkdjhkasjhdkashdkasjdhakjdskjdhakdjhahdjfhajhsfsjhkjhakjfhakjshfkjahfkajhklashflashfalkfjhakjfhalksjfhasklfhalskfhashalkfhlakhflakshflkajshfaklfhlaasljfaasklfhlaksjhflkjashlkjhflkhlfkahlkfhlsaklakfhakhflakhflasmzxncsjhwiwowr");
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 3] : Verification of the Description field in the OtherCalculationFrequency when the its length is more than 350 chars");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/product");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.addHeaderEntry("x-fapi-customer-ip-address",PropertyUtils.getProperty("cust_ip_add"));
		restRequest.addHeaderEntry("x-fapi-customer-last-logged-time",PropertyUtils.getProperty("cust_last_log_time"));
		restRequest.addHeaderEntry("x-fapi-interaction-id",PropertyUtils.getProperty("inter_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"500", 
				"Response Code is correct when the value of description under OtherCalculationFrequency having length of more than 350 chars");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		
		mongo.updateArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber, "BCA.creditInterest.tierBandSet",0, "tierBand.0.otherCalculationFrequency.description", "abhkjfhadkfhakhakfhkfhajkfhdksfhsdkfdsh");
	}
}
