package com.psd2.tests.aisp.SingleAccountProducts;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description: Verification of fields in OverdraftFeesCharges
 * @author Priya Chauhan
 * 
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_075 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAP_075() throws Throwable{	

		TestLogger.logStep("[Step 1] : Verification of fields in OverdraftFeesCharges");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/"+"product");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client-id")+",client_secret:"+PropertyUtils.getProperty("client-secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
	
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for single account product uri");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.Overdraft.OverdraftTierBandSet.OverdraftTierBand.OverdraftFeesCharges").isEmpty()), 
				"Mandatory field i.e OverdraftFeesCharges is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.Overdraft.OverdraftTierBandSet.OverdraftTierBand.OverdraftFeesCharges.OverdraftFeeChargeCap.FeeType").isEmpty()), 
				"Mandatory field i.e FeeType is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.Overdraft.OverdraftTierBandSet.OverdraftTierBand.OverdraftFeesCharges.OverdraftFeeChargeCap.MinMaxType").isEmpty()), 
				"Mandatory field i.e MinMaxType is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.Overdraft.OverdraftTierBandSet.OverdraftTierBand.OverdraftFeesCharges.OverdraftFeeChargeCap.FeeCapAmount").isEmpty()), 
				"Optional field i.e FeeCapAmount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.Overdraft.OverdraftTierBandSet.OverdraftTierBand.OverdraftFeesCharges.OverdraftFeeChargeCap.CappingPeriod").isEmpty()), 
				"Optional field i.e CappingPeriod is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.Overdraft.OverdraftTierBandSet.OverdraftTierBand.OverdraftFeesCharges.OverdraftFeeChargeCap.FeeCapOccurrence").isEmpty()), 
				"Optional field i.e FeeCapOccurrence is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Product[0].BCA.Overdraft.OverdraftTierBandSet.OverdraftFeesCharges.OverdraftFeeChargeCap.OverdraftTierBand.Notes").isEmpty()), 
				"Optional field i.e Notes is present and is not null");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
