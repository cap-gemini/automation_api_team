package com.psd2.tests.aisp.SingleAccountProducts;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of AccountId as mandatory while accessing Single Account Products API
 * @author Priya Chauhan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_003 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAP_003() throws Throwable{
		TestLogger.logStep("[Step 1] :Verification of AccountId as mandatory while accessing Single Account Products API ");
		restRequest.setURL(apiConst.account_endpoint+"product");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client-id")+",client_secret:"+PropertyUtils.getProperty("client-secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(),"400", 
				"Response Code is correct if AccountId is absent in Single Account Product");
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors.ErrorCode")),"[UK.OBIE.Resource.NotFound]", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Errors.Message")),"[IntentId Validation Error]","Error message is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
	
}

