package com.psd2.tests.aisp.SingleAccountProducts;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of field OtherProductType in the response when the Product Type is "Other" in the Data array.
 * @author Priya Chauhan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Database"}, dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_008 extends TestBase{	

	@BeforeClass
	public void loadTestData() throws Throwable{
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token !=null, "Access token is not null");
	}

	@Test
	public void m_AISP_SAP_008() throws Throwable
	{
		TestLogger.logStep("[Step 1] : Get account number");
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "200", "Response code is correct for request");
		TestLogger.logBlankLine();
		
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+API_Constant.getAisp_AccountId(),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number = "+accountNumber);
		TestLogger.logStep("[Step 2] : Update productType value to Other in database for account number "+accountNumber);
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_product"));
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"productType:OTHER");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of field OtherProductType in the response when the Product Type is Other in the Data array");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/"+"product");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "200", "Response code is correct when productType is updated to Other in database");
		
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].ProductName.AccountId")).isEmpty(), 
				"Product array has AccountId under it");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].ProductName.ProductId")).isEmpty(), 
				"Product array has ProductId under it");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].ProductName.ProductType")).isEmpty(), 
				"Product array has ProductType under it");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Links.Self")).isEmpty(), 
				"Self field is present under Links");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Meta.TotalPages")).isEmpty(), 
				"TotalPages is present under Meta field");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"productType:BUSINESSCURRENTACCOUNT");
	}
}
