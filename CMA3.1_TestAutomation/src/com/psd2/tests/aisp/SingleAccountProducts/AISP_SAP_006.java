package com.psd2.tests.aisp.SingleAccountProducts;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of fields included in the Data array
 * @author Priya Chauhan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAP"})
public class AISP_SAP_006 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAP_006() throws Throwable{	
		TestLogger.logStep("[Step 1] : Verification of fields included in the Data array");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/"+"product");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client-id")+",client_secret:"+PropertyUtils.getProperty("client-secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account product request");
		
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].ProductName.AccountId")).isEmpty(), 
				"Product array has AccountId under it");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].ProductName.ProductId")).isEmpty(), 
				"Product array has ProductId under it");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Product[].ProductName.ProductType")).isEmpty(), 
				"Product array has ProductType under it");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Links.Self")).isEmpty(), 
				"Self field is present under Links");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Meta.TotalPages")).isEmpty(), 
				"TotalPages is present under Meta field");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
