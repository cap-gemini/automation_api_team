package com.psd2.tests.aisp.SingleAccountDirectDebits;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of backward compatibility for account setup with V3.0 and get Single Account DirectDebit Information with V2.0
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SADD_056 extends TestBase{	
	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	@Test
	public void m_AISP_SADD_056() throws Throwable{	
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent(null,false,false,false,true,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verify the open banking standard and get Single Account Direct Debits");	
		restRequest.setURL(apiConst.backcomp_account_endpoint+consentDetails.get("accountId")+"/"+"direct-debits");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account direct-debits request");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}



