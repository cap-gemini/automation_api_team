package com.psd2.tests.aisp.SingleAccountDirectDebits;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status with no token value in the Authorization (Access Token) header
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SADD_015 extends TestBase
{ 
	
	@Test
	public void m_AISP_SADD_015() throws Throwable{
		TestLogger.logStep("[Step 1] : Verify the response when access token is null");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/direct-debits");
		restRequest.setHeadersString("Authorization:Bearer ,client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", 
				"Response Code is correct for no authorization header sent with the request");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
