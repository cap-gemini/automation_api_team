package com.psd2.tests.aisp.SingleAccountDirectDebits;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description: Verification of fields included in the Data array
 * @author Kiran Dewangan
 * 
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SADD"})
public class AISP_SADD_026 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SADD_026() throws Throwable{
		
		
		TestLogger.logStep("[Step 1] : Verification of fields included in the Data array");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/direct-debits");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account direct debits request");
		testVP.verifyTrue((restRequest.getResponseValueByPath("Data")!=null), 
				"Mandatory field i.e Data is present and is not null "+restRequest.getResponseNodeStringByPath("Data"));
		testVP.verifyTrue((restRequest.getResponseValueByPath("Links")!=null), 
				"Mandatory field i.e Links is present and is not null"+restRequest.getResponseValueByPath("Links"));
		testVP.verifyTrue((restRequest.getResponseValueByPath("Meta")!=null), 
				"Mandatory field i.e Meta is present and is not null"+restRequest.getResponseValueByPath("Meta"));
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.DirectDebit[0]")!=null),
				"Optional field DirectDebit is present"+restRequest.getResponseNodeStringByPath("Data.DirectDebit[0]"));
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
