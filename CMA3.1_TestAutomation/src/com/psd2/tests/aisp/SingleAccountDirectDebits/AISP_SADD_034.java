package com.psd2.tests.aisp.SingleAccountDirectDebits;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description: Verification of value of DirectDebitStatusCode field at path /Data/DirectDebit/DirectDebitStatusCode
 * @author Kiran Dewangan
 * 
 */
 @Listeners({TestListener.class})
@Test(groups={"Regression", "Database"}, dependsOnGroups={"Pre_AISP_SADD"})
public class AISP_SADD_034 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SADD_034() throws Throwable
	{
		TestLogger.logStep("[Step 1] : Get Account Number");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId());
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client-id")+",client_secret:"+PropertyUtils.getProperty("client-secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account request");
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+API_Constant.getAisp_AccountId(),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		
		TestLogger.logStep("[Step 2] : Update directDebitStatusCode value - Active for Account : "+accountNumber);
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("collection_directdebit"));
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"directDebitStatusCode:ACTIVE");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of DirectDebitStatusCode for Active value at path /Data/DirectDebit/directDebitStatusCode");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/direct-debits");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct ");
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseNodeStringByPath("Data.DirectDebit[0].DirectDebitStatusCode")),"Active",
				"DirectDebitStatusCode should be ACTIVE");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Update directDebitStatusCode value - InActive for Account : "+accountNumber);
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"directDebitStatusCode:INACTIVE");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 5] : Verification of DirectDebitStatusCode for InActive value at path /Data/DirectDebit/DirectDebitStatusCode");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/direct-debits");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct ");
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseNodeStringByPath("Data.DirectDebit[0].DirectDebitStatusCode")),"Inactive",
				"DirectDebitStatusCode should be INACTIVE");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();	
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"directDebitStatusCode:ACTIVE");
	}	
}