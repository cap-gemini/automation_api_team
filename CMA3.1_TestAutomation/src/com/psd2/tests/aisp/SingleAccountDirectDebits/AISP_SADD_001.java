package com.psd2.tests.aisp.SingleAccountDirectDebits;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of CMA compliance for Single Account Direct Debits URI 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Pre_AISP_SADD","Sanity"})
public class AISP_SADD_001 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SADD_001() throws Throwable{	
		TestLogger.logStep("[Step 1] : Verify the open banking standard and get Single Account Direct Debits");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/"+"direct-debits");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client-id")+",client_secret:"+PropertyUtils.getProperty("client-secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account direct-debits request");
		testVP.verifyStringEquals(restRequest.getURL(),apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/"+"direct-debits", 
				"URI for Single account direct debit is as per open banking standard");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
