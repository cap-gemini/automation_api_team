package com.psd2.tests.aisp.SingleAccountDirectDebits;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status when AccountId exists but it doesn't have the directDebit 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SADD"})
public class AISP_SADD_058 extends TestBase{	
	
		@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SADD_058() throws Throwable{
				
		TestLogger.logStep("[Step 1] : Checking the request status when AccountId exists but it doesn't have the directDebit");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_EmptyAccountId()+"/"+"direct-debits");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account direct debits request");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.DirectDebit[0]")==null,"Account ID doesnt have Direct Debit");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
	
	
	
	
}



