package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of 'BookingDateTime' field under data  
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_059 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAT_059() throws Throwable{	
		TestLogger.logStep("[Step 1] : Verification of fields BookingDateTime");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for SIngle Account Txn API");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction[0].BookingDateTime")) != null, 
				"Value for BookingDateTime is not null");
		String bookingDatetime=restRequest.getResponseNodeStringByPath("Data.Transaction[0].BookingDateTime");
		testVP.verifyTrue(Misc.verifyDateTimeFormat(bookingDatetime.split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(bookingDatetime.split("T")[1], "HH:mm:ss+00:00"),"BookingDateTime is as per expected format");	
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
	
}


