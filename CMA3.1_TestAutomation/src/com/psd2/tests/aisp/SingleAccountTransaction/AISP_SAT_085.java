package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of ChargeAmount under data.transaction.
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_085 extends TestBase 
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAT_085() throws Throwable{	
		TestLogger.logStep("[Step 1] : Verification of ChargeAmount under data.transaction");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for chargeAmount field");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction.ChargeAmount.Amount"))!=null, 
				"Mandatory field i.e Amount field in ChargeAmount is present and is not null - "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].ChargeAmount.Amount"));
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction.ChargeAmount.Currency"))!=null, 
				"Mandatory field i.e Currency field in ChargeAmount is present and is not null - "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].ChargeAmount.Currency"));
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}

