package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Currency field under data.transaction.balance.AmountArray
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_149 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAT_149() throws Throwable{			

		TestLogger.logStep("[Step 1] : Verification of Currency field under data.transaction.balance.AmountArray");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction request");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Amount.Currency").isEmpty()), 
				"Currency field value is in ISO standard format");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Amount.Currency").length()==3, 
				"Currency field length is correct i.e. equal to 3 characters");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Amount.Currency").equals(restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Amount.Currency").toUpperCase()), 
				"Currency value is in capital letters");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

