package com.psd2.tests.aisp.SingleAccountTransaction;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Currency field under ChargeAmount for blank value
 * @author Kiran Dewangan
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression","Database"}, dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_094 extends TestBase
{

	@BeforeClass
	public void loadTestData() throws Throwable{
		
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAT_94() throws Throwable{	
		TestLogger.logStep("[Step 1] : Get account number");
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+API_Constant.getAisp_AccountId(),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Update Currency field under chargeAmount subType is blank for Account Number : "+accountNumber);
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("collection_transaction"));
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"chargeAmount.currency: ");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verify the status code when Currency field under Amount subType is blank");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"500", 
				"Response Code is correct when Currency field under chargeAmount subType is blank");		
		testVP.testResultFinalize();
		
		TestLogger.logStep("[Step 4] : Update Currency field under chargeAmount subType as incorrect for Account Number : "+accountNumber);
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"chargeAmount.currency:AB");
		TestLogger.logBlankLine();

		
		TestLogger.logStep("[Step 5] : Verify the status code when Currency field under chargeAmount subType is incorrect");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"500", 
				"Response Code is correct when Currency field under chargeAmount subType is Incorrect");		
		testVP.testResultFinalize();		
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"chargeAmount.currency:GBP");
	}	
}
