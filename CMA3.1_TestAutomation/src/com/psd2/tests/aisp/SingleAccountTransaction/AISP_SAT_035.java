package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Hit the Single Account Transaction API URL with all valid and mandatory headers and check x-fapi-interaction-id value
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_035 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAT_035() throws Throwable{
		TestLogger.logStep("[Step 1] : Comparison of x-fapi-interaction-id value in the response with the value sent in the request");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.addHeaderEntry("x-fapi-interaction-id",PropertyUtils.getProperty("inter_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct with all mandatory headers for comparing the value of x-fapi-financial-id");
		testVP.verifyStringEquals(restRequest.getResponseHeader("x-fapi-interaction-id"),PropertyUtils.getProperty("inter_id"), 
				"Value for 'x-fapi-interaction-id header' matched as per sent in request");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
	
}

