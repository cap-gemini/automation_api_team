package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of fields included in Data array 
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_007 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAT_007() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Get Single account Transaction");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct");
		System.out.println("Kiran Test"+API_Constant.getAisp_AccountId());
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Data.Transaction[0].AccountId"),API_Constant.getAisp_AccountId(), 
				"Account Id is same in response");
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditDebitIndicator").isEmpty(), 
				"CreditDebitIndicator should not be empty : "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditDebitIndicator"));
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.Transaction[0].Status").isEmpty(), 
				"Status should not be empty : "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].Status"));
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.Transaction[0].BookingDateTime").isEmpty(), 
				"BookingDateTime should not be empty : "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].BookingDateTime"));
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.Transaction[0].TransactionId").isEmpty(), 
				"TransactionId should not be empty : "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].TransactionId"));
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.Transaction[0].TransactionReference").isEmpty(), 
				"TransactionReference should not be empty : "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].TransactionReference"));
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.Transaction[0].ValueDateTime").isEmpty(), 
				"ValueDateTime should not be empty : "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].ValueDateTime"));
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.Transaction[0].TransactionInformation").isEmpty(), 
				"TransactionInformation should not be empty : "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].TransactionInformation"));
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.Transaction[0].BankTransactionCode.Code").isEmpty(), 
				"BankTransactionCode.Code should not be empty : "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].BankTransactionCode.Code"));
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.Transaction[0].BankTransactionCode.SubCode").isEmpty(), 
				"BankTransactionCode.SubCode should not be empty : "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].BankTransactionCode.SubCode"));
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.Transaction[0].ProprietaryBankTransactionCode.Code").isEmpty(), 
				"ProprietaryBankTransactionCode.Code should not be empty : "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].ProprietaryBankTransactionCode.Code"));
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.Transaction[0].ProprietaryBankTransactionCode.Issuer").isEmpty(), 
				"ProprietaryBankTransactionCode.Issuer should not be empty : "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].ProprietaryBankTransactionCode.Issuer"));
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Amount.Amount").isEmpty(), 
				"Balance.Amount.Amount should not be empty : "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Amount.Amount"));
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Amount.Currency").isEmpty(), 
				"Balance.Amount.Currency should not be empty : "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Amount.Currency"));
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
	
}

