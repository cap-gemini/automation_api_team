package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request when all permissions are submitted in POST Account Request & Consent to access the Single Account Transactions API:"ReadTransactionsBasic","ReadTransactionsDetail","ReadTransactionsCredits","ReadTransactionsDebits" 
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SAT_013 extends TestBase 
{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_AISP_SAT_013() throws Throwable{	
		
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsBasic;ReadTransactionsBasic;ReadTransactionsDetail;ReadTransactionsCredits;ReadTransactionsDebits",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 2] : Verification of request when all permissions are submitted in POST Account Request & Consent to access the Single Account Transactions API:");	
			
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for the given permissions");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].AccountId").isEmpty()), 
				"Mandatory field i.e AccountId is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].Amount").isEmpty()), 
				"Mandatory field i.e Amount is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditDebitIndicator").isEmpty()), 
				"Mandatory field i.e CreditDebitIndicator is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].Status").isEmpty()), 
				"Mandatory field i.e Status is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].BookingDateTime").isEmpty()), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
