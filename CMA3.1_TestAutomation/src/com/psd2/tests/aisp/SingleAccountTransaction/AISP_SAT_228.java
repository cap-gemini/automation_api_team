package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of 'AuthorisationType' field under data.CardInstrument
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_228 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAT_228() throws Throwable{			

		TestLogger.logStep("[Step 1] : Verification of 'AuthorisationType' field under data.CardInstrument");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction request");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CardInstrument.AuthorisationType").equals("Contactless")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CardInstrument.AuthorisationType").equals("ConsumerDevice")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CardInstrument.AuthorisationType").equals("None")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CardInstrument.AuthorisationType").equals("PIN"), 
				"AuthorisationType field value is correct i.e. "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].CardInstrument.AuthorisationType"));
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

