package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request where permissions are as required to access the Single Account Transactions API are not valid in submitted POST Account Request & Consent i.e. permissions set does not contain any one of them as -ReadTransactionsBasic,ReadTransactionsDetail,ReadTransactionsCredits,ReadTransactionsDebits  
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SAT_018 extends TestBase 
{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_AISP_SAT_018() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsDetail;ReadAccountsBasic",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 2] : Verification of request where permissions are as required to access the Single Account Transactions API are not valid in submitted POST Account Request & Consent i.e. permissions set does not contain any one of them as -");	
			
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"403", 
				"Response Code is correct if the given permissions are not available for creating the consent");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
