package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Varification of value of SchemeName field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_165 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAT_165() throws Throwable{			

		TestLogger.logStep("[Step 1] : Verification of value of SchemeName field");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction request");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName").length()<=40,
				"SchemeName field length is correct i.e. less than or equal to 40 characters");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName").equals("UK.OBIE.SortCodeAccountNumber")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName").equals("UK.OBIE.SortCodeAccountNumber")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName").equals(PropertyUtils.getProperty("DrAccount_SchemeName"))
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName").equals(PropertyUtils.getProperty("DrAccount_SchemeName"))
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName").equals("UK.OBIE.PAN")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName").equals("UK.OBIE.PAN")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName").equals("UK.OBIE.Paym")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName").equals("UK.OBIE.BBAN")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme1")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme2"), 
				"SchemeName field value is correct i.e. "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName"));
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

