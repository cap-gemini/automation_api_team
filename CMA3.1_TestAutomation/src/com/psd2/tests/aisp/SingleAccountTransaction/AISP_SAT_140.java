package com.psd2.tests.aisp.SingleAccountTransaction;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of 'Type' field value
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_140 extends TestBase{	

	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAT_140() throws Throwable{	
		
        TestLogger.logStep("[Step 1] : Verification of 'Type' field value");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
	
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction");		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type").equals("ClosingAvailable") 
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type").equals("ClosingBooked")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type").equals("ClosingCleared")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type").equals("Expected")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type").equals("ForwardAvailable")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type").equals("Information")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type").equals("InterimAvailable")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type").equals("InterimBooked")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type").equals("InterimCleared")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type").equals("OpeningAvailable")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type").equals("OpeningBooked")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type").equals("OpeningCleared")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type").equals("PreviouslyClosedBooked"), 
				"Type field value is correct i.e. "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance.Type"));				
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}	
}