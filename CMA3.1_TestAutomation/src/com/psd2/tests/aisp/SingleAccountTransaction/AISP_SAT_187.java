package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of PostalAddress Array under the Data/Transaction/DebtorAgent/PostalAddress
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_187 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAT_187() throws Throwable{			

		TestLogger.logStep("[Step 1] : Verification of PostalAddress array under data.DebtorAgent");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction request");
		testVP.verifyTrue(restRequest.getResponseValueByPath("Data.Transaction[0].DebtorAgent.PostalAddress")!=null, 
				"PostalAddress array is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].DebtorAgent.PostalAddress.AddressType").isEmpty()), 
				"Optional field i.e. AddressType is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].DebtorAgent.PostalAddress.Department").isEmpty()), 
				"Optional field i.e. Department is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].DebtorAgent.PostalAddress.SubDepartment").isEmpty()), 
				"Optional field i.e. SubDepartment is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].DebtorAgent.PostalAddress.StreetName").isEmpty()), 
				"Optional field i.e. StreetName is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].DebtorAgent.PostalAddress.BuildingNumber").isEmpty()), 
				"Optional field i.e. BuildingNumber is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].DebtorAgent.PostalAddress.PostCode").isEmpty()), 
				"Optional field i.e. PostCode is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].DebtorAgent.PostalAddress.TownName").isEmpty()), 
				"Optional field i.e. TownName is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].DebtorAgent.PostalAddress.CountrySubDivision").isEmpty()), 
				"Optional field i.e. CountrySubDivision is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].DebtorAgent.PostalAddress.Country").isEmpty()), 
				"Optional field i.e. Country is present and is not null");
		testVP.verifyTrue(restRequest.getResponseValueByPath("Data.Transaction[0].DebtorAgent.PostalAddress.AddressLine")!=null, 
				"Optional field i.e. AddressLine is present and is not null");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

