package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request when all permissions are submitted in POST Account Request & Consent to access the Single Account Transactions API:"ReadTransactionsBasic","ReadTransactionsCredits"  
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Database"})
public class AISP_SAT_014 extends TestBase
{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_AISP_SAT_014() throws Throwable{	
		
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsBasic;ReadAccountsDetail;ReadTransactionsBasic;ReadTransactionsCredits",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Get account number");
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+consentDetails.get("accountId"),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		TestLogger.logBlankLine();		
		
		TestLogger.logStep("[Step 3] : Update 'CreditDebitIndicator' field under data transactionType as CR for Account Number : "+accountNumber);
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("collection_transaction"));
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditDebitIndicator:CREDIT");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of request when all permissions are submitted in POST Account Request & Consent to access the Single Account Transactions API:");	
			
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for the given permissions");
		testVP.verifyStringEquals((restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditDebitIndicator")),"Credit", 
				"Mandatory field i.e CreditDebitIndicator is present and correct");		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
