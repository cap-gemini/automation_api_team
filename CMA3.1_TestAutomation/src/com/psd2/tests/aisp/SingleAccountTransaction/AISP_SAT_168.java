package com.psd2.tests.aisp.SingleAccountTransaction;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Identification field value when SchemeName as IBAN/UK.OBIE.IBAN
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Database"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_168 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAT_168() throws Throwable{			

		TestLogger.logStep("[Step 1] : Get Account Number");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction request");
		TestLogger.logBlankLine();
		
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+API_Constant.getAisp_AccountId(),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Update SchemeName field value as IBAN and Identification field value as IE85BOFI90120412345679 under CreditorAccount in DB for account number : "+accountNumber);
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_transaction"));
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditorAccount.schemeName:IBAN,creditorAccount.identification:"+PropertyUtils.getProperty("DrAccount_Identification"));
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of SchemeName field under CreditorAccount when SchemeName field value as IBAN");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction request when SchemeName field value as IBAN");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName").equals(PropertyUtils.getProperty("DrAccount_SchemeName")), 
				"SchemeName field value is correct i.e. "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName"));
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.Identification").equals(PropertyUtils.getProperty("DrAccount_Identification")), 
				"Identification field value is correct i.e. "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.Identification"));
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Update SchemeName field value as UK.OBIE.IBAN under CreditorAccount in DB for account number : "+accountNumber);
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditorAccount.schemeName:UK.OBIE.IBAN");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 5] : Verification of SchemeName field under CreditorAccount when SchemeName field value as UK.OBIE.IBAN");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction request when SchemeName field value as UK.OBIE.IBAN");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName").equals(PropertyUtils.getProperty("DrAccount_SchemeName")), 
				"SchemeName field value is correct i.e. "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.SchemeName"));
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.Identification").equals(PropertyUtils.getProperty("DrAccount_Identification")), 
				"Identification field value is correct i.e. "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount.Identification"));
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditorAccount.schemeName:UK.OBIE.IBAN,creditorAccount.identification:"+PropertyUtils.getProperty("DrAccount_Identification"));
	}
}

