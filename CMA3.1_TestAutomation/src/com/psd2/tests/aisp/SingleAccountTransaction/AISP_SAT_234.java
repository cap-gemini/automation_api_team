package com.psd2.tests.aisp.SingleAccountTransaction;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of masking rule for TransactionInformation field when any 8 numeric characters are in the sequence
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Database"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_234 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAT_234() throws Throwable{			

		TestLogger.logStep("[Step 1] : Get Account Id");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction request");
		TestLogger.logBlankLine();
		
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+API_Constant.getAisp_AccountId(),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Update TransactionInformation value to any 8 numeric characters in sequence for account number : "+accountNumber);
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_transaction"));
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"transactionInformation:12345678");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of masking rule for TransactionInformation field when any 8 numeric characters are in the sequence");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction request");
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Data.Transaction[0].TransactionInformation"), "XXXXXXXX", 
				"Masking rule is correct for TransactionInformation field when any 8 numeric characters are in the sequence");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"transactionInformation:Transaction processed on account XXXXXXXX");
	}
}

