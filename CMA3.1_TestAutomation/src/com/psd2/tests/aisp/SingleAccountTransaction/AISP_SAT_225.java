package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of 'CardSchemeName' field under data.CardInstrument
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_225 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAT_225() throws Throwable{			

		TestLogger.logStep("[Step 1] : Verification of 'CardSchemeName' field under data.CardInstrument");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction request");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CardInstrument.CardSchemeName").equals("AmericanExpress")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CardInstrument.CardSchemeName").equals("Diners")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CardInstrument.CardSchemeName").equals("Discover")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CardInstrument.CardSchemeName").equals("MasterCard")
				|| restRequest.getResponseNodeStringByPath("Data.Transaction[0].CardInstrument.CardSchemeName").equals("VISA"), 
				"CardSchemeName field value is correct i.e. "+restRequest.getResponseNodeStringByPath("Data.Transaction[0].CardInstrument.CardSchemeName"));
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

