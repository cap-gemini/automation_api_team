package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of CreditorAgent array under data field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_157 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAT_157() throws Throwable{			

		TestLogger.logStep("[Step 1] : Verification of CreditorAgent array under data field");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction request");
		testVP.verifyTrue(restRequest.getResponseValueByPath("Data.Transaction[0].CreditorAgent")!=null, 
				"Optional field i.e. DebtorAccount is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAgent.SchemeName").isEmpty()), 
				"Optional field i.e. SchemeName is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAgent.Identification").isEmpty()), 
				"Optional field i.e. Identification is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAgent.Name").isEmpty()), 
				"Optional field i.e. Name is present and is not null");
		testVP.verifyTrue(restRequest.getResponseValueByPath("Data.Transaction[0].CreditorAgent.PostalAddress")!=null, 
				"Optional field i.e. PostalAddress is present and is not null");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

