package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request when all permissions are submitted in POST Account Request & Consent to access the Single Account Transactions API:"ReadTransactionsDetail","ReadTransactionsDebits"  
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Database"})
public class AISP_SAT_017 extends TestBase 
{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_AISP_SAT_017() throws Throwable{	
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsBasic;ReadTransactionsDetail;ReadTransactionsDebits",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Get account number");
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+consentDetails.get("accountId"),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		TestLogger.logBlankLine();		
		
		TestLogger.logStep("[Step 3] : Update 'CreditDebitIndicator' field under data transactionType as CR for Account Number : "+accountNumber);
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("collection_transaction"));
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditDebitIndicator:DEBIT");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request when all permissions are submitted in POST Account Request & Consent to access the Single Account Transactions API:");	
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for the given permissions");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].AccountId").isEmpty()), 
				"Mandatory field i.e AccountId is present and is not null");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction[0].Amount")!=null), 
				"Mandatory field i.e Amount is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditDebitIndicator").isEmpty()), 
				"Mandatory field i.e CreditDebitIndicator is present and is not null");
		testVP.verifyEquals(restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditDebitIndicator"),"Debit", 
				"Mandatory field i.e CreditDebitIndicator is present and the value is Debit");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].Status").isEmpty()), 
				"Mandatory field i.e Status is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].BookingDateTime").isEmpty()), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction[0].ChargeAmount")!=null), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction[0].CurrencyExchange")!=null), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction[0].BankTransactionCode")!=null), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction[0].ProprietaryBankTransactionCode")!=null), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAgent")!=null), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction[0].DebtorAgent")!=null), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction[0].DebtorAccount")!=null), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction[0].CardInstrument")!=null), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].TransactionInformation").isEmpty()), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction[0].Balance")!=null), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction[0].MerchantDetails")!=null), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.Transaction[0].CreditorAccount")!=null), 
				"Mandatory field i.e BookingDateTime is present and is not null");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
