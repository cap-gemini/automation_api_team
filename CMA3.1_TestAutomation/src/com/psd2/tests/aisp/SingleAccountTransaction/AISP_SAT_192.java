package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of SubDepartment Field in the range 1-70 characters under data.DebtorAgent field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_192 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAT_192() throws Throwable{			

		TestLogger.logStep("[Step 1] : Verification of SubDepartment under data.DebtorAgent.PostalAddress");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction request");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Transaction[0].DebtorAgent.PostalAddress.SubDepartment").isEmpty()), 
				"SubDepartment field is present and is not null");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Transaction[0].DebtorAgent.PostalAddress.SubDepartment").length()<=70, 
				"SubDepartment field length is correct i.e. less than or equal to 70 characters");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

