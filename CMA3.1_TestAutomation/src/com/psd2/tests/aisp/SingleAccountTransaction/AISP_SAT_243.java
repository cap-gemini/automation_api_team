package com.psd2.tests.aisp.SingleAccountTransaction;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of LastAvailableDateTime in the Meta subtype 
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAT"})
public class AISP_SAT_243 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAT_243() throws Throwable{			

		TestLogger.logStep("[Step 1] : Verification of LastAvailableDateTime in the Meta subtype");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account transaction request");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Meta.LastAvailableDateTime").isEmpty()),
				"LastAvailableDateTime field is present and is not null");
		TestLogger.logVariable("LastAvailableDateTime : "+restRequest.getResponseNodeStringByPath("Meta.LastAvailableDateTime"));
		testVP.verifyTrue(Misc.verifyDateTimeFormat(restRequest.getResponseNodeStringByPath("Meta.LastAvailableDateTime").split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(restRequest.getResponseNodeStringByPath("Meta.LastAvailableDateTime").split("T")[1], "HH:mm:ss+00:00"),
				"LastAvailableDateTime is as per ISO Standard format");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
}

