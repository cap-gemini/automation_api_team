package com.psd2.tests.aisp.DeleteAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request when ConsentID length is greater than 128
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_DAR_008 extends TestBase
{
	API_E2E_Utility apiUtil = new API_E2E_Utility();

	@Test
	public void m_AISP_DAR_008() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Account SetUp....");
		
		consentDetails = apiUtil.performAccountSetUp(false, null);
		
		TestLogger.logStep("[Step 2] : Verify the request status when ConsentID having length more than 128");	
		
		restRequest.setURL(apiConst.as_endpoint+"/"+consentDetails.get("consentId")+consentDetails.get("consentId")+consentDetails.get("consentId")+consentDetails.get("consentId"));
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Content-Type", "application/json");
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("DELETE");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", "Response Code is correct for ConsentID with length more than 128");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors.ErrorCode"), "[UK.OBIE.Resource.NotFound]", "Actual Error Code is matched with Expected Error Code");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Errors.Message").isEmpty()), "Errors message is present : " + restRequest.getResponseNodeStringByPath("Errors.Message"));
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
