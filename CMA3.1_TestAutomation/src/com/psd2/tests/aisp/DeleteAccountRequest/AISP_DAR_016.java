package com.psd2.tests.aisp.DeleteAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status with expired token value in the Authorization (Client-Credential Token) header
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_DAR_016 extends TestBase
{
	API_E2E_Utility apiUtil = new API_E2E_Utility();

	@Test
	public void m_AISP_DAR_016() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Account SetUp....");
		
		consentDetails = apiUtil.performAccountSetUp(false, null);
		
		TestLogger.logStep("[Step 2] : Verify the response when access token is expired");	
		
		restRequest.setURL(apiConst.as_endpoint+"/"+consentDetails.get("consentId"));
		restRequest.setHeadersString("Authorization:Bearer "+apiConst.expired_cc_accesstoken);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Content-Type", "application/json");
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("DELETE");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", "Response Code is correct for expired access token");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
