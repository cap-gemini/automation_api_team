package com.psd2.tests.aisp.DeleteAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of x-fapi-interaction-id value in the response if not sent in the request.
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_DAR_024 extends TestBase{	
	
	API_E2E_Utility apiUtil = new API_E2E_Utility();

	@Test
	public void m_AISP_DAR_024() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Account SetUp....");
		
		consentDetails = apiUtil.performAccountSetUp(false, null);
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getHeaderEntry("x-fapi-financial-id")),"0015800000jfQ9aAAE", 
				"Response Code is correct on hitting the POST Account Request api URL with x-fapi-financial-id header key value other than 0015800000jfQ9aAAE");
		TestLogger.logBlankLine();
		
	    TestLogger.logStep("[Step 2] : Comparing x-fapi-interaction-id value in the response with the value sent in the request ");
		
	    restRequest.setURL(apiConst.as_endpoint+"/"+consentDetails.get("consentId"));
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Content-Type", "application/json");
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("DELETE");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"204", "Response Code is correct for DELETE account request");
		
		testVP.verifyTrue(!(restRequest.getResponseHeader("x-fapi-interaction-id")).isEmpty(),"x-fapi-interaction-id value in the response header is not empty :" + restRequest.getResponseHeader("x-fapi-interaction-id"));
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}