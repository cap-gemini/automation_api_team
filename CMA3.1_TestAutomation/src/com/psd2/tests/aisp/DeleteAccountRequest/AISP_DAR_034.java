package com.psd2.tests.aisp.DeleteAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of backward compatibility for account setup with V2.0 and delete account request with V2.0
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_DAR_034 extends TestBase{	
	
	API_E2E_Utility apiUtil = new API_E2E_Utility();

	@Test
	public void m_AISP_DAR_034() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtil.performAccountSetUp(true, null);
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 2] :Verification of backward compatibility for account setup with V2.0 and delete account request with V2.0");
		
        restRequest.setURL(apiConst.backcomp_as_endpoint+"/"+consentDetails.get("consentId"));
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Content-Type", "application/json");
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("DELETE");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"204", 
				"Response Code is correct to verify the backward compatibility for account setup with V2.0 and delete account request with V2.0");
		
		testVP.testResultFinalize();		
	}
}