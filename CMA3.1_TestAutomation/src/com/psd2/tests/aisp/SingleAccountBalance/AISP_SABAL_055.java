package com.psd2.tests.aisp.SingleAccountBalance;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Currency field under Account subtype
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SABAL"})
public class AISP_SABAL_055 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABAL_055() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of Currency field under Account subtype");	
			
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/balances");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for Currency field which is according to ISO 4217 standards");
		
        testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Balance[0].Amount.Currency").length()<=3,
        		"Currency field length is not more than 3 characters"); 
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Balance[0].Amount.Currency").equals(restRequest.getResponseNodeStringByPath("Data.Balance[0].Amount.Currency").toUpperCase()), 
				"Currency field is in Capital letters"); 
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
}
