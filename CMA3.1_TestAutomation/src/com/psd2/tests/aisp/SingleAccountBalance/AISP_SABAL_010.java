package com.psd2.tests.aisp.SingleAccountBalance;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request where permissions are as required to access the Single Account Balance API i.e. "ReadBalances" in submitted POST Account Request & consent.
 * @author Mohit Patidar
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SABAL_010 extends TestBase{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_AISP_SABAL_010() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Verification of request where permissions are as required to access the Single Account Balance API i.e. 'ReadBalances' in submitted POST Account Request & consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsDetail;ReadBalances",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request where permissions are as required to access the Single Account Information API i.e. ReadAccountsBasic in submitted POST Account Request");
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/balances");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account balance request");
		
		testVP.verifyTrue(restRequest.getResponseValueByPath("Data.Balance")!=null, 
				"Mandatory field i.e Balance is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].AccountId").isEmpty()), 
				"Mandatory field i.e AccountId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditDebitIndicator").isEmpty()), 
				"Mandatory field i.e CreditDebitIndicator is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").isEmpty()), 
				"Mandatory field i.e Type is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].DateTime").isEmpty()), 
				"Mandatory field i.e DateTime is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].Amount").isEmpty()), 
				"Mandatory field i.e Amount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].Amount.Amount").isEmpty()), 
				"Mandatory field i.e Amount under Amount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].Amount.Currency").isEmpty()), 
				"Mandatory field i.e Currency under Amount is present and is not null");
		
		testVP.verifyTrue(restRequest.getResponseValueByPath("Data.Balance[0].CreditLine[0]")!=null, 
				"Optional field i.e CreditLine is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Included").isEmpty()), 
				"Mendatory field i.e Included under CreditLine is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Type").isEmpty()), 
				"Optional field i.e Type under CreditLine is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Amount").isEmpty()), 
				"Optional field i.e Amount under CreditLine is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Amount.Amount").isEmpty()), 
				"Mendatory field i.e Amount under CreditLine/Amount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Amount.Currency").isEmpty()), 
				"Mendatory field i.e Currency under CreditLine/Amount is present and is not null");
		
		testVP.verifyTrue(restRequest.getResponseValueByPath("Links")!=null, 
				"Mandatory field i.e Links is present and is not null");
		
		testVP.verifyTrue(restRequest.getResponseValueByPath("Meta")!=null, 
				"Mandatory field i.e Meta is present and is not null");
		
		testVP.verifyStringEquals(restRequest.getResponseHeader("Content-Type"), "application/json;charset=UTF-8", 
				"Response is UTF-8 character encoded");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}	
}
