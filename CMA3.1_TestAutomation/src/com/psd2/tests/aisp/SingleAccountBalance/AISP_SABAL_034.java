package com.psd2.tests.aisp.SingleAccountBalance;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of authorization code creation by using refresh token created in another consent
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SABAL_034 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_AISP_SABAL_034() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of request where permissions are as required to access the Single Account Balance API i.e. 'ReadBalances' in submitted POST Account Request & consent");	
		consentDetails = apiUtility.generateAISPConsent(null,false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Generation of new access token");

		createAccessToken.setBaseURL(apiConst.at_endpoint);
		//createAccessToken.setAuthentication(PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		createAccessToken.setRefreshToken(consentDetails.get("api_refresh_token"));
		createAccessToken.submit();
		
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"200", 
				"Verified refresh token URI");	
		testVP.verifyTrue(String.valueOf(createAccessToken.getResponseHeader("access_token"))!=null,
				"The access token is present in the response");
		
		access_token=createAccessToken.getAccessToken();
		
		TestLogger.logStep("[Step 3] : Verification of newly created access token using refresh token");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/balances");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"403", 
				"Response Code is correct for single account balance request with newly generated access token");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
