package com.psd2.tests.aisp.SingleAccountBalance;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of fields included in Data array 
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SABAL"})
public class AISP_SABAL_006 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABAL_006() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of request (UTF-8 character encoded) status through GET method with mandatory and optional fields");	
			
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/balances");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.addHeaderEntry("x-fapi-customer-ip-address",PropertyUtils.getProperty("cust_ip_add"));
		restRequest.addHeaderEntry("x-fapi-customer-last-logged-time",PropertyUtils.getProperty("cust_last_log_time"));
		restRequest.addHeaderEntry("x-fapi-interaction-id",PropertyUtils.getProperty("inter_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for Single ACcount Balances through GET method with mandatory and optional fields");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].AccountId").isEmpty()), 
				"Mandatory field i.e AccountId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditDebitIndicator").isEmpty()), 
				"Mandatory field i.e CreditDebitIndicator is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").isEmpty()), 
				"Mandatory field i.e Type is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].DateTime").isEmpty()), 
				"Mandatory field i.e DateTime is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].Amount").isEmpty()), 
				"Mandatory field i.e Amount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].Amount.Amount").isEmpty()), 
				"Mandatory field i.e Amount under Amount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].Amount.Currency").isEmpty()), 
				"Mandatory field i.e Currency under Amount is present and is not null");
		
		testVP.verifyTrue(restRequest.getResponseValueByPath("Data.Balance[0].CreditLine[0]")!=null, 
				"Optional field i.e CreditLine is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Included").isEmpty()), 
				"Mendatory field i.e Included under CreditLine is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Type").isEmpty()), 
				"Optional field i.e Type under CreditLine is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Amount").isEmpty()), 
				"Optional field i.e Amount under CreditLine is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Amount.Amount").isEmpty()), 
				"Mendatory field i.e Amount under CreditLine/Amount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Amount.Currency").isEmpty()), 
				"Mendatory field i.e Currency under CreditLine/Amount is present and is not null");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
}
