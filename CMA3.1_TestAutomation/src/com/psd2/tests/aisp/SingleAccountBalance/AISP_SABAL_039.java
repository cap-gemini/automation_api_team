package com.psd2.tests.aisp.SingleAccountBalance;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of 'Type' field for valid values
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SABAL"})
public class AISP_SABAL_039 extends TestBase{	

	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABAL_039() throws Throwable{	
		
        TestLogger.logStep("[Step 1] : Verification of 'Type' field value");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/balances");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
	
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account balance");		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").equals("ClosingAvailable") 
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").equals("ClosingBooked")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").equals("ClosingCleared")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").equals("Expected")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").equals("ForwardAvailable")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").equals("Information")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").equals("InterimAvailable")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").equals("InterimBooked")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").equals("InterimCleared")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").equals("OpeningAvailable")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").equals("OpeningBooked")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").equals("OpeningCleared")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].Type").equals("PreviouslyClosedBooked"), 
				"Type field value is correct i.e. "+restRequest.getResponseNodeStringByPath("Data.Balance[0].Type"));				
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}	
}