package com.psd2.tests.aisp.SingleAccountBalance;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Type field under Credit Line
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SABAL"})
public class AISP_SABAL_063 extends TestBase{	

	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABAL_063() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of Type field under Credit Line");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/balances");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account balance");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Type").equals("Available")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Type").equals("Credit")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Type").equals("Emergency")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Type").equals("Pre-Agreed")
				|| restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Type").equals("Temporary"), 
				"Type field value under Credit Line is correct i.e. "+restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Type"));			
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}