package com.psd2.tests.aisp.SingleAccountBalance;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request without Included field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression", "Database"},dependsOnGroups={"Pre_AISP_SABAL"})
public class AISP_SABAL_062 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABAL_062() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Get Account Id");		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/balances");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
	
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account balance request");
		TestLogger.logBlankLine();
		
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+API_Constant.getAisp_AccountId(),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Delete Included field under data.balance.creditline from DB for Account Number : "+accountNumber);	
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("collection_balance"));
		mongo.removeArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditLine",0,"included");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verify the response when Included field not present in DB");		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/balances");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"500", 
				"Response Code is correct for get single account balance when Included field not present in DB");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		mongo.updateArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditLine",0,"included","true");
	}
}
