package com.psd2.tests.aisp.SingleAccountBalance;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of backward compatibility for account setup with V2.0 and get balances with V2.0
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SABAL_075 extends TestBase
{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_AISP_SABAL_075() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent(null,false,true,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of backward compatibility for account setup with V2.0 and get balances with V2.0");	
		
		restRequest.setURL(apiConst.backcomp_account_endpoint+consentDetails.get("accountId")+"/balances");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for single account balance when account setup with V2.0 and get balances with V2.0");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
