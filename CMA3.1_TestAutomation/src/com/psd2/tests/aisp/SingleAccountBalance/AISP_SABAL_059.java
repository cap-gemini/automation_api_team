package com.psd2.tests.aisp.SingleAccountBalance;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of CreditLine array under data.balance field
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SABAL"})
public class AISP_SABAL_059 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABAL_059() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of CreditLine array under data.balance field");	
			
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/balances");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account balance request");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Included").isEmpty()), 
				"Mandatory field i.e Included is present and is not null");		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Amount.Amount").isEmpty()), 
				"Mandatory field i.e Amount is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Amount.Currency").isEmpty()), 
				"Mandatory field i.e Currency is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Balance[0].CreditLine[0].Type").isEmpty()), 
				"Mandatory field i.e Type is present and is not null");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
}
