package com.psd2.tests.aisp.SingleAccountBalance;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request when accountId length is greater than 40 or AccountID field with invalid value
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SABAL"})
public class AISP_SABAL_013 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABAL_013() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Verification of request when accountId length is greater than 40");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+API_Constant.getAisp_AccountId()+"/balances");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400",
				"Response Code is correct for account Id with length more than 40 characters request");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors.ErrorCode"), "[UK.OBIE.Resource.NotFound]", 
				"Error code for the request is correct i.e. 'UK.OBIE.Resource.NotFound'");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Errors.Message").isEmpty()), 
				"Message for the error code is present i.e. "+restRequest.getResponseNodeStringByPath("Errors.Message"));
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request when accountId value is invalid");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"asj/balances");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400",
				"Response Code is correct when account Id value is invalid");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors.ErrorCode"), "[UK.OBIE.Resource.NotFound]", 
				"Error code for the request is correct i.e. 'UK.OBIE.Resource.NotFound'");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Errors.Message").isEmpty()), 
				"Message for the error code is present i.e. "+restRequest.getResponseNodeStringByPath("Errors.Message"));
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
}
