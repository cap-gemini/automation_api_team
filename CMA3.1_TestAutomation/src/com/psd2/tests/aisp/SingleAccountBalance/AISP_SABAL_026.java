package com.psd2.tests.aisp.SingleAccountBalance;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Comparison of x-fapi-interaction-id value in the response with the value sent in the request
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SABAL"})
public class AISP_SABAL_026 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABAL_026() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Checking if x-fapi-interaction-id in response header has same value as sent in request");	
			
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/balances");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("x-fapi-interaction-id",PropertyUtils.getProperty("inter_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account balance request");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseHeader("x-fapi-interaction-id")),PropertyUtils.getProperty("inter_id"),
				"Value of x-fapi-interaction-id in response header is same as that sent in request");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
}
