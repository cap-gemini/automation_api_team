package com.psd2.tests.aisp.AccessTokenWithRefreshToken;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of CMA compliance for Fund Confirmation URI with old refresh token
 * @author Alok Kumar 
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class AISP_ATWRT_017 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, PropertyUtils.getDynamicVariable("CISP_RT"),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}	
	@Test
	public void m_AISP_ATWRT_017() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Check Fund Confirmation request... ");
	
		fundConf.setBaseURL(apiConst.fcc_endpoint);
		fundConf.setHeadersString("Authorization:Bearer "+access_token);
		fundConf.setConsentIdField(PropertyUtils.getDynamicVariable("CISP_ConsentId"));
		fundConf.submit();
	
		testVP.verifyStringEquals(String.valueOf(fundConf.getResponseStatusCode()),"201", 
				"Response Code is correct for Fund Confiramtion Request");
		testVP.verifyStringEquals(fundConf.getURL(),apiConst.fcc_endpoint, 
				"URI for Fund Confiramtion Request is as per open banking standard");
		
		testVP.testResultFinalize();
	}
}