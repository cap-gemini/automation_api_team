package com.psd2.tests.aisp.AccessTokenWithRefreshToken;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;

/**
 * Class Description : Verification of Refresh Token URI with incorrect format
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_ATWRT_002 extends TestBase{	
	
	@Test
	public void m_AISP_ATWRT_002() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of response on hitting incorrect refresh token URI");

		createAccessToken.setBaseURL(apiConst.at_endpoint+"12@");
		createAccessToken.setRefreshToken(API_Constant.getAisp_RefreshToken());
		createAccessToken.submit();
		
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"404", 
				"Response code is correct for Refresh Token URI with incorrect format");	
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

