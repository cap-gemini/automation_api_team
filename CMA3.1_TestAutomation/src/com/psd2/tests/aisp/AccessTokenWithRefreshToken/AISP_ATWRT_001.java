package com.psd2.tests.aisp.AccessTokenWithRefreshToken;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;

/**
 * Class Description : Verification of Refresh Token URI 
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class AISP_ATWRT_001 extends TestBase{	
	
	@Test
	public void m_AISP_ATWRT_001() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of refresh token URI");

		createAccessToken.setBaseURL(apiConst.at_endpoint);
		createAccessToken.setRefreshToken(API_Constant.getAisp_RefreshToken());
		createAccessToken.submit();
		
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"200", 
				"Verified refresh token URI");	
		
		testVP.verifyStringEquals(createAccessToken.getURL(),apiConst.at_endpoint, 
				"URI for Access Token generation using refresh token is as per open banking standard");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

