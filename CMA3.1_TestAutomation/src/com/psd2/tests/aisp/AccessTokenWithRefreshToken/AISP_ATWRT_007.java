package com.psd2.tests.aisp.AccessTokenWithRefreshToken;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Generation of new (by-value) access token by using refresh token endpoint when Consent got expired or revoked.
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_ATWRT_007 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_AISP_ATWRT_007() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent(null,true,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of refresh token URI");
  
		createAccessToken.setBaseURL(apiConst.at_endpoint);
		createAccessToken.setRefreshToken(consentDetails.get("api_refresh_token"));
		createAccessToken.submit();
		
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"200", 
				"Response Code is correct for expired consent while access token is still valid");	
		
		access_token=createAccessToken.getAccessToken();
		
		TestLogger.logStep("[Step 3] : Verification of newly created access token using refresh token");
		
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"403", 
				"Response Code is correct for get single account request with access token of expired consent");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

