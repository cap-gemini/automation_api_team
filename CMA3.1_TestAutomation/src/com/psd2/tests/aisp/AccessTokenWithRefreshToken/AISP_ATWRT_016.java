package com.psd2.tests.aisp.AccessTokenWithRefreshToken;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of old AISP Refresh Token URI 
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class AISP_ATWRT_016 extends TestBase{	
	
	@Test
	public void m_AISP_ATWRT_016() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of refresh token URI");

		createAccessToken.setBaseURL(apiConst.at_endpoint);
		createAccessToken.setRefreshToken(PropertyUtils.getDynamicVariable("AISP_RT"));
		createAccessToken.submit();
		
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"200", 
				"Verified refresh token URI");	
		
		testVP.verifyStringEquals(createAccessToken.getURL(),apiConst.at_endpoint, 
				"URI for Access Token generation using refresh token is as per open banking standard");
		TestLogger.logBlankLine();
		
		access_token=createAccessToken.getAccessToken();
		
		TestLogger.logStep("[Step 2] : Get Account Id");	
		getAccount.setBaseURL(apiConst.account_endpoint);
		getAccount.setHeadersString("Authorization:Bearer "+access_token);
		getAccount.submit();
		
		testVP.assertStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"200", 
				"Response Code is correct for get accounts request");	
		
		testVP.assertStringEquals(getAccount.getAccountId(), PropertyUtils.getDynamicVariable("AISP_AccountId"), 
				"New account id with old refresh token is same as old account id");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Get single Account");	
		getAccount.setBaseURL(apiConst.account_endpoint+PropertyUtils.getDynamicVariable("AISP_AccountId"));
		getAccount.setHeadersString("Authorization:Bearer "+access_token);
		getAccount.submit();
		
		testVP.assertStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account request");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Get Account balances");	
		getAccount.setBaseURL(apiConst.account_endpoint+PropertyUtils.getDynamicVariable("AISP_AccountId")+"/balances");
		getAccount.setHeadersString("Authorization:Bearer "+access_token);
		getAccount.submit();
		
		testVP.assertStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"200", 
				"Response Code is correct for get account balance request");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 5] : Get Account beneficiaries");	
		getAccount.setBaseURL(apiConst.account_endpoint+PropertyUtils.getDynamicVariable("AISP_AccountId")+"/beneficiaries");
		getAccount.setHeadersString("Authorization:Bearer "+access_token);
		getAccount.submit();
		
		testVP.assertStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"200", 
				"Response Code is correct for get account beneficiary request");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 6] : Get Account transactions");	
		getAccount.setBaseURL(apiConst.account_endpoint+PropertyUtils.getDynamicVariable("AISP_AccountId")+"/transactions");
		getAccount.setHeadersString("Authorization:Bearer "+access_token);
		getAccount.submit();
		
		testVP.assertStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"200", 
				"Response Code is correct for get account transaction request");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 7] : Get direct-debits");	
		getAccount.setBaseURL(apiConst.account_endpoint+PropertyUtils.getDynamicVariable("AISP_AccountId")+"/direct-debits");
		getAccount.setHeadersString("Authorization:Bearer "+access_token);
		getAccount.submit();
		
		testVP.assertStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"200", 
				"Response Code is correct for get account balance request");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 8] : Get standing-orders");	
		getAccount.setBaseURL(apiConst.account_endpoint+PropertyUtils.getDynamicVariable("AISP_AccountId")+"/standing-orders");
		getAccount.setHeadersString("Authorization:Bearer "+access_token);
		getAccount.submit();
		
		testVP.assertStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"200", 
				"Response Code is correct for get account balance request");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 9] : Get products");	
		getAccount.setBaseURL(apiConst.account_endpoint+PropertyUtils.getDynamicVariable("AISP_AccountId")+"/product");
		getAccount.setHeadersString("Authorization:Bearer "+access_token);
		getAccount.submit();
		
		testVP.assertStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"200", 
				"Response Code is correct for get account balance request");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 10] : Get scheduled-payments");	
		getAccount.setBaseURL(apiConst.account_endpoint+PropertyUtils.getDynamicVariable("AISP_AccountId")+"/scheduled-payments");
		getAccount.setHeadersString("Authorization:Bearer "+access_token);
		getAccount.submit();
		
		testVP.assertStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"200", 
				"Response Code is correct for get account balance request");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 11] : Get Account statements");	
		getAccount.setBaseURL(apiConst.account_endpoint+PropertyUtils.getDynamicVariable("AISP_AccountId")+"/statements");
		getAccount.setHeadersString("Authorization:Bearer "+access_token);
		getAccount.submit();
		
		testVP.assertStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"200", 
				"Response Code is correct for get account balance request");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 12] : Get Account statements");	
		getAccount.setBaseURL(apiConst.account_endpoint+PropertyUtils.getDynamicVariable("AISP_AccountId")+"/statements/"+getAccount.getResponseNodeStringByPath("Data.Statement[0].StatementId"));
		getAccount.setHeadersString("Authorization:Bearer "+access_token);
		getAccount.submit();
		
		testVP.assertStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"200", 
				"Response Code is correct for get account balance request");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 13] : Get Account statements");
		restRequest.setURL(apiConst.account_endpoint+PropertyUtils.getDynamicVariable("AISP_AccountId")+"/statements/"+getAccount.getResponseNodeStringByPath("Data.Statement[0].StatementId")+"/file");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.assertStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get account balance request");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}