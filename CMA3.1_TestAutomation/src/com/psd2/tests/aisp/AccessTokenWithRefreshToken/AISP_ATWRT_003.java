package com.psd2.tests.aisp.AccessTokenWithRefreshToken;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;

/**
 * Class Description : Generation of new (by-value) access token by using refresh token (validity is 90 days) endpoint.
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_ATWRT_003 extends TestBase{	
	
	@Test
	public void m_AISP_ATWRT_003() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generation of new access token using refresh token");

		createAccessToken.setBaseURL(apiConst.at_endpoint);
		createAccessToken.setRefreshToken(API_Constant.getAisp_RefreshToken());
		createAccessToken.submit();
		
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"200", 
				"Verified refresh token URI");	
		testVP.verifyTrue(String.valueOf(createAccessToken.getResponseHeader("access_token"))!=null,
				"The access token is present in the response");
}
}

