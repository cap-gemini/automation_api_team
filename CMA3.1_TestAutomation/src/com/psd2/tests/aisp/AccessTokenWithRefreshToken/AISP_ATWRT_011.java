package com.psd2.tests.aisp.AccessTokenWithRefreshToken;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;

/**
 * Class Description : Generation of new (by-value) access token by using refresh token endpoint when TPP has not provided anything for grant_type parameter.
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_ATWRT_011 extends TestBase{	
	
	@Test
	public void m_AISP_ATWRT_011() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of response code for blank value of grant_type");
		createAccessToken.setBaseURL(apiConst.at_endpoint);
		createAccessToken.setRefreshToken(API_Constant.getAisp_RefreshToken());
		createAccessToken.setGrantType(" ");
		createAccessToken.submit();
		
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"400", 
				"Response Code is correct for blank value of grant type");	
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

