package com.psd2.tests.aisp.AccessTokenWithRefreshToken;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;

/**
 * Class Description : Generation of new (by-value) access token by using refresh token endpoint when TPP has provided invalid value of grant_type parameter.
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_ATWRT_012 extends TestBase{	
	
	@Test
	public void m_AISP_ATWRT_012() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of Response code for invalid value of grant type");
		createAccessToken.setBaseURL(apiConst.at_endpoint);
		createAccessToken.setRefreshToken(API_Constant.getAisp_RefreshToken());
		createAccessToken.setGrantType("access_token");
		createAccessToken.submit();
		
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"400", 
				"Response Code is correct for invalid grant type");	
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

