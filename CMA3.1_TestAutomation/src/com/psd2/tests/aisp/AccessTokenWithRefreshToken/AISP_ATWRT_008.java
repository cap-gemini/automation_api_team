package com.psd2.tests.aisp.AccessTokenWithRefreshToken;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status with CID/Secret are not in right format or invalid combination or not exists in the system 
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_ATWRT_008 extends TestBase{	
	
	@Test
	public void m_AISP_ATWRT_008() throws Throwable{	
		
		TestLogger.logStep("[Step 1] :Verification of response code for malformed values of client credentials");
		createAccessToken.setBaseURL(apiConst.at_endpoint);
		createAccessToken.setClientID(PropertyUtils.getProperty("client_id")+"123");
		createAccessToken.setRefreshToken(API_Constant.getAisp_RefreshToken());
		createAccessToken.submit();
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"401", 
				"Response Code is correct for malformed values of client credentials");	
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

