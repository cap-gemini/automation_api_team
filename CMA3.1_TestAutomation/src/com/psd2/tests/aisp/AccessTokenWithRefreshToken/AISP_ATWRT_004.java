package com.psd2.tests.aisp.AccessTokenWithRefreshToken;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;

/**
 * Class Description : Generation of new (by-value) access token by using refresh token endpoint when 1) Refresh Token is expired after 90 days OR 2) Used Invalid/malformed Refresh Token OR 3) No refresh Token has been sent
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_ATWRT_004 extends TestBase{	
	
	@Test
	public void m_AISP_ATWRT_004() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of Response code for blank value of refresh token");
		createAccessToken.setBaseURL(apiConst.at_endpoint);
		createAccessToken.setRefreshToken("");
		createAccessToken.submit();
		
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"400", 
				"Response Code is correct for blank refresh token");	
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of Response code for invalid refresh token");
		createAccessToken.setBaseURL(apiConst.at_endpoint);
		createAccessToken.setRefreshToken(API_Constant.getAisp_RefreshToken().replace(API_Constant.getAisp_RefreshToken().substring(0,3), "AZX"));
		createAccessToken.submit();
		
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"400", 
				"Response Code is correct for invalid refresh token");	
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

