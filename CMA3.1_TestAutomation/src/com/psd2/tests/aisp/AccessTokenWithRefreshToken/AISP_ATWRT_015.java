package com.psd2.tests.aisp.AccessTokenWithRefreshToken;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : TPP A uses the Refreshtoken of TPP B and send the request
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_ATWRT_015 extends TestBase{	
	
	@Test
	public void m_AISP_ATWRT_015() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of response code for expired refresh token");
		createAccessToken.setBaseURL(apiConst.at_endpoint);
		createAccessToken.setRefreshToken(apiConst.othertpp_RefreshToken);
		createAccessToken.submit();
		
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"400", 
				"Response Code is correct for generating access token using expired refresh token");	
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}

