package com.psd2.tests.aisp.AccessTokenWithRefreshToken;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;

/**
 * Class Description :Checking the request status through other than POST method with mandatory and optional fields.
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_ATWRT_006 extends TestBase{	
	
	@Test
	public void m_AISP_ATWRT_006() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of Response code for method other than POST");
		createAccessToken.setBaseURL(apiConst.at_endpoint);
		createAccessToken.setRefreshToken(API_Constant.getAisp_RefreshToken());
		createAccessToken.setMethod("PUT");
		createAccessToken.submit();
		
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"405", 
				"Response Code is correct for method other than post");	
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}

