package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of DateAndTime of field of ExpirationDateTime for removing Offset from time field
 * @author Kiran Dewangan
 * 
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"})
public class AISP_PAR_024 extends TestBase
{
	@Test
	public void m_AISP_PAR_024() throws Throwable
	{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();		
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of 'ExpirationDateTime' field with removing Offset");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setExpirationDateTime(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm"));
		
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when Expiration DateTime is provided without offset)");
				
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
	
}
