package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status when the value of x-fapi-financial-id is other than 0015800000jfQ9aAAE
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_PAR_013 extends TestBase
{
	@Test
	public void m_AISP_PAR_013() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
						
		TestLogger.logStep("[Step 2] : Account SetUp....");		
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id")+"123");
		accountSetup.setAllPermission();
		accountSetup.submit();
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"403", 
				"Response Code is correct for  Post Account request");
		TestLogger.logBlankLine();		

		testVP.testResultFinalize();			
	}	
}
