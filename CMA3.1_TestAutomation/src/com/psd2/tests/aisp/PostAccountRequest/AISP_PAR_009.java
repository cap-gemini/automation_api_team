package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Checking the request status with expired client credential token value in the Authorization (Access Token) header
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_PAR_009 extends TestBase{	
	
	@Test
	public void m_AISP_PAR_009() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verify the response when access token is expired");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+apiConst.expired_cc_accesstoken);
		accountSetup.setAllPermission();
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"401", 
				"Response Code is correct for expired access token");
		TestLogger.logBlankLine();
				
		testVP.testResultFinalize();			
	}	
}
