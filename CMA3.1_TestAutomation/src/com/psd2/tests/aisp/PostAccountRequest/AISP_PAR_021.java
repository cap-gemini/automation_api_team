package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of 'ExpirationDateTime' date field values
 * @author Kiran Dewangan
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"})
public class AISP_PAR_021 extends TestBase
{
	@Test
	public void m_AISP_PAR_021() throws Throwable
	{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Account SetUp....");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.submit();
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of 'ExpirationDateTime' format");		
		
		String expirationDate = accountSetup.getExpirationDateTime();
		TestLogger.logVariable("ExpirationDateTime : "+expirationDate);
		testVP.verifyTrue(Misc.verifyDateTimeFormat(expirationDate.split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(expirationDate.split("T")[1], "HH:mm:ss+00:00"),"ExpirationDateTime is as per expected format");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
