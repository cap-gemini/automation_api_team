package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of DateAndTime field of ExpirationDateTime  of removing Hours,Minutes and date from date and time field
 * @author Kiran Dewangan
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"})
public class AISP_PAR_025 extends TestBase
{
	@Test
	public void m_AISP_PAR_025() throws Throwable
	{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();		
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of 'ExpirationDateTime' field without hours");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		String dateM= (Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate(":mm+00:00"));
		accountSetup.setExpirationDateTime(dateM);
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when Expiration DateTime is provided without hours)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].Message")).trim(),"Error validating JSON. Error: - Provided value "+dateM+" is not compliant with the format datetime provided in rfc3339 for ExpirationDateTime"
				,"Error message is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of 'ExpirationDateTime' field without minuts");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		String dateH=(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH+00:00"));
		accountSetup.setExpirationDateTime(dateH);
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when Expiration DateTime is provided without minuts)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].Message")),"Error validating JSON. Error: - Provided value 2020-09-25T18+00:00 is not compliant with the format datetime provided in rfc3339 for ExpirationDateTime",
				"Error message is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of 'ExpirationDateTime' field without date");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		dateH="T"+Misc.nextDate("HH:mm+00:00");
		accountSetup.setExpirationDateTime(dateH);
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when Expiration DateTime is provided without date)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].Message")).trim(),"Error validating JSON. Error: - Provided value "+dateH+" is not compliant with the format datetime provided in rfc3339 for ExpirationDateTime",
				"Error message is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}	
}