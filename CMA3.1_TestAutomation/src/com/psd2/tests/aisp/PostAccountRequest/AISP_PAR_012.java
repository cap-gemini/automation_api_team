package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Checking the request status with no header value of x-fapi-financial-id ( header key is present, header value is not present).
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_PAR_012 extends TestBase{	
	
	@Test
	public void m_AISP_PAR_012() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();		
				
		TestLogger.logStep("[Step 2] : Account SetUp....");		
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.addHeaderEntry("x-fapi-financial-id", "");
		accountSetup.setAllPermission();
		accountSetup.submit();
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"403", 
				"Response Code is correct for Post account request");
		TestLogger.logBlankLine();		

		testVP.testResultFinalize();			
	}	
}
