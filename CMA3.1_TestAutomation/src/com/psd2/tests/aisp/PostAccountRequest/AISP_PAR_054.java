package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of 'Status' field value 
 * @author Kiran Dewangan
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"})
public class AISP_PAR_054 extends TestBase
{
	@Test
	public void m_AISP_PAR_054() throws Throwable
	{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Account SetUp....");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.submit();
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of 'Status' field value");
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct for post account request");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Data.Status")), "AwaitingAuthorisation", 
				"Mandatory field i.e Status is present and value is: "
						+ String.valueOf(accountSetup.getResponseValueByPath("Data.Status")));
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
