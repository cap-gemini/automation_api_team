package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of 'TransactionToDateTime' date field values 
 * @author Kiran Dewangan
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"})
public class AISP_PAR_030 extends TestBase
{
	@Test
	public void m_AISP_PAR_030() throws Throwable
	{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();		
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of 'TransactionToDateTime' format");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.submit();
			
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct");
		String txnToDateTime = accountSetup.getTransactionToDateTime();
		TestLogger.logVariable("TransactionToDateTime : "+txnToDateTime);
		testVP.verifyTrue(Misc.verifyDateTimeFormat(txnToDateTime.split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(txnToDateTime.split("T")[1], "HH:mm:ss+05:30"),"TransactionToDateTime is as per expected format");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
