package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of x-fapi-interaction-id value in the response when the value sent is in incorrect format
 * @author : Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_PAR_017 extends TestBase{	
	
	
	@Test
	public void m_AISP_PAR_017() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();	
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Account SetUp....");
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.addHeaderEntry("x-fapi-interaction-id",PropertyUtils.getProperty("inter_id")+"123");
		accountSetup.setAllPermission();		
		accountSetup.submit();
			
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct for Account SetUp");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Header.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].Message")),"invalid headers found in request","Error message is correct");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();			
	}	
}

