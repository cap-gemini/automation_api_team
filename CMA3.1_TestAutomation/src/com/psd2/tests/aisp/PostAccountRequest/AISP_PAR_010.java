package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status with CID/Secret are not in right format or invalid combination or not exists in the system 
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_PAR_010 extends TestBase{	
	
	@Test
	public void m_AISP_PAR_010() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verify status if malformed client id or client secret is used in Authorization header");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client_id")+"123,"
				+ "client_secret:123"+PropertyUtils.getProperty("client_secret"));
		accountSetup.setAllPermission();
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct for malformed values of CID/Secret");
		TestLogger.logBlankLine();
				
		testVP.testResultFinalize();			
	}	
}

