package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of DateAndTime field of 'TransactionToDateTime  of removing Hours,Minutes and date from date and time field
 * @author Kiran Dewangan
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"})
public class AISP_PAR_033 extends TestBase
{
	@Test
	public void m_AISP_PAR_033() throws Throwable
	{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();		
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of 'TransactionToDateTime' field without hours");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setTxnFromDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss"));
		String dateM= (Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate(":mm+00:00"));
		accountSetup.setTxnToDate(dateM);
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when TransactionToDateTime is provided without hours)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].Message")),"Error validating JSON. Error: - Provided value 2020-09-25T18:30:33 is not compliant with the format datetime provided in rfc3339 for TransactionFromDateTime","Error message is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of 'TransactionToDateTime' field without minuts");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setTxnFromDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss"));
		String dateH=(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH+00:00"));
		accountSetup.setTxnToDate(dateH);
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when TransactionToDateTime is provided without minuts)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of 'TransactionToDateTime' field without date");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setTxnFromDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss"));
		dateH=("T"+Misc.prevDate("HH+00:00"));
		accountSetup.setTxnToDate(dateH);
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when TransactionToDateTime is provided without date)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}