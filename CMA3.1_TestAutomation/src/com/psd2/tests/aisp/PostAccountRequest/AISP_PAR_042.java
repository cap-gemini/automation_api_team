package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Checking the response (UTF-8 character encoded) through POST method with mandatory and optional fields
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_PAR_042 extends TestBase
{
	@Test
	public void m_AISP_PAR_042() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Account SetUp....");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.submit();
		
		testVP.verifyStringEquals((accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct for Account SetUp");
		testVP.verifyTrue((accountSetup.getResponseValueByPath("Data"))!=null, 
				"All the mandatory fields are present in the response and not null"+(accountSetup.getResponseValueByPath("Data")));
		testVP.verifyTrue((accountSetup.getResponseValueByPath("Links"))!=null, 
				"All the optional fields are present in the response and not null"+(accountSetup.getResponseValueByPath("Links")));
		testVP.verifyTrue((accountSetup.getResponseValueByPath("Meta"))!=null, 
				"All the optional fields are present in the response and not null"+(accountSetup.getResponseValueByPath("Meta")));
		TestLogger.logBlankLine();

		testVP.testResultFinalize();			
	}	
}