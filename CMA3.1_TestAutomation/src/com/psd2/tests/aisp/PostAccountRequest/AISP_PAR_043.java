package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of fields included in the Data array
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_PAR_043 extends TestBase
{
	@Test
	public void m_AISP_PAR_043() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Account SetUp and verification if all fields are present in data array");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.submit();
		
		testVP.verifyStringEquals((accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct for Account SetUp");
		testVP.verifyTrue((accountSetup.getResponseValueByPath("Data.ConsentId"))!=null, 
				"Account request Id is present in the response and not null"+(accountSetup.getResponseValueByPath("Data.ConsentId")));
		testVP.verifyTrue((accountSetup.getResponseValueByPath("Data.Permissions"))!=null, 
				"Permissions are present in the response and not null"+(accountSetup.getResponseValueByPath("Data.Permissions")));
		testVP.verifyTrue((accountSetup.getResponseValueByPath("Data.CreationDateTime"))!=null, 
				"CreationDateTime is present in the response and not null"+(accountSetup.getResponseValueByPath("Data.CreationDateTime")));
		testVP.verifyTrue((accountSetup.getResponseValueByPath("Data.ExpirationDateTime"))!=null, 
				"ExpirationDateTime is present in the response and not null"+(accountSetup.getResponseValueByPath("Data.ExpirationDateTime")));
		testVP.verifyTrue((accountSetup.getResponseValueByPath("Data.Status"))!=null, 
				"Status is present in the response and not null"+(accountSetup.getResponseValueByPath("Data.Status")));
		testVP.verifyTrue((accountSetup.getResponseValueByPath("Data.TransactionToDateTime"))!=null, 
				"TransactionToDateTime is present in the response and not null"+(accountSetup.getResponseValueByPath("Data.TransactionToDateTime")));
		testVP.verifyTrue((accountSetup.getResponseValueByPath("Data.TransactionFromDateTime"))!=null, 
				"TransactionFromDateTime is present in the response and not null"+(accountSetup.getResponseValueByPath("Data.TransactionFromDateTime")));
		TestLogger.logBlankLine();

		testVP.testResultFinalize();			
	}	
}