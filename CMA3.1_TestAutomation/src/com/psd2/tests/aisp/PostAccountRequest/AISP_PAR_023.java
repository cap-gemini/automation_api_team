package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of date and time of ExpirationDateTime for without Seconds, without milliseconds and with 3 digit milliseconds  
 * @author Kiran Dewangan
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"})
public class AISP_PAR_023 extends TestBase
{
	@Test
	public void m_AISP_PAR_023() throws Throwable
	{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();		
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of 'ExpirationDateTime' field without second and without offset");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setExpirationDateTime(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm"));		
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when Expiration DateTime is provided without second and without offset)");
				
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of 'ExpirationDateTime' field without second and OffsetZ");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setExpirationDateTime(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm")+"Z");
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when Expiration DateTime is provided without second and OffsetZ)");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of 'ExpirationDateTime' field without second and Offset 00:00");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setExpirationDateTime(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm+00:00"));
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when Expiration DateTime is provided without second and offset 00:00)");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 5] : Verification of 'ExpirationDateTime' field without milliseconds and without offset");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setExpirationDateTime(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss"));		
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when Expiration DateTime is provided without milliseconds and without offset)");
		
		TestLogger.logBlankLine();
				
		TestLogger.logStep("[Step 6] : Verification of 'ExpirationDateTime' field without milliseconds and offsetZ");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setExpirationDateTime(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss")+"Z");
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct when Expiration DateTime is provided without milliseconds and offsetZ)");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 7] : Verification of 'ExpirationDateTime' field without milliseconds and offset 00:00");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setExpirationDateTime(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss+00:00"));
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct when Expiration DateTime is provided without milliseconds and offset 00:00)");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 8] : Verification of 'ExpirationDateTime' field with milliseconds and offset Z");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setExpirationDateTime(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss.SSS")+"Z");
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct when Expiration DateTime is provided with milliseconds and offset Z)");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 9] : Verification of 'ExpirationDateTime' field with milliseconds and without offset");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setExpirationDateTime(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss.SSS"));
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when Expiration DateTime is provided with milliseconds and without offset)");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 10] : Verification of 'ExpirationDateTime' field with milliseconds and offset 00:00");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setExpirationDateTime(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss.SSS+00:00"));
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct when Expiration DateTime is provided with milliseconds and offset 00:00)");
		TestLogger.logBlankLine();
				
		testVP.testResultFinalize();		
	}
	
	
}
