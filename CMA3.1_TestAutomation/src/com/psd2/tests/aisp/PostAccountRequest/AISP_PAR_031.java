package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of date and time of 'TransactionToDateTime for without Seconds, without milliseconds and with 3 digit milliseconds  
 * @author Kiran Dewangan
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"})
public class AISP_PAR_031 extends TestBase
{
	@Test
	public void m_AISP_PAR_031() throws Throwable
	{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();		
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of ''TransactionToDateTime' field without second and without offset");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setTxnFromDate(Misc.prevDate("yyyy-MM-dd")+"T"+Misc.prevDate("HH:mm:ss"));
		accountSetup.setTxnToDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm"));
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when 'TransactionToDateTime is provided without seconds and without offset)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of ''TransactionToDateTime' field without second and OffsetZ");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setTxnToDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm")+"Z");
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when 'TransactionToDateTime is provided without second and OffsetZ)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of ''TransactionToDateTime' field without second and Offset 00:00");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setTxnToDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm+00:00"));
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when 'TransactionToDateTime is provided without second and Offset 00:00)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 5] : Verification of ''TransactionToDateTime' field without milliseconds and without offset");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setTxnToDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss"));
		accountSetup.submit();
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when 'TransactionToDateTime is provided without milliseconds and without offset)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
	
		TestLogger.logBlankLine();
				
		TestLogger.logStep("[Step 6] : Verification of ''TransactionToDateTime' field without milliseconds and offsetZ");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setTxnToDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss")+"Z");
		accountSetup.submit();
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when 'TransactionToDateTime is provided without milliseconds and offsetZ)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 7] : Verification of ''TransactionToDateTime' field without milliseconds and offset 00:00");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setTxnToDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss+00:00"));
		accountSetup.submit();
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when 'TransactionToDateTime is provided without milliseconds and offset 00:00)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 8] : Verification of ''TransactionToDateTime' field with milliseconds and offset Z");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setTxnToDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss.SSS")+"Z");
		accountSetup.submit();
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when 'TransactionToDateTime is provided with milliseconds and offset Z)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 9] : Verification of ''TransactionToDateTime' field with milliseconds and without offset");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setTxnToDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss.SSS"));
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when 'TransactionToDateTime is provided with milliseconds and without offset)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 10] : Verification of ''TransactionToDateTime' field with milliseconds and offset 00:00");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setTxnToDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss.SSS+00:00"));
		accountSetup.submit();
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when 'TransactionToDateTime is provided with milliseconds and offset 00:00)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		
		TestLogger.logBlankLine();
				
		testVP.testResultFinalize();
	}
}