package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of backward compatibility for account setup with V3.0 contains only permissions supported by V2.0
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_PAR_058 extends TestBase{	
	
	@Test
	public void m_AISP_PAR_058() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
        TestLogger.logStep("[Step 2] :Verification of backward compatibility for account setup with V2.0 contains only permissions supported by V1.1");
		
        accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.submit();
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct for verification of backward compatibility for account setup with V3.0 contains only permissions supported by V2.0");
		
		testVP.testResultFinalize();		
	}
}