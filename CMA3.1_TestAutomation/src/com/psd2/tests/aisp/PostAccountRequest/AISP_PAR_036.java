package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of 'Permission' sets in the body are correct when only "ReadTransactionsCredits" or "ReadTransactionsDebits" permissions are entered
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_PAR_036 extends TestBase 
{
	@Test
	public void m_AISP_PAR_036() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Account SetUp....");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setCustomPermissions("ReadTransactionsCredits;ReadTransactionsDebits");
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct for Account SetUp when ReadTransactionsCredits & ReadTransactionsDebits permissions are passed in the request body");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Missing", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].Message")),"Required Permission in Setup Not Found","Error message is correct");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();			
	}	
}
