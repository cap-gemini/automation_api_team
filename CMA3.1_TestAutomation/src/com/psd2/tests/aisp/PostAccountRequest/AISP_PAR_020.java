package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of fields included in the Data array
 * @author Kiran Dewangan
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"})
public class AISP_PAR_020 extends TestBase
{
	@Test
	public void m_AISP_PAR_020() throws Throwable
	{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Account SetUp....");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"201", 
				"Response Code is correct for Account SetUp");
		TestLogger.logBlankLine();
				
		TestLogger.logStep("[Step 3] : Verification of fields included in the Data array");	
		
		testVP.verifyTrue((accountSetup.getResponseNodeStringByPath("Data.ConsentId"))!=null,
				"Mandatory field i.e Account Request Id is present and is not null"+(accountSetup.getResponseNodeStringByPath("Data.ConsentId")));
		testVP.verifyTrue((accountSetup.getResponseValueByPath("Data.Status"))!=null, 
				"Mandatory field i.e Status is present and is not null"+(accountSetup.getResponseValueByPath("Data.Status")));
		testVP.verifyTrue((accountSetup.getResponseValueByPath("Data.CreationDateTime"))!=null, 
				"Mandatory field i.e Creation Date Time is present and is not null"+(accountSetup.getResponseValueByPath("Data.CreationDateTime")));
		testVP.verifyTrue((accountSetup.getResponseValueByPath("Data.Permissions"))!=null, 
				"Mandatory field i.e Permissions is present and is not null"+(accountSetup.getResponseValueByPath("Data.Permissions")));
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
