package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of DateAndTime of field of 'TransactionToDateTime for removing Offset from time field
 * @author Kiran Dewangan
 *
 */
@Listeners({TestListener.class})
@Test(groups={"Regression"})
public class AISP_PAR_032 extends TestBase
{
	@Test
	public void m_AISP_PAR_032() throws Throwable
	{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();		
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of 'TransactionFromDateTime' field with removing Offset");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		accountSetup.setTxnFromDate(Misc.prevDate("yyyy-MM-dd")+"T"+Misc.prevDate("HH:mm:ss"));
		accountSetup.setTxnToDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm"));
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when TransactionToDateTime is provided without offset)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
	
}
