package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Checking the request status without Authorization (Access Token)
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_PAR_008 extends TestBase{	
	
	@Test
	public void m_AISP_PAR_008() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Checking the request status without Authorization (Access Token)");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setAllPermission();
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"401", 
				"Response Code is correct with no access token");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();			
	}	
}


