package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of 'Permission' sets in the body are correct when it contains only ReadTransactionsBasic 
 *                     and does not contain ReadTransactions Credits or ReadTransactionsDebits.
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_PAR_038 extends TestBase{	
	
	@Test
	public void m_AISP_PAR_038() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();	
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of 'Permission' sets in the body are correct when it contains only ReadTransactionsBasic");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setCustomPermissions("ReadAccountsBasic;ReadTransactionsBasic");
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct for Account setup when only ReadTransactionsBasic permission is used");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].Message")),"Permissions in account request is either empty or invalid","Error message is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();			

	}
}

