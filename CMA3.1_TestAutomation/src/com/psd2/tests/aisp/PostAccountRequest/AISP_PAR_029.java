package com.psd2.tests.aisp.PostAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.Misc;

/**
 * Class Description : Verification of DateAndTime field of 'TransactionFromDateTime  of removing Hours,Minutes and date from date and time field
 * @author Kiran Dewangan
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"})
public class AISP_PAR_029 extends TestBase
{
	@Test
	public void m_AISP_PAR_029() throws Throwable
	{	
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.submit();		
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of 'TransactionFromDateTime' field without hours");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		String dateM= (Misc.prevDate("yyyy-MM-dd")+"T"+Misc.prevDate(":mm+00:00"));
		accountSetup.setTxnFromDate(dateM);
		accountSetup.setTxnToDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss"));
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when TransactionFromDateTime is provided without hours)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of 'TransactionFromDateTime' field without minuts");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		String dateH=("T"+Misc.prevDate("HH+00:00"));
		accountSetup.setTxnFromDate(dateH);
		accountSetup.setTxnToDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss"));
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when TransactionFromDateTime is provided without minuts)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Verification of 'TransactionFromDateTime' field without date");
		
		accountSetup.setBaseURL(apiConst.as_endpoint);
		accountSetup.setHeadersString("Authorization:Bearer "+access_token);
		accountSetup.setAllPermission();
		dateH="T"+Misc.nextDate("HH:mm+00:00");
		accountSetup.setTxnFromDate(dateH);
		accountSetup.setTxnToDate(Misc.nextDate("yyyy-MM-dd")+"T"+Misc.nextDate("HH:mm:ss"));
		accountSetup.submit();
		
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseStatusCode()),"400", 
				"Response Code is correct when TransactionFromDateTime is provided without date)");
		testVP.verifyStringEquals(String.valueOf(accountSetup.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
	
}
