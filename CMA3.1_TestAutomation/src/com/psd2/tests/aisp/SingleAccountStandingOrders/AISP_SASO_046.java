package com.psd2.tests.aisp.SingleAccountStandingOrders;

import java.io.IOException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Reference field Under Data.StandingOrder should be Greater than 35 text value
 * @author Jasmin Patel
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression","Database"}, dependsOnGroups={"Pre_AISP_SASO"})
public class AISP_SASO_046 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("aisp_consent"));
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SASO_046() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Get account number");

		restRequest.setURL(apiConst.account_endpoint + API_Constant.getAisp_AccountId());
		restRequest.setHeadersString("Authorization:Bearer " + access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()), "200",
				"Response Code is correct for AccountId ");

		accountNumber = mongo.getFirstArrayObject("psuId:" + PropertyUtils.getProperty("usr_name") + ","
				+ "accountDetails.accountId:" + API_Constant.getAisp_AccountId(), "accountDetails", "accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		TestLogger.logBlankLine();

		TestLogger.logStep(
				"[Step 2] : Update Reference field value with equal to 40 characters for Account Number : "+ accountNumber);
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("collection_standingorder"));
		mongo.updateDocumentObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),
				"reference:7647345647636567467567475765874564582342345");
		TestLogger.logBlankLine();

		TestLogger.logStep(
				"[Step 7] : Verify the status code when standingOrderId field value with more than 40 characters");

		restRequest.setURL(apiConst.account_endpoint + API_Constant.getAisp_AccountId() + "/standing-orders");
		restRequest.setHeadersString("Authorization:Bearer " + access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()), "500",
				"Response Code is correct when reference field value with more than 40 characters");

		testVP.testResultFinalize();
	}

	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException {
		mongo.updateDocumentObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),"reference:123");
	}
}
