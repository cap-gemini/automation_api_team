package com.psd2.tests.aisp.SingleAccountStandingOrders;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request when the TPP is blocked for the given accountId
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SASO"})
public class AISP_SASO_036 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SASO_036() throws Throwable{
		
		
		TestLogger.logStep("[Step 1] : Valid AccountID but not associated with TPP originating the request");
		
		restRequest.setURL(apiConst.account_endpoint+"9e876899-d52d-4aff-9b7e-5a5634cc9aeb/standing-orders");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400",
				"Response Code is correct for account Id not associated with TPP originating the request");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();			
	}
}
