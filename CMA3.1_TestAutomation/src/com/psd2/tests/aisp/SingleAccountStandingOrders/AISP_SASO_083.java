package com.psd2.tests.aisp.SingleAccountStandingOrders;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of FinalPaymentAmount Array field 
 * @author Priya Chauhan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SASO"})
public class AISP_SASO_083 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SASO_001() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of FinalPaymentAmount Array field");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/"+"standing-orders");
		restRequest.setHeadersString("Authorization:Bearer "+access_token+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account standing-orders request");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.StandingOrder[].FinalPaymentAmount.Amount")).isEmpty(),
				"Mandatory field Amount is present in FinalPaymentAmount array");
		
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.StandingOrder[].FinalPaymentAmount.Currency")).isEmpty(),
				"Mandatory field Currency is present in FinalPaymentAmount array");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
}