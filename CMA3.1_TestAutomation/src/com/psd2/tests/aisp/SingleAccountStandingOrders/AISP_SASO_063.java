package com.psd2.tests.aisp.SingleAccountStandingOrders;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of FirstPaymentAmount field value under the Data/StandingOrder/FirstPaymentAmount
 * @author Priya Chauhan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SASO"})
public class AISP_SASO_063 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}	
	@Test
	public void m_AISP_SASO_063() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of FirstPaymentAmount field value under the Data/StandingOrder/FirstPaymentAmount");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/"+"standing-orders");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account standing order");
		
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.StandingOrder[].FirstPaymentAmount.Amount")).isEmpty(), 
				"Mandatory field Amount is present under FirstPaymentAmount array");
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.StandingOrder[].FirstPaymentAmount.Currency")).isEmpty(), 
				"Mandatory field Currency is present under FirstPaymentAmount array");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
}
