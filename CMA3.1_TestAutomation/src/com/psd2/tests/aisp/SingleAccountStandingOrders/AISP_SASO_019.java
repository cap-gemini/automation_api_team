package com.psd2.tests.aisp.SingleAccountStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description: Checking the request status with expired token value in the Authorization (Access Token) header
 * @author Jasmin Patel
 * 
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SASO"})
public class AISP_SASO_019 extends TestBase
{
	
	@Test
	public void m_AISP_SASO_019() throws Throwable{
		
		
		TestLogger.logStep("[Step 1] : Verify status if expired access token is used in Authorization header");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/standing-orders");
		restRequest.setHeadersString("Authorization:Bearer "+apiConst.expired_accesstoken+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", 
				"Response Code is correct for expired access token");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
