package com.psd2.tests.aisp.SingleAccountStandingOrders;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of mandatory and optional field for Account at xpath : /Data/StandingOrderArray 
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SASO"})
public class AISP_SASO_031 extends TestBase 
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SASO_031() throws Throwable{
	
		
		TestLogger.logStep("[Step 1] : Verification of fields included in Data array");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/standing-orders");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for incorrect Http request");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data").isEmpty()), 
				"Mandatory field i.e Data array is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].AccountId").isEmpty()), 
				"Mandatory field i.e AccountId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].StandingOrderId").isEmpty()), 
				"Optional field i.e StandingOrderId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].Frequency").isEmpty()), 
				"Mandetory field i.e Frequency is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].Reference").isEmpty()), 
				"Optional field i.e Reference is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].FirstPaymentDateTime").isEmpty()), 
				"Optional field i.e FirstPaymentDateTime is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].NextPaymentDateTime").isEmpty()), 
				"Optional field i.e NextPaymentDateTime is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].FinalPaymentDateTime").isEmpty()), 
				"Optional field i.e FinalPaymentDateTime is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].StandingOrderStatusCode").isEmpty()), 
				"Optional field i.e StandingOrderStatusCode is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].NextPaymentAmount").isEmpty()), 
				"Optional field i.e NextPaymentAmount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].NextPaymentAmount.Amount").isEmpty()), 
				"Mandetory field i.e Amount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].NextPaymentAmount.Currency").isEmpty()), 
				"Mandetory field i.e Currency is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].CreditorAgent").isEmpty()), 
				"Optional field i.e CreditorAgent is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].CreditorAgent.SchemeName").isEmpty()), 
				"Mandetory field i.e SchemeName is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].CreditorAgent.Identification").isEmpty()), 
				"Mandetory field i.e Identification is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].CreditorAccount").isEmpty()), 
				"Optional field i.e CreditorAccount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].CreditorAccount.SchemeName").isEmpty()), 
				"Mandetory field i.e SchemeName is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].CreditorAccount.Identification").isEmpty()), 
				"Mandetory field i.e Identification is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].CreditorAccount.Name").isEmpty()), 
				"Optional field i.e Name is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder[0].CreditorAccount.SecondaryIdentification").isEmpty()), 
				"Optional field i.e SecondaryIdentification is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Links").isEmpty()), 
				"Mandatory field i.e Links is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Meta").isEmpty()), 
				"Mandatory field i.e Meta is present and is not null");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
