package com.psd2.tests.aisp.SingleAccountStandingOrders;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description: Checking the request status with invalid value of Accept header.
 * @author Jasmin Patel
 * 
 */
@Listeners({TestListener.class})
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SASO"})
public class AISP_SASO_030 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SASO_030() throws Throwable{
		
	
		TestLogger.logStep("[Step 1] : Get Single account Standing Orders with no  x-fapi-financial-id header");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/standing-orders");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/xml");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"406", 
				"Response Code is correct for get single account standing orders request with no Authorization header");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}	
}
