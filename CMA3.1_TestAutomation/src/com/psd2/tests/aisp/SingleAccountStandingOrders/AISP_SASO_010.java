package com.psd2.tests.aisp.SingleAccountStandingOrders;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request where permissions are as required to access the Single Account Standing Orders API i.e. "ReadStandingOrdersDetail" in submitted POST Account Request & consent
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SASO_010 extends TestBase
{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	
	@Test
	public void m_AISP_SASO_010() throws Throwable{	
		
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsBasic;ReadStandingOrdersDetail",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 2] : Verification of request where permissions are as required to access the Single Account Standing Orders API i.e. ReadStandingOrdersDetail");	
			
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/"+"standing-orders");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account standing-orders request");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.StandingOrder").isEmpty()), 
				"Mandatory fields are present in the response");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Links").isEmpty()), 
				"Links field is present in the response");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Meta").isEmpty()), 
				"Meta field is present in the response");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
