package com.psd2.tests.aisp.SingleAccount;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status with no token value in the Authorization (Access Token) header 
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAI"})
public class AISP_SAI_019 extends TestBase{	
	
	@Test
	public void m_AISP_SAI_019() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Get Single account Info with blank value of access token");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId());
		restRequest.setHeadersString("Authorization:Bearer");
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", 
				"Response Code is correct for get single account request with blank value of access token");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}	
}
