package com.psd2.tests.aisp.SingleAccount;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Deny Authorisation of AISP Consent Renewal flow
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SAI_098 extends TestBase
{
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_AISP_SAI_098() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsDetail",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Go to URL and re-authenticate consent");	
		startDriverInstance();
		consentOps.renewAISPConsent(consentDetails.get("ConsentURL"),false,true);		
		closeDriverInstance();
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verify the consent status ");
		restRequest.setURL(apiConst.as_endpoint+"/"+consentDetails.get("consentId"));
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Content-Type", "application/json");
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(),"200", 
				"Response Code is correct for get account request "+restRequest.getResponseStatusCode());
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Data.Status"),"Rejected", 
				"Consent is successfully rejected");
	
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
