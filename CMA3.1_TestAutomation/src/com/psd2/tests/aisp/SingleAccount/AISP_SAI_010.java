package com.psd2.tests.aisp.SingleAccount;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request where permissions are as required to access the Single Account Information API i.e. "ReadAccountsBasic" in submitted POST Account Request & consent.
 * @author Mohit Patidar
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SAI_010 extends TestBase{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_AISP_SAI_010() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsBasic",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request where permissions are as required to access the Single Account Information API i.e. ReadAccountsBasic in submitted POST Account Request");
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId"));
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
			"Response Code is correct for the request ");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Data.Account[0].AccountId"),consentDetails.get("accountId"), 
			"Mandatory field i.e AccountId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].Currency").isEmpty()), 
			"Mandatory field i.e Currency is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].AccountType").isEmpty()), 
			"Mandatory field i.e AccountType is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].AccountSubType").isEmpty()), 
				"Mandatory field i.e AccountSubType is present and is not null");
		
		testVP.verifyTrue(restRequest.getResponseValueByPath("Data.Account[0].Account")==null, 
				"Optional field i.e. Account under Data/Account is not present");
		
		testVP.verifyTrue(restRequest.getResponseValueByPath("Data.Account[0].Servicer")==null, 
				"Optional field i.e. Servicer under Data/Account is not present");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].Nickname").isEmpty()), 
				"Optional field i.e Nickname is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].Description").isEmpty()), 
				"Optional field i.e. Description is present and value is not null");
		
		testVP.verifyStringEquals(restRequest.getResponseHeader("Content-Type"), "application/json;charset=UTF-8", 
				"Response is UTF-8 character encoded");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}	
}
