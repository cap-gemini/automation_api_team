package com.psd2.tests.aisp.SingleAccount;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status with invalid or null value of x-fapi-customer-ip-address in header.
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAI"})
public class AISP_SAI_030 extends TestBase{	
		
		@BeforeClass
		public void loadTestData() throws Throwable{
			
			access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
					PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
			testVP.verifyTrue(access_token != null,	"Access token is not null");
		}
		
		@Test
		public void m_AISP_SAI_030() throws Throwable{
			
			TestLogger.logStep("[Step 1] : Get Single account Info with invalid value of x-fapi-customer-ip-address");	
			restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId());
			restRequest.setHeadersString("Authorization:Bearer "+access_token);
			restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
			restRequest.addHeaderEntry("x-fapi-customer-ip-address", "asdasdasd");
			restRequest.addHeaderEntry("Accept", "application/json");
			restRequest.setMethod("GET");
			restRequest.submit();
			testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
					"Response Code is correct for get single account request with invalid value of x-fapi-customer-ip-address");
			
			testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
					"Error code for the response is correct i.e. '"+restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			
			testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Errors[0].Message").equals("Invalid value found in x-fapi-customer-ip-address header"), 
					"Message for error code is '"+restRequest.getResponseNodeStringByPath("Errors[0].Message")+"'");
			
			
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : Get Single account Info with null value of x-fapi-customer-ip-address");	
			restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId());
			restRequest.setHeadersString("Authorization:Bearer "+access_token);
			restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
			restRequest.addHeaderEntry("x-fapi-customer-ip-address", "");
			restRequest.addHeaderEntry("Accept", "application/json");
			restRequest.setMethod("GET");
			restRequest.submit();
			testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
					"Response Code is correct for get Single account request with null value of x-fapi-customer-ip-address");
			testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", 
					"Error code for the response is correct i.e. '"+restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode")+"'");
			
			testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Errors[0].Message").equals("Invalid value found in x-fapi-customer-ip-address header"), 
					"Message for error code is '"+restRequest.getResponseNodeStringByPath("Errors[0].Message")+"'");
			
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();		
		}	
	}
