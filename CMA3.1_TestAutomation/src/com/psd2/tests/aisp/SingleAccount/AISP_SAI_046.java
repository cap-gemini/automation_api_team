package com.psd2.tests.aisp.SingleAccount;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of value for AccountSubType field
 * @author Mohit Patidar
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAI"})
public class AISP_SAI_046 extends TestBase {
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAI_046() throws Throwable{	 
		
		TestLogger.logStep("[Step 1] : Verification of value of AccountSubType field");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId());
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
	
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for single account information API request");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Account[0].AccountSubType").equals("ChargeCard") 
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].AccountSubType").equals("CreditCard") 
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].AccountSubType").equals("CurrentAccount")
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].AccountSubType").equals("EMoney") 
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].AccountSubType").equals("Loan") 
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].AccountSubType").equals("Mortgage")
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].AccountSubType").equals("PrePaidCard") 
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].AccountSubType").equals("Savings"), 
				"AccountSubType field value is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
