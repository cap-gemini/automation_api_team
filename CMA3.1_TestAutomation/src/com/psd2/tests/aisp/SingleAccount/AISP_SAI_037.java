package com.psd2.tests.aisp.SingleAccount;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of AccountId field value more than 40 characters or invalid or blank value
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression", "Database"},dependsOnGroups={"Pre_AISP_SAI"})
public class AISP_SAI_037 extends TestBase {
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAI_037() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verify the response when accountid field value is more than 40 characters");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"12sdf");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct when accountid field value is more than 40 characters");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors.ErrorCode"), 
				"[UK.OBIE.Resource.NotFound]", "Error code for the response is correct i.e. UK.OBIE.Resource.NotFound");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Errors.Message").isEmpty()), 
				"Message for error code is '"+restRequest.getResponseNodeStringByPath("Errors.Message")+"'");
		
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verify the response when accountid field value is invalid");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"wer");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct when accountid field value invalid");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors.ErrorCode"), 
				"[UK.OBIE.Resource.NotFound]", "Error code for the response is correct i.e. UK.OBIE.Resource.NotFound");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Errors.Message").isEmpty()), 
				"Message for error code is '"+restRequest.getResponseNodeStringByPath("Errors.Message")+"'");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
