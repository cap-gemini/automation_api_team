package com.psd2.tests.aisp.SingleAccount;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Verification of AISP Consent Renewal flow
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Sanity"})
public class AISP_SAI_096 extends TestBase
{
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_AISP_SAI_096() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsDetail",false,false,false,false,"Obj_CMA1_Account",false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Go to URL and re-authenticate consent");
		startDriverInstance();
		String renew_AuthCode= consentOps.renewAISPConsent(consentDetails.get("ConsentURL"),false,false);
		closeDriverInstance();
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 3] : Get access and refresh token");	
		accesstoken.setBaseURL(apiConst.at_endpoint);
		accesstoken.setAuthCode(renew_AuthCode);
		accesstoken.submit();
		
		testVP.verifyStringEquals(String.valueOf(accesstoken.getResponseStatusCode()),"200", 
				"Response Code is correct for get access token request");	
		access_token = accesstoken.getAccessToken();
		refresh_token = accesstoken.getRefreshToken();
		TestLogger.logVariable("Access Token : " + access_token);
		TestLogger.logVariable("Refresh Token : " + refresh_token);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Get Accound Id");	
		getAccount.setBaseURL(apiConst.account_endpoint);
		getAccount.setHeadersString("Authorization:Bearer "+access_token);
		getAccount.submit();
		
		testVP.verifyStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"200", 
				"Response Code is correct for get accounts request");	
		testVP.verifyStringEquals(String.valueOf(getAccount.getAccountId()),consentDetails.get("accountId"), 
				"Account Id matches after consent renewal");

		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
