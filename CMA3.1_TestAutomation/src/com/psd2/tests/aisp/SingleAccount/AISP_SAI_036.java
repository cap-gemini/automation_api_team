package com.psd2.tests.aisp.SingleAccount;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of AccountId field value
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAI"})
public class AISP_SAI_036 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAI_036() throws Throwable{	
		
		
		TestLogger.logStep("[Step 1] : Verify AccountId field value");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId());
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account request");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].AccountId").isEmpty()), 
				"AccountId field value is present and not null");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Account[0].AccountId").length()<=40, 
				"AccountId field is correct i.e. less than or equal to 40");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Data.Account[0].AccountId"), API_Constant.getAisp_AccountId(), 
				"AccountId value is same as sent in the request");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
}