package com.psd2.tests.aisp.SingleAccount;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status with invalid or null value of x-fapi-financial-id
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SAI"})
public class AISP_SAI_025 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SAI_025() throws Throwable{	 
		
		TestLogger.logStep("[Step 1] : Verif the request when x-fapi-financial-id value is invalid in header");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId());
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", "jhsadhjkhasjkh");
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"403", 
				"Response Code is correct for get single account request");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verif the request when x-fapi-financial-id value is null in header");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId());
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", "");
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"403", 
				"Response Code is correct for get single account request");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
}
