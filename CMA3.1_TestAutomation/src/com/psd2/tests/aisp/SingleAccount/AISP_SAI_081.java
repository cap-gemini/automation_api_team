package com.psd2.tests.aisp.SingleAccount;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Self field in the links section.
 * @author Mohit Patidar
 *
 */


@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAI"})
public class AISP_SAI_081 extends TestBase 
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAI_081() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verification of HTTP Response Code i.e. 200 & value in the Self field");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId());
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for request sent successfully");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseNodeStringByPath("Links.Self")), apiConst.account_endpoint+API_Constant.getAisp_AccountId(), 
				"Mandatory field i.e Self field is present and is not null");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
