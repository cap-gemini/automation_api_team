package com.psd2.tests.aisp.SingleAccount;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request where permissions are as required to access the Single Account Information API i.e. "ReadAccountsDetail" in submitted POST Account Request & consent.
 * @author Mohit Patidar
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SAI_011 extends TestBase{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_AISP_SAI_011() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsDetail",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request where permissions are as required to access the Single Account Information API i.e. ReadAccountsDetail in submitted POST Account Request");
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId"));
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is 200 OK ");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Data.Account[0].AccountId"),consentDetails.get("accountId"), 
				"Mandatory field i.e AccountId is present and is not null");
			
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].Currency").isEmpty()), 
				"Mandatory field i.e Currency is present and is not null");
			
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].AccountType").isEmpty()), 
				"Mandatory field i.e AccountType is present and is not null");
			
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].AccountSubType").isEmpty()), 
				"Mandatory field i.e AccountSubType is present and is not null");
			
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].Nickname").isEmpty()), 
				"Optional field i.e Nickname is present and is not null");

		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].Description").isEmpty()), 
				"Optional field i.e. Description is present and value is not null");
			
		testVP.verifyTrue(restRequest.getResponseValueByPath("Data.Account[0].Account[0]")!=null, 
				"Optional field i.e Account under Data/Account is present and is not null");
			
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].Servicer").isEmpty()), 
				"Optional field i.e Servicer under Data/Account is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName").isEmpty()), 
				"Optional field i.e SchemeName of account is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].Identification").isEmpty()), 
				"Optional field i.e Identification of account is present and is not null");
		
		testVP.verifyStringEquals(restRequest.getResponseHeader("Content-Type"),
				"application/json;charset=UTF-8", "Response is UTF-8 character encoded");
		
		TestLogger.logBlankLine();
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
}
