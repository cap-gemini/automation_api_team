package com.psd2.tests.aisp.SingleAccount;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;
	
/**
* Class Description : Verification of valid accountRequestId where status of Account Request Resource, Consent and Access Token is Revoked
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SAI_009 extends TestBase{	
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test	
	public void m_AISP_SAI_009() throws Throwable{
	
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent(null,false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : To revoke the consent");
		
		revokeConsent.setBaseURL(apiConst.as_endpoint+"/"+consentDetails.get("consentId"));
		revokeConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token")+",client_id:"+PropertyUtils.getProperty("client_id")+","
				+ "client_secret:"+PropertyUtils.getProperty("client_secret"));
		revokeConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(revokeConsent.getResponseStatusCode()),"204", 
				"Access token is revoked successfully");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verify of valid accountRequestId where status of Account Request Resource, Consent and Access Token is Revoked");	
		
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId"));
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"403", 
				"Response Code is correct for valid accountRequestId where status of Account Request Resource, Consent and Access Token is Revoked for get single account request");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
		
}
