package com.psd2.tests.aisp.SingleAccount;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request when SchemeName field not present in Beckend
 * @author Mohit Patidar
 *
 */

@Listeners(TestListener.class)
@Test(groups={"Regression", "Database"}, dependsOnGroups={"Pre_AISP_SAI"})
public class AISP_SAI_058 extends TestBase {

	@BeforeClass
	public void loadTestData() throws Throwable{
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token !=null, "Access token is not null");
	}
	
	@Test
	public void m_AISP_SAI_58() throws Throwable
	{
		TestLogger.logStep("[Step 1] : Get account number");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId());
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "200", "Response code is correct for request");
		TestLogger.logBlankLine();
		
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+API_Constant.getAisp_AccountId(),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number = "+accountNumber);
		TestLogger.logStep("[Step 2] : Delete SchemeName field from database for account nuber "+accountNumber);
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_account"));
		mongo.removeArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"account",0,"schemeName");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verify the response when SchemeName field not present in database");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId());
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "500", 
				"Response code is correct when SchemeName field not present in database");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		mongo.updateArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"account",0,"schemeName",PropertyUtils.getProperty("DrAccount_SchemeName"));
	}
}