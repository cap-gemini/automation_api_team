package com.psd2.tests.aisp.SingleAccountBeneficiaries;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of backward compatibility for account setup with V3.0 contains only permissions supported by V3.0 and single account beneficiaries with V2.0
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SABF_094 extends TestBase
{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_AISP_SABF_094() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent(null,false,false,false,true,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of backward compatibility for account setup with V3.0 contains only permissions supported by V3.0 and get multi account with V3.0");	
		
		restRequest.setURL(apiConst.backcomp_account_endpoint+consentDetails.get("accountId")+"/beneficiaries");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get Single account beneficiaries with 3.0 version of account request and single account beneficiaries url and permissions of 3.1");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
}
