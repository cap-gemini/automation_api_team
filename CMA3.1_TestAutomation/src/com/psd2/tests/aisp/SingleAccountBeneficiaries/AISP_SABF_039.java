package com.psd2.tests.aisp.SingleAccountBeneficiaries;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of fields included in Data array
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SABF"})
public class AISP_SABF_039 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABF_039() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Verification of fields included in Data array");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/beneficiaries");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for incorrect Http request");
		
		testVP.verifyTrue(restRequest.getResponseValueByPath("Data")!=null, 
				"Mandatory field i.e Data array is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[0].AccountId").isEmpty()), 
				"Optional field i.e AccountId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[0].BeneficiaryId").isEmpty()), 
				"Optional field i.e BeneficiaryId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[0].Reference").isEmpty()), 
				"Optional field i.e Reference is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[0].CreditorAgent").isEmpty()), 
				"Optional field i.e CreditorAgent is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[0].CreditorAccount").isEmpty()), 
				"Optional field i.e CreditorAccount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Links").isEmpty()), 
				"Mandatory field i.e Links is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Meta").isEmpty()), 
				"Mandatory field i.e Meta is present and is not null");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
}
