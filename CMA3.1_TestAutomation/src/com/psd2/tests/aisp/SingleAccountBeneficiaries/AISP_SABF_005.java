package com.psd2.tests.aisp.SingleAccountBeneficiaries;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through GET method with mandatory and optional fields. 
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SABF"})
public class AISP_SABF_005 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABF_005() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Checking the request (UTF-8 character encoded) status through GET method with mandatory and optional fields");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/beneficiaries");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary").isEmpty()), 
				"Mandatory field i.e Beneficiary is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Links").isEmpty()), 
				"Mandatory field i.e Links is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Links.Self").isEmpty()), 
				"Mandatory field i.e Self is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Meta").isEmpty()), 
				"Mandatory field i.e Meta is present and is not null");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct through GET method with mandatory and optional fields");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
}
