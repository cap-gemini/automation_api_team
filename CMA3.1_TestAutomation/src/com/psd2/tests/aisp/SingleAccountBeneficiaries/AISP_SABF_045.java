package com.psd2.tests.aisp.SingleAccountBeneficiaries;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of SchemeName under data.CreditorAgent field in the response
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SABF"})
public class AISP_SABF_045 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABF_045() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Verification of SchemeName under data.CreditorAgent field in the response");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/beneficiaries");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for incorrect Http request");
	
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[].CreditorAgent").isEmpty()), 
				"Mandatory field i.e CreditorAgent is present under Beneficiary and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[].CreditorAgent[].SchemeName").isEmpty()), 
				"Optional field i.e Schemename in CreditorAgent array is present and is not null");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
}
