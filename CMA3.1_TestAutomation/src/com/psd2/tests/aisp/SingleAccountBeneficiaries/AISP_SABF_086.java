package com.psd2.tests.aisp.SingleAccountBeneficiaries;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;
/**
 * Class Description : Verification of Identification field length upto 256 character max
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression", "Database"},dependsOnGroups={"Pre_AISP_SABF"})
public class AISP_SABF_086 extends TestBase{	

	@BeforeClass
	public void loadTestData() throws Throwable{
		
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	@Test
	public void m_AISP_SABF_086() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Get Account Id");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId());
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account beneficiaries");
		
		
		TestLogger.logBlankLine();
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+API_Constant.getAisp_AccountId(),"accountDetails","accountNumber");
		
		TestLogger.logVariable("Account Number: "+accountNumber);
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("collection_beneficiary"));
		
		TestLogger.logStep("[Step 2] : Update identification field length is upto 256 character for Account Number : "+accountNumber);	
		
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditorAccount.identification:asusgjfgjfgjgfjgdfjsgjgdfjgfdjgdfggfdshjvvvdfjjfdjgdfkgfdkgkfdjgkjdgkjgdkjgjkhdgkjgjkhhkdjghkghdkjghkghkghkghkhgkhgkhgkjjghkhgkhghglkhglhlghlghlghljghlhglhlghlghghhghghghhgihgihoighiohgoihgoihgodhoghdodghodhgodhdgohoighdoihgoihioghoighoighhgohgoihgghhgohgh");

		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 3] : Verify the status code when creditorAccount identification is upto 256  ");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/beneficiaries");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct when creditorAccount identification field length is upto 256 characters");
		
		testVP.testResultFinalize();	
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditorAccount.identification:IE31BOFIXXXXXXXX345678");
	}	
}