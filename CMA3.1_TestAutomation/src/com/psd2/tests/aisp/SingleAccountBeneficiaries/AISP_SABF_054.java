package com.psd2.tests.aisp.SingleAccountBeneficiaries;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of PostalAddress array under data.CreditorAgent field
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SABF"})
public class AISP_SABF_054 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABF_054() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Verification of PostalAddress array under data.CreditorAgent field");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/beneficiaries");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for incorrect Http request");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[].CreditorAgent[].PostalAddress").isEmpty()), 
				"Mandatory field i.e PostalAddress is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[].CreditorAgent[].PostalAddress[].AddressType").isEmpty()), 
				"Optional field i.e AddressType is present in PostalAddress array and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[].CreditorAgent[].PostalAddress[].Department").isEmpty()), 
				"Optional field i.e Department is present in PostalAddress array and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[].CreditorAgent[].PostalAddress[].SubDepartment").isEmpty()), 
				"Optional field i.e SubDepartment is present in PostalAddress array and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[].CreditorAgent[].PostalAddress[].StreetName").isEmpty()), 
				"Optional field i.e StreetName is present in PostalAddress array and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[].CreditorAgent[].PostalAddress[].BuildingNumber").isEmpty()), 
				"Optional field i.e BuildingNumber is present in PostalAddress array and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[].CreditorAgent[].PostalAddress[].PostCode").isEmpty()), 
				"Optional field i.e PostCode is present in PostalAddress array and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[].CreditorAgent[].PostalAddress[].TownName").isEmpty()), 
				"Optional field i.e TownName is present in PostalAddress array and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[].CreditorAgent[].PostalAddress[].CountrySubDivision").isEmpty()), 
				"Optional field i.e CountrySubDivision is present in PostalAddress array and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[].CreditorAgent[].PostalAddress[].Country").isEmpty()), 
				"Optional field i.e Country is present in PostalAddress array and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[].CreditorAgent[].PostalAddress[].AddressLine").isEmpty()), 
				"Optional field i.e AddressLine is present in PostalAddress array and is not null");
		
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
}
