package com.psd2.tests.aisp.SingleAccountBeneficiaries;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status when accountId is exists but it doesn't have the beneficiaries 
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SABF"})
public class AISP_SABF_028 extends TestBase{	
	

	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABF_028() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Verify the response when account does not have Beneficiaries");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_EmptyAccountId()+"/beneficiaries");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", "0015800000jfQ9aAAE");
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account beneficiaries request");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
}
