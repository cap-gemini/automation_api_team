package com.psd2.tests.aisp.SingleAccountBeneficiaries;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of CreditorAccount array under data field
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SABF"})
public class AISP_SABF_075 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABF_075() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Verification of CreditorAccount array under data field");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/beneficiaries");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for get account beneficiaries");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[0].CreditorAccount").isEmpty()), 
				"Mandatory field i.e CreditorAccount array is present in the response");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[0].CreditorAccount.SchemeName").isEmpty()), 
				"Mandatory field i.e SchemeName is present in CreditorAccount array");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[0].CreditorAccount.Identification").isEmpty()), 
				"Mandatory field i.e Identification is present in CreditorAccount array");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[0].CreditorAccount.Name").isEmpty()), 
				"Optional field i.e Name is present in CreditorAccount array");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary[0].CreditorAccount.SecondaryIdentification").isEmpty()), 
				"Optional field i.e SecondaryIdentification is present in CreditorAccount array");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
}
