package com.psd2.tests.aisp.SingleAccountBeneficiaries;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request when ReadBeneficiariesBasic permission is NOT submitted,ReadBeneficiariesDetail permission is submitted 
                       in POST Account Request & Consent to access the Single Account Beneficiaries API
 * @author Abhimanyu Sawant
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SABF_014 extends TestBase{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_AISP_SABF_014() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsDetail;ReadBalances;ReadBeneficiariesDetail",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request where permissions are as required to access the Single Account Information API i.e. ReadAccountsBasic in submitted POST Account Request");
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/beneficiaries");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Beneficiary.CreditorAccount").isEmpty()), 
				"Mandatory field i.e CreditorAccount Array is present and is not null");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is 200 when permissions set contains only ReadBeneficiariesDetail");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}	
}
