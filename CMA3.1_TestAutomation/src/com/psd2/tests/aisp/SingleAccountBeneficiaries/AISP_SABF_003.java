package com.psd2.tests.aisp.SingleAccountBeneficiaries;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of AccountId as mandatory while accessing Single Account Beneficiaries API 
 * @author Abhimanyu Sawant
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SABF"})
public class AISP_SABF_003 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SABF_003() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Verification of AccountId as mandatory while accessing Single Account Beneficiaries API");	
		restRequest.setURL(apiConst.account_endpoint+"beneficiaries");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct if AccountId is absent");
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors.ErrorCode"), "[UK.OBIE.Resource.NotFound]", "error code is correct i.e.[UK.OBIE.Resource.NotFound]");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Errors.Message").isEmpty()), "error message is is present i.e.IntentId Validation Error");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
}
