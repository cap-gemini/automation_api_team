package com.psd2.tests.aisp.SingleAccountBeneficiaries;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request when following permissions
                       - ReadBeneficiariesBasic 
                       - ReadBeneficiariesDetail  
                       are submitted in POST Account Request & Consent to access the Single Account Beneficiaries API
 * @author Abhimanyu Sawant
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SABF_012 extends TestBase{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();

	@Test
	public void m_AISP_SABF_012() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsDetail;ReadBalances;ReadBeneficiariesBasic;ReadBeneficiariesDetail",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of request when following permissions ReadBeneficiariesBasic,ReadBeneficiariesDetail are submitted in POST Account Request & Consent to access the Single Account Beneficiaries API");
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/beneficiaries");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is 200 OK when permissions set contains ReadBeneficiariesBasic and ReadBeneficiariesDetail");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}	
}
