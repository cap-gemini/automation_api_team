package com.psd2.tests.aisp.MultiAccountInfo;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Varification of value of SchemeName field
 * @author Mohit Patidar
 *
 */

@Listeners(TestListener.class)
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_MAI"})
public class AISP_MAI_047 extends TestBase{

	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token !=null, "Access token is not null");
	}
	
	@Test
	public void m_AISP_MAI_047() throws Throwable
	{
		TestLogger.logStep("[Step 1] : Verification of value for SchemeName field");
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()), 
				"200", "Response code is correct for the request");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName").length()<=40, 
				"Schemename field length is correct i.e. less than or equal to 40 characters");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName").equals("UK.OBIE.SortCodeAccountNumber") 
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName").equals("UK.OBIE.SortCodeAccountNumber") 
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName").equals(PropertyUtils.getProperty("DrAccount_SchemeName"))
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName").equals(PropertyUtils.getProperty("DrAccount_SchemeName")) 
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName").equals("UK.OBIE.PAN") 
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName").equals("UK.OBIE.PAN")
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName").equals("UK.OBIE.Paym") 
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName").equals("UK.OBIE.BBAN") 
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName").equals("UK.OBIE.any.bank.scheme1")
				|| restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName").equals("UK.OBIE.any.bank.scheme2"), 
				"SchemeName field value is correct i.e. "+restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName"));
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
}
