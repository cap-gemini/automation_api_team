package com.psd2.tests.aisp.MultiAccountInfo;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;

/**
 * Class Description : Verification of creation of access token by using malformed refresh token value
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_MAI"})
public class AISP_MAI_083 extends TestBase{	
	
	@Test
	public void m_AISP_MAI_083() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Generate new access token using existing refresh token");	
		createAccessToken.setBaseURL(apiConst.rt_endpoint);
		createAccessToken.setRefreshToken(API_Constant.getAisp_RefreshToken().replace(API_Constant.getAisp_RefreshToken().substring(0, 1), "KL"));
		createAccessToken.submit();
		testVP.verifyStringEquals(String.valueOf(createAccessToken.getResponseStatusCode()),"400", 
			"Response Code is correct for creating new access token using malformed refresh token");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
}