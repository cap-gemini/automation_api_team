package com.psd2.tests.aisp.MultiAccountInfo;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Links field   
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_MAI"})
public class AISP_MAI_074 extends TestBase 
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_MAI_074() throws Throwable{	
	
		TestLogger.logStep("[Step 1] : Verification of Links field with all mandatory headers");	
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct with verified Links field");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Links").isEmpty()), 
				"Links field is present and is not null");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
}
