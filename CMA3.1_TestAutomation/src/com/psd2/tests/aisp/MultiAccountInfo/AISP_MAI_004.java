package com.psd2.tests.aisp.MultiAccountInfo;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through GET method with mandatory and optional fields. 
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_MAI"})
public class AISP_MAI_004 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_MAI_004() throws Throwable{	
	
		TestLogger.logStep("[Step 1] : Verification of request (UTF-8 character encoded) status through GET method with mandatory and optional fields");	
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.addHeaderEntry("x-fapi-customer-ip-address",PropertyUtils.getProperty("cust_ip_add"));
		restRequest.addHeaderEntry("x-fapi-customer-last-logged-time",PropertyUtils.getProperty("cust_last_log_time"));
		restRequest.addHeaderEntry("x-fapi-interaction-id",PropertyUtils.getProperty("inter_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct through GET method with mandatory and optional fields");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].AccountId").isEmpty()), 
				"Mandatory field i.e AccountId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].Currency").isEmpty()), 
				"Mandatory field i.e Currency is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].Nickname").isEmpty()), 
				"Mandatory field i.e Nickname is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].SchemeName").isEmpty()), 
				"Mandatory field i.e SchemeName is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Account[0].Account[0].Identification").isEmpty()), 
				"Mandatory field i.e Identification is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Links.Self").isEmpty()), 
				"Mandatory field i.e Self in Links field is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Meta.TotalPages").isEmpty()), 
				"Mandatory field i.e TotalPages in Meta field is present and is not null");
		
		testVP.verifyStringEquals(restRequest.getResponseHeader("Content-Type"),"application/json;charset=UTF-8", "Response is UTF-8 character encoded");
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
}

