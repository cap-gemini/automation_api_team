package com.psd2.tests.aisp.MultiAccountInfo;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Name field when field length is greater than 70 characters or Blank value
 * @author Mohit Patidar
 *
 */

@Listeners(TestListener.class)
@Test(groups={"Regression", "Database"}, dependsOnGroups={"Pre_AISP_MAI"})
public class AISP_MAI_061 extends TestBase {

	@BeforeClass
	public void loadTestData() throws Throwable{
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token !=null, "Access token is not null");
	}
	
	@Test
	public void m_AISP_MAI_061() throws Throwable
	{
		TestLogger.logStep("[Step 1] : Get account number");
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "200", "Response code is correct for request");
		TestLogger.logBlankLine();
		
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+API_Constant.getAisp_AccountId(),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number = "+accountNumber);
		TestLogger.logStep("[Step 2] : Update Name field value as more than 70 text in database for account nuber "+accountNumber);
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_account"));
		mongo.updateArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"account",0,"name","AnythingAnythingAnythingAnythingAnythingAnythingAnythingAnythingAnythiq");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verify the response when Name field length is more than 70 text");
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "500", "Response code is correct when Name field length is more than 70 text");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Update Name field value as blank in database for account nuber "+accountNumber);
		mongo.updateArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"account",0,"name","");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 5] : Verify the response when Name field value is null");
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "500", "Response code is correct when Name field value is null");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		mongo.updateArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"account",0,"name","Avinash");
	}
}