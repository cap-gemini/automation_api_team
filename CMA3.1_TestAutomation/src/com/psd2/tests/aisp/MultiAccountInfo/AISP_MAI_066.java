package com.psd2.tests.aisp.MultiAccountInfo;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of SchemeName  field as BICFI under the servicer also check the SchemeName field must be BICFI and at least 35 text
 * @author Mohit Patidar
 *
 */
@Listeners(TestListener.class)
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_MAI"})
public class AISP_MAI_066 extends TestBase {
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token !=null, "Access token is not null");
	}
	
	@Test
	public void m_AISP_MAI_066() throws Throwable
	{
		TestLogger.logStep("[Step 1] : Verification of SchemeName field value under servicer");
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "200", "Response code is correct for request");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Account[0].Servicer.SchemeName").equals("BICFI"), "SchemeName field value is correct i.e. BICFI");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Account[0].Servicer.SchemeName").length()<=40, "Schemename field length is correct i.e. less than or equal to 40 characters");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
}
