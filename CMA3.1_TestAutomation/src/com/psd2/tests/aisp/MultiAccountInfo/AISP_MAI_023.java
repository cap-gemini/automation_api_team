package com.psd2.tests.aisp.MultiAccountInfo;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of x-fapi-interaction-id value in the response if not sent in the request
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_MAI"})
public class AISP_MAI_023 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}

	@Test
	public void m_AISP_MAI_023() throws Throwable{
	
		TestLogger.logStep("[Step 1] : Get Multiple account Info for checking x-fapi-interaction-id value in the response if the value is not sent in the request");
		
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for multiple account request the x-fapi-interaction-id is present in the response if not set in the request header");
		
		testVP.verifyTrue(restRequest.getResponseHeader("x-fapi-interaction-id")!=null, 
				"Response header value is not empty for : x-fapi-interaction-id. Value - "+restRequest.getResponseHeader("x-fapi-interaction-id"));
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}	
}
