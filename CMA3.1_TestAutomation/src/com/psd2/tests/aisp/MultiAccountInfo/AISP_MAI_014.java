package com.psd2.tests.aisp.MultiAccountInfo;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status without Authorization (Access Token)
 * @author Mohit Patidar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_MAI"})
public class AISP_MAI_014 extends TestBase
{
	@Test
	public void m_AISP_MAI_014() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Verify the response when access token is not passed in the request header");	
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"401", 
				"Response Code is correct without access token in the header");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
}
