package com.psd2.tests.aisp.MultiAccountInfo;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status with invalid value of scope. Access token having value of scope other than 'accounts'
 * @author Mohit Patidar
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_MAI_017 extends TestBase{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_AISP_MAI_017() throws Throwable{
		TestLogger.logStep("[Step 1] : Generate access token with scope as payment");
		apiUtility.pispAccessToken=true;
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Get multi account info with access token of pisp consent");	

		getAccount.setBaseURL(apiConst.account_endpoint);
		getAccount.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		getAccount.submit();
		
		testVP.verifyStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"403", 
				"Response Code is correct for scope other than accounts");	
		
		testVP.testResultFinalize();		
	}	
}
