package com.psd2.tests.aisp.MultiAccountInfo;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;


/**
 * Class Description : Verification of Currency field With Standard Value
 * @author Mohit Patidar
 *
 */

@Listeners(TestListener.class)
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_MAI"})
public class AISP_MAI_030 extends TestBase {

	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token !=null, "Access token is not null");
	}
	
	@Test
	public void m_AISP_MAI_030() throws Throwable
	{
		TestLogger.logStep("[Step 1] : Verification of Currency field value");
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "200", "Response Code is correct for Currency("+restRequest.getResponseNodeStringByPath("Data.Account[0].Currency")+") field which is according to ISO 4217 standards");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Account[0].Currency").length()==3, "Currency field value is correct");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Account[0].Currency").equals(restRequest.getResponseNodeStringByPath("Data.Account[0].Currency").toUpperCase()), "Currency field is in Capital letters");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
}
