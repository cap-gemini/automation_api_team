package com.psd2.tests.aisp.MultiAccountInfo;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of AccountId field value
 * @author Mohit Patidar
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_MAI"})
public class AISP_MAI_029 extends TestBase {
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token !=null, "Access token is not null");
	}
	
	@Test
	public void m_AISP_MAI_029() throws Throwable
	{
		TestLogger.logStep("[Step 1] : Verification of AccountId field value");
		restRequest.setURL(apiConst.account_endpoint);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()), "200", "Response code is correct for the request");
		accountId=restRequest.getResponseNodeStringByPath("Data.Account.AccountId[0]");
		testVP.verifyTrue(!(accountId.isEmpty()), "AccountId("+accountId+") is present and not null");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verification of AccountId field length");
		testVP.verifyTrue(accountId.length()<=40, "AccountId field length is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}

}
