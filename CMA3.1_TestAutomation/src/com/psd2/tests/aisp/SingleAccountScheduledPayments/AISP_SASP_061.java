package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of SchemeName under data.CreditorAgent field in the response
 * @author : Rama Arora
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression","Database"},dependsOnGroups={"Pre_AISP_SASP"})
public class AISP_SASP_061 extends TestBase
{

	@BeforeClass
	public void loadTestData() throws Throwable{
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SASP_061() throws Throwable{		
		
		TestLogger.logStep("[Step 1] : Update SchemeName field value as blank in database for account nuber "+accountNumber);
		accountNumber = mongo.getFirstArrayObject("psuId:" + PropertyUtils.getProperty("usr_name") + ","+ "accountDetails.accountId:"+ API_Constant.getAisp_AccountId(), "accountDetails","accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_schedulepayments"));
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditorAgent.schemeName:BICFI");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verify the Scheme Name under Creditor Agent array");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct");	
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAgent.SchemeName"),"UK.OBIE.BICFI",
				"Scheme Name field is correct");
		
		testVP.testResultFinalize();		
	}
}
