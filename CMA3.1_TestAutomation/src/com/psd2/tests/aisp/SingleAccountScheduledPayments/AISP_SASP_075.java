package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the value of MANDATORY CreditorAccount/Identification field when SchemeName = UK.OBIE.PAN or PAN
 * @author Rama Arora
 *
 */

@Listeners(TestListener.class)
@Test(groups={"Regression","Database"}, dependsOnGroups={"Pre_AISP_SASP"})
public class AISP_SASP_075 extends TestBase {
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token !=null, "Access token is not null");
	}
	
	@Test
	public void m_AISP_SASP_075() throws Throwable
	{
		TestLogger.logStep("[Step 1] : Get account number");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "200", "Response code is correct for request");
		TestLogger.logBlankLine();
		
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+API_Constant.getAisp_AccountId(),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number = "+accountNumber);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Update SchemeName field value as PAN and identification field value as IE85BOFI90120412345679 in database for account nuber "+accountNumber);
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_schedulepayments"));
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditorAccount.schemeName:PAN");
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditorAccount.identification:IE85BOFI90120412345679");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verify the Identification field value when SchemeName field value is PAN");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "200", "Response code is correct when SchemeName field value is PAN");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").equals("UK.OBIE.PAN"), "SchemeName field value is correct");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.Identification").equals(PropertyUtils.getProperty("DrAccount_Identification")), "Identification field value is correct");
		TestLogger.logBlankLine();
		

		TestLogger.logStep("[Step 4] : Update SchemeName field value as UK.OBIE.PAN in database for account nuber "+accountNumber);
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditorAccount.schemeName:UK.OBIE.PAN");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 5] : Verify the Identification field value when SchemeName field value is UK.OBIE.PAN");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "200", "Response code is correct when SchemeName field value is UK.OBIE.PAN");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").equals("UK.OBIE.PAN"), "SchemeName field value is correct");
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.Identification").equals(PropertyUtils.getProperty("DrAccount_Identification")), "Identification field value is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber+",psuId:"+PropertyUtils.getProperty("usr_name"),"creditorAccount.schemeName:UK.OBIE.IBAN");
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber+",psuId:"+PropertyUtils.getProperty("usr_name"),"creditorAccount.identification:23605490179017");
	}

}
