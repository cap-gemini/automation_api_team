package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request when permission ReadScheduledPaymentsBasic is submitted but ReadScheduledPaymentsDetail is not submitted in POST Account Request & Consent to access the Single Account Scheduled Payments API 
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SASP_013 extends TestBase
{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	
	@Test
	public void m_AISP_SASP_013() throws Throwable{	
		
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsBasic;ReadScheduledPaymentsBasic",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 2] : Verification of request when permission ReadScheduledPaymentsBasic is submitted but ReadScheduledPaymentsDetail is not submitted in POST Account Request & Consent to access the Single Account Scheduled Payments API");	
		
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for the given permissions");

		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].AccountId").isEmpty()), 
				"Mandatory field i.e AccountId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].ScheduledPaymentId").isEmpty()), 
				"Optional field i.e ScheduledPaymentId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].ScheduledPaymentDateTime").isEmpty()), 
				"Mandatory field i.e ScheduledPaymentDateTime is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].ScheduledType").isEmpty()), 
				"Mandatory field i.e ScheduledType is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].InstructedAmount").isEmpty()), 
				"Mandatory field i.e InstructedAmount is present and is not null");
		
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount"))==null, 
				"Mandatory field i.e CreditorAccount is not present");

		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Links.Self").isEmpty()), 
				"Mandatory field i.e Self is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Meta.TotalPages").isEmpty()), 
				"Mandatory field i.e Meta is present and is not null");
		
		testVP.verifyTrue((restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAgent"))==null, 
				"Optional field i.e CreditorAgent is not present");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
