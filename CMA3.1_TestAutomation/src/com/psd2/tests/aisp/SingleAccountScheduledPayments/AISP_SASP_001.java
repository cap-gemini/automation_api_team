package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of CMA compliance for Single Account Scheduled Payments URI    
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Pre_AISP_SASP","Sanity"})
public class AISP_SASP_001 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SASP_001() throws Throwable{	 
		
		TestLogger.logStep("[Step 1] : Verify the open banking standard and get Single Account Scheduled Payments");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account scheduled-payments request");
		testVP.verifyStringEquals(restRequest.getURL(),apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments", 
				"URI for GET Single account scheduled-payments is as per open banking standard");
		
		TestLogger.logBlankLine();

		testVP.testResultFinalize();		
	}
	
}
