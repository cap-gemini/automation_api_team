package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of date and time of ScheduledPaymentDateTime
 * for without Seconds, without milliseconds and with 3 digit milliseconds and removed Offset
 * @author : Rama Arora
 *
 */
@Listeners({TestListener.class})
@Test(groups={"Regression","Database"},dependsOnGroups={"Pre_AISP_SASP"})
public class AISP_SASP_041 extends TestBase{
	
	@BeforeClass
	public void loadTestData() throws Throwable {
		
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint,API_Constant.aisp_RefreshToken,PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null, "Access token is not null");
	}

	@Test
	public void m_AISP_SASP_041() throws Throwable {
		
		TestLogger.logStep("[Step 1] : Get account number");

		restRequest.setURL(apiConst.account_endpoint+ API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer " + access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()), "200","Response Code is correct for get single account scheduled-payments request");
		accountNumber = mongo.getFirstArrayObject("psuId:" + PropertyUtils.getProperty("usr_name") + ","+ "accountDetails.accountId:"+ API_Constant.getAisp_AccountId(), "accountDetails","accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		
		TestLogger.logBlankLine();

		TestLogger.logStep("Step[2] : Update ScheduledPaymentDateTime field value without seconds & removed offset 00:00 for Account Number "+ accountNumber);
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_schedulepayments"));
		mongo.updateDocumentObject("accountNumber:" + accountNumber + ",psuId:"+ PropertyUtils.getProperty("usr_name"),"scheduledPaymentDateTime:2018-05-05T07#07");

		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 3] : Verify the status code when ScheduledPaymentDateTime field value without seconds & removed offset 00:00");

		restRequest.setURL(apiConst.account_endpoint+ API_Constant.getAisp_AccountId() + "/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer " + access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()), "500","Response Code is correct when ScheduledPaymentDateTime field value is giving without seconds & removed offset 00:0");

		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Update ScheduledPaymentDateTime value without milliseconds and removed offset for Account Number "+ accountNumber);
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"scheduledPaymentDateTime:1981-03-20T07#07#07");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"500",
				"Response Code is correct for scheduledPaymentDateTime field without milliseconds and without offset");
	
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 5] : Update ScheduledPaymentDateTime value with milliseconds and without offset and verify the response code for ScheduledPaymentDateTime");
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"previousPaymentDateTime:1981-03-20T07#07#07.777");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"500",
				"Response Code is correct for ScheduledPaymentDateTime field with milliseconds and without offset");
	
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber+",psuId:"+PropertyUtils.getProperty("usr_name"),"scheduledPaymentDateTime:2018-05-05T00#00#00+00#00");
	}
	

}