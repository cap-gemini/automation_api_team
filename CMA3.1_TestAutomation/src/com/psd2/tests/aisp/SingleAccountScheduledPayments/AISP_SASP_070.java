package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of the values of  CreditorAccount/SchemeName field having OB defined values
 * @author : Rama Arora
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SASP"})
public class AISP_SASP_070 extends TestBase
{

	@BeforeClass
	public void loadTestData() throws Throwable{
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token !=null, "Access token is not null");
	}
	
	@Test
	public void m_AISP_SASP_070() throws Throwable
	{
		TestLogger.logStep("[Step 1] : Verification of value for SchemeName field");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()), "200", "Response code is correct for the request");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").length()<=40, 
				"Schemename field length is correct i.e. less than or equal to 40 characters");
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").equals("UK.OBIE.SortCodeAccountNumber") 
				|| restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").equals("UK.OBIE.SortCodeAccountNumber") 
				|| restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").equals(PropertyUtils.getProperty("DrAccount_SchemeName"))
				|| restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").equals(PropertyUtils.getProperty("DrAccount_SchemeName")) 
				|| restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").equals("UK.OBIE.PAN") 
				|| restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").equals("UK.OBIE.PAN")
				|| restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").equals("UK.OBIE.Paym") 
				|| restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").equals("UK.OBIE.BBAN") 
				|| restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme1")
				|| restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").equals("UK.OBIE.any.bank.scheme2"), 
				"SchemeName field value is correct i.e. "+restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName"));
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
}
