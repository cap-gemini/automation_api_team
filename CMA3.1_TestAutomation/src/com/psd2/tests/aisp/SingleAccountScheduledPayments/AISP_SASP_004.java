package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Account Id as mandatory in endpoint URI while accessing Single Account Scheduled Payments API
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SASP"})
public class AISP_SASP_004 extends TestBase
{
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	
	@Test
	public void m_AISP_SASP_004() throws Throwable{	 
		
		TestLogger.logStep("[Step 1] : Verification of Account Id as mandatory in endpoint URI while accessing Single Account Scheduled Payments API");	
		
		restRequest.setURL(apiConst.account_endpoint+"scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400", 
				"Response Code is correct for get single account scheduled-payments request when Account Id is not provided in the url");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors.ErrorCode"), "[UK.OBIE.Resource.NotFound]", "Error Code are matched");
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors.Message"), "[IntentId Validation Error]", "Error Message are matched");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
