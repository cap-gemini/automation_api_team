package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of SecondaryIdentification outside the range 1-34 characters under data.CreditorAccount field
 * @author : Rama Arora
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression","Database"},dependsOnGroups={"Pre_AISP_SASP"})
public class AISP_SASP_089 extends TestBase
{

	@BeforeClass
	public void loadTestData() throws Throwable{
		
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("aisp_consent"));
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SASP_089() throws Throwable{	
	
		
		TestLogger.logStep("[Step 1] : Get account number");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for AccountId ");
		
		accountNumber = mongo.getFirstArrayObject("psuId:" + PropertyUtils.getProperty("usr_name") + ","+ "accountDetails.accountId:"+ API_Constant.getAisp_AccountId(), "accountDetails","accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		TestLogger.logBlankLine();	
		
		TestLogger.logStep("[Step 2] : Update SecondaryIdentification field value with more than 34 characters for Account Number : "+accountNumber);
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_schedulepayments"));
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber+",psuId:"+PropertyUtils.getProperty("usr_name"),"creditorAccount.secondaryIdentification:TestUserTestUserTestUserTestUserTestUserTestUserTestUserTestUser");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verify the status code when SecondaryIdentification field value with more than 34 characters");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"500", 
				"Response Code is correct when SecondaryIdentification field value with more than 34 characters");		

		testVP.testResultFinalize();		
	}
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber+",psuId:"+PropertyUtils.getProperty("usr_name"),"creditorAccount.secondaryIdentification:54988");

	}
}
