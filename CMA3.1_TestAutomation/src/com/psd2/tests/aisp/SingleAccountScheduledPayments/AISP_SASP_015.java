package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request for CreditorAccount when following permissions ReadScheduledPaymentsDetail are submitted but ReadScheduledPaymentsBasic is not submitted in POST Account Request & Consent to access the Single Account Scheduled Payments API 
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SASP_015 extends TestBase
{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	
	@Test
	public void m_AISP_SASP_015() throws Throwable{	
		
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsBasic;ReadScheduledPaymentsDetail",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 2] : Verification of request for CreditorAccount when following permissions ReadScheduledPaymentsBasic, ReadScheduledPaymentsDetail are submitted in POST Account Request & Consent to access the Single Account Scheduled Payments API");	
		
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token")+",client_id:"+PropertyUtils.getProperty("client_id")+",client_secret:"+PropertyUtils.getProperty("client_secret"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for the given permissions");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment.AccountId").isEmpty()), 
				"Mandatory field i.e AccountId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment.ScheduledPaymentDateTime").isEmpty()), 
				"Mandatory field i.e ScheduledPaymentDateTime is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment.ScheduledType").isEmpty()), 
				"Mandatory field i.e ScheduledType is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment.InstructedAmount").isEmpty()), 
				"Mandatory field i.e InstructedAmount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Links.Self").isEmpty()), 
				"Mandatory field i.e Self is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Meta.TotalPages").isEmpty()), 
				"Mandatory field i.e Meta is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment.CreditorAccount").isEmpty()), 
				"Optional field i.e CreditorAccount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment.ScheduledPaymentId").isEmpty()), 
				"Optional field i.e ScheduledPaymentId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment.Reference").isEmpty()), 
				"Optional field i.e Reference is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment.CreditorAgent").isEmpty()), 
				"Optional field i.e CreditorAgent is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment.CreditorAccount.SchemeName").isEmpty()), 
				"Array field under CreditorAcount i.e SchemeName is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment.CreditorAccount.Identification").isEmpty()), 
				"Array field under CreditorAcount i.e Identification is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment.CreditorAccount.Name").isEmpty()), 
				"Array field under CreditorAcount i.e Name is present and is not null");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
