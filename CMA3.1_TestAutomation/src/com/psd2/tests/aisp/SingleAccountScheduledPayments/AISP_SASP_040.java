package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of date and time of ScheduledPaymentDateTime
 * for without Seconds, without milliseconds and with 3 digit milliseconds.
 * 
 * @author : Rama Arora
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression","Database"},dependsOnGroups={"Pre_AISP_SASP"})
public class AISP_SASP_040 extends TestBase {

	@BeforeClass
	public void loadTestData() throws Throwable {
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint,API_Constant.aisp_RefreshToken,PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null, "Access token is not null");
	}

	@Test
	public void m_AISP_SASP_040() throws Throwable {
		
		TestLogger.logStep("[Step 1] : Get account number");
		restRequest.setURL(apiConst.account_endpoint+ API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer " + access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()), "200","Response Code is correct for get single account scheduled-payments request");
		accountNumber = mongo.getFirstArrayObject("psuId:" + PropertyUtils.getProperty("usr_name") + ","+ "accountDetails.accountId:"+ API_Constant.getAisp_AccountId(), "accountDetails","accountNumber");
        TestLogger.logVariable("Account Number : " + accountNumber);
		
        TestLogger.logBlankLine();

		TestLogger.logStep("Step[2] : Update ScheduledPaymentDateTime field value without seconds & add offset 00:00 for Account Number "+ accountNumber);
        mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("collection_schedulepayments"));
        mongo.updateDocumentObject("accountNumber:" + accountNumber + ",psuId:"+ PropertyUtils.getProperty("usr_name"),"scheduledPaymentDateTime:2018-05-05T07#07+00#00");

		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 3] : Verify the status code when ScheduledPaymentDateTime field value without seconds & add offset 00:00");
		restRequest.setURL(apiConst.account_endpoint+ API_Constant.getAisp_AccountId() + "/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer " + access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()), "200","Response Code is correct when ScheduledPaymentDateTime field value is giving without seconds & add offset 00:00");

		TestLogger.logBlankLine();

		TestLogger.logStep("Step[4] : Update ScheduledPaymentDateTime field value without seconds & add offset Z for Account Number "+ accountNumber);
        mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_schedulepayments"));
        mongo.updateDocumentObject("accountNumber:" + accountNumber + ",psuId:"+ PropertyUtils.getProperty("usr_name"),"scheduledPaymentDateTime:2018-05-05T07#07Z");

		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 5] : Verify the status code when ScheduledPaymentDateTime field value without seconds & add offset Z");
		restRequest.setURL(apiConst.account_endpoint+ API_Constant.getAisp_AccountId() + "/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer " + access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200","Response Code is correct when ScheduledPaymentDateTime field value is giving without seconds & add offset Z");

		TestLogger.logBlankLine();

		TestLogger.logStep("Step[6] : Update ScheduledPaymentDateTime field value without milliseconds & add offset 00:00 for Account Number "+ accountNumber);

		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("collection_schedulepayments"));

		mongo.updateDocumentObject("accountNumber:" + accountNumber + ",psuId:"+ PropertyUtils.getProperty("usr_name"),"scheduledPaymentDateTime:2018-05-05T07#07#07+00#00");

		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 7] : Verify the status code when ScheduledPaymentDateTime field value without milliseconds & add offset 00:00");

		restRequest.setURL(apiConst.account_endpoint+ API_Constant.getAisp_AccountId() + "/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer " + access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200","Response Code is correct when ScheduledPaymentDateTime field value is giving without milliseconds & add offset 00:00");
		
		TestLogger.logBlankLine();

		TestLogger.logStep("Step[8] : Update ScheduledPaymentDateTime field value without milliseconds & offset Z for Account Number "+ accountNumber);

		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_schedulepayments"));

		mongo.updateDocumentObject("accountNumber:" + accountNumber + ",psuId:"+ PropertyUtils.getProperty("usr_name"),"scheduledPaymentDateTime:2018-05-05T07#07#07Z");

		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 9] : Verify the status code when ScheduledPaymentDateTime field value without milliseconds & offset Z");

		restRequest.setURL(apiConst.account_endpoint+ API_Constant.getAisp_AccountId() + "/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer " + access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200","Response Code is correct when ScheduledPaymentDateTime field value is giving without milliseconds & offset Z");

		TestLogger.logBlankLine();
		
		TestLogger.logStep("Step[10] : Update ScheduledPaymentDateTime field value With 3 digit milliseconds & offset Z for Account Number "+ accountNumber);

		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_schedulepayments"));

		mongo.updateDocumentObject("accountNumber:" + accountNumber + ",psuId:"+ PropertyUtils.getProperty("usr_name"),"scheduledPaymentDateTime:2018-05-05T06#06#06.777Z");

		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 11] : Verify the status code when ScheduledPaymentDateTime field value With 3 digit milliseconds & offset Z");

		restRequest.setURL(apiConst.account_endpoint+ API_Constant.getAisp_AccountId() + "/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer " + access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200","Response Code is correct when ScheduledPaymentDateTime field value is giving With 3 digit milliseconds & offset Z");

		TestLogger.logBlankLine();
		
		TestLogger.logStep("Step[12] : Update ScheduledPaymentDateTime field value With 3 digit milliseconds & offset 00:00 for Account Number "+ accountNumber);

		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("collection_schedulepayments"));

		mongo.updateDocumentObject("accountNumber:" + accountNumber + ",psuId:"+ PropertyUtils.getProperty("usr_name"),"scheduledPaymentDateTime:2018-05-05T06#06#06.777+00#00");

		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 13] : Verify the status code when ScheduledPaymentDateTime field value With 3 digit milliseconds & offset 00:00");

		restRequest.setURL(apiConst.account_endpoint+ API_Constant.getAisp_AccountId() + "/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer " + access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id",PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200","Response Code is correct when ScheduledPaymentDateTime field value is giving With 3 digit milliseconds & offset 00:00");

		TestLogger.logBlankLine();
	}

	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		
		mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber+",psuId:"+PropertyUtils.getProperty("usr_name"),"scheduledPaymentDateTime:2018-05-05T00#00#00+00#00");
	}
}
