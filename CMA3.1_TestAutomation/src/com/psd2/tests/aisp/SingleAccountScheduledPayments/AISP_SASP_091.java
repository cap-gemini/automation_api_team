package com.psd2.tests.aisp.SingleAccountScheduledPayments;
import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of  SchemeName removed from CreditorAccount from backend
 * @author Rama Arora
 *
 */

@Listeners(TestListener.class)
@Test(groups={"Regression","Database"}, dependsOnGroups={"Pre_AISP_SASP"})
public class AISP_SASP_091 extends TestBase{
	
	    @BeforeClass
	    public void loadTestData() throws Throwable{
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token !=null, "Access token is not null");
	}
	    
	    @Test
		public void m_AISP_SASP_091() throws Throwable
		{
			TestLogger.logStep("[Step 1] : Get account number");
			restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
			restRequest.setHeadersString("Authorization:Bearer "+access_token);
			restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
			restRequest.addHeaderEntry("Accept", "application/json");
			restRequest.setMethod("GET");
			restRequest.submit();
			testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "200", "Response code is correct for request");
			TestLogger.logBlankLine();
			
			accountNumber = mongo.getFirstArrayObject("psuId:" + PropertyUtils.getProperty("usr_name") + ","+ "accountDetails.accountId:"+ API_Constant.getAisp_AccountId(), "accountDetails","accountNumber");
			TestLogger.logVariable("Account Number : " + accountNumber);
			
			TestLogger.logStep("[Step 2] : Removed schemeName from CreditorAccount block from database for account number "+accountNumber);
			mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_schedulepayments"));
			mongo.removeDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditorAccount.schemeName");
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 3] : Verify the response when schemeName from CreditorAccount block removed from DataBase");
			restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
			restRequest.setHeadersString("Authorization:Bearer "+access_token);
			restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
			restRequest.addHeaderEntry("Accept", "application/json");
			restRequest.setMethod("GET");
			restRequest.submit();
			
			testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "500", "Response code is correct when schemeName removed from CreditorAccount block from DataBase");
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();
}
	    @AfterClass
		public void revokeTestData() throws IOException, InterruptedException{
			mongo.updateDocumentObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"creditorAccount.schemeName:UK.OBIE.IBAN");

		}
	    
}
