package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request (UTF-8 character encoded) status through GET method with mandatory and optional fields.
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SASP"})
public class AISP_SASP_005 extends TestBase
{
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	
	@Test
	public void m_AISP_SASP_005() throws Throwable{
	

		TestLogger.logStep("[Step 1] : Get Single account scedule-payments with GET Method");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+ "/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.addHeaderEntry("x-fapi-customer-ip-address",PropertyUtils.getProperty("cust_ip_add"));
		restRequest.addHeaderEntry("x-fapi-customer-last-logged-time",PropertyUtils.getProperty("cust_last_log_time"));
		restRequest.addHeaderEntry("x-fapi-interaction-id",PropertyUtils.getProperty("inter_id"));
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for HTTP method with GET");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].AccountId").isEmpty()), 
				"Mandatory field i.e AccountId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].ScheduledPaymentDateTime").isEmpty()), 
				"Mandatory field i.e ScheduledPaymentDateTime is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].ScheduledType").isEmpty()), 
				"Mandatory field i.e ScheduledType is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].InstructedAmount").isEmpty()), 
				"Mandatory field i.e InstructedAmount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].InstructedAmount.Amount").isEmpty()), 
				"Mandatory field i.e Amount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].InstructedAmount.Currency").isEmpty()), 
				"Mandatory field i.e Currency is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAgent").isEmpty()), 
				"Optional field i.e CreditorAgent of account is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAgent.SchemeName").isEmpty()), 
				"Mandatory field i.e SchemeName of CreditorAgent is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAgent.Identification").isEmpty()), 
				"Mandatory field i.e Identification of CreditorAgent is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount").isEmpty()), 
				"Optional field i.e CreditorAccount of account is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.SchemeName").isEmpty()), 
				"Mandatory field i.e SchemeName under CreditorAccount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].CreditorAccount.Identification").isEmpty()), 
				"Mandatory field i.e Identification under CreditorAccount is present and is not null");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
