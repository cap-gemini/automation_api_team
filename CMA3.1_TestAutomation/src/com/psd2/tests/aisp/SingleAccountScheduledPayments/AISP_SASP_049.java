package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of InstructedAmount array under ScheduledPayment
 * @author : Rama Arora
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SASP"})
public class AISP_SASP_049 extends TestBase
{

	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SASP_049() throws Throwable{		
		
		TestLogger.logStep("[Step 1] : Verify the fields under InstructedAmount array");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct");	
		
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].InstructedAmount.Amount").isEmpty(),
				"Amount field is not empty : "+restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].InstructedAmount.Amount"));
		testVP.verifyTrue(!restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].InstructedAmount.Currency").isEmpty(),
				"Currency field is not empty : "+restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].InstructedAmount.Currency"));
			
		testVP.testResultFinalize();		
	}
}
