package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request when accountId does not exist i.e. accountId format is correct but it is not available into database.
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SASP"})
public class AISP_SASP_019 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	
	@Test
	public void m_AISP_SASP_019() throws Throwable{
		
	TestLogger.logStep("[Step 1] : Verification of request when accountId does not exist");	
	
	restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"1234"+"/scheduled-payments");
	restRequest.setHeadersString("Authorization:Bearer "+access_token);
	restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
	restRequest.addHeaderEntry("Accept", "application/json");
	restRequest.setMethod("GET");
	restRequest.submit();
	testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400",
			"Response Code is correct for account Id not exisiting in the database");
	
	testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors.ErrorCode"), "[UK.OBIE.Resource.NotFound]", "Error Code are matched");
	testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors.Message"), "[IntentId Validation Error]", "Error Message are matched");
	
	TestLogger.logBlankLine();
	testVP.testResultFinalize();
	}
}
