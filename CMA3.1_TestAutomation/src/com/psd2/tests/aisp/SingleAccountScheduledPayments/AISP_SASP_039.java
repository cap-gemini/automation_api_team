package com.psd2.tests.aisp.SingleAccountScheduledPayments;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of ScheduledPaymentDateTime under ScheduledPayment
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SASP"})
public class AISP_SASP_039 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SASP_039() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Checking the value of 'Content-type' in response header");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/scheduled-payments");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single scheduled-payments request");
		
		
		
		testVP.verifyTrue(Misc.verifyDateTimeFormat(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].ScheduledPaymentDateTime").split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(restRequest.getResponseNodeStringByPath("Data.ScheduledPayment[0].ScheduledPaymentDateTime").split("T")[1], "HH:mm:ss+00:00"), 
				"ScheduledPaymentDateTime is as per expected format");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();		
	}
	
}
