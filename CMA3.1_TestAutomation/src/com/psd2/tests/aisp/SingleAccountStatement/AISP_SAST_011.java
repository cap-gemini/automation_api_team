package com.psd2.tests.aisp.SingleAccountStatement;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request to get the transactions of statement using valid statement Id with requires permission i.e. "ReadTransactionsBasic", "ReadTransactionsDetail", "ReadTransactionsCredits" and "ReadTransactionsDebits" permission should be present in consent..
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SAST_011 extends TestBase
{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_AISP_SAST_011() throws Throwable{	
		
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent(null,false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 2] : Verification of request when following permissions are submitted in POST Account Request & Consent to access the Single Account statement API");	
		
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct if the given permissions are not available for creating the consent");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verification of request when following permissions are submitted in POST Account Request & Consent to access the Single Account statement API");	
		
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/statements/333333/transactions");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct if the given permissions are not available for creating the consent");
		
		testVP.testResultFinalize();		
	}
}
