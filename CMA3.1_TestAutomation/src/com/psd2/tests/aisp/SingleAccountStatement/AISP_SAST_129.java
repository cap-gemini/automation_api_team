package com.psd2.tests.aisp.SingleAccountStatement;
import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification removed Amount block from backend under Statement/StatementBenefit/Amount
 * @author Rama Arora
 *
 */

@Listeners(TestListener.class)
@Test(groups={"Regression","Database"}, dependsOnGroups={"Pre_AISP_SAST"})
public class AISP_SAST_129 extends TestBase{
	
	    @BeforeClass
	    public void loadTestData() throws Throwable{
		mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"), PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token !=null, "Access token is not null");
	}
	    
	    @Test
		public void m_AISP_SAST_129() throws Throwable
		{
			TestLogger.logStep("[Step 1] : Get account number");
			restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
			restRequest.setHeadersString("Authorization:Bearer "+access_token);
			restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
			restRequest.addHeaderEntry("Accept", "application/json");
			restRequest.setMethod("GET");
			restRequest.submit();
			testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "200", "Response code is correct for request");
			TestLogger.logBlankLine();
			
			accountNumber = mongo.getFirstArrayObject("psuId:" + PropertyUtils.getProperty("usr_name") + ","+ "accountDetails.accountId:"+ API_Constant.getAisp_AccountId(), "accountDetails","accountNumber");
			TestLogger.logVariable("Account Number : " + accountNumber);
			TestLogger.logBlankLine();	
			
			TestLogger.logStep("[Step 2] : Removed Amount block from backend under Statement for account number "+accountNumber);
			mongo=new MongoDBconfig(PropertyUtils.getProperty("db_name"),PropertyUtils.getProperty("collection_statements"));
			mongo.removeArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountNumber:"+accountNumber,"statementBenefit",0,"amount");
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 3] : Verification removed Amount block from backend under Statement/StatementBenefit/Amount");
			restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
			restRequest.setHeadersString("Authorization:Bearer "+access_token);
			restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
			restRequest.addHeaderEntry("Accept", "application/json");
			restRequest.setMethod("GET");
			restRequest.submit();
			
			testVP.verifyStringEquals(restRequest.getResponseStatusCode(), "500", "Response code is correct when removed Amount block from backend under Statement/StatementBenefit/Amount");
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();
}
	    @AfterClass
		public void revokeTestData() throws IOException, InterruptedException{
	    	mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"), "statementBenefit", 0, "amount.amount", "1234567891234.12345");
	    	mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"), "statementBenefit", 0, "amount.currency", "GBP");
		}
	    
}
