package com.psd2.tests.aisp.SingleAccountStatement;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Get Statement Request for without required permission i.e. "ReadStatementsBasic" and "ReadStatementsDetail" permission should not be present in consent.
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SAST_010 extends TestBase
{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_AISP_SAST_010() throws Throwable{	
		
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadBalances;ReadAccountsDetail;ReadAccountsBasic;ReadBeneficiariesBasic;ReadBeneficiariesDetail;ReadDirectDebits;ReadProducts;ReadStandingOrdersBasic;ReadStandingOrdersDetail;ReadTransactionsBasic;ReadTransactionsDetail;ReadTransactionsCredits;ReadTransactionsDebits",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 2] : Verification of request when following permissions ReadStatementsDetail are submitted in POST Account Request & Consent to access the Single Account Scheduled Payments API");	
		
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"403", 
				"Response Code is correct if the given permissions are not available for creating the consent");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
