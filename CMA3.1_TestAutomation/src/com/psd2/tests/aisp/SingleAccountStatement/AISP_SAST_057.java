package com.psd2.tests.aisp.SingleAccountStatement;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of fields (Amount, Currency, Type) value of StatementBenefit array at path : Data/Statement/StatementBenefit
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAST"})
public class AISP_SAST_057 extends TestBase{	
	
	@BeforeClass 
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAST_057() throws Throwable{	 
		
		TestLogger.logStep("[Step 1] : Verification of fields (Amount, Currency, Type) value of StatementBenefit array at path : Data/Statement/StatementBenefit");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account statement request");
		
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Statement[0].StatementBenefit.Amount.Amount")).isEmpty(), 
				"Amount field under StatementBenefit array is not empty");
		
		testVP.verifyTrue(!String.valueOf(restRequest.getResponseValueByPath("Data.Statement[0].StatementBenefit.Amount.Currency")).isEmpty(), 
				"Currency field under StatementBenefit array is not empty");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Data.Statement[0].StatementBenefit[0].Type")), "CASHBACK", 
				"Type field under StatementBenefit array is correct");
		TestLogger.logBlankLine();
	
		testVP.testResultFinalize();		
	}
					
}
