package com.psd2.tests.aisp.SingleAccountStatement;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request status with invalid value of scope. Access token having value of scope other than 'accounts'
 * @author Rama Arora
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SAST_027 extends TestBase{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_AISP_SAST_027() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Generate access token with scope as payment");
		apiUtility.pispAccessToken=true;
        consentDetails=apiUtility.generatePayments(false, apiConst.domesticPayments, false, false);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Get single account scheduled-payments with invalid client credential access token");	

		getAccount.setBaseURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		getAccount.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		getAccount.submit();
		
		testVP.verifyStringEquals(String.valueOf(getAccount.getResponseStatusCode()),"403", 
				"Response Code is correct for scope other than accounts");	
		
		testVP.testResultFinalize();		
	}	
}
