package com.psd2.tests.aisp.SingleAccountStatement;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Get Statement Request with required permission i.e. only "ReadStatementsDetail" permission should be present in consent.
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_SAST_009 extends TestBase
{
	
	API_E2E_Utility apiUtility = new API_E2E_Utility();
	
	@Test
	public void m_AISP_SAST_009() throws Throwable{	
		
		
		TestLogger.logStep("[Step 1] : Create and authenticate AISP Consent");	
		consentDetails = apiUtility.generateAISPConsent("ReadAccountsBasic;ReadStatementsDetail",false,false,false,false,null,false,false);
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 2] : Verification of request when following permissions ReadStatementsDetail are submitted in POST Account Request & Consent to access the Single Account Scheduled Payments API");	
		
		restRequest.setURL(apiConst.account_endpoint+consentDetails.get("accountId")+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("api_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for the given permissions");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].AccountId").isEmpty()), 
				"Mandatory field i.e AccountId is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].Type").isEmpty()), 
				"Mandatory field i.e Type is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StartDateTime").isEmpty()), 
				"Mandatory field i.e StartDateTime is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].EndDateTime").isEmpty()), 
				"Mandatory field i.e EndDateTime is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].CreationDateTime").isEmpty()), 
				"Mandatory field i.e CreationDateTime is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementBenefit").isEmpty()), 
				"Mandatory field i.e StatementBenefit block is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementFee").isEmpty()), 
				"Mandatory field i.e StatementFee is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementInterest").isEmpty()), 
				"Mandatory field i.e StatementInterest is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementAmount").isEmpty()), 
				"Mandatory field i.e StatementAmount is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementDateTime").isEmpty()), 
				"Mandatory field i.e StatementDateTime is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementRate").isEmpty()), 
				"Mandatory field i.e StatementRate is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementValue").isEmpty()), 
				"Mandatory field i.e StatementValue is present and is not null");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();		
	}
}
