package com.psd2.tests.aisp.SingleAccountStatement;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of request when accountId length is 40 but not as per defined format i.e. including special characters except '-'
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SAST"})
public class AISP_SAST_018 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}	
	
	@Test
	public void m_AISP_SAST_018() throws Throwable{
		
	TestLogger.logStep("[Step 1] : Verification of request when accountId length is 40 but not as per defined format i.e. including special characters except '-'");	
	
	restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"!@#"+"/statements");
	restRequest.setHeadersString("Authorization:Bearer "+access_token);
	restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
	restRequest.addHeaderEntry("Accept", "application/json");
	restRequest.setMethod("GET");
	restRequest.submit();
	testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400",
			"Response Code is correct for account Id with special caracters request");
	
	testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Resource.NotFound", "Error Code are matched");
	testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].Message"), "Validation error found with the provided input", "Error Message are matched");
	
	TestLogger.logBlankLine();
	testVP.testResultFinalize();
	}
}
