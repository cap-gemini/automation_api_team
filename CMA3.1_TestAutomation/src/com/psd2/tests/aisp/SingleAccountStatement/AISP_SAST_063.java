package com.psd2.tests.aisp.SingleAccountStatement;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Currency field under  Data/Statement/StatementBenefit/Amount
 * @author : Rama Arora
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAST"})
public class AISP_SAST_063 extends TestBase
{

	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAST_063() throws Throwable{		
		
		TestLogger.logStep("[Step 1] : Verify the currency fields under StatementBenefit");
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct");	
		
		testVP.verifyTrue(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementBenefit[0].Amount.Currency").length() == 3,
				"Currency field is contains only 3 characters");
		
		String currency = restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementBenefit[0].Amount.Currency");
		testVP.verifyTrue(currency.equals(currency.toUpperCase()),"Currency field is in Capital letters");
			
		testVP.testResultFinalize();		
	}
}
