package com.psd2.tests.aisp.SingleAccountStatement;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of pdf file type for the downloaded file for the statement using valid statement Id.
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAST"})
public class AISP_SAST_110 extends TestBase{	
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAST_110() throws Throwable{	 
		
		TestLogger.logStep("[Step 1] : Verify the open banking standard and get Single Account Statement");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/csv");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"406", 
				"Response Code is correct for get single account statement request");
		testVP.verifyStringEquals(restRequest.getURL(),apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements", 
				"URI for GET Single account statements is as per open banking standard");		
		
		TestLogger.logBlankLine();
		

		testVP.testResultFinalize();		
	}
	
}
