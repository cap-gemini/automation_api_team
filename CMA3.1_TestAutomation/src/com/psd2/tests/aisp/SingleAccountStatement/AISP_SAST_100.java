package com.psd2.tests.aisp.SingleAccountStatement;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description :Verification of 'Rate' Field Outside the range 1-10 characters under data.Statement.StatementRate/Rate.TypeArray
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression","Database"},dependsOnGroups={"Pre_AISP_SAST"})
public class AISP_SAST_100 extends TestBase
{
	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAST_100() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Get account number");

		restRequest.setURL(apiConst.account_endpoint + API_Constant.getAisp_AccountId());
		restRequest.setHeadersString("Authorization:Bearer " + access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()), "200",
				"Response Code is correct for AccountId ");

		accountNumber = mongo.getFirstArrayObject("psuId:" + PropertyUtils.getProperty("usr_name") + ","
				+ "accountDetails.accountId:" + API_Constant.getAisp_AccountId(), "accountDetails", "accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Update 'Type' Field Outside the range: "+ accountNumber);
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("collection_statements"));
		mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),"statementRate",0,"type","ANNUALCASHANNUALCASHANNUALCASHANNUALCASHANNUALCASHANNUALCASH");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verify the open banking standard and get Single Account Statement");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"500", 
				"Response Code is correct for get single account statement request");
		
		
		testVP.testResultFinalize();
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException {
		mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),"statementRate",0,"type","ANNUALCASH");
	}
}
	
			
		
		