package com.psd2.tests.aisp.SingleAccountStatement;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.MongoDBconfig;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of date and time of StatementDateTime for without Seconds, without milliseconds and with 3 digit milliseconds 
 * @author Jasmin Patel
 *
 */

@Listeners({TestListener.class})
@Test(groups={"Regression","Database"},dependsOnGroups={"Pre_AISP_SAST"})
public class AISP_SAST_094 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), PropertyUtils.getProperty("aisp_consent"));
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAST_094() throws Throwable{
		
		TestLogger.logStep("[Step 1] : Verification of value of StatementDateTime field");	
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account statement request");
		accountNumber=mongo.getFirstArrayObject("psuId:"+PropertyUtils.getProperty("usr_name")+",accountDetails.accountId:"+API_Constant.getAisp_AccountId(),"accountDetails","accountNumber");
		TestLogger.logVariable("Account Number : " + accountNumber);
		
		TestLogger.logStep("[Step 2] :Update 'StatementDateTime' value without second and without offset and verify the response code for StatementDateTime");
		mongo = new MongoDBconfig(PropertyUtils.getProperty("db_name"), "MockAccountStatementCMA2");
		mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),"StatementDateTime",0,"dateTime","1981-03-20T06#06");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for statementDateTime field without second and without offset");
		
		TestLogger.logStep("[Step 3] :Update 'statementDateTime' value without second and Offset and verify the response code for statementDateTime");
		mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),"StatementDateTime",0,"dateTime","1981-03-20T06#06Z");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for statementDateTime field without second and OffsetZ");
		
		TestLogger.logStep("[Step 4] : Update 'statementDateTime' value without second and Offset 00:00 and verify the response code for statementDateTime");
		mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),"StatementDateTime",0,"dateTime","1981-03-20T06#06+00#00");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for statementDateTime field without second and Offset 00:00");
		
		TestLogger.logStep("[Step 5] : Update 'statementDateTime' value without milliseconds and without offset and verify the response code for statementDateTime");
		mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),"StatementDateTime",0,"dateTime","1981-03-20T06#06#06");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for statementDateTime field without milliseconds and without offset");
		
		TestLogger.logStep("[Step 6] : Update 'statementDateTime' value without milliseconds and offset and verify the response code for statementDateTime");
		mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),"StatementDateTime",0,"dateTime","1981-03-20T06#06#06Z");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for statementDateTime field without milliseconds and offset");
		
		TestLogger.logStep("[Step 7] : Update 'statementDateTime' value without milliseconds and offset 00:00 and verify the response code for statementDateTime");
		mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),"StatementDateTime",0,"dateTime","1981-03-20T06#06#06+00#00");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for statementDateTime field without milliseconds and offset 00:00");
		
		TestLogger.logStep("[Step 8] : Update 'statementDateTime' value with milliseconds and offset and verify the response code for statementDateTime");
		mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),"StatementDateTime",0,"dateTime","1981-03-20T06#06#06.777Z");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for statementDateTime field with milliseconds and offset");
		
		TestLogger.logStep("[Step 9] : Update 'statementDateTime' value with milliseconds and without offset and verify the response code for statementDateTime");
		mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),"StatementDateTime",0,"dateTime","1981-03-20T06#06#06.777");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for statementDateTime field with milliseconds and without offset");
		
		TestLogger.logStep("[Step 10] : Update 'statementDateTime' value with milliseconds and offset 00:00 and verify the response code for statementDateTime");
		mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),"StatementDateTime",0,"dateTime","1981-03-20T06#06#06.777+00#00");
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200",
				"Response Code is correct for statementDateTime field with milliseconds and offset 00:00");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();	
	}
	
	@AfterClass
	public void revokeTestData() throws IOException, InterruptedException{
		
		mongo.updateArrayObject("accountNumber:" + accountNumber + ",psuId:" + PropertyUtils.getProperty("usr_name"),"StatementDateTime",0,"dateTime","2018-07-23T10:29:22.765Z");
}
}
