package com.psd2.tests.aisp.SingleAccountStatement;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status with invalid or null value of x-fapi-customer-ip-address in header
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"}, dependsOnGroups={"Pre_AISP_SAST"})
public class AISP_SAST_120 extends TestBase
{
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}	
	
	@Test
	public void m_AISP_SAST_120() throws Throwable{
		
	TestLogger.logStep("[Step 1] : Checking the request status with invalid or null value of x-fapi-customer-ip-address in header");
	
	restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
	restRequest.setHeadersString("Authorization:Bearer "+access_token);
	restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
	restRequest.addHeaderEntry("Accept", "application/json");
	restRequest.addHeaderEntry("x-fapi-customer-ip-address", " ");
	restRequest.setMethod("GET");
	restRequest.submit();
	testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"400",
			"Response Code is correct for account Id with special caracters request");
	
	testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].ErrorCode"), "UK.OBIE.Header.Invalid", "Error Code are matched");
	testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Errors[0].Message"), "Invalid value for header x-fapi-customer-ip-address.", "Error Message are matched");
	
	TestLogger.logBlankLine();
	testVP.testResultFinalize();
	}
}
