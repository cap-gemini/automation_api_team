package com.psd2.tests.aisp.SingleAccountStatement;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of response for requested statement id
 * @author Jasmin Patel
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAST"})
public class AISP_SAST_105 extends TestBase{	
	@BeforeClass
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAST_104() throws Throwable{	 
		
		TestLogger.logStep("[Step 1] : Verify the open banking standard and get Single Account Statement");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account statement request");
		testVP.verifyStringEquals(restRequest.getURL(),apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements", 
				"URI for GET Single account statements is as per open banking standard");		
		String stId = restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementId");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verify the open banking standard and get Single Account Statement with statement Id");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements/"+stId);
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account statement with statement Id request");
		testVP.verifyStringEquals(restRequest.getURL(),apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements/"+stId, 
				"URI for GET Single account statements with statement Id is as per open banking standard");
		TestLogger.logBlankLine();
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data").isEmpty()), 
				"Mandatory field i.e Data array is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0]").isEmpty()), 
				"Optional field i.e Statement is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].AccountId").isEmpty()), 
				"Mandatory field i.e Statement is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementId").isEmpty()), 
				"Optional field i.e StatementId is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementReference").isEmpty()), 
				"Optional field i.e StatementReference is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].Type").isEmpty()), 
				"Mandatory field i.e Type  is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StartDateTime").isEmpty()), 
				"Mandatory field i.e StartDateTime  is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].EndDateTime").isEmpty()), 
				"Mandatory field i.e EndDateTime  is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].CreationDateTime").isEmpty()), 
				"Mandatory field i.e CreationDateTime  is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementDescription").isEmpty()), 
				"Optional field i.e StatementDescription  is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementBenefit").isEmpty()), 
				"Optional field i.e StatementBenefit field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementBenefit[0].Type").isEmpty()), 
				"Mandatory field i.e Type field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementBenefit[0].Amount").isEmpty()), 
				"Mandatory field i.e Amount field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementBenefit[0].Amount.Amount").isEmpty()), 
				"Mandatory field i.e StatementBenefit[0].Amount field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementBenefit[0].Amount.Currency").isEmpty()), 
				"Mandatory field i.e StatementBenefit[0].Amount.Currency field is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementFee").isEmpty()), 
				"Optional field i.e StatementBenefit field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementFee[0].CreditDebitIndicator").isEmpty()), 
				"Mandatory field i.e CreditDebitIndicator field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementFee[0].Type").isEmpty()), 
				"Mandatory field i.e Type field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementBenefit[0].Amount").isEmpty()), 
				"Mandatory field i.e Amount field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementBenefit[0].Amount.Amount").isEmpty()), 
				"Mandatory field i.e StatementBenefit[0].Amount field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementBenefit[0].Amount.Currency").isEmpty()), 
				"Mandatory field i.e StatementBenefit[0].Amount.Currency field is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementInterest[0]").isEmpty()), 
				"Optional field i.e Type field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementInterest[0].Type").isEmpty()), 
				"Mandatory field i.e Type field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementInterest[0].Amount").isEmpty()), 
				"Mandatory field i.e Amount field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementInterest[0].Amount.Amount").isEmpty()), 
				"field i.e Amount field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementInterest[0].Amount.Currency").isEmpty()), 
				"Mandatory field i.e Currency field is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementAmount[0]").isEmpty()), 
				"Optional field i.e Type field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementAmount[0].CreditDebitIndicator").isEmpty()), 
				"Mandatory field i.e CreditDebitIndicator field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementAmount[0].Type").isEmpty()), 
				"Mandatory field i.e Type field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementAmount[0].Amount").isEmpty()), 
				"Mandatory field i.e Amount field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementAmount[0].Amount.Amount").isEmpty()), 
				"field i.e Amount field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementAmount[0].Amount.Currency").isEmpty()), 
				"Mandatory field i.e Currency field is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementDateTime[0]").isEmpty()), 
				"Optional field i.e Type field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementDateTime[0].DateTime").isEmpty()), 
				"Mandatory field i.e CreditDebitIndicator field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementDateTime[0].Type").isEmpty()), 
				"Mandatory field i.e Type field is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementRate[0]").isEmpty()), 
				"Optional field i.e StatementRate field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementRate[0].Rate").isEmpty()), 
				"Mandatory field i.e CreditDebitIndicator field is present and is not null");
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementRate[0].Type").isEmpty()), 
				"Mandatory field i.e Type field is present and is not null");
		
		testVP.verifyTrue(!(restRequest.getResponseNodeStringByPath("Data.Statement[0].StatementValue[0]").isEmpty()), 
				"Optional field i.e StatementValue field is present and is not null");
		
			TestLogger.logBlankLine();


		testVP.testResultFinalize();		
	}
	
}
