package com.psd2.tests.aisp.SingleAccountStatement;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_Constant;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of fields (Amount, Currency, Type, CreditDebitIndicator) value of StatementFee array at path : Data/Statement/StatementFee
 * @author Rama Arora
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"},dependsOnGroups={"Pre_AISP_SAST"})
public class AISP_SAST_065 extends TestBase{	
	
	@BeforeClass 
	public void loadTestData() throws Throwable{
		
		access_token = createNewAccessToken(apiConst.rt_endpoint, API_Constant.getAisp_RefreshToken(),
				PropertyUtils.getProperty("client_id"),PropertyUtils.getProperty("client_secret"));
		testVP.verifyTrue(access_token != null,	"Access token is not null");
	}
	
	@Test
	public void m_AISP_SAST_065() throws Throwable{	 
		
		TestLogger.logStep("[Step 1] : Verification of fields (Amount, Currency, Type, CreditDebitIndicator) value of StatementFee array at path : Data/Statement/StatementFee");	
		
		restRequest.setURL(apiConst.account_endpoint+API_Constant.getAisp_AccountId()+"/statements");
		restRequest.setHeadersString("Authorization:Bearer "+access_token);
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get single account statement request");
		
		testVP.verifyFalse(String.valueOf(restRequest.getResponseValueByPath("Data.Statement[0].StatementFee[0].Amount.Amount")).isEmpty(),
				"Amount field under Statement Fee array is present and not empty");
		
		testVP.verifyFalse(String.valueOf(restRequest.getResponseValueByPath("Data.Statement[0].StatementFee[0].Amount.Currency")).isEmpty(),
				"Currency field under Statement Fee array is present and not empty");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Data.Statement[0].StatementFee[0].CreditDebitIndicator")), "Credit", 
				"CreditDebitIndicator field under StatementFee array is correct");
		
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseValueByPath("Data.Statement[0].StatementFee[0].Type")), "GAMBLING", 
				"Type field under StatementFee is correct");
				
		TestLogger.logBlankLine();
	
		testVP.testResultFinalize();		
	}
	
}
