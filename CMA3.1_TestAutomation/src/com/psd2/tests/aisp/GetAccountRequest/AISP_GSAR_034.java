package com.psd2.tests.aisp.GetAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Valid ConsentId where the status of following- 
1) Account Access Consent Resource - Haven't created
2) Consent - Haven't created
3) Client-Credential Token - Active (1)

 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_GSAR_034 extends TestBase
{
	API_E2E_Utility apiUtil = new API_E2E_Utility();
	
	@Test
	public void m_AISP_GSAR_034() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Account SetUp....");
		
		consentDetails = apiUtil.performAccountSetUp(false, null);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verify the status field : AwaitingAuthorisation");
		restRequest.setURL(apiConst.as_endpoint+"/"+consentDetails.get("consentId"));
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Content-Type", "application/json");
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(),"200", 
				"Response Code is correct");
		
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Data.Status"),"AwaitingAuthorisation", 
				"Status field value is correct");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
