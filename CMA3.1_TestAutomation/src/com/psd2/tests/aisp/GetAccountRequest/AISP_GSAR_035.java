package com.psd2.tests.aisp.GetAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of Valid ConsentId where the status of following- 
1) Account Access Consent Resource - Created
2) Consent - created
3) Client-Credential Token - Active (1)

 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_GSAR_035 extends TestBase
{
	//API_E2E_Utility apiUtility = new API_E2E_Utility();
	API_E2E_Utility apiUtil = new API_E2E_Utility();
	
	@Test
	public void m_AISP_GSAR_035() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Account SetUp....");	
	
		consentDetails = apiUtil.performAccountSetUp(false, null);
		TestLogger.logBlankLine();		
		
		TestLogger.logStep("[Step 2] : Delete/Revoke respective ConsentId");
		
		revokeConsent.setBaseURL(apiConst.as_endpoint+"/"+consentDetails.get("consentId"));
		revokeConsent.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		revokeConsent.submit();
		
		testVP.verifyStringEquals(revokeConsent.getResponseStatusCode(),"204", 
				"Access token is revoked successfully");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Verify the status field : Revoked");
		restRequest.setURL(apiConst.as_endpoint+"/"+consentDetails.get("consentId"));
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Content-Type", "application/json");
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct");
		testVP.verifyStringEquals(restRequest.getResponseNodeStringByPath("Data.Status"),"Revoked", 
				"Status field value is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}
}
