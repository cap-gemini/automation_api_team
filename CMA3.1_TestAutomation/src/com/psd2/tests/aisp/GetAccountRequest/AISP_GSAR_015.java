package com.psd2.tests.aisp.GetAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;

/**
 * Class Description : Checking the request status with invalid value of scope. Client-Credential Token having value of scope other than 'accounts'
 * @author Kiran Dewangan
 * Will be covered in PISP Flow
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_GSAR_015 extends TestBase{	
	
	API_E2E_Utility apiUtil = new API_E2E_Utility();
		@Test
		public void m_AISP_GSAR_015() throws Throwable{	

			TestLogger.logStep("[Step 1] : Creating client credetials....");
			
			createClientCred.setBaseURL(apiConst.cc_endpoint);
			createClientCred.setScope("payments");
			createClientCred.submit();		
			
			testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
					"Response Code is correct for client credetials");
			access_token = createClientCred.getAccessToken();
			TestLogger.logVariable("AccessToken : " + access_token);	
			TestLogger.logBlankLine();
			
			TestLogger.logStep("[Step 2] : Account SetUp....");
			
			accountSetup.setBaseURL(apiConst.as_endpoint);
			accountSetup.setHeadersString("Authorization:Bearer "+access_token);
			accountSetup.setAllPermission();
			accountSetup.submit();
			
			testVP.verifyStringEquals(accountSetup.getResponseStatusCode(),"403", 
					"Response Code is correct for Account SetUp");
					
			TestLogger.logBlankLine();
			
			testVP.testResultFinalize();
		}
	}	


