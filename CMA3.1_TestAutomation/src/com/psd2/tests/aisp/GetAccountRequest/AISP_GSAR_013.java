package com.psd2.tests.aisp.GetAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Checking the request status without Authorization (Client-Credential Token)
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_GSAR_013 extends TestBase
{	
	API_E2E_Utility apiUtil = new API_E2E_Utility();
	
	@Test
	public void m_AISP_GSAR_013() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Account SetUp....");
		
		consentDetails = apiUtil.performAccountSetUp(false, null);
		TestLogger.logBlankLine();		
	
		TestLogger.logStep("[Step 2] : Verify the request status when no authorization header is passed in the request body");
		restRequest.setURL(apiConst.as_endpoint+"/"+consentDetails.get("consentId"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Content-Type", "application/json");
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(restRequest.getResponseStatusCode(),"401", 
				"Response Code is correct when no authorization header is passed in the request body");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}