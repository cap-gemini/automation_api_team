package com.psd2.tests.aisp.GetAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of 'TransactionFromDateTime' field in the response
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_GSAR_038 extends TestBase
{
	API_E2E_Utility apiUtil = new API_E2E_Utility();
	
	@Test
	public void m_AISP_GSAR_038() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Account SetUp....");
		
		consentDetails = apiUtil.performAccountSetUp(false, null);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 2] : Verify the 'TransactionFromDateTime' format");
		restRequest.setURL(apiConst.as_endpoint+"/"+consentDetails.get("consentId"));
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Content-Type", "application/json");
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get account request");
		String txnFromDateTime = restRequest.getResponseNodeStringByPath("Data.TransactionFromDateTime");
		TestLogger.logVariable("TransactionFromDateTime : "+txnFromDateTime);
		testVP.verifyTrue(Misc.verifyDateTimeFormat(txnFromDateTime.split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(txnFromDateTime.split("T")[1], "HH:mm:ss+05:30"),"TransactionFromDateTime is as per expected format");
		
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
