package com.psd2.tests.aisp.GetAccountRequest;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.Misc;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : Verification of 'TransactionToDateTime' field in the response
 * @author Kiran Dewangan
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class AISP_GSAR_039 extends TestBase
{
	API_E2E_Utility apiUtil = new API_E2E_Utility();
	
	@Test
	public void m_AISP_GSAR_039() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Account SetUp....");
		
		consentDetails = apiUtil.performAccountSetUp(false, null);
		TestLogger.logBlankLine();
			
		TestLogger.logStep("[Step 2] : Verify the 'TransactionToDateTime' format");
		restRequest.setURL(apiConst.as_endpoint+"/"+consentDetails.get("consentId"));
		restRequest.setHeadersString("Authorization:Bearer "+consentDetails.get("cc_access_token"));
		restRequest.addHeaderEntry("x-fapi-financial-id", PropertyUtils.getProperty("fin_id"));
		restRequest.addHeaderEntry("Content-Type", "application/json");
		restRequest.addHeaderEntry("Accept", "application/json");
		restRequest.setMethod("GET");
		restRequest.submit();
		testVP.verifyStringEquals(String.valueOf(restRequest.getResponseStatusCode()),"200", 
				"Response Code is correct for get account request");
		String txnToDateTime = restRequest.getResponseNodeStringByPath("Data.TransactionToDateTime");
		TestLogger.logVariable("TransactionToDateTime : "+txnToDateTime);
		testVP.verifyTrue(Misc.verifyDateTimeFormat(txnToDateTime.split("T")[0], "yyyy-MM-dd") && 
				Misc.verifyDateTimeFormat(txnToDateTime.split("T")[1], "HH:mm:ss+05:30"),"TransactionToDateTime is as per expected format");
		TestLogger.logBlankLine();
		testVP.testResultFinalize();
	}
}
