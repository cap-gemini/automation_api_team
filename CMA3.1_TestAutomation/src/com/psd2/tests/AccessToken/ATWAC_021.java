package com.psd2.tests.AccessToken;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : To verify the consent url with negative value of state.  
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class ATWAC_021 extends TestBase{	
	
	API_E2E_Utility apiUtil = new API_E2E_Utility();

	@Test
	public void m_ATWAC_021() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create AISP Consent	....");
		
		consentDetails = apiUtil.performAccountSetUp(false,null);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : JWT Token Creation....");
		
		reqObject.setValueField(consentDetails.get("consentId"));
		outId =  reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Launch the consent url without state field");	
		String consentUrl = PropertyUtils.getProperty("auth_servr_url")+"oauth/as/"+PropertyUtils.getProperty("account_type")+"/authorization.oauth2?client_id="+PropertyUtils.getProperty("client_id")+"&response_type=code id_token&scope=openid accounts&nonce="+PropertyUtils.getProperty("nonce")+"&redirect_uri="+PropertyUtils.getProperty("redirect_url")+"&request="+outId;
		startDriverInstance();
		String authCode = consentOps.authoriseAISPConsent(consentUrl);
		testVP.verifyTrue(authCode.length()>0,"Able to authorise the consent without state field");
		
		TestLogger.logStep("[Step 4] : Launch the consent url with blank value of state field");	
		consentUrl = PropertyUtils.getProperty("auth_servr_url")+"oauth/as/"+PropertyUtils.getProperty("account_type")+"/authorization.oauth2?client_id="+PropertyUtils.getProperty("client_id")+"&response_type=code id_token&scope=openid accounts&state=&nonce="+PropertyUtils.getProperty("nonce")+"&redirect_uri="+PropertyUtils.getProperty("redirect_url")+"&request="+outId;
		driverOps.navigateToURL(consentUrl);
		testVP.verifyTrue(driverOps.getURL().contains("errorpage"),"Getting the error on landing page");
		
		TestLogger.logStep("[Step 5] : Launch the consent url with invalid value of state field");	
		consentUrl = PropertyUtils.getProperty("auth_servr_url")+"oauth/as/"+PropertyUtils.getProperty("account_type")+"/authorization.oauth2?client_id="+PropertyUtils.getProperty("client_id")+"&response_type=code id_token&scope=openid accounts&state=Invalid State&nonce="+PropertyUtils.getProperty("nonce")+"&redirect_uri="+PropertyUtils.getProperty("redirect_url")+"&request="+outId;
		authCode = consentOps.authoriseAISPConsent(consentUrl);
		testVP.verifyTrue(authCode.length()>0,"Able to authorise the consent with any value of state field");
		closeDriverInstance();
		
		testVP.testResultFinalize();		
	}	
}
