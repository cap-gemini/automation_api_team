package com.psd2.tests.AccessToken;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;
import com.psd2.utils.API_E2E_Utility;
import com.psd2.utils.PropertyUtils;

/**
 * Class Description : To verify the consent url with negative value of client Id.  
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class ATWAC_019 extends TestBase{	
	
	API_E2E_Utility apiUtil = new API_E2E_Utility();

	@Test
	public void m_ATWAC_019() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Create AISP Consent	....");
		
		consentDetails = apiUtil.performAccountSetUp(false,null);
		TestLogger.logBlankLine();

		TestLogger.logStep("[Step 2] : JWT Token Creation....");
		
		reqObject.setValueField(consentDetails.get("consentId"));
		outId =  reqObject.submit();
		
		TestLogger.logVariable("JWT Token : " + outId);
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Launch the consent url without Client Id field");	
		String consentUrl = PropertyUtils.getProperty("auth_servr_url")+"oauth/as/"+PropertyUtils.getProperty("account_type")+"/authorization.oauth2?response_type=code id_token&scope=openid accounts&state="+PropertyUtils.getProperty("state")+"&nonce="+PropertyUtils.getProperty("nonce")+"&redirect_uri="+PropertyUtils.getProperty("redirect_url")+"&request="+outId;
		startDriverInstance();
		driverOps.navigateToURL(consentUrl);
		testVP.verifyTrue(driverOps.getURL().contains("errorpage"),"Getting the error on landing page");
		
		TestLogger.logStep("[Step 4] : Launch the consent url with blank value of Client Id field");	
		consentUrl = PropertyUtils.getProperty("auth_servr_url")+"oauth/as/"+PropertyUtils.getProperty("account_type")+"/authorization.oauth2?client_id=&response_type=code id_token&scope=openid accounts&state="+PropertyUtils.getProperty("state")+"&nonce="+PropertyUtils.getProperty("nonce")+"&redirect_uri="+PropertyUtils.getProperty("redirect_url")+"&request="+outId;		startDriverInstance();
		driverOps.navigateToURL(consentUrl);
		testVP.verifyTrue(driverOps.getURL().contains("errorpage"),"Getting the error on landing page");
		
		TestLogger.logStep("[Step 5] : Launch the consent url with invalid value of Client Id field");	
		consentUrl = PropertyUtils.getProperty("auth_servr_url")+"oauth/as/"+PropertyUtils.getProperty("account_type")+"/authorization.oauth2?client_id=4Qv4SFmtsMjHmHOF4JaddF&response_type=code id_token&scope=openid accounts&state="+PropertyUtils.getProperty("state")+"&nonce="+PropertyUtils.getProperty("nonce")+"&redirect_uri="+PropertyUtils.getProperty("redirect_url")+"&request="+outId;		startDriverInstance();
		driverOps.navigateToURL(consentUrl);
		testVP.verifyStringEquals(driverOps.getText(objectRepo.getString("Obj_ErrorScreen1")),"Error","Getting the error on landing page");
		closeDriverInstance();
		
		testVP.testResultFinalize();		
	}	
}
