package com.psd2.tests.cisp.PostFundConfConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of 'ExpirationDateTime' date field values without seconds, without milliseconds and with 3 digit milliseconds
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class CISP_PFCC_059 extends TestBase
{
	@Test
	public void m_CISP_PFCC_059() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("fundsconfirmations");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 2] : Fund confirmation consent....");
		
		fundConfConsent.setBaseURL(apiConst.fc_endpoint);
		fundConfConsent.setHeadersString("Authorization:Bearer "+access_token);
		fundConfConsent.setExpirationDateTime("2025-12-27T18:00");
		fundConfConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(fundConfConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for fund Confiramtion Consent when expiration date time doesn't include second");
		
		testVP.verifyStringEquals(String.valueOf(fundConfConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(fundConfConsent.getResponseValueByPath("Errors[0].Message")),"Error validating JSON. Error: - Provided value 2025-12-27T18:00 is not compliant with the format datetime provided in rfc3339 for ExpirationDateTime",
				"Error message is correct");
		
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 3] : Fund confirmation consent....");
		
		fundConfConsent.setBaseURL(apiConst.fc_endpoint);
		fundConfConsent.setHeadersString("Authorization:Bearer "+access_token);
		fundConfConsent.setExpirationDateTime("2025-12-27T18:00:00");
		fundConfConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(fundConfConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for fund Confiramtion Consent when expiration date time doesn't include millisecond");
		
		testVP.verifyStringEquals(String.valueOf(fundConfConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(fundConfConsent.getResponseValueByPath("Errors[0].Message")),"Error validating JSON. Error: - Provided value 2025-12-27T18:00:00 is not compliant with the format datetime provided in rfc3339 for ExpirationDateTime",
				"Error message is correct");
		TestLogger.logBlankLine();
		
		TestLogger.logStep("[Step 4] : Fund confirmation consent....");
		
		fundConfConsent.setBaseURL(apiConst.fc_endpoint);
		fundConfConsent.setHeadersString("Authorization:Bearer "+access_token);
		fundConfConsent.setExpirationDateTime("2025-12-27T18:00:00.000");
		fundConfConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(fundConfConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for fund Confiramtion Consent when expiration date time include millisecond");
		
		testVP.verifyStringEquals(String.valueOf(fundConfConsent.getResponseValueByPath("Errors[0].ErrorCode")),"UK.OBIE.Field.Invalid", 
				"Response Error Code is correct");
		testVP.verifyStringEquals(String.valueOf(fundConfConsent.getResponseValueByPath("Errors[0].Message")),"Error validating JSON. Error: - Provided value 2025-12-27T18:00:00.000 is not compliant with the format datetime provided in rfc3339 for ExpirationDateTime",
				"Error message is correct");
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();
	}				
}
