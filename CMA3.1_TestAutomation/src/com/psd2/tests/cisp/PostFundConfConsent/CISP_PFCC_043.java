package com.psd2.tests.cisp.PostFundConfConsent;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.psd2.core.TestBase;
import com.psd2.logger.TestListener;
import com.psd2.logger.TestLogger;

/**
 * Class Description : Verification of SchemeName field under DebtorAccount/SchemeName having length variation
 * @author Alok Kumar
 *
 */

@Listeners( { TestListener.class })
@Test(groups={"Regression"})
public class CISP_PFCC_043 extends TestBase
{
	@Test
	public void m_CISP_PFCC_043() throws Throwable{	
		
		TestLogger.logStep("[Step 1] : Creating client credetials....");
		
		createClientCred.setBaseURL(apiConst.cc_endpoint);
		createClientCred.setScope("fundsconfirmations");
		createClientCred.submit();
		
		testVP.verifyStringEquals(String.valueOf(createClientCred.getResponseStatusCode()),"200", 
				"Response Code is correct for client credetials");
		access_token = createClientCred.getAccessToken();
		TestLogger.logVariable("AccessToken : " + access_token);	
		TestLogger.logBlankLine();
		
		
		TestLogger.logStep("[Step 2] : Fund confirmation consent....");
		
		fundConfConsent.setBaseURL(apiConst.fc_endpoint);
		fundConfConsent.setHeadersString("Authorization:Bearer "+access_token);
		fundConfConsent.setSchemeName("UK.OBIE.IBANUK.OBIE.IBANUK.OBIE.IBANUK.OBIE.IBAN");
		fundConfConsent.submit();
		
		testVP.verifyStringEquals(String.valueOf(fundConfConsent.getResponseStatusCode()),"400", 
				"Response Code is correct for fund Confirmation Consent with Scheme Name more than 40 char");
		testVP.verifyStringEquals(fundConfConsent.getResponseNodeStringByPath("Errors[0].ErrorCode"),"UK.OBIE.Field.Invalid", 
				"Error Code is correct for fund Confirmation Consent with Scheme Name more than 40 char");
		testVP.verifyStringEquals(fundConfConsent.getResponseNodeStringByPath("Errors[0].Message"),"Error validating JSON. Error: - Expected max length 40 for field [SchemeName], but got [UK.OBIE.IBANUK.OBIE.IBANUK.OBIE.IBANUK.OBIE.IBAN]", 
				"Error Message is correct for fund Confirmation Consent with Scheme Name more than 40 char");
		
		TestLogger.logBlankLine();
		
		testVP.testResultFinalize();			

	}				
}