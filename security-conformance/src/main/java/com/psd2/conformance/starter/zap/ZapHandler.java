package com.psd2.conformance.starter.zap;

import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.psd2.conformance.starter.adapter.ConformanceHandler;
import com.psd2.conformance.starter.adapter.ConformanceHandlerDelegate;


@PropertySource("classpath:configuration.properties")
@Service
public class ZapHandler extends ConformanceHandler{
	
	//Configuration to be move to config file or properties
	// Report would end on 100% completion. Reduce it if you want test for proper working.
	private boolean ZAP_SERVER_INSTANCE_REQUIRED = false;
	private boolean SKIP_SCAN = false;
	
	private ZapServer zapServer;
	
	public ZapHandler() {
		// TODO Auto-generated constructor stub
	}

	public ZapHandler(ConformanceHandlerDelegate delegate) {
		this.setDelegate(delegate);
	}
	
	public ZapHandler(ConformanceHandlerDelegate delegate, boolean skipScan) {
		SKIP_SCAN = skipScan;
		this.setDelegate(delegate);
	}
	
	@Override
	public void preSuiteExecution() {	
		
		this.startServer();
		ZapClient.connectAndUpdateCert();
	}

	private void startServer() {
		if(ZAP_SERVER_INSTANCE_REQUIRED) {
			this.zapServer = new ZapServer();
			zapServer.start();
		}
	}
	
	@Override
	public void postSuiteExecution() {
		if(!SKIP_SCAN) {
			ZapClient.scanSession();
		}
		
		ZapClient.generateReport();
		if(ZAP_SERVER_INSTANCE_REQUIRED && (null != this.zapServer)) {
			this.zapServer.stopServer();
			this.zapServer = null;
		}
	}
	

	public void stop() {
		
		
	}

}
