package com.psd2.conformance.starter;

import com.psd2.conformance.starter.adapter.ConformanceHandler;
import com.psd2.conformance.starter.adapter.ConformanceHandlerDelegate;
import com.psd2.conformance.starter.zap.ZapHandler;

public class ConformanceManager implements ConformanceHandlerDelegate {
	
	public void run() {
		ConformanceHandler conformanceHandler = new ZapHandler(this);
		conformanceHandler.startExecution();
	}

	@Override
	public void runSuite() {
		
	}
}
