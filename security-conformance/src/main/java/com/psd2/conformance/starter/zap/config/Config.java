package com.psd2.conformance.starter.zap.config;

import java.util.HashMap;
import java.util.Map;

public class Config {
	private Map<String, String> configMap;
	private static Config INSTANCE = new Config();
	
	public static String get(String key) {
		if(INSTANCE.configMap.containsKey(key)) {
			return INSTANCE.configMap.get(key);
		}
		System.out.println("Config: get: No Value for provided key");
		return key;
	}
	
	private Config() {
		configMap = new HashMap<String, String>();
		this.populateMap();
	}
	
	private void populateMap() {
		
	}
	
	
}
