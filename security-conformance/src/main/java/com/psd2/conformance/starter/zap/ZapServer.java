package com.psd2.conformance.starter.zap;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import javax.annotation.PreDestroy;

public class ZapServer extends Thread{
	
	//Configuration to be move to config file or properties
	private String ZAP_WORK_DIRECTORY = "C://zap-data//";
	private String ZAP_JAR = "ZAP_2.8.0//zap-2.8.0.jar";
	private String ZAP_HOST = "127.0.0.1";
	private int ZAP_PORT = 8082;
	private String ZAP_API_KEY = "5tpjflpfqlbkms83kun2o5mqew";
	
	private String START_ZAP_SERVER_BAT = "start-server.bat";
	
	//Local variables
	private Process process = null;

	@Override
	public void run() {
		super.run();
		this.starZapServer();
	}
	
	public void starZapServer() {
		Scanner scanner = null;
		try {
			process = Runtime.getRuntime().exec("java -jar " + ZAP_WORK_DIRECTORY + ZAP_JAR + " -daemon -config api.key=" + ZAP_API_KEY + " -host " + ZAP_HOST + " -port " + ZAP_PORT);
			InputStream in = process.getInputStream();
			
			scanner = new Scanner(in);
			while(scanner.hasNext() ) {
				System.out.println(scanner.nextLine());
			}
			scanner.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}finally {
			scanner.close();
		}
	}
	
	public void starZapServerWithBatch() {
		try {
			String currentDirectory = new File(".").getAbsolutePath();
			Process p = Runtime.getRuntime().exec("cmd.exe /c start " + START_ZAP_SERVER_BAT);
			System.out.println("Starting Server..");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void stopServer() {
		InputStream in = process.getInputStream();
		Scanner scanner = new Scanner(in);
		process.destroy();
		while (scanner.hasNext()) {
			System.out.println(scanner.nextLine());
		}
		scanner.close();
		this.interrupt();
		System.out.println("ZAP Server stopped....");
	}
}
