package com.psd2.conformance.starter.zap;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.zaproxy.clientapi.core.ApiResponse;
import org.zaproxy.clientapi.core.ApiResponseElement;
import org.zaproxy.clientapi.core.ApiResponseList;
import org.zaproxy.clientapi.core.ClientApi;
import org.zaproxy.clientapi.core.ClientApiException;

import com.psd2.conformance.starter.zap.config.ConformanceKeys;

public class ZapClient {
	//Configuration to be move to config file or properties
		public static final String DOMAIN_TO_INCLUDE_IN_PROXY = "apibank-cma2plus.in";
		private static String ZAP_HOST = "127.0.0.1";
		private static int ZAP_PORT = 8082;
		private static String ZAP_API_KEY = "5tpjflpfqlbkms83kun2o5mqew";
		private static String ZAP_WORK_DIRECTORY = "C://zap-data//";
		
		//for client Zh1fQvHP2KeiMwFKArDp0A
		private static String PSD2_CLIENT_CERT_FILE_NAME = "transport-Zh1fQvHP2KeiMwFKArDp0A.p12";
		private static String PSD2_CLIENT_CERT_PASSWORD = "passw0rd";  //kid
		
		//for client test - 
		//private static String PSD2_CLIENT_CERT_FILE_NAME = "transport-W4H36Sejd6Ei4VyywKJD6p.p12";
		//private static String PSD2_CLIENT_CERT_PASSWORD = "3KXbGcO5Wks9kZnhY6uR432M7os";  //kid
		
		//Local variables
		private static boolean ZAP_SERVER_READY = false;
		private static ClientApi clientApi = null;
		private static String certIndex = "0";
		private static String outputFileName = "Zap-Report";
		private static int SCAN_PERCENTAGE_TO_COMPLETE = 100;   // Report would end on 100% completion. Reduce it if you want test for proper working.
		private static int SCAN_PROGRESS_CHECK_DURATION = 5;
		private static String SCAN_REPORT_FILE_NAME_FORMAT = "yyyy-MMM-dd-HHmmss-SSSS";
		
		public static void connectAndUpdateCert() {
			boolean firstAttempt = true;
			while(!ZAP_SERVER_READY) {
				try {
					System.out.println("Connecting ZAP Server ...");
					clientApi = new ClientApi(ZAP_HOST, ZAP_PORT, ZAP_API_KEY, true);
					System.out.println("Connected to the server successfully ...");
					clientApi.core.enablePKCS12ClientCertificate(ZAP_WORK_DIRECTORY + PSD2_CLIENT_CERT_FILE_NAME, PSD2_CLIENT_CERT_PASSWORD, certIndex);
					System.out.println("Registered required certificates with ZAP Server ...");
					ZAP_SERVER_READY = true;
					clientApi.core.newSession("session-" + System.currentTimeMillis(), null);
					System.out.println("Created new session on ZAP Server ...");
					clientApi.core.clearExcludedFromProxy();
					clientApi.core.excludeFromProxy("^(?!https:\\/\\/.*"+ DOMAIN_TO_INCLUDE_IN_PROXY +").*$");
					System.out.println("ZAP Server ready to scan requests ..." );
				} catch (ClientApiException e) {
					System.out.print(firstAttempt ? "Waiting for server to start ." : ". ");
					firstAttempt = false;
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
		
				}
			}
		}
		
		public static void scanSession() {
			System.out.println("Initiating Active Scan ...");
			try {
				
				LinkedList<String> urls = new LinkedList<>();
				ApiResponse urlResponse = clientApi.core.urls();
				if (urlResponse != null && urlResponse instanceof ApiResponseList) {
					List<ApiResponse> apiResponseList = ((ApiResponseList) urlResponse).getItems();
					
					for (ApiResponse urlElement : apiResponseList) {
						try {
							URL url = new URL(((ApiResponseElement)urlElement).getValue());
							String urlString = url.getProtocol() + "://" + url.getHost() + ( url.getPort() != -1 ? ":" + url.getPort() : "");	
							if(!urls.contains(urlString)) {
								urls.add(urlString);
							}
						} catch (MalformedURLException e) {
							
						}
					}
				}
				
				while(urls.size() > 0) {
					ApiResponse resp = clientApi.ascan.scan(urls.pop(), "True", "False", null, null, null);
					
					String scanid = ((ApiResponseElement) resp).getValue();
					int progress = 0;
					int progressInterval = SCAN_PROGRESS_CHECK_DURATION;
					while( progress < SCAN_PERCENTAGE_TO_COMPLETE) {
						Thread.sleep(2000);
						progress = Integer.parseInt(((ApiResponseElement) clientApi.ascan.status(scanid)).getValue());
						if(progress > progressInterval) {
							System.out.println("Scan Progress " + progress + " % *********************");
							progressInterval = progressInterval + SCAN_PROGRESS_CHECK_DURATION;
						}
						if(progress >= SCAN_PERCENTAGE_TO_COMPLETE) {
							clientApi.ascan.stop(scanid);
							if(urls.size() == 0) {
								break;
							}
							
						}
					}
				}

				
				System.out.println("Active Scan Completed ...");
			} catch (ClientApiException e) {
				System.out.println("Error scannig the last session");
			} catch (InterruptedException e) {
				System.out.println("Error scannig the last session");
			}
		}
		
		public static void generateReport() {
			System.out.println("Generating Scan Report ...");
			try {
				byte[] report = clientApi.core.htmlreport();
				String currentTimeStampString = new SimpleDateFormat(SCAN_REPORT_FILE_NAME_FORMAT).format(new Date());
				String fileName = ZAP_WORK_DIRECTORY + outputFileName + "-" + currentTimeStampString + ".html";
				File file = new File(fileName);
				OutputStream os = new FileOutputStream(file);
				os.write(report);
				System.out.println("Report Generated  : " + fileName);
			} catch (ClientApiException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("Error Generating Report");
			}
			
			
		}
}
