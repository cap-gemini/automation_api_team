package com.psd2.conformance.starter.adapter;

public abstract class ConformanceHandler {
	private ConformanceHandlerDelegate delegate;
	private long SUITE_START_TIME, SCAN_START_TIME;
	public void startExecution() {
		SUITE_START_TIME = System.currentTimeMillis();
		this.preSuiteExecution();
		this.delegate.runSuite();
		this.logSuiteExecutionTime();
		this.postSuiteExecution();
		this.logSecurityScanTime();
	}
	public abstract void preSuiteExecution();
	public abstract void postSuiteExecution();
	
	public void setDelegate(ConformanceHandlerDelegate delegate) {
		this.delegate = delegate;
	}
	
	public void logSuiteExecutionTime() {
		SCAN_START_TIME = System.currentTimeMillis();
		System.out.println("TIME TAKEN FOR EXECUTING THE TEST SUITE BEFORE SECURITY SCAN : " + ((SCAN_START_TIME - SUITE_START_TIME) / 1000) + " Seconds");
	}
	
	public void logSecurityScanTime() {
		long SECURITY_SCAN_TIME = System.currentTimeMillis();
		System.out.println("TIME TAKEN FOR SECURITY SCAN : " + ((SECURITY_SCAN_TIME - SCAN_START_TIME) / 1000) + " Seconds");
		System.out.println("TOTAL TIME TAKEN : " + ((SECURITY_SCAN_TIME - SUITE_START_TIME) / 1000) + " Seconds");
	}
	
}
