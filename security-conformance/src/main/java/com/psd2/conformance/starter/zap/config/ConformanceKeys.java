package com.psd2.conformance.starter.zap.config;

public class ConformanceKeys {
	public static final String CONFORMANCE_DOMAIN_TO_INCLUDE_IN_PROXY = "DOMAIN_TO_INCLUDE_IN_PROXY";	
	
	public static final String ZAP_RUNNING_HOST = "ZAP_RUNNING_HOST";
	public static final String ZAP_RUNNING_PORT = "ZAP_RUNNING_PORT";
	public static final String ZAP_SERVER_API_KEY = "ZAP_SERVER_API_KEY";
	public static final String ZAP_WORK_DIRECTORY = "ZAP_WORK_DIRECTORY";
	
	//PSD2 Keys
	public static final String PSD2_CLIENT_CERT_FILE_NAME = "PSD2_CLIENT_CERT_FILE_NAME";
	public static final String PSD2_CLIENT_CERT_PASSWORD = "PSD2_CLIENT_CERT_PASSWORD";  //kid
}
