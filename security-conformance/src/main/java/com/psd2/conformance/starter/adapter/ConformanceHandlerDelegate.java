package com.psd2.conformance.starter.adapter;

public interface ConformanceHandlerDelegate {

	public void runSuite();
	
}
